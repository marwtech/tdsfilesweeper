﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Configuration;
using System.Globalization;
using System.IO;
using SystemLibrary;
using SystemLibrary.Utility;
using System.Xml;
using System.Collections;

namespace FileSweepService.Equipment
{
    public class EQPLogConversionHelper
    {
        public static EPIConfigurationClass Obj_config;

        private static string _TempFolder;
        private static List<string> _TableImagesAttachList;
        private static List<string> _XmlAttachList;
        private static List<string> _FailFileList;
        private static List<string> _SuccessFileList;

        private ArrayList arrComponents = new ArrayList();

        // PRODUCTION
        private static string nTDSTable = @"\\awscastdsprd02.li.lumentuminc.net\data$\Caswell\TableData\";
        private static string nTDSFolder = @"\\awscastdsprd02.li.lumentuminc.net\data$\";
        private static string nTDSAttachment = @"\\awscastdsprd02.li.lumentuminc.net\data$\Caswell\Attachment\";
        private static string nConverted = @"\\awscastdsprd02.li.lumentuminc.net\data$\";
        private static string nToConvert = @"\\awscastdsprd02.li.lumentuminc.net\data$\Caswell\ToConvert\";

        //local processing folder. Add TDS location in order to minimize file transfer delay
        public static string localProcessing = @"C:\FileSweepProcessing\Processing\";

        //-----------------------------------------------//
        //    private string nTDSFolder = @"\\li.lumentuminc.net\data\CAS\TDS\Data\";
        //    private string nTDSAttachment = @"\\li.lumentuminc.net\data\CAS\TDS\Data\Caswell\Attachment\";
        //    private string nTDSTable = @"\\li.lumentuminc.net\data\CAS\TDS\Data\Caswell\TableData\";

        //    private string nToConvert = @"\\li.lumentuminc.net\data\CAS\TDS\Data\Caswell\ToConvert\";

        //    private string nConverted = @"\\li.lumentuminc.net\data\CAS\TDS\Data\";
        //    private string nConverted = @"\\li.lumentuminc.net\data\CAS\TDS\Data\Caswell\Converted\";
        //   public string conversionLoc = @"\\li.lumentuminc.net\data\CAS\TDS\Data\Caswell\ToConvert\";

        //local processing folder 
    //    private static string localProcessing = @"C:\Users\pra70986\Desktop\AWS_TDS\Application\Files To Convert\Processing\";

        public bool EQPMain(string fullFileName, string fileName, string eqpType)
        {
            try
            {
                // eqpType = ConfigurationManager.AppSettings["Equipment Type"].ToString();
                string summaryFolder = ConfigurationManager.AppSettings[string.Format("{0} Summary Source Folder", eqpType)].ToString();
                bool result = false;
                _TableImagesAttachList = new List<string>();
                _XmlAttachList = new List<string>();
                _FailFileList = new List<string>();
                _SuccessFileList = new List<string>();
                List<FileInfo> sumFileList = new List<FileInfo>();
                var filePatterns = ConfigurationManager.AppSettings["FilePatterns"].ToString().Split(',').ToList();
                foreach (var pattern in filePatterns)
                {
                    sumFileList.AddRange(new DirectoryInfo(summaryFolder).GetFiles(pattern, SearchOption.TopDirectoryOnly).ToList());
                }

                //File Count
                NumberFormatInfo nfi = new CultureInfo("en-US", false).NumberFormat;

                //    LogHelper.WriteLine(string.Format("{0}/{1} ({2}):{3}", i, n, (i / n).ToString("P", nfi), fi.Name));

                switch (eqpType)
                {
                    case "COMEGA02":
                        result = CreateXML_COMEGA02(fileName);
                        break;
                    case "CSTEAG01":
                        result = CreateXML_CSTEAG01(fullFileName, fileName);
                        break;
                    default:
                        break;
                }

                if (result == false)
                {
                    _FailFileList.Add(fileName);
                }
                else
                {
                    _SuccessFileList.Add(fileName);
                }


                _TableImagesAttachList.Clear();
                _XmlAttachList.Clear();
                _FailFileList.Clear();
                _SuccessFileList.Clear();

            }
            catch (Exception ex)
            {
                // Program.Error_Message = ExceptionHelper.GetAllFootprints(ex);
                return false;
            }
            return true;
        }

        #region COMEGA02
        public static bool COMEGA02Processing(string summaryFolder)
        {
            try
            {
                _TableImagesAttachList = new List<string>();
                _XmlAttachList = new List<string>();
                _FailFileList = new List<string>();
                _SuccessFileList = new List<string>();
                List<FileInfo> sumFileList = new List<FileInfo>();
                var filePatterns = ConfigurationManager.AppSettings["FilePatterns"].ToString().Split(',').ToList();
                foreach (var pattern in filePatterns)
                {
                    sumFileList.AddRange(new DirectoryInfo(summaryFolder).GetFiles(pattern, SearchOption.TopDirectoryOnly).ToList());
                }
                //File Count
                NumberFormatInfo nfi = new CultureInfo("en-US", false).NumberFormat;
                double n = sumFileList.Count();
                double i = 0; //counter
                foreach (var fi in sumFileList.OrderByDescending(r => r.Name))
                {
                    i++;
                    //        LogHelper.WriteLine(string.Format("{0}/{1} ({2}):{3}", i, n, (i / n).ToString("P", nfi), fi.Name));
                    if (CreateXML_COMEGA02(fi.FullName) == false)
                    {
                        _FailFileList.Add(fi.FullName);
                    }
                    else
                    {
                        _SuccessFileList.Add(fi.FullName);
                    }

                    _TableImagesAttachList.Clear();
                    _XmlAttachList.Clear();
                    _FailFileList.Clear();
                    _SuccessFileList.Clear();
                }

            }
            catch (Exception ex)
            {
                //   Program.Error_Message = "Run Id: " + "" + " - \r\r Error Message:" + ExceptionHelper.GetAllFootprints(ex);
                return false;
            }
            return true;
        }
        public static bool CreateXML_COMEGA02(string srcFileFullName)
        {
            try
            {
                cls_tdsmodel xmlResultRoot = new cls_tdsmodel()
                {
                    Result = "Done"
                };
                List<COMEGA02_DSClass> comega02DsList = new List<COMEGA02_DSClass>();

                var summaryLines = File.ReadAllLines(srcFileFullName);
                //properties
                var recipe = string.Empty;
                var lotnumber = string.Empty;
                var wafernumber = string.Empty;
                var recipedate = new DateTime();
                var header = string.Empty;
                var cassette = string.Empty;
                var cassette_date = string.Empty;
                var cassette_time = string.Empty;
                var csv_data = string.Empty;
                var recipename = string.Empty;
                var teststep = string.Empty;
                var teststepname = string.Empty;
                var sequence = string.Empty;
                var recipeheader = string.Empty;
                var recipenumber = string.Empty;

                var overAllMaxDate = new DateTime(1990, 1, 1);
                var overAllMinDate = new DateTime(2100, 1, 1);

                foreach (var line in summaryLines.Select(r => r.Replace("\"", ""))) //Replcate all double quote in summary file
                {
                    if (string.IsNullOrEmpty(line) == false)
                    {
                        var cells = line.Split(',');
                        var firstCellStr = cells[0];
                        //firstCellStr.Replace("\"", "");
                        switch (firstCellStr.ToUpper())
                        {
                            case "SEQUENCE":
                                sequence = cells[1];
                                break;
                            case "CASSETTE":
                                cassette = cells[1];
                                lotnumber = cells[2].ToUpper();
                                if (string.IsNullOrEmpty(lotnumber))
                                    lotnumber = "NA";
                                cassette_date = cells[5];
                                cassette_time = cells[6];
                                break;
                            case "WAFER":
                                wafernumber = cells[1];
                                var fileDetailList = new List<string>();
                                var filePreCondList = new List<string>();
                                GetSupportDatafiles(cassette, cassette_date, wafernumber, ref fileDetailList, ref filePreCondList);
                                var res = comega02DsList.Where(r => r.WaferNumber == wafernumber).FirstOrDefault();
                                if (res != null)
                                {
                                    res.FileDetailList = fileDetailList;
                                    res.FileDetailList = filePreCondList;
                                }
                                else
                                {
                                    COMEGA02_DSClass ds = new COMEGA02_DSClass()
                                    {
                                        WaferNumber = wafernumber,
                                        FileDetailList = fileDetailList,
                                        FilePreConditionList = filePreCondList
                                    };
                                    comega02DsList.Add(ds);
                                }
                                break;
                            case "RECIPE":
                                //   recipedate = ObjectHelper.UK2USDate(cells[6], cells[5]);
                                recipename = cells[2];
                                recipenumber = cells[1];
                                break;
                            case "STEP":
                                var p = line.IndexOf(",Time");
                                var tempHeader = line.Substring(p + 1, line.Length - p - 1);
                                header = "Cassette,Sequence,Lotnumber,Wafer,Date,RecipeNumber,RecipeName,StepNumber,StepName,DataType," + tempHeader;
                                csv_data = header;
                                teststep = cells[1];
                                teststepname = cells[2];
                                break;
                            case "MIN":
                            case "MAX":
                            case "SET":
                                var tempVal = line.Replace("Set,,", "Set");
                                tempVal = tempVal.Replace("Min,,", "Min");
                                tempVal = tempVal.Replace("Max,,", "Max");
                                tempVal = tempVal.Replace("N/A", "");
                                tempVal = tempVal.Replace("N/M", "");
                                var value = cassette + "," + sequence + "," + lotnumber + "," + wafernumber + "," + recipedate + "," + recipenumber + "," + recipename + "," + teststep + "," + teststepname + "," + tempVal;
                                csv_data = csv_data + Environment.NewLine + value.ToUpper();
                                break;
                            case "AVE":
                                var aveTempVal = line.Replace("Ave,,", "Ave");
                                aveTempVal = aveTempVal.Replace("N/A", "");
                                aveTempVal = aveTempVal.Replace("N/M", "");
                                var aveValue = cassette + "," + sequence + "," + lotnumber + "," + wafernumber + "," + recipedate + "," + recipenumber + "," + recipename + "," + teststep + "," + teststepname + "," + aveTempVal;
                                csv_data = csv_data + Environment.NewLine + aveValue.ToUpper();
                                var res2 = comega02DsList.Where(r => r.WaferNumber == wafernumber).FirstOrDefault();
                                if (res2 != null)
                                {
                                    if (res2.CSVObjectList == null)
                                        res2.CSVObjectList = new List<COMEGA02_CSVClass>();
                                    res2.CSVObjectList.Add(new COMEGA02_CSVClass()
                                    {
                                        RecipeName = recipename,
                                        RecipeDate = recipedate,
                                        TestStep = teststep,
                                        CSV = csv_data
                                    });
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }

                //Build Test Step for Result
                xmlResultRoot.TestStep.AddRange(GenerateTestSteps(comega02DsList, ref overAllMaxDate, ref overAllMinDate));

                var objCfg = Program.Obj_config;
                var startDateTime = ObjectHelper.UK2USDate(cassette_date, cassette_time);
                //Data Source
                var backupFileName = nTDSAttachment + Path.GetFileName(srcFileFullName);
                TestStep dataSrcTestStep = new TestStep()
                {
                    Name = "DATASOURCE",
                    startDateTime = startDateTime.ToString("s"),
                    endDateTime = overAllMaxDate.ToString("s")
                };
                dataSrcTestStep._DataAttachment.Add(new DataAttachment()
                {
                    Name = "COMEGA02_SUMMARY_SOURCE",
                    Value = backupFileName,
                    Status = "Done"
                });
                xmlResultRoot.TestStep.Add(dataSrcTestStep);

                //Header                                
                var xmlHeader = GenerateXmlHeader(lotnumber, "COMEGA02", startDateTime, overAllMaxDate);
                xmlResultRoot.Header = xmlHeader;

                xmlResultRoot.startDateTime = startDateTime.ToString("s");
                xmlResultRoot.endDateTime = overAllMaxDate.ToString("s");

                //Build Xml Info
                string xmlStr = string.Empty;

                string xmlResultFileName = localProcessing
                        + string.Format(@"\Site={0},ProductFamily={1},Operation={2},PartNumber={3},SerialNumber={4},TestDate={5}.xml",
                        "250", "CAS FAB", xmlHeader.Operation, xmlHeader.Partnumber,
                        xmlHeader.Serialnumber, startDateTime.ToString("yyyy-MM-ddTHH.mm.ss"));
                if (File.Exists(xmlResultFileName))
                    File.Delete(xmlResultFileName);
                xmlStr = xmlStr.Replace("\"12:00:00 AM\"", "");
                File.WriteAllText(xmlResultFileName, xmlStr);
                _XmlAttachList.Add(xmlResultFileName);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return true;
        }
        private static bool GetSupportDatafiles(string cassette, string cassetDate, string wafer, ref List<string> fileDetailList, ref List<string> filePreCondList)
        {
            try
            {
                var dd = int.Parse(cassetDate.Substring(0, 2));
                var m = int.Parse(cassetDate.Substring(3, 2));
                var yyyy = int.Parse("20" + cassetDate.Substring(6, 2));
                var xDate = new DateTime(yyyy, m, dd);
                var logFolder = string.Format(@"\\cas-share.int.oclaro.com\casfab\ToolData\COMEGA02\{0}\Logs\DATALOG\FULLMV\{1}\DAY{2}",
                    yyyy, xDate.ToString("MMM", CultureInfo.InvariantCulture).ToUpper(), dd);

                var di = new DirectoryInfo(logFolder);
                //Pattern1 - Get Detail File          
                var waferStr1 = string.Empty;
                if (ObjectHelper.Val(wafer) >= 10)
                    waferStr1 = wafer;
                else
                    waferStr1 = "0" + ObjectHelper.Val(wafer);
                var logFilePattern1 = string.Format("C{0}{1}??.LOG", cassette, waferStr1);
                var detailFile = di.GetFiles(logFilePattern1, SearchOption.TopDirectoryOnly);
                //Pattern2 - Get Pre Condition file
                var waferStr2 = string.Empty;
                if (ObjectHelper.Val(wafer) >= 10)
                    waferStr2 = "0" + ObjectHelper.Val(wafer);
                else
                    waferStr2 = "00" + ObjectHelper.Val(wafer);
                var logFilePattern2 = string.Format("C{0}-{1}.LOG", cassette, waferStr2);
                var preCondFile = di.GetFiles(logFilePattern2, SearchOption.TopDirectoryOnly);

                if (detailFile.Count() > 0)
                    fileDetailList.AddRange(detailFile.Select(r => r.FullName).ToList());
                if (preCondFile.Count() > 0)
                    filePreCondList.AddRange(preCondFile.Select(r => r.FullName).ToList());
            }
            catch (Exception ex)
            {
                //   Program.Error_Message = ExceptionHelper.GetAllFootprints(ex);
                return false;
            }
            return true;
        }
        private static List<TestStep> GenerateTestSteps(List<COMEGA02_DSClass> dsList, ref DateTime overAllMaxDate, ref DateTime overAllMinDate)
        {
            List<TestStep> testStepList = new List<TestStep>();
            _TempFolder = localProcessing;
            //Create Temp Folder if not exists
            if (Directory.Exists(_TempFolder) == false) Directory.CreateDirectory(_TempFolder);
            try
            {
                foreach (var ds in dsList)
                {
                    TestStep testStep = new TestStep()
                    {
                        Name = "WAFER" + ds.WaferNumber
                    };

                    int i = 0;
                    //Set DS Detail List into TestStep
                    foreach (var dFile in ds.FileDetailList)
                    {
                        i++;
                        testStep._DataAttachment.Add(new DataAttachment()
                        {
                            Name = "DETAIL_LOGSTEP" + i,
                            Value = dFile,
                            Status = "Done"
                        });
                    }

                    //Set DS PreCondition List into TestStep
                    foreach (var pFile in ds.FilePreConditionList)
                    {
                        testStep._DataAttachment.Add(new DataAttachment()
                        {
                            Name = "PRE_CONDITION",
                            Value = pFile,
                            Status = "Done"
                        });
                    }


                    var stepMaxDate = new DateTime(1990, 1, 1);
                    var stepMinDate = new DateTime(2100, 1, 1);
                    foreach (var csvObj in ds.CSVObjectList)
                    {
                        var csvFileName = string.Format("{0}_{1}_{2}_{3}.csv", ds.WaferNumber, csvObj.RecipeName,
                            csvObj.TestStep, Convert.ToDateTime(csvObj.RecipeDate).ToString("MM.dd.yyyy.HHmmss"));
                        var localCsvFileName = localProcessing + csvFileName;
                        var netCsvFileName = nTDSAttachment + csvFileName;
                        if (File.Exists(localCsvFileName)) File.Delete(localCsvFileName);
                        File.WriteAllText(localCsvFileName, csvObj.CSV);
                        testStep._DataTable.Add(new DataTable()
                        {
                            Name = "COMEGA02_SUM",
                            Description = "SUMMARY DATA",
                            Value = netCsvFileName,
                            Status = "Done"
                        });
                        _TableImagesAttachList.Add(localCsvFileName);
                        if (csvObj.RecipeDate > stepMaxDate) stepMaxDate = csvObj.RecipeDate;
                        if (csvObj.RecipeDate < stepMinDate) stepMinDate = csvObj.RecipeDate;
                        if (csvObj.RecipeDate > overAllMaxDate) overAllMaxDate = csvObj.RecipeDate;
                        if (csvObj.RecipeDate < overAllMinDate) overAllMinDate = csvObj.RecipeDate;
                    }
                    testStep.startDateTime = stepMinDate.ToString("s");
                    testStep.endDateTime = stepMaxDate.ToString("s");
                    testStepList.Add(testStep);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return testStepList;
        }
        private Header GenerateCOMEGA02XmlHeader(string lotNumber, DateTime cassetteStartDate, DateTime overAllMaxDate)
        {
            Header header = new Header();
            try
            {
                header.Partnumber = "UNKNOWNPN";
                header.Devicetype = "LOT";
                header.Site = "250";
                header.Operation = "COMEGA02";
                header.Teststation = "COMEGA02";
                header.Serialnumber = lotNumber;
                header.OperatorId = "NA";
                header.Result = "Done";
                header.Starttime = cassetteStartDate.ToString("s");
                header.Endtime = overAllMaxDate.ToString("s");
            }
            catch (Exception)
            {
                throw;
            }
            return header;
        }

        #endregion

        #region CSTEGA01
        public bool CreateXML_CSTEAG01(string srcFileFullName, string nfileName)
        {

            //   string fileName = "";
            var startTime = "";
            string headerTime = "";
            string cassetID = "";
            string serialID = "";
            string waferNumber = "";
            string purposeInfo = "";

            try
            {
                cls_tdsmodel xmlResultRoot = new cls_tdsmodel()
                {
                    Result = "Done"
                };
                var cSTEAG = this.GetCSTEAGInfo(srcFileFullName);
                if (cSTEAG.CSVDicList.Count() > 0)
                {
                    var fileName = "";
                    var tempFolder = localProcessing;
                    var csv = this.GenerateCSTEAGCsv(cSTEAG);
                    var csvFileName = string.Format("{0}_{1}_{2}.csv", cSTEAG.SerialNumber, cSTEAG.Recipe,
                        cSTEAG.StartDateTime.ToString("MM.dd.yyyy.HHmmss"));
                    var localCsvFileName = tempFolder + csvFileName;
                    var netCsvFileName = nTDSTable + csvFileName.ToString().ToUpper();
                    if (File.Exists(localCsvFileName)) File.Delete(localCsvFileName);
                    File.WriteAllText(netCsvFileName, csv);

                    ///////


                    Random rnd = new Random();
                    int random = rnd.Next(1, 999999999);  // creates a number for #LIFTUP rec as the date is not recorded 
                    purposeInfo = "" + random;

                    System.Threading.Thread.Sleep(100);
                    //      DateTime outDate = DateTime.ParseExact(arrLogCassette[5] + " " + arrLogCassette[6], "dd/MM/yy HH:mm:ss", null);

                    //    DateTime outDate = Convert.ToDateTime(logItem[3]);
                    //    startTime = outDate.ToString("yyyy-MM-ddTHH:mm:ss").Replace(":", ".");
                    headerTime = cSTEAG.StartDateTime.ToString("yyyy-MM-ddTHH.mm.ss");
                    serialID = cSTEAG.SerialNumber;

                    //    purposeInfo = logItem[5];

                    /*
                            if (inPartNumber != null)
                            {
                                partID = inPartNumber.Split('.');
                            }
                            else
                            {
                                partID[0] = inPartNumber.Replace("-", "_");
                            }
                    */
                    string xsi = "http://www.w3.org/2001/XMLSchema-instance";
                    string xsd = "http://www.w3.org/2001/XMLSchema";

                    XmlDocument doc = new XmlDocument();

                    //data node

                    XmlNode docNode = doc.CreateXmlDeclaration("1.0", "utf-8", null);
                    doc.AppendChild(docNode);

                    XmlNode resultsNode = doc.CreateElement("Results");
                    XmlAttribute xsiAttribute = doc.CreateAttribute("xmlns:xsi");
                    xsiAttribute.Value = xsi;
                    XmlAttribute xsdAttribute = doc.CreateAttribute("xsi:xsd");
                    xsdAttribute.Value = xsd;

                    resultsNode.Attributes.Append(xsiAttribute);
                    resultsNode.Attributes.Append(xsdAttribute);
                    doc.AppendChild(resultsNode);

                    //----------------------------------//
                    XmlNode resultNode = doc.CreateElement("Result");
                    XmlAttribute rNode = doc.CreateAttribute("Result");
                    rNode.Value = "Done";

                    XmlAttribute timeNode = doc.CreateAttribute("startDateTime");
                    timeNode.Value = "" + headerTime;

                    resultNode.Attributes.Append(rNode);
                    resultNode.Attributes.Append(timeNode);
                    resultsNode.AppendChild(resultNode);

                    //------------------- Header ---------------------//

                    XmlNode headerNode = doc.CreateElement("Header");
                    XmlAttribute serialNode = doc.CreateAttribute("Serialnumber");
                    serialNode.Value = "" + serialID;

                    XmlAttribute waferNode = doc.CreateAttribute("Waferid");
                    waferNode.Value = "" + serialID;

                    XmlAttribute operationNode = doc.CreateAttribute("Operation");
                    operationNode.Value = "CSTEAG01";
                    XmlAttribute stationNode = doc.CreateAttribute("Teststation");
                    stationNode.Value = "CSTEAG01";

                    XmlAttribute operatorNode = doc.CreateAttribute("Operator");
                    operatorNode.Value = "NA";
                    XmlAttribute partNode = doc.CreateAttribute("Partnumber");
                    partNode.Value = "UNKNOWNPN";

                    XmlAttribute startNode = doc.CreateAttribute("Starttime");
                    startNode.Value = "" + headerTime;

                    XmlAttribute statusNode = doc.CreateAttribute("Result");
                    statusNode.Value = "Done";

                    XmlAttribute siteNode = doc.CreateAttribute("Site");
                    siteNode.Value = "250";

                    XmlAttribute deviceNode = doc.CreateAttribute("DeviceType");
                    deviceNode.Value = "LOT";

                    XmlAttribute purposeNode = doc.CreateAttribute("Purpose");
                    purposeNode.Value = "" + purposeInfo;

                    headerNode.Attributes.Append(serialNode);
                    headerNode.Attributes.Append(waferNode);
                    headerNode.Attributes.Append(operationNode);
                    headerNode.Attributes.Append(stationNode);
                    headerNode.Attributes.Append(operatorNode);
                    headerNode.Attributes.Append(partNode);
                    headerNode.Attributes.Append(siteNode);
                    headerNode.Attributes.Append(startNode);
                    headerNode.Attributes.Append(statusNode);
                    headerNode.Attributes.Append(deviceNode);
                    //    headerNode.Attributes.Append(purposeNode);

                    //------------------ NOT IN USE ----------------------//
                    XmlNode miscNode = doc.CreateElement("HeaderMisc");
                    XmlAttribute dNode = doc.CreateAttribute("Serialnumber");
                    dNode.Value = "";

                    XmlAttribute opNode = doc.CreateAttribute("Operation");
                    opNode.Value = "operation";

                    //------------------- Genealogy ----------------------//
                    XmlNode geoNode = doc.CreateElement("UnitGenealogy");

                    XmlNode itemNode = doc.CreateElement("Item");

                    if (arrComponents.Count > 0)
                    {
                        for (int c = 0; c < arrComponents.Count; c++)
                        {
                            XmlAttribute ptypeNode = doc.CreateAttribute("PartType");
                            ptypeNode.Value = "Component";
                            XmlAttribute partNoNode = doc.CreateAttribute("Partnumber");
                            partNoNode.Value = "" + arrComponents[c];
                            XmlAttribute iSerialNode = doc.CreateAttribute("SerialNumber");
                            iSerialNode.Value = "" + cSTEAG.SerialNumber.ToString();

                            itemNode.Attributes.Append(ptypeNode);
                            itemNode.Attributes.Append(partNoNode);
                            itemNode.Attributes.Append(iSerialNode);
                        }
                    }
                    //-----------------------------------------//
                    XmlNode testNode = doc.CreateElement("TestStep");

                    XmlAttribute tStatusNode = doc.CreateAttribute("Status");
                    tStatusNode.Value = "Done";

                    XmlAttribute tNameNode = doc.CreateAttribute("Name");
                    tNameNode.Value = "WAFER" + waferNumber;


                    XmlAttribute tStartNode = doc.CreateAttribute("startDateTime");
                    tStartNode.Value = "" + headerTime;

                    //     testNode.Attributes.Append(tTypeNode);
                    testNode.Attributes.Append(tNameNode);
                    testNode.Attributes.Append(tStartNode);

                    //-----------------------------------------//

                    XmlNode dataNode = doc.CreateElement("Data");
                    XmlAttribute typeNode = doc.CreateAttribute("DataType");
                    typeNode.Value = "Table";

                    XmlAttribute dNameNode = doc.CreateAttribute("Name");
                    dNameNode.Value = "CSTEAG01_TABLE_SUMMARY";

                    XmlAttribute valNode = doc.CreateAttribute("Value");
                    valNode.Value = "" + netCsvFileName;

                    dataNode.Attributes.Append(typeNode);
                    dataNode.Attributes.Append(dNameNode);
                    dataNode.Attributes.Append(valNode);

                    //------------------Test step 2  Data ----------------------//
                    XmlNode test2Node = doc.CreateElement("TestStep"); //sweep file att
                    XmlNode data2Node = doc.CreateElement("Data");

                    XmlAttribute t2StatusNode = doc.CreateAttribute("Status");
                    t2StatusNode.Value = "Done";

                    XmlAttribute t2NameNode = doc.CreateAttribute("Name");
                    t2NameNode.Value = "DATASOURCE";

                    XmlAttribute t2StartNode = doc.CreateAttribute("startDateTime");
                    t2StartNode.Value = "" + headerTime;

                    XmlAttribute t2EndNode = doc.CreateAttribute("endDateTime");
                    t2EndNode.Value = "" + headerTime;

                    test2Node.Attributes.Append(t2NameNode);
                    test2Node.Attributes.Append(t2StartNode);
                    test2Node.Attributes.Append(t2EndNode);

                    //--------------------------------------------------------//
                    //END OF PARENT TEST NODE

                    XmlAttribute type2Node = doc.CreateAttribute("DataType");
                    type2Node.Value = "Attachment";

                    XmlAttribute d2NameNode = doc.CreateAttribute("Name");
                    d2NameNode.Value = "CSTEAG01_SOURCE";

                    XmlAttribute val2Node = doc.CreateAttribute("Value");
                    val2Node.Value = "" + nTDSAttachment + nfileName;

                    data2Node.Attributes.Append(type2Node);
                    data2Node.Attributes.Append(d2NameNode);
                    data2Node.Attributes.Append(val2Node);
                    //-------------------------------------------------//

                    resultsNode.AppendChild(resultNode);
                    resultNode.AppendChild(headerNode);
                    //    headerNode.AppendChild(miscNode);
                    //     resultNode.AppendChild(geoNode);
                    geoNode.AppendChild(itemNode);

                    //  testNode.AppendChild(dataNode);

                    //     resultNode.AppendChild(testNode);

                    resultNode.AppendChild(test2Node);
                    resultNode.AppendChild(data2Node);
                    resultNode.AppendChild(dataNode);


                    //   testNode.AppendChild(dataNode);
                    test2Node.AppendChild(data2Node);

                    //
                    resultNode.AppendChild(test2Node);
                    resultNode.AppendChild(data2Node);

                    //    testNode.AppendChild(dataNode);
                    test2Node.AppendChild(dataNode);
                    test2Node.AppendChild(data2Node);
                    /*
                        foreach (ComegaOBJ omegaData in omegaList)
                        {
                            mzTestNode = doc.CreateElement("TestStep");
                            mzDataNode = doc.CreateElement("Data");
                            mzStatusNode = doc.CreateAttribute("Status");
                            mzNameNode = doc.CreateAttribute("Name");
                            mzStartNode = doc.CreateAttribute("startDateTime");

                            mzTypeNode = doc.CreateAttribute("DataType");
                            mzDataNameNode = doc.CreateAttribute("Name");
                            mzValNode = doc.CreateAttribute("Value");

                            mzStatusNode.Value = "Done";

                            mzNameNode.Value = "WAFER" + omegaData.Wafer;
                            mzStartNode.Value = "" + omegaData.Date;


                            mzTypeNode.Value = "Table";

                            mzDataNameNode.Value = "" + comegaTableName;

                            mzValNode.Value = "" + nTDSTable + "EQUIPMENT\\" + omegaData.Name;

                            mzDataNode.Attributes.Append(mzTypeNode);
                            mzDataNode.Attributes.Append(mzDataNameNode);

                            mzDataNode.Attributes.Append(mzValNode);

                            mzTestNode.Attributes.Append(mzNameNode);
                            mzTestNode.Attributes.Append(mzStatusNode);
                            mzTestNode.Attributes.Append(mzStartNode);

                            mzTestNode.AppendChild(mzDataNode);

                            resultNode.AppendChild(mzTestNode);
                        }
                    */
                    //  resultNode.AppendChild(mzTestNode);


                    fileName = "Site=250,ProductFamily=CAS EQUIPMENT" + ",Operation=CSTEAG01" + ",Partnumber=" + "UNKNOWNPN" + ",SerialNumber=" + cSTEAG.SerialNumber.ToString() + ",TestDate=" + headerTime.ToString().Replace(":", ".") + ".xml";

                    //    fileName = "Site=250,ProductFamily=CAS EQUIPMENT" + ",Operation=COMEGA02" + ",Partnumber=" + "UNKNOWNPN" + ",SerialNumber=" + serialID + ",Testdate=" + startTime + ".xml";
                    string fullName = Path.Combine(nConverted, fileName);

                    doc.Save(fullName);

                    System.Threading.Thread.Sleep(500);
                    RemoveLogFile(srcFileFullName, nfileName, "CSTEAG01");


                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return true;
        }

        private void RemoveLogFile(string sourceFile, string fileName, string kitName)
        {
            try
            {
                System.GC.Collect();
                System.GC.WaitForPendingFinalizers();

                if (File.Exists(sourceFile))
                {
                    File.Copy(sourceFile, nTDSAttachment + fileName);

                    System.GC.Collect();
                    System.GC.WaitForPendingFinalizers();
                    File.SetAttributes(sourceFile, FileAttributes.Normal);
                    File.Delete(sourceFile);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("" + ex);
            }
        }

        private CSTEAGClass GetCSTEAGInfo(string srcFileFullName)
        {
            CSTEAGClass cSTEAG = new CSTEAGClass();
            cSTEAG.CSVDicList = new Dictionary<string, List<double>>();
            try
            {
                string lineStr = string.Empty;
                string[] cells = null;
                var summaryLines = File.ReadAllLines(srcFileFullName);
                //For Content
                double rate = 0;
                double number = 0;
                int addRowCount = 0;
                //All Content start from _samples key
                int contentStartRow = Array.IndexOf(summaryLines, summaryLines.Where(r => r.Contains("_samples")).FirstOrDefault());
                //Get Header
                for (int i = 0; i < summaryLines.Count(); i++)
                {
                    lineStr = summaryLines[i];
                    if (lineStr.TrimStart(' ').Equals("$"))
                    {
                        i++;
                        continue;
                    }
                    if (string.IsNullOrEmpty(lineStr) == false && lineStr.Contains("$"))
                    {
                        lineStr = lineStr.TrimStart(' ').TrimStart('$');
                        lineStr = lineStr.Replace("$", ",").Replace(", ,", ",");
                        cells = lineStr.Split(',');
                        switch (cells[0])
                        {
                            case "_matid":
                                cSTEAG.SerialNumber = cells[1];
                                break;
                            case "_recipe":
                                cSTEAG.Recipe = cells[1];
                                break;
                            case "_operator":
                                cSTEAG.Operator = cells[1];
                                break;
                            case "_time":
                                cSTEAG.StartDateTime = Convert.ToDateTime(cells[1]);
                                break;
                            default:
                                if (i > contentStartRow) //Content
                                {
                                    var resList = new List<double>();
                                    var timeList = new List<double>();
                                    var dList = new List<double>();
                                    rate = double.Parse(new string(cells[1].Where(c => char.IsDigit(c)).ToArray()));
                                    number = int.Parse(new string(cells[2].Where(c => char.IsDigit(c)).ToArray()));
                                    addRowCount = Convert.ToInt32(Math.Ceiling(number / 10)); //each row data has 10 columns
                                    i = i + 2; //add 2 rows to find the data content
                                    var unit = rate / 1000; //ms to sec
                                    for (double j = 0; j < number * unit; j = j + unit) //Get Timing by number * unit
                                    {
                                        if (j == 0)
                                            timeList.Add(j);
                                        timeList.Add(j + unit);
                                    }
                                    timeList.RemoveRange(Convert.ToInt32(number), 1); //Remove extra 1 data
                                    cSTEAG.CSVDicList.Add(cells[0].ToUpper().Replace(" ", "_") + "_TIMING", timeList);
                                    var data = summaryLines.ToList().GetRange(i, addRowCount);
                                    foreach (var item in data) //Get all data and combine them into one list
                                    {
                                        dList = item.TrimEnd(',').Split(',').Select(x => double.Parse(x)).ToList();
                                        resList.AddRange(dList);
                                    }
                                    cSTEAG.CSVDicList.Add(cells[0].ToUpper().Replace(" ", "_") + "_SAMPLING", resList);
                                    i = i + addRowCount; //Move to next Test Parameter by using row count
                                }
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return cSTEAG;
        }

        private string GenerateCSTEAGCsv(CSTEAGClass cSTEAG)
        {
            var dic = cSTEAG.CSVDicList;
            StringBuilder csv = new StringBuilder();
            try
            {
                var maxRow = dic.Select(r => r.Value.Count()).Max();
                csv.Append("SerialNumber,");
                csv.AppendLine(String.Join(",", dic.Select(r => r.Key)));
                for (int i = 0; i < maxRow; i++)
                {
                    csv.Append(cSTEAG.SerialNumber + ",");
                    csv.AppendLine(String.Join(",", dic.Select(r => r.Value.Count > i ? r.Value[i].ToString() : "")));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return csv.ToString();
        }
        private void TestMarco()
        {
            //string marcoPath = ConfigHelper.AppPath() + "\\SteagReader.xlsm";
            //ExcelMacroHelper excelMacroHelper = new ExcelMacroHelper();
            //object[] inputObj = new object[] { @"C:\Users\lic67888\Box\jacky.li\Documents\Public\20 Kaizen TDS\Caswell\00 Doc\Equipment\CSTEAG01_02\cqr0460.1.000" };
            //object rtnVal;
            //excelMacroHelper.RunExcelMacro(marcoPath, "OpenSteagLog", inputObj, out rtnVal, false);
        }
        #endregion

        #region Utilities
        private static Header GenerateXmlHeader(string serialNum, string testStation, DateTime startDate, DateTime overAllMaxDate)
        {
            Header header = new Header();
            try
            {
                header.Partnumber = "UNKNOWNPN";
                header.Devicetype = "LOT";
                header.Site = "250";
                header.Operation = "empty";
                header.Teststation = testStation;
                header.Serialnumber = serialNum;
                header.OperatorId = "NA";
                header.Result = "Done";
                header.Starttime = startDate.ToString("s");
                header.Endtime = overAllMaxDate.ToString("s");
            }
            catch (Exception)
            {
                throw;
            }
            return header;
        }
        #endregion
    }
}
