﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileSweepService
{
    public class DBAccess
    {
        //local DB
        /*
        private static string sPassword = "Sw33per123";
        private static string sUsername = @"sweeper";
        private static string sServerName = @"96LT1Z2\SQLEXPRESS";
        private static string sDatabaseName = @"FileSweepEngine";
        */

        private static string sPassword = @"Sw33p123";
        private static string sUsername = @"sweepService";
        private static string sServerName = @"CAS-SPP-EPI-01\SQLPASTIEP";
        private static string sDatabaseName = @"FileSweepEngine";
        
        /*
        private static string dbServer = Properties.Settings.Default.SQLServer;
        private static string dbUsername = Properties.Settings.Default.dbUsername;
        private static string dbPassword = Properties.Settings.Default.dbPassword;
        private static string dbDatabase = Properties.Settings.Default.dbDatabase;
        */

        private SqlConnection mSqlServerConnection = null;
        public ArrayList arrLocations = new ArrayList();
        public struct SweepInformation
        {
            public string MaterialType, SweepLocations;
        }

        public DBAccess()
        {

        }

        public void ConnectionOpen()
        {
            string CurrentDatabase = null;
            try
            {
                mSqlServerConnection = new SqlConnection(@"server =" + sServerName + ";database =" + sDatabaseName + ";user=" + sUsername + ";password=" + sPassword + ";Connection Timeout=30; Trusted_Connection=True");
                CurrentDatabase = "Server=" + sServerName + "; Database=" + sDatabaseName + "; User=" + sUsername;

                mSqlServerConnection.Open();
            }
            catch (Exception ex)
            {
                // Create a file to write to.
                string createText = "Error: " + ex + Environment.NewLine;
                File.WriteAllText(@"C:\FileSweep\Log.txt", createText);

                throw new ExceptionHandler("Can't Open database\n" + CurrentDatabase, ex);
            }
        }

        public void ConnectionClose()
        {
            mSqlServerConnection.Close();
        }

        //Not in use for service
        public ArrayList GetLocationInfo()
        {
            try
            {
                ConnectionOpen();
            }
            catch (Exception ex) {
                Console.WriteLine("Error");
            }
            ArrayList arrLocation = new ArrayList();

            StringBuilder SelectionCmnd = new StringBuilder("SELECT * FROM [" + sDatabaseName + "].[dbo].SweepLocation");
            SqlCommand GetData = new SqlCommand(SelectionCmnd.ToString(), mSqlServerConnection);

            SqlDataReader ReadData = null;

            try
            {
                ReadData = GetData.ExecuteReader();
                if (ReadData.HasRows)
                {
                    while (ReadData.Read())
                    {
                        SweepInfo ThisInfo = new SweepInfo();

                        ThisInfo.MaterialType = ReadData["Material"].ToString();
                        ThisInfo.FolderLocation = ReadData["Location"].ToString();
                        arrLocation.Add(ThisInfo);
                    }
                    return arrLocation;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception MyDBException)
            {
                this.InsertLogEvent("Error @ 90", "" + MyDBException.Message);
                throw MyDBException;
            }
            finally
            {
                ConnectionClose();
            }
        }

        /************************************
         * Insert new sweep location info   *
         * **********************************/
        public void InsertLocation(string materialType, string location)
        {
            ConnectionOpen();
            SqlCommand InsertQuery = new SqlCommand();

            String CommandToRun = "INSERT INTO SweepLocation(Material, Location) VALUES ('" + materialType + "','" + location + "')";

            SqlCommand sqlcom = new SqlCommand(CommandToRun, mSqlServerConnection);
            try
            {
                sqlcom.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                this.InsertLogEvent("DB Error", "@ 109" + ex.Message);
            }
            finally
            {
                ConnectionClose();
            }
        }

        public void GetFileLocations()
        {
            try
            {
                ConnectionOpen();
                SqlCommand SelectQuery = new SqlCommand();

                String CommandToRun = "SELECT [Location] FROM [SweepLocation]";

                SqlCommand sqlcom = new SqlCommand(CommandToRun, mSqlServerConnection);

                try
                {

                    using (SqlDataReader DataEntry = sqlcom.ExecuteReader())
                    {
                        if (DataEntry.HasRows)
                        {
                            while (DataEntry.Read())
                            {
                                arrLocations.Add(DataEntry["Location"]);
                            }
                        }
                        else
                        {
                            ConnectionClose();
                        }
                    }

                }
                catch (Exception ex)
                {
                    // Create a file to write to.
                    string createText = "Error: " + ex + Environment.NewLine;
                    File.WriteAllText(@"C:\FileSweep\Log.txt", createText);

                    this.InsertLogEvent("DB Error", " @ 145" + ex.Message);
                }
                finally
                {
                    ConnectionClose();
                }
            }
            catch (Exception ex)
            {
                // Create a file to write to.
                string createText = "Error: " + ex + Environment.NewLine;
                File.WriteAllText(@"C:\FileSweep\Log.txt", createText);

                this.InsertLogEvent("DB Error", " @ 180" + ex.Message);
            }
        }

        /**************************************************
         * Get list of the folders to sweep for the files *
         **************************************************/
        public string[] GetLocation()
        {
            string line = "";
            List<string> AvailableLocations = new List<string>();

            ConnectionOpen();

            SqlCommand GetData = new SqlCommand("SELECT * FROM SweepLocation", mSqlServerConnection);
            SqlDataReader ReadData = null;

            try
            {
                ReadData = GetData.ExecuteReader();

                if (ReadData.HasRows)
                {
                    while (ReadData.Read())
                    {
                        line = ReadData["Location"].ToString();
                        AvailableLocations.Add(line);
                    }
                }
                return AvailableLocations.ToArray();
            }
            catch (Exception ex)
            {
                throw new Exception("Can't get database info : " + ex.Message);
            }
            finally
            {
                ConnectionClose();
            }
        }

        /***************************************
         * Insert file names into the database *
         **************************************/
        public void InsertFileInfo(string pWaferID, string pFileName, DateTime pModTime, int pFileSize)
        {
            ConnectionOpen();

            //this will avoid DB parse 
            var inModTime = pModTime.ToString("yyyy-MM-dd HH:mm:ss");
            var inCurrentTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");


            SqlCommand InsertQuery = new SqlCommand();

            String CommandToRun = "INSERT INTO FileInformation(WaferID, FileName, ModTime, Size, DateAdded) VALUES ('" + pWaferID + "','" + pFileName + "','" + inModTime + "','" + pFileSize + "', '" + inCurrentTime + "')";

            SqlCommand sqlcom = new SqlCommand(CommandToRun, mSqlServerConnection);
            try
            {
                sqlcom.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                InsertLogEvent("DB Error", " @ 204" + ex.Message);
            }
            finally
            {
                ConnectionClose();
            }
        }

        /*******************************************
         * Update file information in the database *
         * *****************************************/
        public void UpdateFileInfo(string pWaferID, string pFileName, DateTime pModTime, int pFileSize)
        {
            //   ConnectionOpen();
            SqlCommand InsertQuery = new SqlCommand();

            //format date in order to overcome SQL date parse
            var inModTime = pModTime.ToString("yyyy-MM-dd HH:mm:ss");

            String CommandToRun = "UPDATE FileInformation SET Size = ('" + pFileSize + "'), ModTime = ('" + inModTime + "') WHERE WaferID = ('" + pWaferID + "') AND FileName = ('" + pFileName + "')";

            SqlCommand sqlcom = new SqlCommand(CommandToRun, mSqlServerConnection);
            try
            {
                sqlcom.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                this.InsertLogEvent("DB Error", "@ 238" + ex.Message);
            }
            finally
            {
                ConnectionClose();
            }
        }

        public void RemoveRecord(string pMaterial, string pPath)
        {
            ConnectionOpen();
            SqlCommand InsertQuery = new SqlCommand();

            String CommandToRun = "DELETE FROM SweepLocation WHERE Location =  ('" + pPath + "') AND Material =('" + pMaterial + "')";

            SqlCommand sqlcom = new SqlCommand(CommandToRun, mSqlServerConnection);
            try
            {
                sqlcom.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                this.InsertLogEvent("DB Error", "@ 337" + ex.Message);
            }
            finally
            {
                ConnectionClose();
            }
        }
        /***************************************************
         * Get full sweep information                      *
         * *************************************************/
        public ArrayList GetFullSweepInfo()
        {
            ConnectionOpen();

            ArrayList arrSweepInfo = new ArrayList();

            StringBuilder SelectionCmd = new StringBuilder("SELECT * FROM [" + sDatabaseName + "].[dbo].SweepLocation");

            SqlCommand GetDBInfo = new SqlCommand(SelectionCmd.ToString(), mSqlServerConnection);
            try
            {
                using (SqlDataReader GetInfo = GetDBInfo.ExecuteReader())
                {
                    if (GetInfo.HasRows)
                    {
                        while (GetInfo.Read())
                        {
                            SweepInfo ThisInfo = new SweepInfo();

                            ThisInfo.FolderLocation = GetInfo["Location"].ToString();
                            ThisInfo.MaterialType = GetInfo["Material"].ToString();

                            arrSweepInfo.Add(ThisInfo);
                        }
                        return arrSweepInfo;
                    }

                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception MyDBException)
            {
                InsertLogEvent("DB Error", "@ 271 " + MyDBException.Message);
                throw MyDBException;
            }
            finally
            {
                ConnectionClose();
            }
        }

        /***************************************************
         * Get information stored for the file from the DB *
         * *************************************************/
        public ArrayList GetFileInformation(string pWaferID, string pName)
        {
            ConnectionOpen();
            ArrayList arrInfo = new ArrayList();

            StringBuilder SelectionCmnd = new StringBuilder("SELECT * FROM [" + sDatabaseName + "].[dbo].FileInformation WHERE [WaferID] = '" + pWaferID + "' AND [FileName] = '" + pName + "'");

            SqlCommand GetDBInfo = new SqlCommand(SelectionCmnd.ToString(), mSqlServerConnection);
            try
            {
                using (SqlDataReader GetInfo = GetDBInfo.ExecuteReader())
                {
                    if (GetInfo.HasRows)
                    {
                        while (GetInfo.Read())
                        {
                            FileInformation ThisInfo = new FileInformation();

                            ThisInfo.WaferID = GetInfo["WaferID"].ToString();
                            ThisInfo.FileName = GetInfo["FileName"].ToString();
                            ThisInfo.FileModTime = Convert.ToDateTime(GetInfo["ModTime"]);
                            ThisInfo.FileSize = (int)GetInfo["Size"];
                            ThisInfo.FileDate = GetInfo["DateAdded"].ToString();
                            arrInfo.Add(ThisInfo);
                        }
                        return arrInfo;
                    }

                    else
                    {
                        //clear stuff
                        return null;
                    }
                }
            }
            catch (Exception MyDBException)
            {
                InsertLogEvent("DB Error", "@ 321 " + MyDBException.Message);
                throw MyDBException;
            }
            finally
            {
                ConnectionClose();
            }
        }

        /*********************************************
         * Get application settings info from the DB *
         * *******************************************/
        public ArrayList GetSettingsInformation()
        {
            ConnectionOpen();
            ArrayList arrInfo = new ArrayList();

            StringBuilder SelectionCmnd = new StringBuilder("SELECT * FROM [" + sDatabaseName + "].[dbo].Settings");

            SqlCommand GetSettingsInfo = new SqlCommand(SelectionCmnd.ToString(), mSqlServerConnection);

            try
            {
                using (SqlDataReader GetInfo = GetSettingsInfo.ExecuteReader())
                {
                    if (GetInfo.HasRows)
                    {
                        while (GetInfo.Read())
                        {
                            SettingsOBJ ThisInfo = new SettingsOBJ();

                            ThisInfo.SettingName = GetInfo["SettingName"].ToString();
                            ThisInfo.SettingValue = GetInfo["Value"].ToString();
                            ThisInfo.Status = GetInfo["Status"].ToString();

                            arrInfo.Add(ThisInfo);
                        }
                        return arrInfo;
                    }

                    else
                    {
                        //clear stuff
                        return null;
                    }
                }
            }
            catch (Exception MyDBException)
            {
                this.InsertLogEvent("DB Error", "@ 317" + MyDBException.Message);
                throw MyDBException;
            }
            finally
            {
                ConnectionClose();
            }
        }

        public void InsertLogEvent(string pState, string pInformation)
        {

            ConnectionOpen();
            SqlCommand InsertQuery = new SqlCommand();

            String CommandToRun = "INSERT INTO ServiceLog(State,LogInformation, Date) VALUES ('" + pState + "','" + pInformation + "','" + DateTime.Now + "')";

            SqlCommand sqlcom = new SqlCommand(CommandToRun, mSqlServerConnection);
            try
            {
                sqlcom.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                this.InsertLogEvent("DB Error", "@ 337" + ex.Message);
            }
            finally
            {
                ConnectionClose();
            }
        }

        public ArrayList GetLimitsLocation(string pMaskID)
        {
            ConnectionOpen();
            ArrayList arrInfo = new ArrayList();

            //       StringBuilder SelectionCmnd = new StringBuilder("SELECT * FROM [" + sDatabaseName + "].[dbo].LimitsLocation");

            StringBuilder SelectionCmnd = new StringBuilder("SELECT * FROM [" + sDatabaseName + "].[dbo].LimitsLocation WHERE maskShort = '" + pMaskID + "'");

            SqlCommand GetSettingsInfo = new SqlCommand(SelectionCmnd.ToString(), mSqlServerConnection);

            try
            {
                using (SqlDataReader GetInfo = GetSettingsInfo.ExecuteReader())
                {
                    if (GetInfo.HasRows)
                    {
                        while (GetInfo.Read())
                        {
                            LimitsOBJ ThisInfo = new LimitsOBJ();

                            ThisInfo.PartShort = GetInfo["partShort"].ToString();
                            ThisInfo.FileLocation = GetInfo["fileLocation"].ToString();
                            ThisInfo.SitePartName = GetInfo["sitePartName"].ToString();

                            arrInfo.Add(ThisInfo);
                        }
                        return arrInfo;
                    }

                    else
                    {
                        //clear stuff
                        return null;
                    }
                }
            }
            catch (Exception MyDBException)
            {
                this.InsertLogEvent("DB Error", "@ 317" + MyDBException.Message);
                throw MyDBException;
            }
            finally
            {
                ConnectionClose();
            }
        }
    }

}
