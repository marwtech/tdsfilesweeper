﻿using FileSweepService.Equipment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

// using Lumentum.FileSweep;

namespace FileSweepService
{
    public static class Program
    {
        public static EPIConfigurationClass Obj_config;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new SweepService()
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
