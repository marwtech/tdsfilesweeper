﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileSweepService
{
    class DataTableOBJ
    {
        public string Name { get; set; }
        public string Status { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
    }
}
