﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileSweepService
{
    class ResultOBJ
    {
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Result { get; set; }
    }
}
