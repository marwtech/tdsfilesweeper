﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileSweepService
{
    class HeaderOBJ
    {
        public string WaveLength { get; set; }
        public string Value { get; set; }
        public string Parameter { get; set; }
        public string SerialNumber { get; set; }
        public string CustomerSerialNumber { get; set; }
        public string WaferID { get; set; }
        public string BatchNumber { get; set; }
        public string LotNumber { get; set; }
        public string WorkOrderNumber { get; set; }
        public string ShipmentID { get; set; }
        public string Gate { get; set; }
        public string ProductRevision { get; set; }
        public DateTime ProductReleaseDate { get; set; }
        public string ProductFirmware { get; set; }
        public string ParentProcessStep { get; set; }
        public string OperatorID { get; set; }
        public string Purpose { get; set; }
        public string SpecificationRevision { get; set; }
        public string SoftwareAppName { get; set; }
        public string SoftwareAppVersion { get; set; }
        public string SoftwareRevision { get; set; }
        public string Validated { get; set; }
        public string DataLock { get; set; }
        public string TestPlanID { get; set; }
        public string ResultCode { get; set; }
        public string FailureMode { get; set; }
        public int FailureCount { get; set; }
        public int ErrorCount { get; set; }
        public string ReworkAssignment { get; set; }
        public string TestSpecificationReleaseDate { get; set; }
        public string TestSpecificationReleaseState { get; set; }
        public int TestSpecificationRevID { get; set; }
        public string TestSpecDataLock { get; set; }
        public int StopOnFirstFail { get; set; }
        public int StopOnFirstError { get; set; }
        public string Operation { get; set; }
        public string TestStation { get; set; }
        public string PartNumber { get; set; }
        public string StartTime { get; set; }
        public string Site { get; set; }
        public string StartDate { get; set; }
        public string BarID { get; set; }
        public string ChipID { get; set; }
        public string Temperature { get; set; }
        public string StripID { get; set; }
        public string Comments { get; set; }
        public string SampleSize { get; set; }
        public string MiscInfo { get; set; }
        public string DeviceXLocation { get; set; }
        public string DeviceYLocation { get; set; }
        public string EndTime { get; set; }
        public double CycleTime_M { get; set; }
        public double AutomatedTime_M { get; set; }
        public double ManualActiveTime_M { get; set; }
        public double ManualIdleTime_M { get; set; }
        public double FaultActiveTime_M { get; set; }
        public double FaultIdleTime_M { get; set; }
        public int MeasurementCount { get; set; }
        public int TestEquipmentCount { get; set; }
        public int TestConditionID { get; set; }
        public string TestSequenceFile { get; set; }
        public string Result { get; set; }
        public string FileName { get; set; }
        public string DrawerID { get; set; }
        public string RecipeName { get; set; }
        public string RecipeVersion { get; set; }
        public string HWRevision { get; set; }
        public string ParetoCodename { get; set; }
        public string DeviceType { get; set; }




    }
}
