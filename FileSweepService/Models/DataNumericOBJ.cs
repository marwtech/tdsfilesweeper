﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileSweepService
{
    class DataNumericOBJ
    {
        public string Name { get; set; }
        public string Units { get; set; }
        public string CompOperator { get; set; }
        public string Status { get; set; }
        public string Specmin { get; set; }
        public string SpecMax { get; set; }
        public double Value { get; set; }

    }
}
