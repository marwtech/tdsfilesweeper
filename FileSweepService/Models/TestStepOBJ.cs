﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileSweepService
{
    class TestStepOBJ
    {
        public string Name { get; set; }
        public string StartDateTime { get; set; }
        public string EndDateTime { get; set; }
        public string Status { get; set; }
        public string Temperature { get; set; }
        public string TestType { get; set; }
        public string SampleRate { get; set; }
        public List<DataNumericOBJ> _DataNumeric { get; set; }
        public List<DataStringOBJ> _DataString { get; set; }
        public List<DataImageOBJ> _DataImage { get; set; }
        public List<DataAttachementOBJ> _DataAttachement { get; set; }
        public List<DataTableOBJ> _DataTable { get; set; }
        public List<DataSerialOBJ> _DataSerial { get; set; }
        public List<DataArrayOBJ> _DataArray { get; set; }
        public string Result { get; set; }

    }
}
