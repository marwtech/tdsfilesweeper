﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileSweepService
{
    class DataSerialOBJ
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public string PartType { get; set; }
        public string PartNumber { get; set; }
        public string SerialNumber { get; set; }
        public string Rev { get; set; }
    }
}
