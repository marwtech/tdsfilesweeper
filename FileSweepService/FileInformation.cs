﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileSweepService
{
    class FileInformation
    {
        public string WaferID { get; set; }
        public string FileName { get; set; }
        public int FileSize { get; set; }
        public DateTime FileWriteTime { get; set; }
        public DateTime FileModTime { get; set; }
        public string FileDate { get; set; }
    }
}
