﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileSweepService
{
    class SweepInfo
    {
        public string MaterialType { get; set; }
        public string FolderLocation { get; set; }
        public string Status { get; set; }
    }
}
