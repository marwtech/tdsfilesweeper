﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XMLConvertor
{
    class LimitsOBJ
    {
        public string PartShort { get; set; }
        public string SitePartName { get; set; }
        public string FileLocation { get; set; }
    }
}
