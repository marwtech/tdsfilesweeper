﻿namespace XMLConvertor
{
    internal class DataArrayOBJ
    {
        public string Parameter { get; set; }
        public string Title { get; set; }
        public string Status { get; set; }
        public string Units { get; set; }
        public string Value { get; set; }
        public string SpecLow { get; set; }
        public string SpecHigh { get; set; }
    }
}