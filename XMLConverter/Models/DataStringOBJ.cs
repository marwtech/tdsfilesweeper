﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XMLConvertor
{
    class DataStringOBJ
    {
        public string Name { get; set; }
        public string Units { get; set; }
        public string CompOperator { get; set; }
        public string Status { get; set; }
        public string SpecMin { get; set; }
        public string SpecMax { get; set; }
        public string Value { get; set; }
    }
}
