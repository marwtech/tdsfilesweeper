﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace XMLConvertor
{
    class XMLConstructor
    {
        public XElement ResultXML(string pStartTime, string pEndTime, string pResult)
        {
            XElement xElement = new XElement("Result",
                                new XAttribute("startDateTime", "" + DateTime.Now.ToString()),
                                new XAttribute("endDateTime", "" + DateTime.Now.ToString()),
                                new XAttribute("Result", "" + pResult));
            return xElement;
        }

        public XElement HeaderXML(string pSerialNumber, string pOperation, string pTestStation, string pPartNumber, string pStartTime, string pSite)
        {
            pSite = "250"; //hard coded as this probably not going to change soon

            XElement xElement = new XElement("Header",
                                new XAttribute("SerialNumber", ""+pSerialNumber),
                                new XAttribute("Operation", "" + pOperation),
                                new XAttribute("Teststation", "" + pTestStation),
                                new XAttribute("Partnumber", "" + pPartNumber),
                                new XAttribute("Starttime", "" + pStartTime),
                                new XAttribute("Site", pSite));
            return xElement;
        }
        public XElement HeaderMiscXML(string pComponent)
        {
            XElement xElement = new XElement("HeaderMisc",
                                new XAttribute("Component", "" + pComponent));
            return xElement;
        }

        public XElement DataXML(string pDataType, string pName, string pValue)
        {
            XElement xElement = new XElement("Data",
                                new XAttribute("DataType", "" + pDataType),
                                new XAttribute("Name", "" + pName),
                                new XAttribute("Value", "" + pValue)
                                );
            return xElement;
        }
        public XElement ParametricDataXML(string pDataType, string pName, string pValue)
        {
            XElement xElement = new XElement("Data",
                                new XAttribute("DataType", "" + pDataType),
                                new XAttribute("Units", "" + pDataType),
                                new XAttribute("Name", "" + pName),
                                new XAttribute("Value", "" + pValue)
                                );
            return xElement;
        }
    }
}
