﻿using System;
using System.Xml;

namespace XMLConvertor
{
    internal class XmlCsvReader
    {
        private Uri uri;
        private XmlNameTable nameTable;

        public XmlCsvReader(Uri uri, XmlNameTable nameTable)
        {
            this.uri = uri;
            this.nameTable = nameTable;
        }
    }
}