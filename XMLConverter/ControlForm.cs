﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using System.Diagnostics;
using System.IO;
using Oclaro.PromService;

namespace XMLConvertor
{
    public partial class ControlForm : Form
    {
        public static Promis mPromisManager = null;
        public static string CurrentUser = "";

        PastieUserDetails mCurrentUser = null;
        PastieConfigDetails mConfigData = null;

        int widthChange = 40;
        int heightChange = 105;

        enum State
        {
            logIn,
            mainForm
        };

        State mState = State.logIn;
  
        Form mMainForm = null;


        private void LoadPromisManager(PastieUserDetails thisUser)
        {
            mPromisManager = new PromisSppSip02();

            mPromisManager.Init("jprakelis_tp", "07062016_JP");
        }

        

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == 0x0112) // WM_SYSCOMMAND
            {
                // Check your window state here
                if (m.WParam == new IntPtr(0xF030)) // Maximize event - SC_MAXIMIZE from Winuser.h
                {
                    // THe window is being maximized
                    //this.TopMost = true;
                    //this.FormBorderStyle = FormBorderStyle.None;
                    //this.WindowState = FormWindowState.Maximized;
                }
            }
            else
            {
                //this.TopMost = false;
                //this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
                //this.WindowState = FormWindowState.Normal;
            }
            base.WndProc(ref m);
        }

        private void NextButton_Click(object sender, EventArgs e)
        {
            LoadPromisManager(mCurrentUser);
        }
    }
}
