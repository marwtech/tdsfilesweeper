﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XMLConvertor.XMLTemplates
{
    class xmlItem
    {
        public string PartType { get; set; }
        public string PartNumber { get; set; }
        public string SerialNumber { get; set; }
        public string Rev { get; set; }
    }
}
