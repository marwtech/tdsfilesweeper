﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XMLConvertor.XMLTemplates
{
    class xmlTestStep
    {
        public string SerialNumber { get; set; }
        public string Operation { get; set; }
        public string TestStation { get; set; }
        public string Operator { get; set; }
        public string PartNumber { get; set; }
        public string StartTime { get; set; }
        public string Site { get; set; }
    }
}
