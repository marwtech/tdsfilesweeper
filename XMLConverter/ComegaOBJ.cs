﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XMLConvertor
{
    internal class ComegaOBJ
    {
        public string Cassette { get; set; }
        public string Sequence { get; set; }
        public string LotNumber { get; set; }
        public string Date { get; set; }
        public string Wafer { get; set; }
        public string RecName { get; set; }
        public string RecNumber { get; set; }
        public string StepName { get; set; }
        public string StepNumber { get; set; }
        public string FileName { get; set; }
        public string DataType { get; set; }
        public string Name { get; set; }

        public string SourceAttachmentName { get; set; }
    }
}
