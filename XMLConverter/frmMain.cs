﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using Newtonsoft.Json;
using Oclaro.PromService;
using XMLConvertor.Equipment;

namespace XMLConvertor
{
    public partial class frmConverterMain : Form
    {
        public static EPIConfigurationClass Obj_config;
        string _TempFolder;
        private List<string> _TableImagesAttachList = new List<string>();
        private List<string> _XmlAttachList = new List<string>();
        private List<string> _FailFileList = new List<string>();
        private List<string> _SuccessFileList = new List<string>();

        private Boolean contRead = true;
        //------- Location settings ----------//

        // Local dev settings        
        /*
        private string nTDSTable = @"C:\Users\pra70986\Desktop\AWS_TDS\Application\Files To Convert\Table\";
        private string nTDSFolder = @"C:\Users\pra70986\Desktop\AWS_TDS\Application\Files To Convert\Data\";
        private string nTDSAttachment = @"C:\Users\pra70986\Desktop\AWS_TDS\Application\Files To Convert\Attachment\";
        
        private string nToConvert = @"C:\Users\pra70986\Desktop\AWS_TDS\Application\Files To Convert\ToConvert\";
        private string nConverted = @"C:\Users\pra70986\Desktop\AWS_TDS\Application\Files To Convert\Converted\";
        private string localProcessing = @"C:\Users\pra70986\Desktop\AWS_TDS\Application\Files To Convert\Processing\";
        */

        //TDS Dev
        //start
        
        private string nTDSTable = @"\\THAAPPTDSDEV03\Data\TableData\CAS\";
        private string nTDSFolder = @"C:\Users\pra70986\Desktop\AWS_TDS\Application\Files To Convert\Data\";
    //    private string nTDSFolder = @"\\THAAPPTDSDEV03\Data\";

        private string nTDSAttachment = @"\\THAAPPTDSDEV03\Data\Attachment\";
        private string nConverted = @"\\THAAPPTDSDEV03\Data\Caswell Data\Converted\"; //test location
        private string nToConvert = @"C:\Users\pra70986\Desktop\AWS_TDS\Application\Files To Convert\ToConvert\";
        public string localProcessing = @"C:\FileSweepProcessing\Processing\";
        
        //end        

        //PRoduction with client access
        //start
        /*
        private string nTDSTable = @"\\awscastdsprd02.li.lumentuminc.net\data$\Caswell\TableData\";
        private string nTDSFolder = @"\\awscastdsprd02.li.lumentuminc.net\data$\";
        private string nTDSAttachment = @"\\awscastdsprd02.li.lumentuminc.net\data$\Caswell\Attachment\";
        private string nConverted = @"\\awscastdsprd02.li.lumentuminc.net\data$\Caswell\Converted\";
        private string nToConvert = @"\\awscastdsprd02.li.lumentuminc.net\data$\Caswell\ToConvert\";
        public string localProcessing = @"C:\FileSweepProcessing\Processing\"; //local processing folder. Add TDS location in order to minimize file transfer delay
        */
        //End

        //PROMIS control
        public static Promis mPromisManager = null;        

        private DataArrayOBJ arrayData = new DataArrayOBJ();
        private ArrayList paramData = new ArrayList();
        private ArrayList valueData = new ArrayList();
        private ArrayList arrComponents = new ArrayList();
        private ArrayList arrLogSource = new ArrayList();
        private ArrayList attNames = new ArrayList();

        private ArrayList arrHeaderMap = new ArrayList();
        private ArrayList arrHeader = new ArrayList();
        private ArrayList arrValues = new ArrayList();
        private ArrayList arrCSVInfo = new ArrayList();
        private ArrayList arrTestData = new ArrayList();
        private ArrayList arrAttImage = new ArrayList();

        //table data
        private string[] arrTableInfo;
        private string[] arrTableHeader;

        //table info in list format
        private List<string> listTableInfo = new List<string>();
        private List<string> listTableHeader = new List<string>();

        //log data
        private ArrayList arrLogHeader = new ArrayList();
        private ArrayList arrLogData = new ArrayList();

        //mz data
        private ArrayList arrMZData = new ArrayList();
        private List<string> listUnits = new List<string>();
        private string logAttachmentName = "";

        //Spec Data
        private List<string> listSpecName = new List<string>();
        private List<string> listSpecMin = new List<string>();
        private List<string> listSpecMax = new List<string>();
        private List<string> listSpecUnits = new List<string>();

        private float value;
        private Boolean readData = false;
        private Boolean countSet = false;
        private bool newFile = false;

        private XElement xmlFile = null;

        public DBAccess mDBAccess = new DBAccess();
        private Timer sweepTimer;
        private string nWaferID = "";
        private string sFileType = "";
        private string inLotType = "";

        private string saveFile = "";
    
        public XDocument xDoc = null;
        private ArrayList aData = new ArrayList();

        //header strings
        private string inWaferID, inOperation, inOperator, inPartNumber, inStartTime, inEndTime, inComponent, inMask, inDeviceType;

        private string oldFile = "";
        private string sumFileName;

        private string[] partID;

        private string bSweepDataLoc;
        private string bHeaders;

        private string lineHeaders;
        private string lineCassette;
        private string lineRec;

        //log elements
        private string completeLogName = "";
        private string convertedTable;
        private string[] logName;
        private string logDate;
        private string logDateTime;

        private string logSummary;
        private string logSource;

        private bool logDelete = false;
        private string logFileName = "";

        private string saveXMLLog = "";
        private bool mzExists = false;
        private bool attExists = false;

        string logFullName;
        private string convertedLogCSV;

        private bool createFile = false;
        private string summaryFile = "";
        private string destAttFile = "";

        private string attachmentLocation;
        private string tableLocation;

        //arrays for COMEGA structure
        private ArrayList arrLogCassette = new ArrayList();
        private ArrayList arrLogWafer = new ArrayList();
        private ArrayList arrLogRec = new ArrayList();
        private ArrayList arrLogStep = new ArrayList();
        private ArrayList arrLogSet = new ArrayList();
        private ArrayList arrLogMin = new ArrayList();
        private ArrayList arrLogMax = new ArrayList();
        private ArrayList arrLogAve = new ArrayList();

        private ArrayList arrLogFileName = new ArrayList();

        private ArrayList arrLogFileData = new ArrayList();

        private ComegaOBJ omega = new ComegaOBJ();
        private ArrayList omegaList = new ArrayList();

        private XmlNode mzTestNode;
        private XmlNode mzDataNode;
        private XmlAttribute mzStatusNode;
        private XmlAttribute mzNameNode;
        private XmlAttribute mzStartNode;

        private XmlAttribute mzTypeNode;
        private XmlAttribute mzDataNameNode;
        private XmlAttribute mzValNode;

        private int headerLength, dataLength;
        private string[] dataValues, headerValues;

        //additional parameters to add
        private double LOSS_IMB_DB, ER_X_ASE_DB, X_ER_CORRECTED_DB, ER_Y_ASE_DB, Y_ER_CORRECTED_DB, XOUTER_INS_LOSS_DB, YOUTER_INS_LOSS_DB, XOUTER_DET_PMAX_MZ1_DBM, XOUTER_DET_PMAX_MZ2_DBM, OP_SOA_X_ASE_DBM, XOUTER_ER_DB, YOUTER_DET_PMAX_MZ3_DBM;
        private double YOUTER_DET_PMAX_MZ4_DBM, OP_SOA_Y_ASE_DBM, YOUTER_ER_DB, minValue;

        private string testEndDate = "";

        //test data strings
        private int totalLines = 0;

        //set timezone for the file names etc.
        private TimeZone localZone = TimeZone.CurrentTimeZone;

        //Limits 
        private string limitsRevision, passFail;
        private int passFailIndex;

        public frmConverterMain()
        {
            InitializeComponent();

            InitTimer();
        }

        public void InitTimer()
        {
            sweepTimer = new Timer();
            sweepTimer.Tick += new EventHandler(sweepTimer_Tick);
            sweepTimer.Interval = 10; // in milliseconds
            sweepTimer.Start();
        }
        private void frmConverterMain_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void sweepTimer_Tick(object sender, EventArgs e)
        {

        //     StartSweepTasks();

              StartConvertionTasks();

        }
        private void StartSweepTasks()
        {
            mDBAccess.InsertLogEvent("START", "Service Started");

            while (true)
            {
                try
                {
                    mDBAccess.GetFileLocations();
                }
                catch (Exception ex)
                {
                    mDBAccess.InsertLogEvent("Error", "" + ex.Message);
                }
                foreach (string loc in mDBAccess.arrLocations)
                {
                    DirSearch(loc);
                }
            }
        }
        private void RemoveLogFile()
        {
                try
                {
                System.Threading.Thread.Sleep(2);
                System.GC.Collect();
                System.GC.WaitForPendingFinalizers();

                if (File.Exists(oldFile))
                {
                    File.SetAttributes(oldFile, FileAttributes.Normal);
                    File.Delete(oldFile);
                }
                }catch(Exception ex)
                {
                    Console.WriteLine(""+ex);
                }
        }

        /***********************************************
         * Check if database entry needs to be updated *
         * *********************************************/
        private bool GetUpdateFlag(string pFileName, DateTime pModTime, int pFileSize)
        {
            bool nRead = false;

            try
            {
                //check if file exists in the database
                if (mDBAccess.GetFileInformation(nWaferID, pFileName) != null)
                {
                    //current information from the database
                    foreach (FileInformation dbFile in mDBAccess.GetFileInformation(nWaferID, pFileName))
                    {
                        //do check on NULL data
                        if (dbFile != null)
                        {
                            if (pModTime > dbFile.FileModTime.AddSeconds(2.0))
                            {
                                nRead = true;
                                return nRead;
                            }
                            else
                            {
                                nRead = false;
                                return nRead;
                            }
                        }
                    }
                }
            }
            catch (Exception ex) { mDBAccess.InsertLogEvent("DB error", "@ 198" + ex.Message); }
            return nRead;
        }

        #region file sweep methods
        //Main Sweep method 
        private List<String> DirSearch(string sDir)
        {
            string[] toolName = new string[] { };

            //kit info check-up
            if (sDir.Contains("ToolData"))
            {
                toolName = sDir.Split('\\');
                if (sDir.Contains("COMEGA01"))
                {
                    //    sDir = sDir + DateTime.Now.Year.ToString() + @" Logs\DATALOG\SUMMARY\"; //use this after historical data loaded
                    sDir = sDir + @"2020 Logs\DATALOG\SUMMARY\"; //for historical load only
                }
                if (sDir.Contains("COMEGA02"))
                {
                    //sDir = sDir + DateTime.Now.Year.ToString() + @"\Logs\DATALOG\SUMMARY\"; //summary data

                //    sDir = sDir + @"2020\Logs\DATALOG\FULLMV\"; //for historical load only

                    sDir = sDir + DateTime.Now.Year.ToString() + @"\Logs\DATALOG\FULLMV\"; //raw data
                }
                if (sDir.Contains("CSTEAG01"))
                {
                    sDir = sDir; //no processing for now but this will change in the future
                }
                if (sDir.Contains("CDELTA01"))
                {
                    sDir = sDir + DateTime.Now.Year.ToString() + @" Logs\DATALOG\CASSETTE\";
                }
            }

            var currentDateTime = DateTime.Now;
            string dateStr = currentDateTime.ToString("yyyy-MM-dd");

            //last write time interval - read files older than
            var nTargetTime = DateTime.Now.AddHours(-1.0);
            var nLastWriteTime = DateTime.Now.AddMilliseconds(-500);

            List<String> files = new List<String>();
            if (Directory.Exists(sDir))
                try
                {
                    if (!sDir.Contains("ToolData"))
                    {
                        foreach (string d in Directory.GetDirectories(sDir))
                        {
                            DirectoryInfo nDirectory = new DirectoryInfo(sDir);
                            FileInfo[] nFile = nDirectory.GetFiles("*.*");

                            foreach (FileInfo nInfo in nFile)
                            {
                                if (nInfo.Exists)
                                {
                                    var creationTime = nInfo.CreationTime;

                                    //4 years, 9 months, 4 days from 05/10/20
                                    DateTime histDate = DateTime.Parse("01/01/2016");

                                    if (nInfo.Name.EndsWith("Processed.csv") || nInfo.Name.EndsWith("Screened.csv") && creationTime > histDate)
                                        if (nInfo.Extension.Contains(".csv"))
                                        {
                                            string[] rPathInfo = nInfo.Name.Split('_');
                                            nWaferID = rPathInfo[0].ToString();

                                            //get DB data
                                            ArrayList dbFileInfo = mDBAccess.GetFileInformation(rPathInfo[0], nInfo.Name.ToString());

                                            if (dbFileInfo != null)
                                            {
                                                if (dbFileInfo.Count != 0 && dbFileInfo != null)
                                                {
                                                    foreach (FileInformation dbInfo in dbFileInfo)
                                                    {
                                                        if (dbInfo.WaferID.Equals(rPathInfo[0]) && dbInfo.FileName.Equals(nInfo.Name))
                                                        {
                                                            //check this updates failing
                                                            if (nInfo.LastWriteTime > dbInfo.FileModTime.AddSeconds(2.0))
                                                            {
                                                                try
                                                                {
                                                                    if (File.Exists(nToConvert + nInfo.Name))
                                                                    {
                                                                        if (GetUpdateFlag(nInfo.Name, nInfo.LastWriteTime, (int)nInfo.Length))
                                                                        {
                                                                            try
                                                                            {
                                                                                System.GC.Collect();
                                                                                System.GC.WaitForPendingFinalizers();
                                                                                File.SetAttributes(nToConvert + nInfo.Name, FileAttributes.Normal);

                                                                                File.Delete(nToConvert + nInfo.Name);
                                                                                File.Copy(nInfo.DirectoryName + "\\" + nInfo.Name, nToConvert + nWaferID + "_" + nInfo.Name);
                                                                            }
                                                                            catch (Exception exp)
                                                                            {
                                                                                mDBAccess.InsertLogEvent("Error", exp.Message);
                                                                            }
                                                                            mDBAccess.UpdateFileInfo(rPathInfo[0], nInfo.Name, nInfo.LastWriteTime, (int)nInfo.Length);
                                                                        }
                                                                        else
                                                                        {
                                                                            try
                                                                            {
                                                                                File.Copy(nInfo.DirectoryName + "\\" + nInfo.Name, nToConvert + nInfo.Name);
                                                                            }
                                                                            catch (Exception exp)
                                                                            {
                                                                                mDBAccess.InsertLogEvent("Error", exp.Message);
                                                                            }

                                                                            mDBAccess.InsertFileInfo(nWaferID, nInfo.Name, nInfo.LastWriteTime, (int)nInfo.Length);

                                                                        }

                                                                    }
                                                                }
                                                                catch (Exception ex)
                                                                {
                                                                    if (ex.Message.Contains("already exists"))
                                                                    {
                                                                            mDBAccess.InsertLogEvent("Error", ex.Message);
                                                                        
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                try
                                                {
                                                    if (File.Exists(nToConvert + nInfo.Name))
                                                    {
                                                        try
                                                        {
                                                            System.GC.Collect();
                                                            System.GC.WaitForPendingFinalizers();
                                                            File.SetAttributes(nToConvert + nInfo.Name, FileAttributes.Normal);

                                                            File.Delete(nToConvert + nInfo.Name);
                                                            File.Copy(nInfo.FullName, nToConvert + nInfo.Name);
                                                        }
                                                        catch (Exception exp) { mDBAccess.InsertLogEvent("Error", exp.Message); }
                                                    }
                                                    else
                                                    {
                                                        try
                                                        {
                                                            if (File.Exists(nInfo.DirectoryName + "\\" + nInfo.Name))
                                                            {
                                                                File.Copy(nInfo.FullName, nToConvert + nInfo.Name);
                                                            }
                                                        }
                                                        catch (Exception exp) { mDBAccess.InsertLogEvent("Error", "" + exp); }
                                                    }
                                                    if (GetUpdateFlag(nInfo.Name, nInfo.LastWriteTime, (int)nInfo.Length))
                                                    {
                                                        mDBAccess.UpdateFileInfo(rPathInfo[0], nInfo.Name, nInfo.LastWriteTime, (int)nInfo.Length);
                                                    }
                                                    else
                                                    {
                                                        mDBAccess.InsertFileInfo(nWaferID, nInfo.Name, nInfo.LastWriteTime, (int)nInfo.Length);
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    if (ex.Message.Contains("already exists"))
                                                    {
                                                        try
                                                        {
                                                            if (dbFileInfo != null)
                                                            {
                                                                foreach (FileInformation dbInfo in dbFileInfo)
                                                                {
                                                                    if (nInfo.LastWriteTime > dbInfo.FileModTime)
                                                                    {
                                                                        System.GC.Collect();
                                                                        System.GC.WaitForPendingFinalizers();

                                                                        File.Delete(nToConvert + nInfo.Name);
                                                                        File.Copy(nInfo.FullName, nToConvert + nInfo.Name);

                                                                        mDBAccess.UpdateFileInfo(rPathInfo[0], nInfo.Name, nInfo.LastWriteTime, (int)nInfo.Length);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        catch (Exception exp)
                                                        {
                                                            mDBAccess.InsertLogEvent("Error", exp.Message);
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                }
                            }
                            files.AddRange(DirSearch(d));
                        }
                    }
                    else
                    {
                        for (int i = 0; i < toolName.Length; i++)
                        {
                        var tool = toolName[toolName.Length - 2].ToString();

                        //CDELTA01 processing
                        if (toolName[i].ToString().Contains("CDELTA01"))
                            {
                                DirectoryInfo nDirectory = new DirectoryInfo(sDir);
                                FileInfo[] nFile = nDirectory.GetFiles("*.*", SearchOption.AllDirectories);
                                //  FileStream fs = null;

                                foreach (FileInfo nInfo in nFile)
                                {
                                    if (nInfo.Exists)
                                    {
                                        var creationTime = nInfo.CreationTime;

                                        DateTime histDate = DateTime.Parse("10/21/2020");

                                        if (!nInfo.Name.Contains("CASSETTE") && creationTime > histDate)
                                        {
                                            string[] rPathInfo = nInfo.Name.Split('.');
                                            nWaferID = rPathInfo[0].ToString();
                                            var nLogFileName = tool + "." + nWaferID + "." + nInfo.LastWriteTime.ToString().Replace("/", "_").Replace(":", ".") + ".csv";

                                            //get DB data
                                            ArrayList dbFileInfo = mDBAccess.GetFileInformation(tool + "_" + nWaferID, nLogFileName);

                                            if (dbFileInfo != null)
                                            {
                                                if (dbFileInfo.Count != 0 && dbFileInfo != null)
                                                {
                                                    foreach (FileInformation dbInfo in dbFileInfo)
                                                    {
                                                        if (dbInfo.WaferID.Equals(tool + "_" + nWaferID) && dbInfo.FileName.Equals(nLogFileName))
                                                        {
                                                            //check this updates failing
                                                            if (nInfo.LastWriteTime > dbInfo.FileModTime.AddSeconds(2.0))
                                                            {
                                                                try
                                                                {
                                                                    if (File.Exists(nToConvert + nLogFileName))
                                                                    {
                                                                        if (GetUpdateFlag(nLogFileName, nInfo.LastWriteTime, (int)nInfo.Length))
                                                                        {
                                                                            try
                                                                            {
                                                                                System.GC.Collect();
                                                                                System.GC.WaitForPendingFinalizers();
                                                                                File.SetAttributes(nToConvert + nInfo.Name, FileAttributes.Normal);

                                                                                File.Delete(nToConvert + nLogFileName);
                                                                                File.Copy(nInfo.DirectoryName + "\\" + nInfo.Name, nToConvert + nLogFileName);
                                                                            }
                                                                            catch (Exception exp)
                                                                            {
                                                                                mDBAccess.InsertLogEvent("Error", exp.Message);
                                                                            }
                                                                            mDBAccess.UpdateFileInfo(tool + "_" + nWaferID, nInfo.Name, nInfo.LastWriteTime, (int)nInfo.Length);
                                                                        }
                                                                        else
                                                                        {
                                                                            try
                                                                            {
                                                                                File.Copy(nInfo.DirectoryName + "\\" + nInfo.Name, nToConvert + nLogFileName);
                                                                            }
                                                                            catch (Exception exp)
                                                                            {
                                                                                mDBAccess.InsertLogEvent("Error", exp.Message);
                                                                            }

                                                                            mDBAccess.InsertFileInfo(tool + "_" + nWaferID, nLogFileName, nInfo.LastWriteTime, (int)nInfo.Length);
                                                                        }

                                                                    }
                                                                }
                                                                catch (Exception ex)
                                                                {
                                                                    if (ex.Message.Contains("already exists"))
                                                                    {
                                                                            mDBAccess.InsertLogEvent("Error", ex.Message);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                try
                                                {
                                                    //if file is already loaded in temp -> replace it
                                                    if (File.Exists(nToConvert + nLogFileName))
                                                    {
                                                        try
                                                        {
                                                            System.GC.Collect();
                                                            System.GC.WaitForPendingFinalizers();

                                                            File.SetAttributes(nToConvert + nLogFileName, FileAttributes.Normal);

                                                            File.Delete(nToConvert + nLogFileName);
                                                            File.Copy(nInfo.FullName, nToConvert + nLogFileName);
                                                        }
                                                        catch (Exception exp) { mDBAccess.InsertLogEvent("Error", exp.Message); }
                                                    }
                                                    else
                                                    {
                                                        try
                                                        {
                                                            //copy file from the original location
                                                            if (File.Exists(nInfo.DirectoryName + "\\" + nInfo.Name))
                                                            {
                                                                File.Copy(nInfo.FullName, nToConvert + nLogFileName);
                                                            }
                                                        }
                                                        catch (Exception exp) { mDBAccess.InsertLogEvent("Error", "" + exp); }
                                                    }
                                                    //check if file was already registered and if any data changed -> update existing file if changed
                                                    if (GetUpdateFlag(nLogFileName, nInfo.LastWriteTime, (int)nInfo.Length))
                                                    {
                                                        mDBAccess.UpdateFileInfo(tool + "_" + nWaferID, nLogFileName, nInfo.LastWriteTime, (int)nInfo.Length);
                                                    }
                                                    else
                                                    {
                                                        mDBAccess.InsertFileInfo(tool + "_" + nWaferID, nLogFileName, nInfo.LastWriteTime, (int)nInfo.Length);
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    if (ex.Message.Contains("already exists"))
                                                    {
                                                        try
                                                        {
                                                            if (dbFileInfo != null)
                                                            {
                                                                foreach (FileInformation dbInfo in dbFileInfo)
                                                                {
                                                                    if (nInfo.LastWriteTime > dbInfo.FileModTime)
                                                                    {
                                                                        System.GC.Collect();
                                                                        System.GC.WaitForPendingFinalizers();

                                                                        File.Delete(nToConvert + nLogFileName);
                                                                        File.Copy(nInfo.FullName, nToConvert + nLogFileName);

                                                                        mDBAccess.UpdateFileInfo(tool + "_" + nWaferID, nLogFileName, nInfo.LastWriteTime, (int)nInfo.Length);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        catch (Exception exp)
                                                        {
                                                            mDBAccess.InsertLogEvent("Error", exp.Message);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                        //COMEGA01 processing
                        if (toolName[i].ToString().Contains("COMEGA01")) {
                            DirectoryInfo nDirectory = new DirectoryInfo(sDir);
                            FileInfo[] nFile = nDirectory.GetFiles("*.*", SearchOption.AllDirectories);

                            foreach (FileInfo nInfo in nFile)
                            {
                                if (nInfo.Exists)
                                {
                                    var creationTime = nInfo.CreationTime;

                                    DateTime histDate = DateTime.Parse("01/01/2016");

                                    if (nInfo.Name.Contains("LOG") && !nInfo.Name.Contains("-") && nInfo.Name.Length < 9 && creationTime > histDate)
                                    {
                                        string[] rPathInfo = nInfo.Name.Split('.');
                                        nWaferID = rPathInfo[0].ToString();
                                        var nLogFileName = tool + "." + nWaferID + "." + nInfo.LastWriteTime.ToString().Replace("/", "_").Replace(":", ".") + ".csv";

                                        //get DB data
                                        ArrayList dbFileInfo = mDBAccess.GetFileInformation(tool + "_" + nWaferID, nLogFileName);

                                        if (dbFileInfo != null)
                                        {
                                            if (dbFileInfo.Count != 0 && dbFileInfo != null)
                                            {
                                                foreach (FileInformation dbInfo in dbFileInfo)
                                                {
                                                    if (dbInfo.WaferID.Equals(tool + "_" + nWaferID) && dbInfo.FileName.Equals(nLogFileName))
                                                    {
                                                        //check this updates failing
                                                        if (nInfo.LastWriteTime > dbInfo.FileModTime.AddSeconds(2.0))
                                                        {
                                                            try
                                                            {
                                                                if (File.Exists(nToConvert + nLogFileName))
                                                                {
                                                                    if (GetUpdateFlag(nLogFileName, nInfo.LastWriteTime, (int)nInfo.Length))
                                                                    {
                                                                        try
                                                                        {
                                                                            System.GC.Collect();
                                                                            System.GC.WaitForPendingFinalizers();
                                                                            File.SetAttributes(nToConvert + nInfo.Name, FileAttributes.Normal);

                                                                            File.Delete(nToConvert + nLogFileName);
                                                                            File.Copy(nInfo.DirectoryName + "\\" + nInfo.Name, nToConvert + nLogFileName);
                                                                        }
                                                                        catch (Exception exp)
                                                                        {
                                                                            mDBAccess.InsertLogEvent("Error", exp.Message);
                                                                        }
                                                                        mDBAccess.UpdateFileInfo(tool + "_" + nWaferID, nInfo.Name, nInfo.LastWriteTime, (int)nInfo.Length);
                                                                    }
                                                                    else
                                                                    {
                                                                        try
                                                                        {
                                                                            File.Copy(nInfo.DirectoryName + "\\" + nInfo.Name, nToConvert + nLogFileName);
                                                                        }
                                                                        catch (Exception exp)
                                                                        {
                                                                            mDBAccess.InsertLogEvent("Error", exp.Message);
                                                                        }

                                                                        mDBAccess.InsertFileInfo(tool + "_" + nWaferID, nLogFileName, nInfo.LastWriteTime, (int)nInfo.Length);

                                                                    }

                                                                }
                                                            }
                                                            catch (Exception ex)
                                                            {
                                                                if (ex.Message.Contains("already exists"))
                                                                {
                                                                        mDBAccess.InsertLogEvent("Error", ex.Message);
                                         
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            try
                                            {
                                                    //if file is already loaded in temp -> replace it
                                                if (File.Exists(nToConvert + nLogFileName))
                                                {
                                                    try
                                                    {
                                                        System.GC.Collect();
                                                        System.GC.WaitForPendingFinalizers();

                                                        File.SetAttributes(nToConvert + nLogFileName, FileAttributes.Normal);

                                                        File.Delete(nToConvert + nLogFileName);
                                                        File.Copy(nInfo.FullName, nToConvert + nLogFileName);
                                                    }
                                                    catch (Exception exp) { mDBAccess.InsertLogEvent("Error", exp.Message); }
                                                }
                                                else
                                                {
                                                    try
                                                    {
                                                            //copy file from the original location
                                                        if (File.Exists(nInfo.DirectoryName + "\\" + nInfo.Name))
                                                        {
                                                            File.Copy(nInfo.FullName, nToConvert + nLogFileName);
                                                            //   File.Copy(nInfo.DirectoryName + "\\" + nInfo.Name, nToConvert + nInfo.Name);
                                                        }
                                                    }
                                                    catch (Exception exp) { mDBAccess.InsertLogEvent("Error", "" + exp); }
                                                }
                                                //check if file was already registered and if any data changed -> update existing file if changed
                                                if (GetUpdateFlag(nLogFileName, nInfo.LastWriteTime, (int)nInfo.Length))
                                                {
                                                    mDBAccess.UpdateFileInfo(tool + "_" + nWaferID, nLogFileName, nInfo.LastWriteTime, (int)nInfo.Length);
                                                }
                                                else
                                                {
                                                    mDBAccess.InsertFileInfo(tool + "_" + nWaferID, nLogFileName, nInfo.LastWriteTime, (int)nInfo.Length);
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                if (ex.Message.Contains("already exists"))
                                                {
                                                    try
                                                    {
                                                        if (dbFileInfo != null)
                                                        {
                                                            foreach (FileInformation dbInfo in dbFileInfo)
                                                            {
                                                                if (nInfo.LastWriteTime > dbInfo.FileModTime)
                                                                {
                                                                    System.GC.Collect();
                                                                    System.GC.WaitForPendingFinalizers();

                                                                    File.Delete(nToConvert + nLogFileName);
                                                                    File.Copy(nInfo.FullName, nToConvert + nLogFileName);

                                                                    mDBAccess.UpdateFileInfo(tool + "_" + nWaferID, nLogFileName, nInfo.LastWriteTime, (int)nInfo.Length);
                                                                }
                                                            }
                                                        }
                                                    }
                                                    catch (Exception exp)
                                                    {
                                                        mDBAccess.InsertLogEvent("Error", exp.Message);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        //COMEGA02 processing
                        if (toolName[i].ToString().Contains("COMEGA02")) {
                            DirectoryInfo nDirectory = new DirectoryInfo(sDir);

                                //      string[] allfiles = Directory.GetFiles(sDir, "*.*", SearchOption.AllDirectories);

                                try
                                {
                                    FileInfo[] nFile = nDirectory.GetFiles("*.*", SearchOption.AllDirectories);

                                //  Directory.GetFiles(dirPath, "*", SearchOption.AllDirectories);

                            foreach (FileInfo nInfo in nFile)
                            {
                                if (nInfo.Exists)
                                {
                                    var creationTime = nInfo.CreationTime;

                                    DateTime histDate = DateTime.Parse("01/01/2017");

                                    if (nInfo.Name.Contains("LOG") && nInfo.Name.Length > 6 && !nInfo.Name.Contains("(") && nInfo.Name.Contains("C") && creationTime > histDate)
                                    {
                                        string[] rPathInfo = nInfo.Name.Split('.');
                                        nWaferID = rPathInfo[0].ToString();
                                        var nLogFileName = tool + "." + nWaferID + "." + nInfo.LastWriteTime.ToString().Replace("/", "_").Replace(":", ".") + ".LOG";

                                        //get DB data
                                        ArrayList dbFileInfo = mDBAccess.GetFileInformation(tool + "_" + nWaferID, nLogFileName);

                                        if (dbFileInfo != null)
                                        {
                                            if (dbFileInfo.Count != 0 && dbFileInfo != null)
                                            {
                                                foreach (FileInformation dbInfo in dbFileInfo)
                                                {
                                                    if (dbInfo.WaferID.Equals(tool + "_" + nWaferID) && dbInfo.FileName.Equals(nLogFileName))
                                                    {
                                                        //check this updates failing
                                                        if (nInfo.LastWriteTime > dbInfo.FileModTime.AddSeconds(2.0))
                                                        {
                                                            try
                                                            {
                                                                if (File.Exists(nToConvert + nLogFileName))
                                                                {
                                                                    if (GetUpdateFlag(nLogFileName, nInfo.LastWriteTime, (int)nInfo.Length))
                                                                    {
                                                                        try
                                                                        {
                                                                            System.GC.Collect();
                                                                            System.GC.WaitForPendingFinalizers();
                                                                            File.SetAttributes(nToConvert + nInfo.Name, FileAttributes.Normal);

                                                                            File.Delete(nToConvert + nLogFileName);
                                                                            File.Copy(nInfo.DirectoryName + "\\" + nInfo.Name, nToConvert + nLogFileName);
                                                                        }
                                                                        catch (Exception exp)
                                                                        {
                                                                            mDBAccess.InsertLogEvent("Error", exp.Message);
                                                                        }
                                                                        mDBAccess.UpdateFileInfo(tool + "_" + nWaferID, nInfo.Name, nInfo.LastWriteTime, (int)nInfo.Length);
                                                                    }
                                                                    else
                                                                    {
                                                                        try
                                                                        {
                                                                            File.Copy(nInfo.DirectoryName + "\\" + nInfo.Name, nToConvert + nLogFileName);
                                                                        }
                                                                        catch (Exception exp)
                                                                        {
                                                                            mDBAccess.InsertLogEvent("Error", exp.Message);
                                                                        }

                                                                        mDBAccess.InsertFileInfo(tool + "_" + nWaferID, nLogFileName, nInfo.LastWriteTime, (int)nInfo.Length);

                                                                    }

                                                                }
                                                            }
                                                            catch (Exception ex)
                                                            {
                                                                if (ex.Message.Contains("already exists"))
                                                                {
                                                                        mDBAccess.InsertLogEvent("Error", ex.Message);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            try
                                            {
                                                    //if file is already loaded in temp -> replace it
                                                if (File.Exists(nToConvert + nLogFileName))
                                                {
                                                    try
                                                    {
                                                        System.GC.Collect();
                                                        System.GC.WaitForPendingFinalizers();

                                                        File.SetAttributes(nToConvert + nLogFileName, FileAttributes.Normal);

                                                        File.Delete(nToConvert + nLogFileName);
                                                        File.Copy(nInfo.FullName, nToConvert + nLogFileName);
                                                    }
                                                    catch (Exception exp) { mDBAccess.InsertLogEvent("Error", exp.Message); }
                                                }
                                                else
                                                {
                                                    try
                                                    {
                                                            //copy file from the original location
                                                        if (File.Exists(nInfo.DirectoryName + "\\" + nInfo.Name))
                                                        {
                                                            File.Copy(nInfo.FullName, nToConvert + nLogFileName);
                                                        }
                                                    }
                                                    catch (Exception exp) { mDBAccess.InsertLogEvent("Error", "" + exp); }
                                                }
                                                //check if file was already registered and if any data changed -> update existing file if changed
                                                if (GetUpdateFlag(nLogFileName, nInfo.LastWriteTime, (int)nInfo.Length))
                                                {
                                                    mDBAccess.UpdateFileInfo(tool + "_" + nWaferID, nLogFileName, nInfo.LastWriteTime, (int)nInfo.Length);
                                                }
                                                else
                                                {
                                                    mDBAccess.InsertFileInfo(tool + "_" + nWaferID, nLogFileName, nInfo.LastWriteTime, (int)nInfo.Length);
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                if (ex.Message.Contains("already exists"))
                                                {
                                                    try
                                                    {
                                                        if (dbFileInfo != null)
                                                        {
                                                            foreach (FileInformation dbInfo in dbFileInfo)
                                                            {
                                                                if (nInfo.LastWriteTime > dbInfo.FileModTime)
                                                                {
                                                                    System.GC.Collect();
                                                                    System.GC.WaitForPendingFinalizers();

                                                                    File.Delete(nToConvert + nLogFileName);
                                                                    File.Copy(nInfo.FullName, nToConvert + nLogFileName);

                                                                    mDBAccess.UpdateFileInfo(tool + "_" + nWaferID, nLogFileName, nInfo.LastWriteTime, (int)nInfo.Length);
                                                                }
                                                            }
                                                        }
                                                    }
                                                    catch (Exception exp)
                                                    {
                                                        mDBAccess.InsertLogEvent("Error", exp.Message);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine(e.Message);
                                }
                            }

                        //CSTEAG01
                        if (toolName[i].ToString().Contains("CSTEAG01"))
                        {
                            DirectoryInfo nDirectory = new DirectoryInfo(sDir);
                            FileInfo[] nFile = nDirectory.GetFiles("*.*", SearchOption.AllDirectories);
                            //  FileStream fs = null;

                            foreach (FileInfo nInfo in nFile)
                            {
                                if (nInfo.Exists)
                                {
                                    var creationTime = nInfo.CreationTime;

                                    //3 years log data
                                    DateTime histDate = DateTime.Parse("01/01/2017");

                                    if (nInfo.Name.Contains("LOG") && !nInfo.Name.Contains("-") && nInfo.Name.Length < 9 && creationTime > histDate)
                                    {
                                        string[] rPathInfo = nInfo.Name.Split('.');
                                        nWaferID = rPathInfo[0].ToString();
                                        var nLogFileName = tool + "." + nWaferID + "." + nInfo.LastWriteTime.ToString().Replace("/", "_").Replace(":", ".") + ".csv";

                                        //get DB data
                                        ArrayList dbFileInfo = mDBAccess.GetFileInformation(tool + "_" + nWaferID, nLogFileName);

                                        if (dbFileInfo != null)
                                        {
                                            if (dbFileInfo.Count != 0 && dbFileInfo != null)
                                            {
                                                foreach (FileInformation dbInfo in dbFileInfo)
                                                {
                                                    if (dbInfo.WaferID.Equals(tool + "_" + nWaferID) && dbInfo.FileName.Equals(nLogFileName))
                                                    {
                                                        //check for any updates for the file
                                                        if (nInfo.LastWriteTime > dbInfo.FileModTime.AddSeconds(2.0))
                                                        {
                                                            try
                                                            {
                                                                if (File.Exists(nToConvert + nLogFileName))
                                                                {
                                                                    if (GetUpdateFlag(nLogFileName, nInfo.LastWriteTime, (int)nInfo.Length))
                                                                    {
                                                                        try
                                                                        {
                                                                            System.GC.Collect();
                                                                            System.GC.WaitForPendingFinalizers();
                                                                            File.SetAttributes(nToConvert + nInfo.Name, FileAttributes.Normal);

                                                                            File.Delete(nToConvert + nLogFileName);
                                                                            File.Copy(nInfo.DirectoryName + "\\" + nInfo.Name, nToConvert + nLogFileName);
                                                                        }
                                                                        catch (Exception exp)
                                                                        {
                                                                            mDBAccess.InsertLogEvent("Error", exp.Message);
                                                                        }
                                                                        mDBAccess.UpdateFileInfo(tool + "_" + nWaferID, nInfo.Name, nInfo.LastWriteTime, (int)nInfo.Length);
                                                                    }
                                                                    else
                                                                    {
                                                                        try
                                                                        {
                                                                            File.Copy(nInfo.DirectoryName + "\\" + nInfo.Name, nToConvert + nLogFileName);
                                                                        }
                                                                        catch (Exception exp)
                                                                        {
                                                                            mDBAccess.InsertLogEvent("Error", exp.Message);
                                                                        }

                                                                        mDBAccess.InsertFileInfo(tool + "_" + nWaferID, nLogFileName, nInfo.LastWriteTime, (int)nInfo.Length);
                                                                    }

                                                                }
                                                            }
                                                            catch (Exception ex)
                                                            {
                                                                if (ex.Message.Contains("already exists"))
                                                                {
                                                                        mDBAccess.InsertLogEvent("Error", ex.Message);                                                                    
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            try
                                            {
                                                if (File.Exists(nToConvert + nLogFileName))
                                                {
                                                    try
                                                    {
                                                        System.GC.Collect();
                                                        System.GC.WaitForPendingFinalizers();

                                                        File.SetAttributes(nToConvert + nLogFileName, FileAttributes.Normal);

                                                        File.Delete(nToConvert + nLogFileName);
                                                        File.Copy(nInfo.FullName, nToConvert + nLogFileName);
                                                    }
                                                    catch (Exception exp) { mDBAccess.InsertLogEvent("Error", exp.Message); }
                                                }
                                                else
                                                {
                                                    try
                                                    {
                                                        if (File.Exists(nInfo.DirectoryName + "\\" + nInfo.Name))
                                                        {
                                                            File.Copy(nInfo.FullName, nToConvert + nLogFileName);
                                                        }
                                                    }
                                                    catch (Exception exp) { mDBAccess.InsertLogEvent("Error", "" + exp); }
                                                }
                                                if (GetUpdateFlag(nLogFileName, nInfo.LastWriteTime, (int)nInfo.Length))
                                                {
                                                    mDBAccess.UpdateFileInfo(tool + "_" + nWaferID, nLogFileName, nInfo.LastWriteTime, (int)nInfo.Length);
                                                }
                                                else
                                                {
                                                    mDBAccess.InsertFileInfo(tool + "_" + nWaferID, nLogFileName, nInfo.LastWriteTime, (int)nInfo.Length);
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                if (ex.Message.Contains("already exists"))
                                                {
                                                    try
                                                    {
                                                        if (dbFileInfo != null)
                                                        {
                                                            foreach (FileInformation dbInfo in dbFileInfo)
                                                            {
                                                                if (nInfo.LastWriteTime > dbInfo.FileModTime)
                                                                {
                                                                    System.GC.Collect();
                                                                    System.GC.WaitForPendingFinalizers();

                                                                    File.Delete(nToConvert + nLogFileName);
                                                                    File.Copy(nInfo.FullName, nToConvert + nLogFileName);
                                                                    //   File.Copy(nInfo.DirectoryName + "\\" + nInfo.Name, nToConvert + nInfo.Name);

                                                                    mDBAccess.UpdateFileInfo(tool + "_" + nWaferID, nLogFileName, nInfo.LastWriteTime, (int)nInfo.Length);
                                                                }
                                                            }
                                                        }
                                                    }
                                                    catch (Exception exp)
                                                    {
                                                        mDBAccess.InsertLogEvent("Error @ 511", exp.Message);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        } 
                        }
                        files.AddRange(DirSearch(sDir));
                        }
                }
                catch (System.Exception exp)
                {
                    mDBAccess.InsertLogEvent("Error @ 1003", "" + exp);
                }
            
            return files;
        }
        private void ReadLogFile()
        {
            DirectoryInfo di = new DirectoryInfo(localProcessing);
            FileInfo[] files = di.GetFiles("*.csv");

            foreach (FileInfo nInfo in files)
            {
                if (nInfo.Name.Contains("COMEGA"))
                {
                    createFile = true;
                    string currentLine;
                    using (StreamReader sr = new StreamReader(nInfo.FullName))
                    {
                        sr.ReadLine(); // skip

                        while ((currentLine = sr.ReadLine()) != null)
                        {
                            arrLogFileData.Add(currentLine.ToString().Split(','));
                            logFullName = nInfo.FullName;
                        }
                    }
                }

            }
        }

        #endregion

        private void StartConvertionTasks()
        {
            while (true)
            {
                CreateFile();
            }
        }

        private void btnReadSpec_Click(object sender, EventArgs e)
        {
            ReadSpecFile("");
        }

        private string GetSpecFile(string pMaskID, string pRevision)
        {
            ArrayList arrLimits = new ArrayList();
            arrLimits = mDBAccess.GetLimitsLocation(pMaskID);

            string limitsLine = "";
            var pFileName = "";

            //  var pFileName = @"\\cas-sfl-01\casfab\NPI\InPMZ_100G\InPMZ-SOA_100G\Test\Documents\Limits\PA013191_Production_Limits.csv";

            //read all files in the folder
            foreach (LimitsOBJ limitsLocation in arrLimits)
            {
                foreach (string file in Directory.EnumerateFiles(limitsLocation.FileLocation, "*.csv")) //limitsLocation.FileLocation
                {
                    using (StreamReader sr = new StreamReader(file))
                    {
                        while ((limitsLine = sr.ReadLine()) != null)
                        {
                            var splitValues = limitsLine.ToString().Split(',');

                            foreach (var line in splitValues)
                            {
                                if (line.ToString().Contains("rev.") || line.ToString().Contains("rev") || line.ToString().Contains("REV"))
                                {
                                    Console.WriteLine(line.Replace(".", "").Replace(" ", ""));
                                    pRevision = pRevision.Replace("_", "");

                                    Regex rgx = new Regex("[^a-zA-Z0-9 -]");
                                    var compareLine = rgx.Replace(line, "");
                                    compareLine = compareLine.Replace(" ", "").ToUpper();

                                    if (pRevision.Contains(compareLine))
                                    {
                                        pFileName = file;
                                        return pFileName;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return pFileName;
        }

        private void ReadSpecFile(string pFileName)
        {
            listSpecName = new List<string>();
            listSpecMin = new List<string>();
            listSpecMax = new List<string>();

            if (File.Exists(pFileName))
            {
                using (StreamReader sr = new StreamReader(pFileName))
                {
                    string currentLine;
                    // currentLine will be null when the StreamReader reaches the end of file
                    while ((currentLine = sr.ReadLine()) != null)
                    {
                        // Search, case insensitive, if the currentLine contains the searched keyword
                        if (currentLine.IndexOf("Dataname", StringComparison.CurrentCultureIgnoreCase) >= 0 || currentLine.IndexOf("Min", StringComparison.CurrentCultureIgnoreCase) >= 0)
                        {
                            var findIndex = currentLine.Split(',');
                            var indexDataName = 0;
                            var indexMin = 0;
                            var indexMax = 0;
                            var indexUnits = 0;
                            
                            for(int n = 0; n < findIndex.Length; n++)
                            {
                                if(findIndex[n].Equals("Dataname", StringComparison.CurrentCultureIgnoreCase))
                                {
                                    indexDataName = n;
                                }
                                if (findIndex[n].Equals("Min", StringComparison.CurrentCultureIgnoreCase))
                                {
                                    indexMin = n;
                                }
                                if (findIndex[n].Equals("Max", StringComparison.CurrentCultureIgnoreCase))
                                {
                                    indexMax = n;
                                }
                                if (findIndex[n].Equals("Units", StringComparison.CurrentCultureIgnoreCase))
                                {
                                    indexUnits = n;
                                }
                            }
                            while ((currentLine = sr.ReadLine()) != null && currentLine != "")
                            {
                                var line = currentLine.Split(',');

                                listSpecName.Add(line[indexDataName].ToString().ToUpper());
                                listSpecMin.Add(line[indexMin].ToString());
                                listSpecMax.Add(line[indexMax].ToString());
                                listSpecUnits.Add(line[indexUnits].ToString().ToUpper());
                            }
                        }
                    }
                }
            }
        }

        //This method will read the data file and will do some filtering
        private void ConvertToCSV(string fileName, string pAction, string pType)
        {
            string fPath = nToConvert + fileName + "." + pType;

            string outFile = localProcessing + fileName + "." + pType;
            convertedLogCSV = outFile;

            var lines = File.ReadAllLines(fPath).Select(line => line.Replace("\t", ","));

            lines = File.ReadAllLines(fPath).Where(l => !string.IsNullOrWhiteSpace(l));

            if (pAction.Equals("copy"))
            {
                using (StreamWriter w = File.AppendText(outFile))
                {
                    foreach (var line in lines)
                    {
                        w.WriteLine(line.Replace("N/A","-999"));
                    }
                    w.Flush();
                    w.Close();
                }

                System.Threading.Thread.Sleep(1);
            }
            System.GC.Collect();
            System.GC.WaitForPendingFinalizers();
        }

        //Read attachment file to retrieve measurement values and store everything to array
        private void ReadMZFile(string pFileName)
        {
            arrMZData = new ArrayList();
            listUnits = new List<string>();

            if (File.Exists(pFileName))
            {
                using (StreamReader sr = new StreamReader(pFileName))
                {
                    string currentLine;
                    // currentLine will be null when the StreamReader reaches the end of file
                    while ((currentLine = sr.ReadLine()) != null)
                    {
                        // Search, case insensitive, if the currentLine contains the searched keyword
                        if (currentLine.IndexOf("Parametric Data", StringComparison.CurrentCultureIgnoreCase) >= 0)
                        {
                            while ((currentLine = sr.ReadLine()) != null && currentLine != "")
                            {
                                arrMZData.Add(currentLine.ToString());

                                listUnits.Add(currentLine.ToString());
                            }
                        }
                    }
                }
            }
        }

        //Construct file attachments
        private void ConstructAttachments(string pSweepAttachment, string pWaferID, string pDate, string pWave, string pKit)
        {
            //we know that the file is MZSweep
            ReadMZFile(pSweepAttachment);

            if (File.Exists(pSweepAttachment))
            {
                if (!File.Exists(nTDSAttachment + pWaferID + "_MZSweep_" + pDate.Replace(":", ".") + "_" + pWave + "_" + pKit + ".csv"))
                {
                    File.Copy(pSweepAttachment, nTDSAttachment + pWaferID + "_MZSweep_" + pDate.Replace(":", ".") + "_" + pWave + "_" + pKit + ".csv");
                }
            }
        }

        //CIL attachments
        private void ConstructAttachmentsGeneral(string pSweepAttachment, string pWaferID, string pDate, string pBar, string pChip, string pKit)
        {
                if (!File.Exists(nTDSAttachment + pWaferID + "_SourceData_" + pDate.Replace(":", ".") + "_Bar_" + pBar + "_Chip_"+pChip+"_" + pKit + ".csv"))
                {
                    File.Copy(pSweepAttachment + ".csv", nTDSAttachment + pWaferID + "_SourceData_" + pDate.Replace(":", ".") + "_Bar_" + pBar + "_Chip_" + pChip + "_" + pKit + ".csv");
                    attNames.Add(nTDSAttachment + pWaferID + "_SourceData_" + pDate.Replace(":", ".") + "_Bar_" + pBar + "_" + pKit + ".csv");
                } else
            {
                File.SetAttributes(nTDSAttachment + pWaferID + "_SourceData_" + pDate.Replace(":", ".") + "_Bar_" + pBar + "_Chip_" + pChip + "_" + pKit + ".csv", FileAttributes.Normal);
                File.Delete(nTDSAttachment + pWaferID + "_SourceData_" + pDate.Replace(":", ".") + "_Bar_" + pBar + "_Chip_" + pChip + "_" + pKit + ".csv");
                    File.Copy(pSweepAttachment + ".csv", nTDSAttachment + pWaferID + "_SourceData_" + pDate.Replace(":", ".") + "_Bar_" + pBar + "_" + pKit + ".csv");
                }

                if (!File.Exists(nTDSAttachment + pWaferID + "_SourceInterface_" + pDate.Replace(":", ".") + "_Bar_" + pBar + "_Chip_" + pChip + "_" + pKit + ".csv"))
                {
                    File.Copy(pSweepAttachment + ".Interface.csv", nTDSAttachment + pWaferID + "_SourceInterface_" + pDate.Replace(":", ".") + "_Bar_" + pBar + "_Chip_" + pChip + "_" + pKit + ".Interface.csv");
                    attNames.Add(nTDSAttachment + pWaferID + "_SourceInterface_" + pDate.Replace(":", ".") + "_Bar_" + pBar + "_Chip_" + pChip + "_" + pKit + ".Interface.csv");
                }
                else
                {
                File.SetAttributes(nTDSAttachment + pWaferID + "_SourceInterface_" + pDate.Replace(":", ".") + "_Bar_" + pBar + "_Chip_" + pChip + "_" + pKit + ".Interface.csv", FileAttributes.Normal);
                File.Delete(nTDSAttachment + pWaferID + "_SourceInterface_" + pDate.Replace(":", ".") + "_Bar_" + pBar + "_Chip_" + pChip + "_" + pKit + ".Interface.csv");
                    File.Copy(pSweepAttachment + ".Interface.csv", nTDSAttachment + pWaferID + "_SourceInterface_" + pDate.Replace(":", ".") + "_Bar_" + pBar + "_Chip_" + pChip + "_" + pKit + "Interface.csv");
                }

                if (!File.Exists(nTDSAttachment + pWaferID + "_ZipData_" + pDate.Replace(":", ".") + "_Bar_" + pBar + "_Chip_" + pChip + "_" + pKit + ".zip"))
                {
                    File.Copy(pSweepAttachment + ".zip", nTDSAttachment + pWaferID + "_ZipData_" + pDate.Replace(":", ".") + "_Bar_" + pBar + "_Chip_" + pChip + "_" + pKit + ".zip");
                    attNames.Add(nTDSAttachment + pWaferID + "_ZipData_" + pDate.Replace(":", ".") + "_Bar_" + pBar + "_Chip_" + pChip + "_" + pKit + ".zip");
                }
                else
                {

                File.SetAttributes(nTDSAttachment + pWaferID + "_ZipData_" + pDate.Replace(":", ".") + "_Bar_" + pBar + "_Chip_" + pChip + "_" + pKit + ".zip", FileAttributes.Normal);
                File.Delete(nTDSAttachment + pWaferID + "_ZipData_" + pDate.Replace(":", ".") + "_Bar_" + pBar + "_Chip_" + pChip + "_" + pKit + ".zip");
                    File.Copy(pSweepAttachment + ".zip", nTDSAttachment + pWaferID + "_ZipData_" + pDate.Replace(":", ".") + "_Bar_" + pBar + "_Chip_" + pChip + "_" + pKit + ".zip");
                }
        }

        //get image attachment for CIL
        private void GetImageAttachment(string pFilePath)
        {
            var fileName = pFilePath.Split('_');
            var barNo = fileName[2].Replace("B", "").Replace("C", "0");

            var completePath = fileName[0] + "_" + fileName[1] + "_" + barNo;

            if (File.Exists(completePath))
            {
                arrAttImage.Add(completePath);
            }
        }

        //Construct arrays from data file
        private void ConstructCSV(string pFileName, string pHeader, string pData, bool pHistorical)
        {
            List<string> csvLines = new List<string>();
            List<string> nItems = new List<string>();
            List<string> newItems = new List<string>();

            Regex lineSplitter = new Regex(@"[\s*\*]*\|[\s*\*]*");
            var newResult = "";

            csvLines.Add(pHeader);

            //replace all possible excel error values to -999
            csvLines.Add(pData.Replace("#VALUE!", "-999").Replace("#N/A", "-999").Replace("#DIV/0!", "-999").Replace("#NUM!", "-999").Replace("#NAME?", "-999").Replace("#NULL!", "-999").Replace("#REF!", "-999"));

            arrTableHeader = csvLines[0].Split(',');
            arrTableInfo = csvLines[1].Split(',');

            //check if this is historical data with the missing parameters
            string[] x = arrTableInfo;

            List<string> y = x.ToList<string>();
            x = y.ToArray();

            if (headerLength > dataLength)
            {
                var difference = headerLength - dataLength;

                foreach (string item in y)
                {
                    nItems.Add(item.ToUpper());
                }

                //add additional columns for the new data
                for (int i = 0; i < difference; i++)
                {
                    nItems.Add("");
                }

                arrTableInfo = nItems.ToArray();
            }

            if (arrTableInfo.Length > arrTableHeader.Length)
            {
                var itemCount = arrTableInfo.Length - arrTableHeader.Length;
                arrTableInfo = arrTableInfo.Take(arrTableInfo.Count() - itemCount).ToArray();
            }

            for (int n = 0; n < arrTableInfo.Length; n++)
            {
                //convert scientific expression to double
                if (arrTableInfo[n].Contains("E-") || arrTableInfo[n].Contains("E+"))
                {
                    double result = Double.Parse(arrTableInfo[n], System.Globalization.NumberStyles.Float);

                    arrTableInfo.SetValue(result.ToString(), n);
                }
            }

            //if data is for historical files - do math and construct
            if (pHistorical)
            {
                for (int m = 0; m < arrTableHeader.Length; m++)
                {

                    //Get X side data for the calculation
                    if (arrTableHeader[m].Equals("XOUTER_INS_LOSS_DB"))
                    {
                        XOUTER_INS_LOSS_DB = Double.Parse(arrTableInfo[m].ToString());
                    }
                    if (arrTableHeader[m].Equals("XOUTER_DET_PMAX_MZ1_DBM"))
                    {
                        XOUTER_DET_PMAX_MZ1_DBM = Double.Parse(arrTableInfo[m].ToString());
                    }
                    if (arrTableHeader[m].Equals("XOUTER_DET_PMAX_MZ2_DBM"))
                    {
                        XOUTER_DET_PMAX_MZ2_DBM = Double.Parse(arrTableInfo[m].ToString());
                    }
                    if (arrTableHeader[m].Equals("OP_SOA_X_ASE_DBM"))
                    {
                        OP_SOA_X_ASE_DBM = Double.Parse(arrTableInfo[m].ToString());
                    }
                    if (arrTableHeader[m].Equals("XOUTER_ER_DB"))
                    {
                        XOUTER_ER_DB = Double.Parse(arrTableInfo[m].ToString());
                    }

                    //Get Y side data for the calculation
                    if (arrTableHeader[m].Equals("YOUTER_INS_LOSS_DB"))
                    {
                        YOUTER_INS_LOSS_DB = Double.Parse(arrTableInfo[m].ToString());
                    }
                    if (arrTableHeader[m].Equals("YOUTER_DET_PMAX_MZ3_DBM"))
                    {
                        YOUTER_DET_PMAX_MZ3_DBM = Double.Parse(arrTableInfo[m].ToString());
                    }
                    if (arrTableHeader[m].Equals("YOUTER_DET_PMAX_MZ4_DBM"))
                    {
                        YOUTER_DET_PMAX_MZ4_DBM = Double.Parse(arrTableInfo[m].ToString());
                    }
                    if (arrTableHeader[m].Equals("OP_SOA_Y_ASE_DBM"))
                    {
                        OP_SOA_Y_ASE_DBM = Double.Parse(arrTableInfo[m].ToString());
                    }
                    if (arrTableHeader[m].Equals("YOUTER_ER_DB"))
                    {
                        YOUTER_ER_DB = Double.Parse(arrTableInfo[m].ToString());
                    }

                    if (arrTableHeader[m].Equals("ER_X_ASE_DB"))
                    {
                        arrTableInfo[m] = ER_X_ASE_DB.ToString();
                    }
                    if (arrTableHeader[m].Equals("X_ER_CORRECTED_DB"))
                    {
                        arrTableInfo[m] = X_ER_CORRECTED_DB.ToString();
                    }

                    //add missing data for the Y side
                    if (arrTableHeader[m].Equals("LOSS_IMB_DB"))
                    {
                        arrTableInfo[m] = LOSS_IMB_DB.ToString();
                    }
                    if (arrTableHeader[m].Equals("ER_Y_ASE_DB"))
                    {
                        arrTableInfo[m] = ER_Y_ASE_DB.ToString();
                    }
                    if (arrTableHeader[m].Equals("Y_ER_CORRECTED_DB"))
                    {
                        arrTableInfo[m] = Y_ER_CORRECTED_DB.ToString();
                    }
                }
                CalculateCorrectedX(LOSS_IMB_DB, XOUTER_DET_PMAX_MZ1_DBM, XOUTER_DET_PMAX_MZ2_DBM, OP_SOA_X_ASE_DBM, XOUTER_ER_DB);
                CaculateCorrectedY(YOUTER_INS_LOSS_DB, YOUTER_DET_PMAX_MZ3_DBM, YOUTER_DET_PMAX_MZ4_DBM, OP_SOA_Y_ASE_DBM, YOUTER_ER_DB);
            }

            if (pHistorical)
            {
                csvLines.RemoveAt(1);

                foreach (var newData in arrTableInfo)
                {
                    newItems.Add(newData.ToString().ToUpper());
                }

                newResult = string.Join(",", newItems);
                csvLines.Add(newResult.ToString().ToUpper());
            }

            if (!File.Exists(nTDSTable + pFileName))
            {
                File.WriteAllLines(nTDSTable + pFileName, csvLines);
            }
            else
            {
                File.SetAttributes(nTDSTable + pFileName, FileAttributes.Normal);
                File.Delete(nTDSTable + pFileName);

                File.WriteAllLines(nTDSTable + pFileName, csvLines);
            }
        }
        //calculate Y side parameters for OR416 historical data
        private void CaculateCorrectedY(double insLoss, double mz1, double mz2, double opSoa, double outerEr)
        {
            //calculated parameters:~
            LOSS_IMB_DB = 0.0;
            ER_Y_ASE_DB = 0.0;
            Y_ER_CORRECTED_DB = 0.0;

            double result = 0.0;

            //Y Side
            YOUTER_INS_LOSS_DB = insLoss;
            YOUTER_DET_PMAX_MZ3_DBM = mz1;
            YOUTER_DET_PMAX_MZ4_DBM = mz2;
            OP_SOA_Y_ASE_DBM = opSoa;
            YOUTER_ER_DB = outerEr;

            LOSS_IMB_DB = XOUTER_INS_LOSS_DB - YOUTER_INS_LOSS_DB; //calculation 1

            minValue = Math.Min(YOUTER_DET_PMAX_MZ3_DBM, YOUTER_DET_PMAX_MZ4_DBM);

            ER_Y_ASE_DB = minValue - (OP_SOA_Y_ASE_DBM); // calculation 2
                                                         //   ER_X_ASE_DB = ER_X_ASE_DB -(OP_SOA_X_ASE_DBM);

            if (ER_Y_ASE_DB < 0.01)
            {
                Y_ER_CORRECTED_DB = 99;
                result = Y_ER_CORRECTED_DB;
            }
            else
            {
                var X_ER = YOUTER_ER_DB + ER_Y_ASE_DB;

                var cal1 = ER_Y_ASE_DB / 10;
                var cal2 = YOUTER_ER_DB / 10;

                var calcPower1 = Math.Pow(10.0, cal1);
                var calcPower2 = Math.Pow(10.0, cal2);

                double calcPowerResult = calcPower1 - calcPower2;

                result = X_ER - 10 * Math.Log10(calcPowerResult);
                Y_ER_CORRECTED_DB = result;
            }
        }

        //calculate X side parameters for OR416 historical data
        private void CalculateCorrectedX(double insLoss, double mz1, double mz2, double opSoa, double outerEr)
        {
            LOSS_IMB_DB = 0.0;
            ER_X_ASE_DB = 0.0;
            X_ER_CORRECTED_DB = 0.0;


            XOUTER_INS_LOSS_DB = insLoss;
            XOUTER_DET_PMAX_MZ1_DBM = mz1;
            XOUTER_DET_PMAX_MZ2_DBM = mz2;
            OP_SOA_X_ASE_DBM = opSoa;
            XOUTER_ER_DB = outerEr;

            double result = 0.0;

            LOSS_IMB_DB = XOUTER_INS_LOSS_DB - YOUTER_INS_LOSS_DB; //calculation 1

            minValue = Math.Min(XOUTER_DET_PMAX_MZ1_DBM, XOUTER_DET_PMAX_MZ2_DBM);

            ER_X_ASE_DB = minValue - (OP_SOA_X_ASE_DBM); // calculation 2
                                                         //   ER_X_ASE_DB = ER_X_ASE_DB -(OP_SOA_X_ASE_DBM);

            if (ER_X_ASE_DB < 0.01)
            {
                X_ER_CORRECTED_DB = 99;
                result = X_ER_CORRECTED_DB;
            }
            else
            {
                var X_ER = XOUTER_ER_DB + ER_X_ASE_DB;

                var cal1 = ER_X_ASE_DB / 10;
                var cal2 = XOUTER_ER_DB / 10;

                var calcPower1 = Math.Pow(10.0, cal1);
                var calcPower2 = Math.Pow(10.0, cal2);


                double calcPowerResult = calcPower1 - calcPower2;

                result = X_ER - 10 * Math.Log10(calcPowerResult);
                X_ER_CORRECTED_DB = result;
            }
        }

        private void ConstructBarXML_ILMZ()
        {
            bool historicalData = false;
            string fileName = "";
            var startTime = "";
            var runDateTime = "";
            arrTestData = new ArrayList();
            //   bool createFile = true;

            //    sFileType = "";
            createFile = true;
            var cilWafer = inWaferID.Split('_');
            var inSerial = cilWafer[0];
            cilWafer[0] = cilWafer[0].Remove(cilWafer[0].ToString().Length - 1);

            try
            {
                //get promis info for the wafer
                PromisInfo(cilWafer[0] + ".1");

                sFileType = "";

                foreach (string valueCSV in arrCSVInfo)
                {
                    string[] value = valueCSV.Split('.');

                    if (value[0].StartsWith("TEST_PLAN"))
                    {
                        lineHeaders = valueCSV.ToUpper();
                    }
                    else
                    {
                        arrTestData.Add(valueCSV.ToUpper());
                    }
                }
                
                //--------------------- create new XML file --------------------------//
                if (createFile)
                {
                    for (var i = arrTestData.Count; arrTestData.Count > 0; i--)
                    {
                        DateTime outDate;
                        string[] value = arrTestData[0].ToString().ToUpper().Split(',');

                        //convert excel serial to date
                        if (value[11].Contains('.'))
                        {
                            outDate = Convert.ToDateTime(DateTime.FromOADate(Convert.ToDouble(value[11])));
                        }
                        else if (value[11].Contains('/') || value[11].Contains(':'))
                        {
                            outDate = Convert.ToDateTime(value[11]);
                        }
                        else
                        {
                            outDate = Convert.ToDateTime(DateTime.FromOADate(Convert.ToDouble(value[11])));
                        }
                        //   DateTime outDate = Convert.ToDateTime(DateTime.FromOADate(Convert.ToDouble(value[11])));
                        startTime = outDate.ToString("yyyy-MM-ddTHH:mm:ss").Replace(":", ".");
                        string headerTime = outDate.ToString("yyyy-MM-ddTHH:mm:ss.ss");

                        if (inPartNumber != null)
                        {
                            partID = inPartNumber.Split('.');
                        }
                        else
                        {
                            partID[0] = inPartNumber.Replace("-", "_");
                        }
                        if (value[1].Contains("@"))
                        {
                            int index = value[1].LastIndexOf("@");
                            if (index > 0)
                                value[1] = value[1].Substring(0, index); // or index + 1 to keep slash
                        }
                        //crate table data file+
                        ConstructCSV(value[1].Replace("-", "_") + "_SUMMARY_" + startTime + "_" + value[14] + "_" + value[13].Replace("-", "_") + ".csv", lineHeaders, arrTestData[0].ToString().ToUpper(), historicalData);

                        if (File.Exists(value[16].ToString() + ".csv"))
                        {
                            attExists = true;

                            //get required attachments
                            ConstructAttachmentsGeneral(value[16], inSerial, headerTime.ToString(), value[17], value[18], value[13]);
                        }
                        else
                        {
                            attExists = false;
                        }

                        GetImageAttachment(value[1]);

                        string xsi = "http://www.w3.org/2001/XMLSchema-instance";
                        string xsd = "http://www.w3.org/2001/XMLSchema";

                        XmlDocument doc = new XmlDocument();

                        //data node

                        XmlNode docNode = doc.CreateXmlDeclaration("1.0", "utf-8", null);
                        doc.AppendChild(docNode);

                        XmlNode resultsNode = doc.CreateElement("Results");
                        XmlAttribute xsiAttribute = doc.CreateAttribute("xmlns:xsi");
                        xsiAttribute.Value = xsi;
                        XmlAttribute xsdAttribute = doc.CreateAttribute("xsi:xsd");
                        xsdAttribute.Value = xsd;

                        resultsNode.Attributes.Append(xsiAttribute);
                        resultsNode.Attributes.Append(xsdAttribute);
                        doc.AppendChild(resultsNode);

                        /*----------------------------------*/
                        XmlNode resultNode = doc.CreateElement("Result");
                        XmlAttribute rNode = doc.CreateAttribute("Result");
                        rNode.Value = "Done";

                        XmlAttribute timeNode = doc.CreateAttribute("startDateTime");
                        timeNode.Value = "" + headerTime;

                        resultNode.Attributes.Append(rNode);
                        resultNode.Attributes.Append(timeNode);
                        resultsNode.AppendChild(resultNode);

                        /*------------------- Header ---------------------*/
                        XmlNode headerNode = doc.CreateElement("Header");
                        XmlAttribute serialNode = doc.CreateAttribute("Serialnumber");
                        serialNode.Value = "" + value[1];

                        XmlAttribute waferNode = doc.CreateAttribute("Waferid");
                        waferNode.Value = "" + inSerial;

                        XmlAttribute operationNode = doc.CreateAttribute("Operation");
                        operationNode.Value = "" + value[3];
                        XmlAttribute stationNode = doc.CreateAttribute("Teststation");
                        stationNode.Value = "" + value[13];
                        XmlAttribute maskNode = doc.CreateAttribute("Maskid");
                        maskNode.Value = "" + value[2];

                        XmlAttribute barNode = doc.CreateAttribute("Barid");
                        barNode.Value = "" + value[17];
                        XmlAttribute chipNode = doc.CreateAttribute("Chipid");
                        chipNode.Value = "" + value[18];

                        XmlAttribute operatorNode = doc.CreateAttribute("Operator");
                        operatorNode.Value = "" + value[12].Replace("-", "_").Replace(".", "_");
                        XmlAttribute partNode = doc.CreateAttribute("Partnumber");
                        partNode.Value = "" + partID[0];
                        XmlAttribute startNode = doc.CreateAttribute("Starttime");
                        startNode.Value = "" + headerTime;
                        XmlAttribute siteNode = doc.CreateAttribute("Site");
                        siteNode.Value = "250";
                        XmlAttribute batchNode = doc.CreateAttribute("BatchNumber");
                        batchNode.Value = "" + cilWafer[0];
                        XmlAttribute lotNode = doc.CreateAttribute("LotNumber");
                        lotNode.Value = "" + cilWafer[0];

                        XmlAttribute xLocNode = doc.CreateAttribute("Devicexlocation");
                        xLocNode.Value = "" + value[7];
                        XmlAttribute yLocNode = doc.CreateAttribute("Deviceylocation");
                        yLocNode.Value = "" + value[8];


                        XmlAttribute deviceNode = doc.CreateAttribute("DeviceType");
                        deviceNode.Value = "" + inDeviceType;

                        XmlAttribute purposeNode = doc.CreateAttribute("Purpose");
                        purposeNode.Value = "" + inLotType;

                        headerNode.Attributes.Append(serialNode);
                        headerNode.Attributes.Append(waferNode);
                        headerNode.Attributes.Append(operationNode);
                        headerNode.Attributes.Append(stationNode);
                        headerNode.Attributes.Append(maskNode);
                        headerNode.Attributes.Append(barNode);
                        headerNode.Attributes.Append(chipNode);
                        headerNode.Attributes.Append(operatorNode);
                        headerNode.Attributes.Append(partNode);
                        headerNode.Attributes.Append(startNode);
                        headerNode.Attributes.Append(siteNode);
                        headerNode.Attributes.Append(batchNode);
                        headerNode.Attributes.Append(lotNode);
                        headerNode.Attributes.Append(xLocNode);
                        headerNode.Attributes.Append(yLocNode);
                        headerNode.Attributes.Append(barNode);
                        headerNode.Attributes.Append(chipNode);
                        headerNode.Attributes.Append(deviceNode);
                        headerNode.Attributes.Append(purposeNode);

                        /*------------------ NOT IN USE ----------------------*/
                        XmlNode miscNode = doc.CreateElement("HeaderMisc");
                        XmlAttribute dNode = doc.CreateAttribute("Serialnumber");
                        dNode.Value = "";

                        XmlAttribute opNode = doc.CreateAttribute("Operation");
                        opNode.Value = "operation";

                        /*------------------- Genealogy ----------------------*/
                        XmlNode geoNode = doc.CreateElement("UnitGenealogy");


                        if (arrComponents.Count > 0)
                        {
                            for (int c = 0; c < arrComponents.Count; c++)
                            {
                                XmlNode itemNode = doc.CreateElement("Item");

                                XmlAttribute ptypeNode = doc.CreateAttribute("PartType");
                                ptypeNode.Value = "Component";
                                XmlAttribute partNoNode = doc.CreateAttribute("Partnumber");
                                partNoNode.Value = "" + arrComponents[c];
                                XmlAttribute iSerialNode = doc.CreateAttribute("SerialNumber");
                                iSerialNode.Value = "" + inSerial;

                                itemNode.Attributes.Append(ptypeNode);
                                itemNode.Attributes.Append(partNoNode);
                                itemNode.Attributes.Append(iSerialNode);

                                geoNode.AppendChild(itemNode);
                            }
                        }
                        /*-----------------------------------------*/
                        XmlNode testNode = doc.CreateElement("TestStep");

                        XmlAttribute tStatusNode = doc.CreateAttribute("Status");
                        tStatusNode.Value = "Done";

                        XmlAttribute tNameNode = doc.CreateAttribute("Name");
                        tNameNode.Value = "SUMMARY";

                        XmlAttribute tStartNode = doc.CreateAttribute("startDateTime");
                        tStartNode.Value = "" + headerTime;

                        XmlAttribute tSampleNode = doc.CreateAttribute("SampleRate");
                        tSampleNode.Value = "Temperature " + cilWafer[1];

                        //     testNode.Attributes.Append(tTypeNode);
                        testNode.Attributes.Append(tNameNode);
                        testNode.Attributes.Append(tStartNode);
                        testNode.Attributes.Append(tSampleNode);
                        testNode.Attributes.Append(tStatusNode);
                        /*-----------------------------------------*/

                        XmlNode dataNode = doc.CreateElement("Data");

                        XmlAttribute typeNode = doc.CreateAttribute("DataType");
                        typeNode.Value = "Table";

                        XmlAttribute dNameNode = doc.CreateAttribute("Name");
                        dNameNode.Value = "CAS_OWT_" + partID[0].Replace("-", "_");

                        if (value[1].Contains("@"))
                        {
                            int index = value[1].LastIndexOf("@");
                            if (index > 0)
                                value[1] = value[1].Substring(0, index); // or index + 1 to keep slash
                        }

                        XmlAttribute valNode = doc.CreateAttribute("Value");
                        valNode.Value = "" + nTDSTable + value[1].Replace("-", "_") + "_SUMMARY_" + startTime.Replace(":", ".") + "_" + value[14] + "_" + value[13] + ".csv";

                        dataNode.Attributes.Append(typeNode);
                        dataNode.Attributes.Append(dNameNode);
                        //   dataNode.Attributes.Append(desNode);

                        dataNode.Attributes.Append(valNode);

                        /*------------------Test step 2 MZ Data ----------------------*/
                        XmlNode test2Node = doc.CreateElement("TestStep"); //sweep file att
                        XmlNode mzTestNode = doc.CreateElement("TestStep");
                        //               XmlNode data2Node = doc.CreateElement("Data");

                        XmlAttribute t2StatusNode = doc.CreateAttribute("Status");
                        t2StatusNode.Value = "Done";

                        XmlAttribute t2NameNode = doc.CreateAttribute("Name");
                        t2NameNode.Value = "SOURCE_DATA";

                        XmlAttribute t2StartNode = doc.CreateAttribute("startDateTime");
                        t2StartNode.Value = "" + headerTime;


                        //     testNode.Attributes.Append(tTypeNode);
                        test2Node.Attributes.Append(t2NameNode);
                        test2Node.Attributes.Append(t2StartNode);

                        /*------------------ MZ Test step 3 ----------------------*/

                        XmlAttribute mzStatusNode = doc.CreateAttribute("Status");
                        mzStatusNode.Value = "Done";

                        XmlAttribute mzNameNode = doc.CreateAttribute("Name");
                        mzNameNode.Value = "PARAMETRIC";

                        XmlAttribute mzStartNode = doc.CreateAttribute("startDateTime");
                        mzStartNode.Value = "" + headerTime;


                        //     testNode.Attributes.Append(tTypeNode);
                        //    mzTestNode.Attributes.Append(mzNameNode);
                        //     mzTestNode.Attributes.Append(mzStatusNode);
                        //     mzTestNode.Attributes.Append(mzStartNode);

                        /*--------------------------------------------------------*/
                        //END OF PARENT TEST NODE

                        // ------------------ Attachment data --------------------

                        //   dataNode.Attributes.Append(desNode);

                        if (attNames.Count > 0)
                        {
                            for (int n = 0; n < attNames.Count; n++)
                            {
                                XmlAttribute type2Node = doc.CreateAttribute("DataType");
                                type2Node.Value = "Attachment";

                                XmlAttribute d2NameNode = doc.CreateAttribute("Name");
                                d2NameNode.Value = "SOURCE_DATA";

                                XmlNode data2Node = doc.CreateElement("Data");

                                data2Node.Attributes.Append(type2Node);
                                data2Node.Attributes.Append(d2NameNode);

                                XmlAttribute val2Node = doc.CreateAttribute("Value");
                                val2Node.Value = "" + attNames[n];

                                data2Node.Attributes.Append(val2Node);

                                test2Node.AppendChild(data2Node);
                                //    test2Node.AppendChild(data2Node);
                            }

                        }
                        /*-------------------------------------------------*/

                        resultsNode.AppendChild(resultNode);
                        resultNode.AppendChild(headerNode);
                        headerNode.AppendChild(miscNode);
                        resultNode.AppendChild(geoNode);
                        //                    geoNode.AppendChild(itemNode);
                        resultNode.AppendChild(testNode);
                        resultNode.AppendChild(test2Node);

                        testNode.AppendChild(dataNode);

                        /*
                        if (attExists)
                        {
                            resultNode.AppendChild(test2Node);
                            resultNode.AppendChild(data2Node);
                            resultNode.AppendChild(mzTestNode);

                            testNode.AppendChild(dataNode);
                            test2Node.AppendChild(data2Node);
                            //write all parametric data
                            if (arrMZData.Count > 0)
                            {
                                for (int n = 0; n < attNames.Count; n++)
                                {
                                    XmlNode mzDataNode = doc.CreateElement("Data");
                                    string[] mzValue = arrMZData[n].ToString().Split(',');
                                    var dataTypes = "Numeric";
                                    XmlAttribute mzTypeNode = doc.CreateAttribute("DataType");

                                    //check for string data type
                                    if (mzValue[2].ToString().Contains("E+"))
                                    {
                                        dataTypes = "String";
                                    }
                                    else
                                    {
                                        dataTypes = "Numeric";
                                    }
                                    mzTypeNode.Value = "" + dataTypes;

                                    XmlAttribute mzDataNameNode = doc.CreateAttribute("Name");
                                    mzDataNameNode.Value = "" + mzValue[0];

                                    XmlAttribute mzUnitsNode = doc.CreateAttribute("Units");
                                    mzUnitsNode.Value = "" + mzValue[1];

                                    XmlAttribute mzValNode = doc.CreateAttribute("Value");
                                    mzValNode.Value = "" + mzValue[2].ToString().Replace("E+", "E");

                                    mzDataNode.Attributes.Append(mzTypeNode);
                                    mzDataNode.Attributes.Append(mzDataNameNode);

                                    mzDataNode.Attributes.Append(mzUnitsNode);
                                    mzDataNode.Attributes.Append(mzValNode);

                                    if (mzValue.Count() > 5)
                                    {
                                        XmlAttribute mzMinNode = doc.CreateAttribute("SpecMin");
                                        mzMinNode.Value = "" + mzValue[3];

                                        XmlAttribute mzMaxNode = doc.CreateAttribute("SpecMax");
                                        mzMaxNode.Value = "" + mzValue[4];

                                        mzDataNode.Attributes.Append(mzMinNode);
                                        mzDataNode.Attributes.Append(mzMaxNode);
                                    }

                                    mzTestNode.AppendChild(mzDataNode);
                                }
                            }
                        }
                        */

                        fileName = "Site=250,ProductFamily=CAS BAR DATA,Operation=" + value[3] + ",Partnumber=" + partID[0] + ",SerialNumber=" + value[1] + ",Testdate=" + startTime + ",TestStation=" + value[13].Replace("-", "_") + ",Purpose=" + inLotType + ".xml";

                        string fullName = Path.Combine(nTDSFolder, fileName);

                        System.Threading.Thread.Sleep(1); //just to cool down the thread

                        doc.Save(fullName);

                        if (arrTestData.Count > 0)
                        {
                            arrTestData.RemoveAt(0);
                        }
                    }

                    if (arrTestData.Count <= 0)
                    {
                        createFile = false;

                        if (File.Exists(oldFile))
                        {
                            try
                            {
                                System.GC.Collect();
                                System.GC.WaitForPendingFinalizers();

                                File.SetAttributes(oldFile, FileAttributes.Normal);
                                File.Delete(oldFile);

                                sFileType = "";
                            }
                            catch (Exception ex)
                            {
                                //TODO: record error to the db
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: " + ex);
            }
        }
        //Construct XML file from the data arrays
        private void ConstructXML()
        {
            bool historicalData = false;
            string fileName = "";
            var startTime = "";
            arrTestData = new ArrayList();

            createFile = true;
            var limitPassFail = "Done";

            try
            {
                //get promis info for the wafer
                PromisInfo(inWaferID + ".1");

                sFileType = "";

                foreach (string valueCSV in arrCSVInfo)
                {
                    string[] value = valueCSV.ToString().ToUpper().Split('.');

                    if (value[0].StartsWith("TEST_PLAN") || value[0].StartsWith("BIN NUMBER"))
                    {
                        string[] x = value[0].Split(',');
                        List<string> nItems = new List<string>();

                        List<string> y = x.ToList<string>();
                        y.RemoveAll(p => string.IsNullOrEmpty(p));

                        x = y.ToArray();

                        //remove space between strings for the table columns
                        foreach (string item in y)
                        {
                            nItems.Add(item.Replace(" ", "_").ToUpper());
                        }

                        //add missing columns and additional data column to the array *this only relevant for OR416C historical data
                        if (inMask.Contains("OR416"))
                        {
                            foreach (string item in y)
                            {
                                if (!nItems.Contains("LOSS_IMB_DB"))
                                {
                                    nItems.Add("LOSS_IMB_DB");
                                    historicalData = true;
                                }
                                if (!nItems.Contains("ER_X_ASE_DB"))
                                {
                                    nItems.Add("ER_X_ASE_DB");
                                }
                                if (!nItems.Contains("X_ER_CORRECTED_DB"))
                                {
                                    nItems.Add("X_ER_CORRECTED_DB");
                                    historicalData = true;
                                }
                                if (!nItems.Contains("ER_Y_ASE_DB"))
                                {
                                    nItems.Add("ER_Y_ASE_DB");
                                }
                                if (!nItems.Contains("Y_ER_CORRECTED_DB"))
                                {
                                    nItems.Add("Y_ER_CORRECTED_DB");
                                    historicalData = true;
                                }
                            }

                            var result = string.Join(",", nItems);

                            headerValues = result.Split(',');
                            headerLength = headerValues.Length;

                            lineHeaders = result;
                        }
                    }
                    else
                    {
                        dataValues = valueCSV.Split(',');
                        dataLength = dataValues.Length;

                        arrTestData.Add(valueCSV.ToUpper());
                    }
                }

                //get revision and BIN number
                for (int x = 0; x < headerValues.Length; x++)
                {
                    if (headerValues[x].Equals("BIN", StringComparison.InvariantCultureIgnoreCase) || headerValues[x].Equals("BIN_NUM", StringComparison.InvariantCultureIgnoreCase))
                    {
                        passFailIndex = x;
                    }
                }

                //check if file has Limits revision in it
                var limitSearch = arrTestData[0].ToString().Trim().Split(',');
                foreach (var limitLine in limitSearch)
                {
                    if (limitLine.ToString().Contains("rev.") || limitLine.ToString().Contains("rev") && !Char.IsLetter(limitLine.FirstOrDefault()) || limitLine.ToString().Contains("REV") && !limitLine.ToString().Contains("REV:"))
                    {
                        limitsRevision = limitLine.ToString();
                    }
                }

                //get Limits
                if (limitsRevision != null)
                {
                    if(limitsRevision != null || limitsRevision != "")
                    {
                        var specFilePath = GetSpecFile(inMask, limitsRevision);

                        if (File.Exists(specFilePath))
                        {
                            ReadSpecFile(specFilePath);
                        }
                    }
                }

                //--------------------- Structure required data --------------------------//
                if (createFile)
                {
                    for (var i = arrTestData.Count; arrTestData.Count > 0; i--)
                    {
                        DateTime runDate;
                        var conDate = "";
                        var resultOutcome = "Done"; //default

                        DateTime outDate = new DateTime();
                        string[] value = arrTestData[0].ToString().Trim().Split(',');

                        //check BIN for pass/fail
                        if (int.Parse(value[passFailIndex]) < 15)
                        {
                            resultOutcome = "Passed";
                        }
                        else
                        {
                            resultOutcome = "Failed";
                        }

                        //convert excel serial to date for the header data
                        if (value[10].Contains('.'))
                        {
                            outDate = Convert.ToDateTime(DateTime.FromOADate(Convert.ToDouble(value[10].ToString())));
                        }
                        else if (value[10].Contains("AM") || value[10].Contains("PM"))
                        {
                            outDate = Convert.ToDateTime(value[10].ToString());
                            conDate = outDate.ToString("yyyy-MM-ddTHH:mm:ss");
                            outDate = Convert.ToDateTime(conDate);
                        }
                        else if (value[10].Contains("/") && value[10].Contains(":") && value[10].Length < 18)
                        {
                            outDate = Convert.ToDateTime(value[10].ToString());
                            conDate = outDate.ToString("yyyy-MM-ddTHH:mm:ss");
                            outDate = Convert.ToDateTime(conDate);
                        }
                        else if (value[10].ToString().Contains('/') && value[10].Contains(":") && value[10].Length > 17)
                        {
                            var dateString = value[10].ToString();
                            var format = "dd/MM/yyyy HH:mm:ss";
                            var dateTime = DateTime.ParseExact(dateString, format, CultureInfo.InvariantCulture);
                            outDate = dateTime;
                        }
                        else
                        {
                            outDate = Convert.ToDateTime(DateTime.FromOADate(Convert.ToDouble(value[10].ToString())));
                        }

                        startTime = outDate.ToString("yyyy-MM-ddTHH:mm:ss").Replace(":", ".");
                        string headerTime = outDate.ToString("yyyy-MM-ddTHH:mm:ss.ss");

                        if (inPartNumber != null)
                        {
                            partID = inPartNumber.Split('.');
                        }
                        else
                        {
                            partID[0] = inPartNumber.Replace("-", "_");
                        }

                        if (value[1].Contains("@"))
                        {
                            int index = value[1].LastIndexOf("@");
                            if (index > 0)
                                value[1] = value[1].Substring(0, index); // or index + 1 to keep @
                        }

                        //crate table data file
                        ConstructCSV(value[1].Replace("-", "_") + "_SUMMARY_" + startTime + "_" + value[21] + "_" + value[13].Replace("-", "_") + "_" + arrTestData.Count.ToString() + ".csv", lineHeaders.ToUpper(), arrTestData[0].ToString().ToUpper(), historicalData);

                        //get MZ sweep file if exists
                        if (File.Exists(value[16].ToString()))
                        {
                            mzExists = true;

                            //get required attachments
                            ConstructAttachments(value[16], value[1].Replace("-", "_"), headerTime.ToString(), value[21], value[13]);
                        }
                        else
                        {
                            mzExists = false; // no MZ file available
                        }

                        //some of the data have strange naming so just we will avoid this
                        if (value[1].Contains("@"))
                        {
                            int index = value[1].LastIndexOf("@");
                            if (index > 0)
                                value[1] = value[1].Substring(0, index); // or index + 1 to keep @
                        }

                        //get Limits

                        //---------------- Create XML NODES --------------------//
                        string xsi = "http://www.w3.org/2001/XMLSchema-instance";
                        string xsd = "http://www.w3.org/2001/XMLSchema";

                        XmlDocument doc = new XmlDocument();

                        XmlNode docNode = doc.CreateXmlDeclaration("1.0", "utf-8", null);
                        doc.AppendChild(docNode);

                        XmlNode resultsNode = doc.CreateElement("Results");
                        XmlAttribute xsiAttribute = doc.CreateAttribute("xmlns:xsi");
                        XmlAttribute xsdAttribute = doc.CreateAttribute("xsi:xsd");
                        xsiAttribute.Value = xsi;
                        xsdAttribute.Value = xsd;

                        resultsNode.Attributes.Append(xsiAttribute);
                        resultsNode.Attributes.Append(xsdAttribute);
                        doc.AppendChild(resultsNode);

                        /*----------------------------------*/
                        XmlNode resultNode = doc.CreateElement("Result");
                        XmlAttribute rNode = doc.CreateAttribute("Result");
                        rNode.Value = "" + resultOutcome;

                        XmlAttribute timeNode = doc.CreateAttribute("startDateTime");
                        timeNode.Value = "" + headerTime;

                        resultNode.Attributes.Append(rNode);
                        resultNode.Attributes.Append(timeNode);
                        resultsNode.AppendChild(resultNode);

                        /*------------------- Header ---------------------*/
                        XmlNode headerNode = doc.CreateElement("Header");
                        XmlAttribute serialNode = doc.CreateAttribute("Serialnumber");
                        if (value[1].Contains("@"))
                        {
                            int index = value[1].LastIndexOf("@");
                            if (index > 0)
                                value[1] = value[1].Substring(0, index); // or index + 1 to keep @
                        }
                        serialNode.Value = "" + value[1];

                        XmlAttribute waferNode = doc.CreateAttribute("Waferid");
                        waferNode.Value = "" + value[18];

                        XmlAttribute operationNode = doc.CreateAttribute("Operation");
                        operationNode.Value = "" + value[3];
                        XmlAttribute stationNode = doc.CreateAttribute("Teststation");
                        stationNode.Value = "" + value[13];
                        XmlAttribute maskNode = doc.CreateAttribute("Maskid");
                        maskNode.Value = "" + value[2];

                        XmlAttribute barNode = doc.CreateAttribute("Barid");
                        barNode.Value = "" + value[19];
                        XmlAttribute chipNode = doc.CreateAttribute("Chipid");
                        chipNode.Value = "" + value[20];

                        XmlAttribute operatorNode = doc.CreateAttribute("Operator");
                        operatorNode.Value = "" + value[12].Replace("-", "_").Replace(".", "_");
                        XmlAttribute partNode = doc.CreateAttribute("Partnumber");
                        partNode.Value = "" + partID[0].ToUpper();
                        XmlAttribute startNode = doc.CreateAttribute("Starttime");
                        startNode.Value = "" + headerTime;
                        XmlAttribute siteNode = doc.CreateAttribute("Site");
                        siteNode.Value = "250";
                        XmlAttribute batchNode = doc.CreateAttribute("BatchNumber");
                        batchNode.Value = "" + value[18];

                        XmlAttribute specLimitNode = doc.CreateAttribute("SpecificationRevision");
                        specLimitNode.Value = "" + limitsRevision;

                        XmlAttribute lotNode = doc.CreateAttribute("LotNumber");
                        lotNode.Value = "" + value[18];

                        XmlAttribute xLocNode = doc.CreateAttribute("Devicexlocation");
                        xLocNode.Value = "" + value[7];
                        XmlAttribute yLocNode = doc.CreateAttribute("Deviceylocation");
                        yLocNode.Value = "" + value[8];

                        XmlAttribute deviceNode = doc.CreateAttribute("DeviceType");
                        deviceNode.Value = "" + inDeviceType;

                        XmlAttribute purposeNode = doc.CreateAttribute("Purpose");
                        purposeNode.Value = "" + inLotType;

                        XmlAttribute testerVersionNode = doc.CreateAttribute("SoftwareAplicationName");
                        testerVersionNode.Value = "" + value[9];

                        XmlAttribute miscWave = doc.CreateAttribute("MiscInfo");
                        miscWave.Value = "Wavelength " + value[21];

                        headerNode.Attributes.Append(serialNode);
                        headerNode.Attributes.Append(waferNode);
                        headerNode.Attributes.Append(operationNode);
                        headerNode.Attributes.Append(stationNode);
                        headerNode.Attributes.Append(maskNode);
                        headerNode.Attributes.Append(barNode);
                        headerNode.Attributes.Append(chipNode);
                        headerNode.Attributes.Append(operatorNode);
                        headerNode.Attributes.Append(partNode);
                        headerNode.Attributes.Append(startNode);
                        headerNode.Attributes.Append(siteNode);
                        headerNode.Attributes.Append(batchNode);
                        headerNode.Attributes.Append(specLimitNode); //limits node
                        headerNode.Attributes.Append(lotNode);
                        headerNode.Attributes.Append(xLocNode);
                        headerNode.Attributes.Append(yLocNode);
                        headerNode.Attributes.Append(barNode);
                        headerNode.Attributes.Append(chipNode);
                        headerNode.Attributes.Append(deviceNode);
                        //    headerNode.Attributes.Append(softwareNode);
                        headerNode.Attributes.Append(purposeNode);
                        headerNode.Attributes.Append(testerVersionNode);
                        headerNode.Attributes.Append(miscWave);

                        /*------------------ Header Misc Node ----------------------*/
                        XmlNode miscNode = doc.CreateElement("HeaderMisc");
                        XmlNode miscItemNode = doc.CreateElement("Item");

                        XmlAttribute miscAttNode = doc.CreateAttribute("Wavelength");
                        miscAttNode.Value = "" + value[21];

                        miscItemNode.Attributes.Append(miscAttNode);

                        XmlAttribute dNode = doc.CreateAttribute("Serialnumber");
                        dNode.Value = "";

                        XmlAttribute opNode = doc.CreateAttribute("Operation");
                        opNode.Value = "OPERATION";

                        #region Genealogy NODE
                        /*------------------- Genealogy ----------------------*/
                        XmlNode geoNode = doc.CreateElement("UnitGenealogy");

                        if (arrComponents.Count > 0)
                        {
                            for (int c = 0; c < arrComponents.Count; c++)
                            {
                                XmlNode itemNode = doc.CreateElement("Item");

                                XmlAttribute ptypeNode = doc.CreateAttribute("PartType");
                                ptypeNode.Value = "Component";
                                XmlAttribute partNoNode = doc.CreateAttribute("Partnumber");
                                partNoNode.Value = "" + arrComponents[c];
                                XmlAttribute iSerialNode = doc.CreateAttribute("SerialNumber");
                                iSerialNode.Value = "" + inWaferID;

                                itemNode.Attributes.Append(ptypeNode);
                                itemNode.Attributes.Append(partNoNode);
                                itemNode.Attributes.Append(iSerialNode);

                                geoNode.AppendChild(itemNode);
                            }
                        }
                        #endregion
                        #region Main TestStep NODE
                        /*---------------- Main Test Step -------------------------*/
                        XmlNode testNode = doc.CreateElement("TestStep");

                        XmlAttribute tStatusNode = doc.CreateAttribute("Status");
                        tStatusNode.Value = resultOutcome;

                        XmlAttribute tNameNode = doc.CreateAttribute("Name");
                        tNameNode.Value = "SUMMARY_TABLE_DATA";

                        XmlAttribute tStartNode = doc.CreateAttribute("startDateTime");
                        tStartNode.Value = "" + headerTime;

                        XmlAttribute tSampleNode = doc.CreateAttribute("SampleRate");
                        tSampleNode.Value = "Wavelength " + value[21];

                        //     testNode.Attributes.Append(tTypeNode);
                        testNode.Attributes.Append(tNameNode);
                        testNode.Attributes.Append(tStartNode);
                        testNode.Attributes.Append(tSampleNode);
                        testNode.Attributes.Append(tStatusNode);
                        /*-----------------------------------------*/
                        #endregion

                        #region Table Data NODE
                        XmlNode dataNode = doc.CreateElement("Data");

                        XmlAttribute typeNode = doc.CreateAttribute("DataType");
                        typeNode.Value = "Table";

                        XmlAttribute dNameNode = doc.CreateAttribute("Name");
                    //    dNameNode.Value = "CASWELL_BAR_" + partID[0].Replace("-", "_");

                        dNameNode.Value = "CASWELL_BAR_" + partID[0].Replace("-", "_") + "_TEST"; //dev

                        if (value[1].Contains("@"))
                        {
                            int index = value[1].LastIndexOf("@");
                            if (index > 0)
                                value[1] = value[1].Substring(0, index); // or index + 1 to keep @
                        }

                        XmlAttribute valNode = doc.CreateAttribute("Value");
                        valNode.Value = "" + nTDSTable + value[1].Replace("-", "_") + "_SUMMARY_" + startTime.Replace(":", ".") + "_" + value[21] + "_" + value[13] + "_" + arrTestData.Count.ToString() + ".csv";

                        dataNode.Attributes.Append(typeNode);
                        dataNode.Attributes.Append(dNameNode);
                        //   dataNode.Attributes.Append(desNode);

                        dataNode.Attributes.Append(valNode);

                        #endregion

                        #region MZ Sweep attachment NODE
                        /*------------------ MZ Sweep attachment NODE ----------------------*/

                        XmlNode test2Node = doc.CreateElement("TestStep"); //sweep file att
                        XmlNode mzTestNode = doc.CreateElement("TestStep");
                        XmlNode data2Node = doc.CreateElement("Data");

                        XmlAttribute t2StatusNode = doc.CreateAttribute("Status");
                        t2StatusNode.Value = "Done";

                        XmlAttribute t2NameNode = doc.CreateAttribute("Name");
                        t2NameNode.Value = "MZSWEEP_DATA";

                        XmlAttribute t2StartNode = doc.CreateAttribute("startDateTime");
                        t2StartNode.Value = "" + headerTime;


                        //     testNode.Attributes.Append(tTypeNode);
                        test2Node.Attributes.Append(t2NameNode);
                        test2Node.Attributes.Append(t2StartNode);

                        /*------------------ MZ Test step 3 ----------------------*/

                        XmlAttribute mzStatusNode = doc.CreateAttribute("Status");
                        mzStatusNode.Value = resultOutcome;

                        XmlAttribute mzNameNode = doc.CreateAttribute("Name");
                        mzNameNode.Value = "SUMMARY_DATA";

                        XmlAttribute mzStartNode = doc.CreateAttribute("startDateTime");
                        mzStartNode.Value = "" + headerTime;

                        //     testNode.Attributes.Append(tTypeNode);

                        mzTestNode.Attributes.Append(mzNameNode);
                        mzTestNode.Attributes.Append(mzStatusNode);
                        mzTestNode.Attributes.Append(mzStartNode);

                        /*--------------------------------------------------------*/
                        #endregion
                        //END OF PARENT TEST NODE

                        //--------------- MZ sweep attachment -----------------//
                        XmlAttribute d2NameNode = doc.CreateAttribute("Name");
                        d2NameNode.Value = "MZSWEEP_DATA";

                        if (value[1].Contains("@"))
                        {
                            int index = value[1].LastIndexOf("@");
                            if (index > 0)
                                value[1] = value[1].Substring(0, index); // or index + 1 to keep slash
                        }
                        XmlAttribute type2Node = doc.CreateAttribute("DataType");
                        type2Node.Value = "Attachment";

                        XmlAttribute val2Node = doc.CreateAttribute("Value");
                        val2Node.Value = "" + nTDSAttachment + value[1].Replace("-", "_") + "_MZSweep_" + headerTime.Replace(":", ".") + "_" + value[21] + "_" + value[13] + ".csv";

                        data2Node.Attributes.Append(type2Node);
                        data2Node.Attributes.Append(d2NameNode);
                        data2Node.Attributes.Append(val2Node);
                        //-------------------------------------------------/

                        resultsNode.AppendChild(resultNode);
                        resultNode.AppendChild(headerNode);

                        miscNode.AppendChild(miscItemNode);

                        headerNode.AppendChild(miscNode);
                        resultNode.AppendChild(geoNode);
                        //        geoNode.AppendChild(itemNode);
                        resultNode.AppendChild(testNode);

                        testNode.AppendChild(dataNode);

                        //MZ attachment node
                        if (mzExists)
                        {
                            resultNode.AppendChild(test2Node); 
                            resultNode.AppendChild(data2Node); 
                            test2Node.AppendChild(data2Node);
                        }

                        resultNode.AppendChild(mzTestNode);
                        testNode.AppendChild(dataNode);

                        if (arrTableHeader.Length > 0)
                        {
                            for (int n = 0; n < arrTableHeader.Length; n++)
                            {
                                string[] tableInfo = arrTableInfo[n].ToString().Split(',');
                                string[] tableHead = arrTableHeader[n].ToString().Split(',');

                                var dataTypes = "String";

                                XmlNode resDataNode = doc.CreateElement("Data");
                                XmlAttribute resTypeNode = doc.CreateAttribute("DataType");
                                XmlAttribute resDataNameNode = doc.CreateAttribute("Name");
                                XmlAttribute resUnitsNode = doc.CreateAttribute("Units");
                                XmlAttribute resValNode = doc.CreateAttribute("Value");

                                XmlAttribute resStatusNode = doc.CreateAttribute("Status");
                                XmlAttribute resSpecMinNode = doc.CreateAttribute("SpecMin");
                                XmlAttribute resSpecMaxNode = doc.CreateAttribute("SpecMax");

                                //identify data type
                                if (arrTableInfo[n].ToString().Any(char.IsLetter) || arrTableInfo[n].ToString().Contains(":") || arrTableInfo[n].ToString().Contains("/") || arrTableInfo[n].ToString().Contains("_") || arrTableHeader[n].ToString().Equals("LOT_ID", StringComparison.InvariantCultureIgnoreCase) || arrTableHeader[n].ToString().Equals("COMP_ID", StringComparison.InvariantCultureIgnoreCase))
                                {
                                    dataTypes = "String";
                                }

                                else
                                {
                                    dataTypes = "Numeric";
                                }

                                resTypeNode.Value = "" + dataTypes;
                                resDataNameNode.Value = "" + arrTableHeader[n].ToUpper();

                                var headUnit = "NA";

                                //TODO: change with the values from Limits file
                                /*
                                if (arrTableHeader[n].Length > 3)
                                {
                                    headUnit = arrTableHeader[n].ToString().Substring(0, arrTableHeader[n].Length - 4).ToUpper();
                                }
                                var result = listUnits.FirstOrDefault(s => s.ToUpper().StartsWith(headUnit));

                                if (arrTableHeader[n].ToString().EndsWith("_OHMS"))
                                {
                                    resUnitsNode.Value = "OHMS";
                                }
                                else if (arrTableHeader[n].ToString().EndsWith("_V"))
                                {
                                    resUnitsNode.Value = "V";
                                }
                                else if (result != null)
                                {
                                    var uReas = result.ToString().Split(',');
                                    resUnitsNode.Value = "" + uReas[1].ToString().ToUpper();
                                }
                                else
                                {
                                    resUnitsNode.Value = "NA";
                                }

                                */

                                limitPassFail = "Done";
                                resUnitsNode.Value = "NA";

                                if (listSpecName.Count > 0)
                                {
                                    //Get Limits for the parameter 
                                    foreach (var limitHeader in listSpecName)
                                    {
                                        //check if parameter has limits set
                                        if (limitHeader.Equals(arrTableHeader[n]))
                                        {
                                            for (int l = 0; l < listSpecName.Count; l++)
                                            {
                                                if (limitHeader.Equals(listSpecName[l]))
                                                {
                                                    resValNode.Value = "" + arrTableInfo[n].ToString().ToUpper();
                                                    resUnitsNode.Value = listSpecUnits[l].ToString().ToUpper();
                                                    resSpecMinNode.Value = listSpecMin[l].ToString().ToUpper();
                                                    resSpecMaxNode.Value = listSpecMax[l].ToString().ToUpper();

                                                    var rawData = double.Parse(arrTableInfo[n].ToString());


                                                    if (!(rawData <= double.Parse(listSpecMax[l]) && rawData >= double.Parse(listSpecMin[l])))
                                                    {
                                                        limitPassFail = "Failed";
                                                    }
                                                    else
                                                    {
                                                        limitPassFail = "Passed";
                                                    }

                                                    /*
                                                    //check MIN pass / fail
                                                    if (rawData < double.Parse(listSpecMin[l]))
                                                    {
                                                        limitPassFail = "FAILED";
                                                    }
                                                    if(rawData >= double.Parse(listSpecMin[l]) && rawData <= double.Parse(listSpecMax[l]))
                                                    {
                                                        limitPassFail = "PASSED";
                                                    }

                                                        /*
                                                    if (rawData >= double.Parse(listSpecMin[l]) && rawData <= double.Parse(listSpecMax[l]))
                                                    {
                                                        limitPassFail = "FAILED";
                                                    }
                                                        */

                                                    /*
                                                    //check MAX pass / fail
                                                    if (rawData > double.Parse(listSpecMax[l]))
                                                    {
                                                        limitPassFail = "FAILED";
                                                    }
                                                    if (rawData <= double.Parse(listSpecMin[l]) && rawData <= double.Parse(listSpecMax[l]))
                                                    {
                                                        limitPassFail = "PASSED";
                                                    }*/
                                                }
                                            }
                                        }
                                        else
                                        {
                                            resValNode.Value = "" + arrTableInfo[n].ToString().ToUpper();
                                        }
                                    }
                                } else
                                {
                                    if (arrTableHeader[n].Length > 3)
                                    {
                                        headUnit = arrTableHeader[n].ToString().Substring(0, arrTableHeader[n].Length - 4).ToUpper();
                                    }
                                    var result = listUnits.FirstOrDefault(s => s.ToUpper().StartsWith(headUnit));

                                    if (arrTableHeader[n].ToString().EndsWith("_OHMS"))
                                    {
                                        resUnitsNode.Value = "OHMS";
                                    }
                                    else if (arrTableHeader[n].ToString().EndsWith("_V"))
                                    {
                                        resUnitsNode.Value = "V";
                                    }
                                    else if (result != null)
                                    {
                                        var uReas = result.ToString().Split(',');
                                        resUnitsNode.Value = "" + uReas[1].ToString().ToUpper();
                                    }
                                    else
                                    {
                                        resUnitsNode.Value = "NA";
                                    }
                                    resValNode.Value = "" + arrTableInfo[n].ToString().ToUpper();
                                }
                                resStatusNode.Value = "" + limitPassFail;

                                resDataNode.Attributes.Append(resTypeNode); //data type
                                resDataNode.Attributes.Append(resDataNameNode); //header name

                                resDataNode.Attributes.Append(resUnitsNode); //units
                                resDataNode.Attributes.Append(resValNode); //values

                                resDataNode.Attributes.Append(resStatusNode); // pass / fail / done
                                resDataNode.Attributes.Append(resSpecMinNode); //Limit min
                                resDataNode.Attributes.Append(resSpecMaxNode); //Limit max

                                mzTestNode.AppendChild(resDataNode);
                            }
                        }

                        //old product family -  ProductFamily=" + value[4] +
                        fileName = "Site=250,ProductFamily=CAS BAR DATA,Operation=" + value[3] + ",Partnumber=" + partID[0] + ",SerialNumber=" + value[1] + ",Testdate=" + startTime + ",TestStation=" + value[13].Replace("-", "_") + ",WL=" + value[21] + ",Purpose=" + inLotType + ".xml";

                        string fullName = Path.Combine(nTDSFolder, fileName);


                        doc.Save(fullName);

                        System.Threading.Thread.Sleep(1);

                        if (arrTestData.Count > 0)
                        {
                            arrTestData.RemoveAt(0);
                        }
                        else
                        {
                            createFile = false;
                            arrTestData = new ArrayList();
                        }

                        if (arrTestData.Count <= 0)
                        {
                            createFile = false;
                            arrTestData = new ArrayList();

                            if (File.Exists(oldFile))
                            {
                                try
                                {
                                    System.GC.Collect();
                                    System.GC.WaitForPendingFinalizers();

                                    File.SetAttributes(oldFile, FileAttributes.Normal);
                                    File.Delete(oldFile);

                                    sFileType = "";
                                }
                                catch (Exception ex)
                                {
                                    //TODO: record error to DB
                                }
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: " + ex);
            }
        }
        private void ConstructLogXML()
        {
            DirectoryInfo di = new DirectoryInfo(nToConvert);
            FileInfo[] files = di.GetFiles("*.csv");
            string fileName = "";
            var startTime = "";
            string headerTime = "";
            string cassetID = "";
            string serialID = "";
            string waferNumber = "";
            string purposeInfo = "";

            string equipType = "";

            sFileType = "";

            //    omegaList = new ArrayList();
            arrLogSource = new ArrayList();

            //    RemoveLogFile();

            //-------------- Get all info ready for binding -------------------//

            //construct array -> 1st line for headers, rest -> for data
            DirectoryInfo nDirectory = new DirectoryInfo(localProcessing);
            FileInfo[] nFile = nDirectory.GetFiles("*.*", SearchOption.AllDirectories);

            int fileCount = nFile.Length;

            foreach (FileInfo nInfo in nFile)
            {
                if (File.Exists(nInfo.FullName))
                {
                    if (Path.GetFileNameWithoutExtension(nInfo.Name).Contains("CDELTA"))
                    {
                        equipType = "CDELTA01";
                        //    foreach (FileInfo file in nFile)
                        //    {

                        string currentLine;
                        using (StreamReader sr = new StreamReader(nInfo.FullName))
                        {
                            sr.ReadLine(); // skip
                            sr.ReadLine(); // skip
                            sr.ReadLine(); // skip
                            sr.ReadLine(); // skip

                            arrLogFileData = new ArrayList();
                            omega = new ComegaOBJ();
                            while ((currentLine = sr.ReadLine()) != null)
                            {
                                arrLogFileData.Add(currentLine.ToString().Split(','));
                                string[] lineVal = currentLine.ToString().Split(',');

                                logFullName = nInfo.FullName;
                                logSummary = nInfo.Name;

                                omega.Cassette = lineVal[0].ToString().Trim();
                                omega.Sequence = lineVal[1].ToString().Trim();
                                omega.LotNumber = lineVal[6].ToString().Trim();
                                omega.Wafer = lineVal[3].ToString().Trim();
                                omega.FileName = logFullName.ToString().Trim();
                                omega.Name = nInfo.Name;

                                DateTime inDate = DateTime.ParseExact(lineVal[4], "dd/MM/yy HH:mm:ss", null);

                                //    DateTime outDate = Convert.ToDateTime(logItem[3]);
                                startTime = inDate.ToString("yyyy-MM-ddTHH:mm:ss");
                                omega.Date = startTime;
                            }
                            omegaList.Add(omega);
                        }
                        //    }

                        //    createFile = true;

                        if (File.Exists(oldFile))
                        {
                            RemoveLogFile();
                        }

                        foreach (ComegaOBJ o in omegaList)
                        {
                            try
                            {
                                if (File.Exists(o.FileName))
                                {
                                    System.GC.Collect();
                                    System.GC.WaitForPendingFinalizers();

                                    File.SetAttributes(o.FileName, FileAttributes.Normal);
                                    System.Threading.Thread.Sleep(10);

                                    File.Move(o.FileName, nTDSTable + o.Name);
                                    File.Delete(o.FileName);
                                }
                            }
                            catch (Exception) { }
                        }
                    }
                    if (Path.GetFileNameWithoutExtension(nInfo.Name).Contains("COMEGA01"))
                    {
                        equipType = "COMEGA01";
                        //    foreach (FileInfo file in nFile)
                        //    {
                        var sourceFile = nInfo.Name.Split('.');

                        string currentLine;
                        using (StreamReader sr = new StreamReader(nInfo.FullName))
                        {
                            sr.ReadLine(); // skip
                            sr.ReadLine(); // skip
                            sr.ReadLine(); // skip
                            sr.ReadLine(); // skip

                            arrLogFileData = new ArrayList();
                            omega = new ComegaOBJ();
                            while ((currentLine = sr.ReadLine()) != null)
                            {
                                arrLogFileData.Add(currentLine.ToString().Split(','));
                                string[] lineVal = currentLine.ToString().Split(',');

                                logFullName = nInfo.FullName;
                                logSummary = nInfo.Name;

                                omega.Cassette = lineVal[0].ToString();
                                omega.Sequence = lineVal[1].ToString();
                                omega.LotNumber = lineVal[6].ToString();
                                omega.Wafer = lineVal[3].ToString();
                                omega.FileName = logFullName.ToString();
                                omega.Name = nInfo.Name;

                                omega.RecNumber = lineVal[5].ToString();

                                DateTime inDate = DateTime.ParseExact(lineVal[4], "dd/MM/yy HH:mm:ss", null);

                                //    DateTime outDate = Convert.ToDateTime(logItem[3]);
                                startTime = inDate.ToString("yyyy-MM-ddTHH:mm:ss");
                                omega.Date = startTime;

                            //    CheckLogSource(equipType, lineVal[0].ToString(), lineVal[3].ToString(), lineVal[5].ToString(), inDate);

                                if (logAttachmentName.Length > 0 || logAttachmentName != "")
                                {
                                    omega.SourceAttachmentName = logAttachmentName;
                                }
                            }
                            omegaList.Add(omega);
                        }
                        //    }

                        //    createFile = true;

                        if (File.Exists(oldFile))
                        {
                            RemoveLogFile();
                        }

                        foreach (ComegaOBJ o in omegaList)
                        {
                            try
                            {
                                if (File.Exists(o.FileName))
                                {
                                    System.GC.Collect();
                                    System.GC.WaitForPendingFinalizers();

                                    File.SetAttributes(o.FileName, FileAttributes.Normal);
                                    System.Threading.Thread.Sleep(1);

                                    File.Move(o.FileName, nTDSTable + o.Name);
                                    File.Delete(o.FileName);
                                }
                            }
                            catch (Exception) { }
                        }
                    }
                    if (Path.GetFileNameWithoutExtension(nInfo.Name).Contains("COMEGA02"))
                    {
                        equipType = "COMEGA02";

                        if (File.Exists(oldFile))
                        {
                            RemoveLogFile();
                        }

                        foreach (ComegaOBJ o in omegaList)
                        {
                            try
                            {
                                if (File.Exists(o.FileName))
                                {
                                    System.GC.Collect();
                                    System.GC.WaitForPendingFinalizers();

                                    File.SetAttributes(o.FileName, FileAttributes.Normal);

                            //        File.Copy(o.FileName, nTDSTable +"TABLE_"+ o.Name);

                                    File.Delete(o.FileName);
                    //                System.Threading.Thread.Sleep(1);
                                }
                            }
                            catch (Exception) { }
                        }
                    }
                    if (Path.GetFileNameWithoutExtension(nInfo.Name).EndsWith("CSTEAG01"))
                    {
                        equipType = "CSTEAG01";

                        string currentLine;
                        using (StreamReader sr = new StreamReader(nInfo.FullName))
                        {
                            sr.ReadLine(); // skip
                            sr.ReadLine(); // skip
                            sr.ReadLine(); // skip
                            sr.ReadLine(); // skip

                            arrLogFileData = new ArrayList();
                            omega = new ComegaOBJ();
                            while ((currentLine = sr.ReadLine()) != null)
                            {
                                arrLogFileData.Add(currentLine.ToString().Split(','));
                                string[] lineVal = currentLine.ToString().Split('.');

                                logFullName = nInfo.FullName;
                                logSummary = nInfo.Name;

                                omega.Cassette = lineVal[0].ToString();
                                omega.Sequence = lineVal[1].ToString();
                                omega.LotNumber = lineVal[6].ToString();
                                omega.Wafer = lineVal[3].ToString();
                                omega.FileName = logFullName.ToString();
                                omega.Name = nInfo.Name;

                                DateTime inDate = DateTime.ParseExact(lineVal[4], "dd/MM/yy HH:mm:ss", null);

                                //    DateTime outDate = Convert.ToDateTime(logItem[3]);
                                startTime = inDate.ToString("yyyy-MM-ddTHH:mm:ss");
                                omega.Date = startTime;
                            }
                            omegaList.Add(omega);
                        }

                        EquipmentConversionMain(nInfo.FullName, nInfo.Name, "CSTEAG01");

                    }
                }
                if (omegaList.Count > 0) //!=
                {
                    createFile = true;
                }
                else
                {
                    //    File.Move(nConverted + completeLogName, nTDSFolder + completeLogName);
                    createFile = false;
                    completeLogName = "";
                }
                if (createFile)
                {
                    switch (equipType)
                    {
                        case "CDELTA01":
                            EquipCDELTA(equipType);
                            break;
                        case "COMEGA01":
                            EquipCOMEGA(equipType);
                            break;
                        case "COMEGA02":
                            EquipCOMEGA(equipType);
                            break;
                        default:
                            break;
                    }

                }
            }


            //get promis info for the wafer
            //    PromisInfo(inWaferID + ".1");
            //--------------------- create new XML file --------------------------//

        }

        //NOT IN USE
        private void CheckLogSource(string pEquipType, string pCasette, string pWafer, string pRec, DateTime pDate)
        {
            logAttachmentName = "";

            var sourceYear = pDate.ToString("yy");
            var sourceMonth = pDate.ToString("MMM");
            var sourceDay = pDate.ToString("dd");

            var equipFolder = @"\\cas-sfl-01\CASFAB\ToolData\";
            var completePath = "";

            var cassette = "";

            //    var sLocation = pCasette + "" + nLocation;

            //set cassette name
            switch (pCasette.Length)
            {
                case 1:
                    cassette = "C00" + pCasette; 
                    break;
                case 2:
                    cassette = "C0" + pCasette;
                    break;
                default:
                    cassette = "C" + pCasette;
                    break;
            }

            switch (pEquipType)
            {
                case "COMEGA01":

                    completePath = equipFolder + DateTime.Now.Year.ToString() + @" Logs\DATALOG\FULLMV\"+sourceMonth.ToUpper()+ @"\DAY" +sourceDay+"\\"+cassette+"0"+pWafer+"0"+pRec+".LOG";

                    if (File.Exists(completePath))
                    {
                        File.Copy(completePath, nTDSAttachment + cassette + "0" + pWafer + "0" + pRec + ".LOG");
                        logAttachmentName = nTDSAttachment + cassette + "0" + pWafer + "0" + pRec + ".LOG";
                    }
                    
                    break;
                case "COMEGA02":
                    //TODO:
                    break;
                case "CDELTA01":
                    //TODO:
                    break;
                default:
                    break;
            }
        }

        private void EquipCDELTA(string EquipType)
        {
            string fileName = "";
            var startTime = "";
            string cassetID = "";
            string serialID = "";
            string waferNumber = "";
            string purposeInfo = "";

            string[] value = arrLogHeader[0].ToString().Split(',');
            string[] cassetteInfo = arrLogCassette[3].ToString().Split(',');

                Random rnd = new Random();
                int random = rnd.Next(1, 999999999);  // creates a number for #LIFTUP rec as the date is not recorded 
                purposeInfo = "" + random;

            System.Threading.Thread.Sleep(1);

            DateTime nDate = Convert.ToDateTime(arrLogCassette[6] + " " + arrLogCassette[7]);

            var headerTime = nDate.ToString("yyyy-MM-ddTHH:mm:ss").Replace(".", ":");

            serialID = cassetteInfo[0].Trim();

                string xsi = "http://www.w3.org/2001/XMLSchema-instance";
                string xsd = "http://www.w3.org/2001/XMLSchema";

                XmlDocument doc = new XmlDocument();

                //data node

                XmlNode docNode = doc.CreateXmlDeclaration("1.0", "utf-8", null);
                doc.AppendChild(docNode);

                XmlNode resultsNode = doc.CreateElement("Results");
                XmlAttribute xsiAttribute = doc.CreateAttribute("xmlns:xsi");
                xsiAttribute.Value = xsi;
                XmlAttribute xsdAttribute = doc.CreateAttribute("xsi:xsd");
                xsdAttribute.Value = xsd;

                resultsNode.Attributes.Append(xsiAttribute);
                resultsNode.Attributes.Append(xsdAttribute);
                doc.AppendChild(resultsNode);

                //----------------------------------//
                XmlNode resultNode = doc.CreateElement("Result");
                XmlAttribute rNode = doc.CreateAttribute("Result");
                rNode.Value = "Done";

                XmlAttribute timeNode = doc.CreateAttribute("startDateTime");
                timeNode.Value = "" + headerTime;

                resultNode.Attributes.Append(rNode);
                resultNode.Attributes.Append(timeNode);
                resultsNode.AppendChild(resultNode);

                //------------------- Header ---------------------//

                XmlNode headerNode = doc.CreateElement("Header");
                XmlAttribute serialNode = doc.CreateAttribute("Serialnumber");
                serialNode.Value = "" + serialID;

                XmlAttribute waferNode = doc.CreateAttribute("Waferid");
                waferNode.Value = "" + serialID;

                XmlAttribute operationNode = doc.CreateAttribute("Operation");
                operationNode.Value = "" + EquipType;
                XmlAttribute stationNode = doc.CreateAttribute("Teststation");
                stationNode.Value = "" + EquipType;

                XmlAttribute operatorNode = doc.CreateAttribute("Operator");
                operatorNode.Value = "NA";
                XmlAttribute partNode = doc.CreateAttribute("Partnumber");
                partNode.Value = "UNKNOWNPN";

                XmlAttribute startNode = doc.CreateAttribute("Starttime");
                startNode.Value = "" + headerTime;

                XmlAttribute statusNode = doc.CreateAttribute("Result");
                statusNode.Value = "Done";

                XmlAttribute siteNode = doc.CreateAttribute("Site");
                siteNode.Value = "250";

                XmlAttribute deviceNode = doc.CreateAttribute("DeviceType");
                deviceNode.Value = "LOT";

                XmlAttribute purposeNode = doc.CreateAttribute("Purpose");
                purposeNode.Value = "" + purposeInfo;

                headerNode.Attributes.Append(serialNode);
                headerNode.Attributes.Append(waferNode);
                headerNode.Attributes.Append(operationNode);
                headerNode.Attributes.Append(stationNode);
                headerNode.Attributes.Append(operatorNode);
                headerNode.Attributes.Append(partNode);
                headerNode.Attributes.Append(siteNode);
                headerNode.Attributes.Append(startNode);
                headerNode.Attributes.Append(statusNode);
                headerNode.Attributes.Append(deviceNode);
                //    headerNode.Attributes.Append(purposeNode);

                //------------------ NOT IN USE ----------------------//
                XmlNode miscNode = doc.CreateElement("HeaderMisc");
                XmlAttribute dNode = doc.CreateAttribute("Serialnumber");
                dNode.Value = "";

                XmlAttribute opNode = doc.CreateAttribute("Operation");
                opNode.Value = "operation";

                //------------------- Genealogy ----------------------//
                XmlNode geoNode = doc.CreateElement("UnitGenealogy");

                XmlNode itemNode = doc.CreateElement("Item");

                if (arrComponents.Count > 0)
                {
                    for (int c = 0; c < arrComponents.Count; c++)
                    {
                        XmlAttribute ptypeNode = doc.CreateAttribute("PartType");
                        ptypeNode.Value = "Component";
                        XmlAttribute partNoNode = doc.CreateAttribute("Partnumber");
                        partNoNode.Value = "" + arrComponents[c];
                        XmlAttribute iSerialNode = doc.CreateAttribute("SerialNumber");
                        iSerialNode.Value = "" + value[1].Replace("-", "_");

                        itemNode.Attributes.Append(ptypeNode);
                        itemNode.Attributes.Append(partNoNode);
                        itemNode.Attributes.Append(iSerialNode);
                    }
                }
                //-----------------------------------------//
                XmlNode testNode = doc.CreateElement("TestStep");

                XmlAttribute tStatusNode = doc.CreateAttribute("Status");
                tStatusNode.Value = "Done";

                XmlAttribute tNameNode = doc.CreateAttribute("Name");
                tNameNode.Value = "WAFER" + waferNumber;

                XmlAttribute tStartNode = doc.CreateAttribute("startDateTime");
                tStartNode.Value = "" + headerTime;

                //     testNode.Attributes.Append(tTypeNode);
                testNode.Attributes.Append(tNameNode);
                testNode.Attributes.Append(tStartNode);
                //-----------------------------------------//

                XmlNode dataNode = doc.CreateElement("Data");

                XmlAttribute typeNode = doc.CreateAttribute("DataType");
                typeNode.Value = "Table";

                XmlAttribute dNameNode = doc.CreateAttribute("Name");
                dNameNode.Value = "CASWELL_"+ EquipType+"_SUMMARY";

                XmlAttribute valNode = doc.CreateAttribute("Value");
                valNode.Value = "TABLE_" + logFullName;

                dataNode.Attributes.Append(typeNode);
                dataNode.Attributes.Append(dNameNode);
                //   dataNode.Attributes.Append(desNode);

                dataNode.Attributes.Append(valNode);

                //------------------Test step 2 MZ Data ----------------------//
                XmlNode test2Node = doc.CreateElement("TestStep"); //sweep file att
                XmlNode data2Node = doc.CreateElement("Data");

                XmlAttribute t2StatusNode = doc.CreateAttribute("Status");
                t2StatusNode.Value = "Done";

                XmlAttribute t2NameNode = doc.CreateAttribute("Name");
                t2NameNode.Value = "" + EquipType + "_LOG_RAW";

            XmlAttribute t2StartNode = doc.CreateAttribute("startDateTime");
                t2StartNode.Value = "" + headerTime;

                //     testNode.Attributes.Append(tTypeNode);
                test2Node.Attributes.Append(t2NameNode);
                test2Node.Attributes.Append(t2StartNode);

                //--------------------------------------------------------//
                //END OF PARENT TEST NODE

                XmlAttribute type2Node = doc.CreateAttribute("DataType");
                type2Node.Value = "Attachment";

                XmlAttribute d2NameNode = doc.CreateAttribute("Name");
                d2NameNode.Value = ""+EquipType+"_LOG_RAW_ATTACHMENT";

                XmlAttribute val2Node = doc.CreateAttribute("Value");
                val2Node.Value = "" + attachmentLocation;
                                                   //        val2Node.Value = "" + nTDSAttachment + logName[0] + "_SUMMARY_" + headerTime.Replace(":", ".") + ".csv"; //destAttFile

                data2Node.Attributes.Append(type2Node);
                data2Node.Attributes.Append(d2NameNode);
                //   dataNode.Attributes.Append(desNode);

                data2Node.Attributes.Append(val2Node);
                //-------------------------------------------------//

                resultsNode.AppendChild(resultNode);
                resultNode.AppendChild(headerNode);
                //    headerNode.AppendChild(miscNode);
                //     resultNode.AppendChild(geoNode);
                geoNode.AppendChild(itemNode);

                testNode.AppendChild(dataNode);

                //        resultNode.AppendChild(testNode);
                resultNode.AppendChild(test2Node);
                resultNode.AppendChild(data2Node);


                testNode.AppendChild(dataNode);
                test2Node.AppendChild(data2Node);

                resultNode.AppendChild(test2Node);
                resultNode.AppendChild(data2Node);

                testNode.AppendChild(dataNode);
                test2Node.AppendChild(data2Node);

                foreach (ComegaOBJ omegaData in omegaList)
                {
                    mzTestNode = doc.CreateElement("TestStep");
                    mzDataNode = doc.CreateElement("Data");
                    mzStatusNode = doc.CreateAttribute("Status");
                    mzNameNode = doc.CreateAttribute("Name");
                    mzStartNode = doc.CreateAttribute("startDateTime");

                    mzTypeNode = doc.CreateAttribute("DataType");
                    mzDataNameNode = doc.CreateAttribute("Name");
                    mzValNode = doc.CreateAttribute("Value");

                    mzStatusNode.Value = "Done";

                    mzNameNode.Value = "WAFER " + (omegaData.Wafer);
                    mzStartNode.Value = "" + omegaData.Date;

                    mzTypeNode.Value = "Table";

                    mzDataNameNode.Value = "CASWELL_" + EquipType +"_LOG";

                    mzValNode.Value = "" + nTDSTable + omegaData.Name;

                    mzDataNode.Attributes.Append(mzTypeNode);
                    mzDataNode.Attributes.Append(mzDataNameNode);

                    mzDataNode.Attributes.Append(mzValNode);

                    mzTestNode.Attributes.Append(mzNameNode);
                    mzTestNode.Attributes.Append(mzStatusNode);
                    mzTestNode.Attributes.Append(mzStartNode);

                    mzTestNode.AppendChild(mzDataNode);

                    resultNode.AppendChild(mzTestNode);
                }
                //  resultNode.AppendChild(mzTestNode);


                fileName = "Site=250,ProductFamily=CAS EQUIPMENT" + ",Operation="+EquipType+ ",Partnumber=" + "UNKNOWNPN" + ",SerialNumber=" + serialID + ",TestDate=" + headerTime.ToString().Replace(":", ".") + ".xml";

                string fullName = Path.Combine(nConverted, fileName);

                completeLogName = fileName;
            
                doc.Save(fullName);
                createFile = false;

        }
        private void EquipCOMEGA(string EquipType)
        {
            string fileName = "";
            var startTime = "";
            string headerTime = "";
            string cassetID = "";
            string serialID = "";
            string waferNumber = "";
            string purposeInfo = "";

            Random rnd = new Random();
            int random = rnd.Next(1, 999999999);  // creates a number for #LIFTUP rec as the date is not recorded 
            purposeInfo = "" + random;

            DateTime outDate = DateTime.ParseExact(omega.Date.Replace("T"," "), "yyyy-MM-dd HH:mm:ss", null);

            headerTime = outDate.ToString("yyyy-MM-ddTHH:mm:ss");
            serialID = omega.Wafer.ToString();

            string xsi = "http://www.w3.org/2001/XMLSchema-instance";
            string xsd = "http://www.w3.org/2001/XMLSchema";

            XmlDocument doc = new XmlDocument();
            //data node

            XmlNode docNode = doc.CreateXmlDeclaration("1.0", "utf-8", null);
            doc.AppendChild(docNode);

            XmlNode resultsNode = doc.CreateElement("Results");
            XmlAttribute xsiAttribute = doc.CreateAttribute("xmlns:xsi");
            xsiAttribute.Value = xsi;
            XmlAttribute xsdAttribute = doc.CreateAttribute("xsi:xsd");
            xsdAttribute.Value = xsd;

            resultsNode.Attributes.Append(xsiAttribute);
            resultsNode.Attributes.Append(xsdAttribute);
            doc.AppendChild(resultsNode);

            //----------------------------------//
            XmlNode resultNode = doc.CreateElement("Result");
            XmlAttribute rNode = doc.CreateAttribute("Result");
            rNode.Value = "Done";

            XmlAttribute timeNode = doc.CreateAttribute("startDateTime");
            timeNode.Value = "" + headerTime;

            resultNode.Attributes.Append(rNode);
            resultNode.Attributes.Append(timeNode);
            resultsNode.AppendChild(resultNode);

            //------------------- Header ---------------------//

            XmlNode headerNode = doc.CreateElement("Header");
            XmlAttribute serialNode = doc.CreateAttribute("Serialnumber");
            serialNode.Value = "" + omega.Cassette.ToString() + "_" + omega.Wafer.ToString();

            XmlAttribute waferNode = doc.CreateAttribute("Waferid");
            waferNode.Value = "" + omega.Wafer.ToString();

            XmlAttribute operationNode = doc.CreateAttribute("Operation");
            operationNode.Value = "" + EquipType;
            XmlAttribute stationNode = doc.CreateAttribute("Teststation");
            stationNode.Value = "" + EquipType;

            XmlAttribute operatorNode = doc.CreateAttribute("Operator");
            operatorNode.Value = "NA";
            XmlAttribute partNode = doc.CreateAttribute("Partnumber");
            partNode.Value = "UNKNOWNPN";

            XmlAttribute startNode = doc.CreateAttribute("Starttime");
            startNode.Value = "" + headerTime;

            XmlAttribute statusNode = doc.CreateAttribute("Result");
            statusNode.Value = "Done";

            XmlAttribute siteNode = doc.CreateAttribute("Site");
            siteNode.Value = "250";

            XmlAttribute deviceNode = doc.CreateAttribute("DeviceType");
            deviceNode.Value = "LOT";

            XmlAttribute purposeNode = doc.CreateAttribute("Purpose");
            purposeNode.Value = "" + purposeInfo;

            XmlAttribute batchNumberNode = doc.CreateAttribute("BatchNumber");
            batchNumberNode.Value = "" + omega.LotNumber;

            headerNode.Attributes.Append(serialNode);
            headerNode.Attributes.Append(waferNode);
            headerNode.Attributes.Append(operationNode);
            headerNode.Attributes.Append(stationNode);
            headerNode.Attributes.Append(operatorNode);
            headerNode.Attributes.Append(partNode);
            headerNode.Attributes.Append(siteNode);
            headerNode.Attributes.Append(startNode);
            headerNode.Attributes.Append(statusNode);
            headerNode.Attributes.Append(deviceNode);
            //    headerNode.Attributes.Append(purposeNode);
            headerNode.Attributes.Append(batchNumberNode);

            //------------------ NOT IN USE ----------------------//
            XmlNode miscNode = doc.CreateElement("HeaderMisc");
            XmlAttribute dNode = doc.CreateAttribute("Serialnumber");
            dNode.Value = "";

            XmlAttribute opNode = doc.CreateAttribute("Operation");
            opNode.Value = "operation";

            //------------------- Genealogy ----------------------//
            XmlNode geoNode = doc.CreateElement("UnitGenealogy");

            XmlNode itemNode = doc.CreateElement("Item");

            if (arrComponents.Count > 0)
            {
                for (int c = 0; c < arrComponents.Count; c++)
                {
                    XmlAttribute ptypeNode = doc.CreateAttribute("PartType");
                    ptypeNode.Value = "Component";
                    XmlAttribute partNoNode = doc.CreateAttribute("Partnumber");
                    partNoNode.Value = "" + arrComponents[c];
                    XmlAttribute iSerialNode = doc.CreateAttribute("SerialNumber");
                    iSerialNode.Value = "" + omega.Wafer.Replace("-", "_");

                    itemNode.Attributes.Append(ptypeNode);
                    itemNode.Attributes.Append(partNoNode);
                    itemNode.Attributes.Append(iSerialNode);
                }
            }
            //-----------------------------------------//
            XmlNode testNode = doc.CreateElement("TestStep");

            XmlAttribute tStatusNode = doc.CreateAttribute("Status");
            tStatusNode.Value = "Done";

            XmlAttribute tNameNode = doc.CreateAttribute("Name");
            tNameNode.Value = "WAFER " + waferNumber;

            XmlAttribute tStartNode = doc.CreateAttribute("startDateTime");
            tStartNode.Value = "" + headerTime;

            //     testNode.Attributes.Append(tTypeNode);
            testNode.Attributes.Append(tNameNode);
            testNode.Attributes.Append(tStartNode);
            //-----------------------------------------//

            XmlNode dataNode = doc.CreateElement("Data");

            XmlAttribute typeNode = doc.CreateAttribute("DataType");
            typeNode.Value = "Table";

            XmlAttribute dNameNode = doc.CreateAttribute("Name");
            dNameNode.Value = ""+ EquipType+"_SUMMARY_DATA";

            XmlAttribute valNode = doc.CreateAttribute("Value");
            valNode.Value = nTDSTable+ tableLocation;

            dataNode.Attributes.Append(typeNode);
            dataNode.Attributes.Append(dNameNode);
            //   dataNode.Attributes.Append(desNode);

            dataNode.Attributes.Append(valNode);

            //------------------T Summary attachment ----------------------//
            XmlNode test2Node = doc.CreateElement("TestStep"); //sweep file att
            XmlNode data2Node = doc.CreateElement("Data");

            XmlAttribute t2StatusNode = doc.CreateAttribute("Status");
            t2StatusNode.Value = "Done";

            XmlAttribute t2NameNode = doc.CreateAttribute("Name");
            t2NameNode.Value = ""+ EquipType+"_LOG_DATA_SUMMARY";

            XmlAttribute t2StartNode = doc.CreateAttribute("startDateTime");
            t2StartNode.Value = "" + headerTime;

            //     testNode.Attributes.Append(tTypeNode);
            test2Node.Attributes.Append(t2NameNode);
            test2Node.Attributes.Append(t2StartNode);

            //--------------------------------------------------------//
            //END OF PARENT TEST NODE

            XmlAttribute type2Node = doc.CreateAttribute("DataType");
            type2Node.Value = "Attachment";

            XmlAttribute d2NameNode = doc.CreateAttribute("Name");
            d2NameNode.Value = ""+ EquipType+"_LOG_DATA";

            XmlAttribute val2Node = doc.CreateAttribute("Value");
            val2Node.Value = "" + nTDSAttachment + attachmentLocation;
            //        val2Node.Value = "" + nTDSAttachment + logName[0] + "_SUMMARY_" + headerTime.Replace(":", ".") + ".csv"; //destAttFile

            data2Node.Attributes.Append(type2Node);
            data2Node.Attributes.Append(d2NameNode);
            //   dataNode.Attributes.Append(desNode);

            data2Node.Attributes.Append(val2Node);
            //-------------------------------------------------//

            //----------------------
            XmlNode sourceNode = doc.CreateElement("TestStep"); //sweep file att
            XmlNode sourceDataNode = doc.CreateElement("Data");
            if (omega.SourceAttachmentName != null)
            {
                //------------------T Summary attachment ----------------------//

                XmlAttribute sourceStatusNode = doc.CreateAttribute("Status");
                sourceStatusNode.Value = "Done";

                XmlAttribute sourceNameNode = doc.CreateAttribute("Name");
                sourceNameNode.Value = "" + EquipType + "_SOURCE";

                XmlAttribute sourceStartNode = doc.CreateAttribute("startDateTime");
                sourceStartNode.Value = "" + headerTime;

                //     testNode.Attributes.Append(tTypeNode);
                sourceNode.Attributes.Append(sourceNameNode);
                test2Node.Attributes.Append(sourceStartNode);

                //--------------------------------------------------------//
                //END OF PARENT TEST NODE

                XmlAttribute sourceTypeNode = doc.CreateAttribute("DataType");
                sourceTypeNode.Value = "Attachment";

                XmlAttribute sourceFileNameNode = doc.CreateAttribute("Name");
                sourceFileNameNode.Value = "" + EquipType + "_RAW_DATA";

                XmlAttribute sourceValNode = doc.CreateAttribute("Value");
                sourceValNode.Value = "" + nTDSAttachment + attachmentLocation; //destAttFile
                                                   //        val2Node.Value = "" + nTDSAttachment + logName[0] + "_SUMMARY_" + headerTime.Replace(":", ".") + ".csv"; //destAttFile

                sourceDataNode.Attributes.Append(type2Node);
                sourceDataNode.Attributes.Append(d2NameNode);
                //   dataNode.Attributes.Append(desNode);

                sourceDataNode.Attributes.Append(sourceValNode);
            }

            resultsNode.AppendChild(resultNode);
            resultNode.AppendChild(headerNode);
            //    headerNode.AppendChild(miscNode);
            //     resultNode.AppendChild(geoNode);
            geoNode.AppendChild(itemNode);

            testNode.AppendChild(dataNode);

            //        resultNode.AppendChild(testNode);
            resultNode.AppendChild(test2Node);
            resultNode.AppendChild(data2Node);

            testNode.AppendChild(dataNode);
            test2Node.AppendChild(data2Node);

            resultNode.AppendChild(test2Node);
            resultNode.AppendChild(data2Node);

            resultNode.AppendChild(sourceDataNode);

            testNode.AppendChild(dataNode);
            test2Node.AppendChild(data2Node);

            foreach (ComegaOBJ omegaData in omegaList)
            {
                mzTestNode = doc.CreateElement("TestStep");
                mzDataNode = doc.CreateElement("Data");
                mzStatusNode = doc.CreateAttribute("Status");
                mzNameNode = doc.CreateAttribute("Name");
                mzStartNode = doc.CreateAttribute("startDateTime");

                mzTypeNode = doc.CreateAttribute("DataType");
                mzDataNameNode = doc.CreateAttribute("Name");
                mzValNode = doc.CreateAttribute("Value");

                mzStatusNode.Value = "Done";

                mzNameNode.Value = "WAFER_" + omegaData.Wafer;
                mzStartNode.Value = "" + omegaData.Date;

                mzTypeNode.Value = "Table";

                //TODO: change table name for the production
                mzDataNameNode.Value = "CASWELL_" + EquipType +"_LOG"; //FOR TESTING ONLY

                mzValNode.Value = "" + nTDSTable + tableLocation;

                mzDataNode.Attributes.Append(mzTypeNode);
                mzDataNode.Attributes.Append(mzDataNameNode);

                mzDataNode.Attributes.Append(mzValNode);

                mzTestNode.Attributes.Append(mzNameNode);
                mzTestNode.Attributes.Append(mzStatusNode);
                mzTestNode.Attributes.Append(mzStartNode);

                mzTestNode.AppendChild(mzDataNode);
                resultNode.AppendChild(mzTestNode);
            }
            //  resultNode.AppendChild(mzTestNode);

                fileName = "Site=250,ProductFamily=CAS EQUIPMENT" + ",Operation=" + EquipType + ",Partnumber=" + "UNKNOWNPN" + ",SerialNumber=" + omega.Cassette.ToString() + "_"+omega.Wafer.ToString() + ",TestDate=" + headerTime.ToString().Replace(":", ".") + ".xml";
            //        fileName = "Site=250,ProductFamily=CAS EQUIPMENT" + ",Operation="+ EquipType + ",Partnumber=" + "UNKNOWNPN" + ",SerialNumber=" + serialID + ",TestDate=" + headerTime.ToString().Replace(":", ".") + ".xml";

        //    string fullName = Path.Combine(nConverted, fileName);

            string fullName = Path.Combine(nTDSFolder, fileName);
            completeLogName = fileName;

            doc.Save(fullName);
            createFile = false;
        }

        #region File Conversion methods
        private void CreateFile()
        {
             SearchForFile();
            
            if (sFileType.Equals("cilData"))
            {
                // bar summary data constructor
            //    ConstructBarXML_ILMZ();
            }

            if (sFileType.Equals("barData"))
            {
                ConstructXML();
            }
            if (sFileType.Equals("equipData"))
            {
                ConstructLogXML();
            }
        }

        private void RemoveCol(string path, int index)
        {
            List<string> lines = new List<string>();
            using (StreamReader reader = new StreamReader(path))
            {
                var line = reader.ReadLine();
                List<string> values = new List<string>();
                while (line != null)
                {
                    values.Clear();
                    var cols = line.Split(',');
                    for (int i = 0; i < cols.Length; i++)
                    {
                        if (i != index)
                        {
                            values.Add(cols[i]);
                        }
                    }
                    var newLine = string.Join(",", values);
                    lines.Add(newLine);
                    line = reader.ReadLine();
                }
            }

            using (StreamWriter writer = new StreamWriter(path, false))
            {
                foreach (var line in lines)
                {
                    writer.WriteLine(line);
                }
            }
        }

        //Search for the data files in the folders 
        public ArrayList SearchForFile()
        {
            Boolean contRead = false;
            int x;
            var currentDateTime = DateTime.Now;

            List<String> files = new List<String>();

            ArrayList arrInfo = new ArrayList();
            ArrayList arrData = new ArrayList();

            DirectoryInfo nDirectory = new DirectoryInfo(nToConvert);
            FileInfo[] nFile = nDirectory.GetFiles("*.*", SearchOption.AllDirectories); //gets all files in the sub-directories

            ResultOBJ resultInfo = new ResultOBJ();
            //     HeaderOBJ headerInfo = new HeaderOBJ();
            HeaderMiscOBJ headerMiscInfo = new HeaderMiscOBJ();
            UnitGenealogyOBJ unitGenInfo = new UnitGenealogyOBJ();
            TestStepOBJ testStepInfo = new TestStepOBJ();
            DataArrayOBJ dataArray = new DataArrayOBJ();

            //bar data
            string fileFullname = "";
            string fileName = "";
            string fileCreationTime = "";
            string fileCreationYear = "";
            string fileLastWriteTime = "";

            string fileCreationDate = "";
            string fileCreationTimeHours = "";
            nWaferID = "";

            sFileType = "";
            arrCSVInfo = new ArrayList();

            if (nDirectory.GetFiles().Length > 0)
            {
                FileInfo fi = nDirectory.GetFiles()[0];

                fileFullname = fi.FullName;
                fileName = fi.Name;
                fileCreationTime = fi.CreationTime.ToString("yyyy-MM-ddTHH:mm:ss.ss");
                fileCreationYear = fi.CreationTime.Year.ToString();

                fileCreationDate = fi.CreationTime.ToString("yyyy-MM-dd");
                fileCreationTimeHours = fi.CreationTime.ToString("HH:mm:ss");
            }
            // NLL and ILMZ
            if (fileName.StartsWith("CIL") || fileName.StartsWith("CTL") && fileName.EndsWith("PROD_Sm.csv")) //remove this to read all files if required or add "ELSE IF" to set the file types
            {
                sFileType = "cilData";
                try
                {
                    oldFile = fileFullname;
                    sumFileName = fileName.ToString();

                    var fs = new FileStream(fileFullname, FileMode.Open, FileAccess.Read);
                    var countFS = new FileStream(fileFullname, FileMode.Open, FileAccess.Read);

                    totalLines = File.ReadAllLines(fileFullname).Length;

                    using (var sr = new StreamReader(fs))
                    {
                        string currentLine;
                        fs = null;

                        while ((currentLine = sr.ReadLine()) != null)
                        {
                            HeaderOBJ headerInfo = new HeaderOBJ();
                            var values = currentLine.Split(',');

                            string info = values[0];
                            x = values.Length;
                            //header information
                            inWaferID = values[5];

                            switch (info)
                            {
                                case "TEST_PLAN":
                                    for (int i = 0; i < x; i++)
                                    {
                                        arrHeader.Add(values[i]);
                                    }
                                    break;
                                default:
                                    break;
                            }
                            arrCSVInfo.Add(currentLine);
                        }
                    }
                }

                catch (Exception ex)
                {
                    Console.WriteLine("" + ex);
                    if (ex.Message.Contains("being used by another process"))
                    {
                        mDBAccess.InsertLogEvent("File LOCK Error", "" + ex);
                    }
                    else
                    {
                        mDBAccess.InsertLogEvent("File Read Error", "File Read error: " + ex);
                    }
                }
            }

            //MZSI
            if (fileFullname.Contains("Processed.csv") || fileFullname.EndsWith("processed.csv") || fileFullname.EndsWith("Screened.csv") || fileFullname.EndsWith("Screened.xlsx"))
            {
                    sFileType = "barData";
                try
                {
                    oldFile = fileFullname;
                    sumFileName = fileName.ToString();

                    var fs = new FileStream(fileFullname, FileMode.Open, FileAccess.Read);
                    var countFS = new FileStream(fileFullname, FileMode.Open, FileAccess.Read);

                    totalLines = File.ReadAllLines(fileFullname).Length;

                    using (var sr = new StreamReader(fs))
                    {
                        string currentLine;
                        fs = null;

                        while ((currentLine = sr.ReadLine()) != null)
                        {
                            HeaderOBJ headerInfo = new HeaderOBJ();
                            var values = currentLine.Split(',');

                            string info = values[0];
                            x = values.Length;
                            //header information
                            if (values[1].ToString() != "" && values[18] != "" && values[18].ToString().Length > 1)
                            {
                                inWaferID = values[18].ToUpper();
                                inMask = values[2].ToString().ToUpper();

                                arrCSVInfo.Add(currentLine.ToString().ToUpper());
                            }
                        }
                    }
                }

                catch (Exception ex)
                {
                    Console.WriteLine("" + ex);
                    if (ex.Message.Contains("being used by another process"))
                    {
                        mDBAccess.InsertLogEvent("File LOCK Error", "" + ex);
                    }
                    else
                    {
                        mDBAccess.InsertLogEvent("File Read Error", "File Read error: " + ex);
                    }
                }
            }
            #region eqpHandling

            //For COMEGA01 & COMEGA02
            if (fileFullname.Contains("COMEGA01") || fileFullname.Contains("COMEGA02"))
            {
                //method below can convert summary files
                //    ComegaConversion(fileCreationTime,fileFullname,fileName, fileLastWriteTime);

                arrCSVInfo = new ArrayList();
                arrLogHeader = new ArrayList();
                arrLogCassette = new ArrayList();
                arrLogHeader = new ArrayList();
                arrLogWafer = new ArrayList();
                arrLogStep = new ArrayList();
                arrLogSet = new ArrayList();
                arrLogRec = new ArrayList();

                sFileType = "equipData";

                testEndDate = fileCreationTime;//.Replace(":", ".");
                oldFile = fileFullname;

                var csvFormat = fileName.Split('.');
                bool newFile = false;
                var countRemove = 0;

                System.GC.Collect();
                System.GC.WaitForPendingFinalizers();
 
                System.Threading.Thread.Sleep(1);
                var fs = new FileStream(fileFullname, FileMode.Open, FileAccess.Read);
                var countFS = new FileStream(fileFullname, FileMode.Open, FileAccess.Read);

                totalLines = File.ReadAllLines(fileFullname).Length;

                logFileName = fileName;
                logName = fileName.Split('.');
                logDate = fileCreationTime;

                //   logSummary = @"\\cas-sfl-01\CASFAB\ToolData\COMEGA02\" + fileCreationYear + "\\Logs\\DATALOG\\SUMMARY\\";

                logFileName = logName[1];
                var nFileName = Path.GetFileNameWithoutExtension(fileName);

                //generate attachment data
                if (File.Exists(fileFullname))
                {
                    if(!File.Exists(nTDSAttachment + "ATTACHMENT_" + fileName))
                    {
                        File.Copy(fileFullname, nTDSAttachment + "ATTACHMENT_" + fileName);
                    }
                }
                if (logName[1] != null && logName[1] != "")
                {
                    if (logName[1].Contains("TEST"))
                    {
                        ConvertToCSV(logName[1].ToString() + "." + logName[2].ToString(), "copy","csv"); //copy data only from csv and move
                    }
                    else
                    {
                        ConvertToCSV(nFileName, "copy","LOG"); //copy data only from csv and move
                    }
                }


                if (File.Exists(localProcessing + nFileName + ".LOG"))
                {

                    var lines = File.ReadAllLines(localProcessing + nFileName + ".LOG").Select(line => line.Replace("\t", ","));

                    lines = File.ReadAllLines(localProcessing + nFileName + ".LOG").Where(l => !string.IsNullOrWhiteSpace(l));

                    using (var sr = new StreamReader(localProcessing + nFileName + ".LOG"))
                    {
                        string currentLine;
                        int removeCount;
                        
                        fs = null;

                        while ((currentLine = sr.ReadLine()) != null)
                        {
                            HeaderOBJ headerInfo = new HeaderOBJ();
                            var values = currentLine.Split(',');

                            string info = Regex.Replace(values[0], "[@,\\\";'\\\\]", string.Empty);

                            x = values.Length;
                            
                            //use this to ignore lines which are not required
                            if (Regex.Replace(currentLine, "[@\\\";'\\\\]", string.Empty).Contains("UNITS") || Regex.Replace(currentLine, "[@\\\";'\\\\]", string.Empty).Contains("CassStr") || Regex.Replace(currentLine, "[@\\\";'\\\\]", string.Empty).Contains("AttrStr") || Regex.Replace(currentLine, "[@\\\";'\\\\]", string.Empty).Contains("@"))
                            {
                                //ignore
                            }
                            else
                            { 
                                arrCSVInfo.Add(Regex.Replace(currentLine, "[@\\\";'\\\\]", string.Empty).ToUpper());
                            }
                        }
                      //  foreach(var removeItem in arrCSVInfo)
                         for (int n = 0; arrCSVInfo.Count > n; n++)
                         {
                            if (arrCSVInfo[n].ToString().Contains("REF"))
                            {
                                countRemove = n;
                            }
                         }

                        arrLogFileName = new ArrayList();

                                Random rnd = new Random();
                                int random = rnd.Next(1, 999999999);  // creates a number for #LIFTUP rec as the date is not recorded 
                                string[] nWafer = arrCSVInfo[0].ToString().Split(',');

                                string[] casseteName = logFileName.Split('-');

                                string fileMove = localProcessing + "Wafer_" + nWafer[5] + "_Recipe_" + nWafer[1] + "_" + "StepName_" + nWafer[2].ToString().Replace("#", "").Replace("-","_") + "_" + nWafer[3].Replace("/", "_") + "_" + nWafer[4].Replace(":", ".") + "_" + random + "_COMEGA02.csv";

                                arrLogFileName.Add(fileMove);
                                var dataCount = arrLogData.Count;

                        List<string> csvLines = new List<string>();
                        List<string> nItems = new List<string>();
                        List<string> newItems = new List<string>();
                        var newResult = "";

                        using (StreamWriter w = File.AppendText(fileMove)) //move file here
                                {
                                    arrCSVInfo.RemoveRange(0, countRemove);
                                
                            csvLines.Add(arrCSVInfo[0].ToString());

                            //   csvLines.RemoveAt(1);

                            //header values
                            foreach (var newData in csvLines)
                            {
                                newItems.Add(newData.ToString().ToUpper().Replace(" ", "_").Replace("-", "_").Replace("@","_").Replace("#","_"));
                            }

                            newResult = string.Join(",", newItems);
                            arrCSVInfo[0] = (newResult.ToString().ToUpper());

                            for (int i = 0; arrCSVInfo.Count > i; i++)
                                    {
                                        w.WriteLine(arrCSVInfo[i].ToString().ToUpper().Replace(":","."));
                                    }
                                    w.Close();
                                }

                                var csv = File.ReadLines(fileMove) // not AllLines
                                    .Select((line, index) => index == 0
                                     ? "TEST_DATE_TIME," + line
                                        : logDate.Replace("T"," ") + "," + line).Select((line, index) => index == 0
                                     ? "TEST_TIME," + line
                                        : fileCreationTimeHours + "," + line).Select((line, index) => index == 0
                                     ? "TEST_DATE," + line
                                        : fileCreationDate.ToString() + "," + line).Select((line, index) => index == 0
                                     ? "RECIPE_STEP," + line
                                        : nWafer[2] + "," + line).Select((line, index) => index == 0
                                     ? "SEQUENCE," + line
                                        : nWafer[1] + "," + line).Select((line, index) => index == 0
                                     ? "LOT_ID," + line
                                        : nWafer[5] + "," + line).Select((line, index) => index == 0
                                     ? "WAFER_NUMBER," + line
                                         : casseteName[1] + "," + line).Select((line, index) => index == 0
                                     ? "CASSETE," + line
                                         : casseteName[0] + "," + line)
                                  .ToList(); // we should write into the same file, that´s why we materialize

                                System.Threading.Thread.Sleep(1);

                                File.WriteAllLines(fileMove.ToUpper(), csv);

                        var startTime = "";

                        //        arrLogFileData.Add(currentLine.ToString().Split(','));
                        //        string[] lineVal = currentLine.ToString().Split(',');

                        //generating new list only for raw data
                        omegaList = new ArrayList();
                        omega = new ComegaOBJ();

                        logFullName = fileMove.ToString();

                        omega.Cassette = casseteName[0].ToString();
                        omega.Sequence = nWafer[1].ToString();
                        omega.RecName = nWafer[2].ToString();
                        omega.LotNumber = nWafer[5].ToString();
                        omega.Wafer = casseteName[1].ToString();
                        omega.FileName = logFullName.ToString();
                        omega.Name = fileName.ToString();

                        DateTime inDate = DateTime.ParseExact(fileCreationDate + " " + fileCreationTimeHours, "yyyy-MM-dd HH:mm:ss", null);

                        startTime = inDate.ToString("yyyy-MM-ddTHH:mm:ss");
                        omega.Date = startTime;

                        omegaList.Add(omega);
                    }
                }
                DateTime outDate = DateTime.ParseExact(fileCreationDate + " " + fileCreationTimeHours, "yyyy-MM-dd HH:mm:ss", null);

                logDateTime = logDate;
                var headerTime = outDate.ToString("yyyy-MM-ddTHH:mm:ss").Replace(":", ".");

                destAttFile = "";
                destAttFile = nTDSAttachment + nFileName + ".LOG";

                //table data
                if (!File.Exists(nTDSTable + "TABLE_"+nFileName + ".LOG"))
                {
                    File.Copy(logFullName, nTDSTable + "TABLE_" + nFileName + ".LOG");
                }

                //attachment data
                if (!File.Exists(destAttFile))
                {
                    File.Copy(localProcessing + nFileName + ".LOG", destAttFile);
                }

                if (File.Exists(localProcessing + nFileName + ".LOG"))
                {
                    File.Delete(localProcessing + nFileName + ".LOG");
                }

                attachmentLocation = "ATTACHMENT_" +nFileName + ".LOG";
                tableLocation = "TABLE_" +nFileName + ".LOG";

                logFileName = fileName;
                logName = fileName.Split('.');
                logDate = fileLastWriteTime.ToString();

                logFileName = logName[0];

                if (nWaferID != "")
                {
                    try
                    {
                        ArrayList dbFileInfo = mDBAccess.GetFileInformation(nWaferID, fileName.ToString());
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("" + ex);
                        mDBAccess.InsertLogEvent("File Read Error @ 2384", "" + ex);
                    }
                }
            }

            if (fileFullname.Contains("CSTEAG01"))
            {
                sFileType = "equipData";

                testEndDate = fileCreationTime;//.Replace(":", ".");
                oldFile = fileFullname;
                sumFileName = fileName.ToString();

                EquipmentConversionMain(oldFile,sumFileName,"CSTEAG01");
            }

            //CDELTA01 & CDELTA02 
            if (fileFullname.Contains("CDELTA"))
            {
                arrCSVInfo = new ArrayList();
                arrLogHeader = new ArrayList();
                arrLogCassette = new ArrayList();
                arrLogHeader = new ArrayList();
                arrLogWafer = new ArrayList();
                arrLogStep = new ArrayList();
                arrLogSet = new ArrayList();
                arrLogRec = new ArrayList();

                sFileType = "equipData";

                testEndDate = fileCreationTime;//.Replace(":", ".");
                oldFile = fileFullname;

                var csvFormat = fileName.Split('.');
                bool newFile = false;

                System.Threading.Thread.Sleep(1);
                var fs = new FileStream(fileFullname, FileMode.Open, FileAccess.Read);
                var countFS = new FileStream(fileFullname, FileMode.Open, FileAccess.Read);

                totalLines = File.ReadAllLines(fileFullname).Length;

                logFileName = fileName;
                logName = fileName.Split('.');
                logDate = fileCreationTime;

                logFileName = logName[1];
                var nFileName = Path.GetFileNameWithoutExtension(fileName);

                if (logName[1] != null && logName[1] != "")
                {
                    if (logName[1].Contains("TEST"))
                    {
                        ConvertToCSV(logName[1].ToString() + "." + logName[2].ToString(), "copy","csv"); //copy data only from csv and move
                    }
                    else
                    {
                        ConvertToCSV(nFileName, "copy","LOG"); //copy data only from csv and move
                    }
                }


                if (File.Exists(localProcessing + nFileName + ".LOG"))
                {
                    var lines = File.ReadAllLines(localProcessing + nFileName + ".LOG").Select(line => line.Replace("\t", ","));

                    lines = File.ReadAllLines(localProcessing + nFileName + ".LOG").Where(l => !string.IsNullOrWhiteSpace(l));


                    using (var sr = new StreamReader(localProcessing + nFileName + ".LOG"))
                    {
                        string currentLine;
                        fs = null;

                        while ((currentLine = sr.ReadLine()) != null)
                        {
                            HeaderOBJ headerInfo = new HeaderOBJ();
                            var values = currentLine.Split(',');

                            string info = Regex.Replace(values[0], "[@,\\\";'\\\\]", string.Empty);

                            x = values.Length;

                            for (int i = 0; i < x; i++)
                            {
                                string filterValues = Regex.Replace(values[i], "[@,\\\";'\\\\]", string.Empty);
                            }

                            switch (info)
                            {
                                case " Sequence":
                                    for (int i = 0; i < x; i++)
                                    {
                                        if (values[i] != "")
                                        {
                                            arrLogHeader.Add(Regex.Replace(values[i], "[@,\\\";'\\\\]", string.Empty));
                                        }
                                    }
                                    break;
                                case " CASSETTE":
                                    for (int i = 0; i < x; i++)
                                    {
                                        if (values[i] != "")
                                        {
                                            arrLogCassette.Add(Regex.Replace(values[i], "[@,\\\";'\\\\]", string.Empty));
                                        }
                                    }
                                    break;
                                case " Wafer":
                                    for (int i = 0; i < x; i++)
                                    {
                                        if (values[i] != "")
                                        {
                                            arrLogWafer.Add(Regex.Replace(values[i], "[@,\\\";'\\\\]", string.Empty));
                                        }
                                    }
                                    break;
                                case " Recipe":
                                    for (int i = 0; i < x; i++)
                                    {
                                        if (values[i] != "")
                                        {
                                            arrLogRec.Add(Regex.Replace(values[i], "[@,\\\";'\\\\]", string.Empty));
                                        }
                                    }
                                    break;
                                case "     Step":
                                    for (int i = 0; i < x; i++)
                                    {
                                        if (values[i] != "")
                                        {
                                            arrLogStep.Add(Regex.Replace(values[i], "[@,\\\";'\\\\]", string.Empty));
                                        }
                                    }
                                    break;
                                case " Set":
                                    for (int i = 0; i < x; i++)
                                    {
                                        if (values[i] != "")
                                        {
                                            arrLogSet.Add(Regex.Replace(values[i], "[@,\\\";'\\\\]", string.Empty));
                                        }
                                    }
                                    break;
                                default:
                                    break;
                            }
                            if (Regex.Replace(currentLine, "[@\\\";'\\\\]", string.Empty).Contains("CassStr") || Regex.Replace(currentLine, "[@\\\";'\\\\]", string.Empty).Contains("AttrStr") || Regex.Replace(currentLine, "[@\\\";'\\\\]", string.Empty).Contains("Loop") || Regex.Replace(currentLine, "[@\\\";'\\\\]", string.Empty).Contains("Stop"))
                            {
                                //ignore
                            }
                            else
                            {
                                arrCSVInfo.Add(Regex.Replace(currentLine, "[@\\\";'\\\\]", string.Empty).Trim());
                            }
                        }
                        arrCSVInfo.RemoveRange(0, 2);

                        string[] dataTypeVars = new string[] { "SET", "MIN", "MAX", "AVE" };

                        arrLogFileName = new ArrayList();

                        for (int n = 0; arrCSVInfo.Count > n; n++)
                        {
                            string inWafer = "";

                            int nPlace = 0;
                            if (arrCSVInfo.Count <= 7)
                            {
                                nPlace = 6;
                            }
                            if (arrCSVInfo.Count > 8)
                            {
                                nPlace = 7;
                            }

                            if (arrCSVInfo.Count > 0)
                                if (arrCSVInfo[0].ToString().Contains("Recipe"))
                            {
                                Random rnd = new Random();
                                int random = rnd.Next(1, 999999999);  // creates a number for #LIFTUP rec as the date is not recorded 
                                string[] nWafer = arrCSVInfo[0].ToString().Split(',');
                                string[] nRecipe = arrCSVInfo[1].ToString().Split(',');
                                string[] nStep = arrCSVInfo[2].ToString().Split(',');

                                var inDate = DateTime.Parse(nRecipe[4]).ToString("dd/MM/yy");
                                var inTime = DateTime.Parse(nRecipe[5]).ToString("HH:MM:ss");

                                string fileMove = localProcessing + "Wafer_" + logName[1].ToString() + "_" + "StepName_" + nRecipe[2].ToString().Replace("#", "").TrimStart() + "_" + inDate.Replace("/", "_") + "_" + inTime.Replace(":", ".") + "_" + random + "_CDELTA.csv";

                                arrLogFileName.Add(fileMove);
                                var dataCount = arrLogData.Count;

                                using (StreamWriter w = File.AppendText(fileMove)) //move file here
                                {
                                    for (int i = 0; i < 6; i++)
                                    {
                                        if (arrCSVInfo[i].ToString().Contains("Step"))
                                        {

                                        }
                                        else
                                        {
                                            w.WriteLine(arrCSVInfo[i].ToString().Replace("Step", "DataType").ToUpper());
                                        }
                                    }
                                    w.Close();
                                    arrCSVInfo.RemoveRange(0, 6);
                                }

                                var csv = File.ReadLines(fileMove) // not AllLines
                                    .Select((line, index) => index == 0
                                         ? "DataType," + line
                                         : dataTypeVars[index - 1] + "," + line)
                                    .Select((line, index) => index == 0
                                     ? "StepName," + line
                                     : nRecipe[2] + "," + line).Select((line, index) => index == 0
                                     ? "StepNumber," + line
                                     : nRecipe[1] + "," + line).Select((line, index) => index == 0
                                     ? "RecipeName," + line
                                     : arrLogHeader[2] + "," + line).Select((line, index) => index == 0
                                     ? "RecipeNumber," + line
                                     : nRecipe[1] + "," + line).Select((line, index) => index == 0
                                     ? "Date," + line
                                     : inDate + " " + inTime + "," + line).Select((line, index) => index == 0
                                     ? "Wafer," + line
                                     : inWafer + "," + line).Select((line, index) => index == 0
                                         ? "Lotnumber," + line
                                         : arrLogCassette[3] + "," + line).Select((line, index) => index == 0
                                         ? "Sequence," + line
                                         : arrLogHeader[1] + "," + line).Select((line, index) => index == 0
                                         ? "Cassette," + line
                                         : arrLogCassette[1] + "," + line)
                                  .ToList(); // we should write into the same file, that´s why we materialize

                                System.Threading.Thread.Sleep(1);
                                File.WriteAllLines(fileMove, csv);

                                    string[] nZero = csv[0].ToString().Split(',');
                                    //            RemoveCol(fileMove, 8);
                                    RemoveCol(fileMove, 10);
                                    RemoveCol(fileMove, 10);
                                    RemoveCol(fileMove, 10);

                                    RemoveCol(fileMove, (nZero.Length) - 3);
                                }

                            if (arrCSVInfo.Count > 0)
                                if (arrCSVInfo[0].ToString().Contains("Wafer"))
                                {
                                    Random rnd = new Random();
                                    int random = rnd.Next(1, 999999999);  // creates a number for #LIFTUP rec as the date is not recorded 

                                    string[] nWafer = arrCSVInfo[0].ToString().Split(',');
                                    string[] nRecipe = arrCSVInfo[1].ToString().Split(',');
                                    string[] nStep = arrCSVInfo[2].ToString().Split(',');

                                    var inDate = DateTime.Parse(nStep[4]).ToString("dd/MM/yy");
                                var inTime = DateTime.Parse(nStep[5]).ToString("HH:MM:ss");
                                    inWafer = nWafer[1].ToString();
                                    

                                string fileMove = localProcessing + "Wafer_" + logName[1].ToString() + "_" + "StepName_" + nRecipe[2].ToString().Replace("#", "").TrimStart() + "_" + inDate.Replace("/", "_") + "_" + inTime.Replace(":", ".") + "_" + random + "_CDELTA.csv";

                                arrLogFileName.Add(fileMove);
                                    var dataCount = arrLogData.Count;

                                    using (StreamWriter w = File.AppendText(fileMove)) //move file here
                                    {
                                        for (int i = 1; i < 7; i++)
                                        {
                                            if (i == 2)
                                            {

                                            }
                                            else
                                            {
                                                w.WriteLine(arrCSVInfo[i].ToString().Replace("RECIPE", "DataType").ToUpper());
                                            }
                                        }
                                        w.Close();
                                        arrCSVInfo.RemoveRange(0, 7);
                                    }

                                   var csv = File.ReadLines(fileMove) // not AllLines
                                         .Select((line, index) => index == 0
                                         ? "DATATYPE," + line
                                         : dataTypeVars[index-1] + "," + line)
                                        .Select((line, index) => index == 0
                                         ? "STEP_NAME," + line
                                         : nRecipe[2] + "," + line).Select((line, index) => index == 0
                                         ? "STEP_NUMBER," + line
                                         : nRecipe[1] + "," + line).Select((line, index) => index == 0
                                         ? "RECIPE_NAME," + line
                                         : arrLogHeader[2] + "," + line).Select((line, index) => index == 0
                                         ? "RECIPE_NUMBER," + line
                                         : nRecipe[1] + "," + line).Select((line, index) => index == 0
                                         ? "DATE," + line
                                         : inDate + " " + inTime + "," + line).Select((line, index) => index == 0
                                         ? "WAFER," + line
                                         : inWafer + "," + line).Select((line, index) => index == 0
                                             ? "LOT_NUMBER," + line
                                             : arrLogCassette[3] + "," + line).Select((line, index) => index == 0
                                             ? "SEQUENCE," + line
                                             : arrLogHeader[1] + "," + line).Select((line, index) => index == 0
                                             ? "CASSETE," + line
                                             : arrLogCassette[1] + "," + line)
                                      .ToList(); // we should write into the same file, that´s why we materialize


                                    System.Threading.Thread.Sleep(1);
                                    File.WriteAllLines(fileMove, csv);

                                    string[] nZero = csv[0].ToString().Split(',');
                                    //            RemoveCol(fileMove, 8);
                                    RemoveCol(fileMove, 10);
                                    RemoveCol(fileMove, 10);
                                    RemoveCol(fileMove, 10);

                                    RemoveCol(fileMove, (nZero.Length)-3);
                                }

                            if (arrCSVInfo.Count > 0)
                                if (arrCSVInfo[0].ToString().Contains("Waferz") && arrCSVInfo.Count <= 7)
                                {
                                    Random rnd = new Random();
                                    int random = rnd.Next(1, 999999999);  // creates a number for #LIFTUP rec as the date is not recorded 
                                    string[] nWafer = arrCSVInfo[0].ToString().Split(',');
                                    string[] nRecipe = arrCSVInfo[1].ToString().Split(',');
                                    string[] nStep = arrCSVInfo[2].ToString().Split(',');

                                var inDate = DateTime.Parse(nRecipe[4]).ToString("dd/MM/yy");
                                var inTime = DateTime.Parse(nRecipe[5]).ToString("HH:MM:ss");
                                    inWafer = nWafer[1];

                                string fileMove = localProcessing + "Wafer_" + logName[1].ToString() + "_" + "StepName_" + nRecipe[2].ToString().Replace("#", "").TrimStart() + "_" + inDate.Replace("/", "_") + "_" + inTime.Replace(":", ".") + "_" + random + "_CDELTA.csv";

                                arrLogFileName.Add(fileMove);

                                    var dataCount = arrLogData.Count;

                                    using (StreamWriter w = File.AppendText(fileMove)) //move file here
                                    {
                                        for (int i = 2; i < 7; i++)
                                        {

                                            w.WriteLine(arrCSVInfo[i].ToString().Replace("Step", "DataType").ToUpper());
                                        }
                                        w.Close();

                                        arrCSVInfo.RemoveRange(0, 7);
                                    }

                                    var csv = File.ReadLines(fileMove) // not AllLines
                                        .Select((line, index) => index == 0
                                         ? "StepName," + line
                                         : nStep[2] + "," + line).Select((line, index) => index == 0
                                         ? "StepNumber," + line
                                         : nRecipe[3] + "," + line).Select((line, index) => index == 0
                                         ? "RecipeName," + line
                                         : nRecipe[2] + "," + line).Select((line, index) => index == 0
                                         ? "RecipeNumber," + line
                                         : nRecipe[1] + "," + line).Select((line, index) => index == 0
                                         ? "Date," + line
                                         : nRecipe[6] + " " + nRecipe[5] + "," + line).Select((line, index) => index == 0
                                         ? "Wafer," + line
                                         : inWafer + "," + line).Select((line, index) => index == 0
                                             ? "Lotnumber," + line
                                             : arrLogCassette[2] + "," + line).Select((line, index) => index == 0
                                             ? "Sequence," + line
                                             : arrLogHeader[1] + "," + line).Select((line, index) => index == 0
                                             ? "Cassette," + line
                                             : arrLogCassette[1] + "," + line)
                                      .ToList(); // we should write into the same file, that´s why we materialize

                                    System.Threading.Thread.Sleep(5);
                                    File.WriteAllLines(fileMove, csv);

                                    RemoveCol(fileMove, 10);
                                }

                            if (arrCSVInfo.Count > 0)
                                if (arrCSVInfo[0].ToString().Contains("Step"))
                                {
                                    Random rnd = new Random();
                                    int random = rnd.Next(1, 999999999);  // creates a number for #LIFTUP rec as the date is not recorded 
                                    string[] nWafer = arrCSVInfo[0].ToString().Split(',');
                                    string[] nRecipe = arrCSVInfo[1].ToString().Split(',');
                                    string[] nStep = arrCSVInfo[2].ToString().Split(',');

                                var inDate = DateTime.Parse(nRecipe[4]).ToString("dd/MM/yy");
                                var inTime = DateTime.Parse(nRecipe[5]).ToString("HH:MM:ss");

                                string fileMove = localProcessing + "Wafer_" + logName[1].ToString() + "_" + "StepName_" + nRecipe[2].ToString().Replace("#", "").TrimStart() + "_" + inDate.Replace("/", "_") + "_" + inTime.Replace(":", ".") + "_" + random + "_CDELTA.csv";

                                arrLogFileName.Add(fileMove);

                                    var dataCount = arrLogData.Count;

                                    using (StreamWriter w = File.AppendText(fileMove)) //move file here
                                    {
                                        for (int i = 2; i < 7; i++)
                                        {
                                            w.WriteLine(arrCSVInfo[i].ToString().Replace("Step", "DataType").ToUpper());
                                        }
                                        w.Close();

                                        if (arrCSVInfo[2].ToString().Contains("Wafer"))
                                        {
                                            arrCSVInfo.RemoveRange(0, 1);
                                        }
                                        else
                                        {
                                            arrCSVInfo.RemoveRange(2, 5);
                                        }

                                        var csv = File.ReadLines(fileMove) // not AllLines
                                         .Select((line, index) => index == 0
                                         ? "DataType," + line
                                         : dataTypeVars[index - 1] + "," + line)
                                            .Select((line, index) => index == 0
                                             ? "StepName," + line
                                             : nStep[2] + "," + line).Select((line, index) => index == 0
                                             ? "StepNumber," + line
                                             : nRecipe[3] + "," + line).Select((line, index) => index == 0
                                             ? "RecipeName," + line
                                             : nRecipe[2] + "," + line).Select((line, index) => index == 0
                                             ? "RecipeNumber," + line
                                             : nRecipe[1] + "," + line).Select((line, index) => index == 0
                                             ? "Date," + line
                                             : nRecipe[6] + " " + nRecipe[5] + "," + line).Select((line, index) => index == 0
                                             ? "Wafer," + line
                                             : nWafer[1] + "," + line).Select((line, index) => index == 0
                                                 ? "Lotnumber," + line
                                                 : arrLogCassette[2] + "," + line).Select((line, index) => index == 0
                                                 ? "Sequence," + line
                                                 : arrLogHeader[1] + "," + line).Select((line, index) => index == 0
                                                 ? "Cassette," + line
                                                 : arrLogCassette[1] + "," + line)
                                          .ToList(); // we should write into the same file, that´s why we materialize

                                        System.Threading.Thread.Sleep(1);
                                        File.WriteAllLines(fileMove, csv);

                                        /*
                                        RemoveCol(fileMove, 11);
                                        RemoveCol(fileMove, 10);
                                        */
                                        string[] nZero = csv[0].ToString().Split(',');
                                        //            RemoveCol(fileMove, 8);
                                        RemoveCol(fileMove, 10);
                                        RemoveCol(fileMove, 10);
                                        RemoveCol(fileMove, 10);

                                        RemoveCol(fileMove, (nZero.Length) - 3);
                                    }
                                }
                        }
                    }
                }

                System.Threading.Thread.Sleep(1);

                DateTime nDate = Convert.ToDateTime(arrLogCassette[6] +" " + arrLogCassette[7]);

                var headerTime = nDate.ToString("yyyy-MM-ddTHH:mm:ss").Replace(":", ".");

                destAttFile = "";
                destAttFile = nTDSAttachment + nFileName + "_SUMMARY.csv";

                if (!File.Exists(destAttFile))
                {
                    File.Copy(localProcessing + nFileName + ".csv", destAttFile);
                }

                File.Delete(localProcessing + nFileName + ".csv");

                attachmentLocation = "ATTACHMENT_" + nFileName + ".LOG";
                tableLocation = "TABLE_" + nFileName + ".LOG";

                logFileName = fileName;
                logName = fileName.Split('.');
                logDate = fileLastWriteTime.ToString();

                logFileName = logName[0];

                if (nWaferID != "")
                {
                    try
                    {
                        ArrayList dbFileInfo = mDBAccess.GetFileInformation(nWaferID, fileName.ToString());
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("" + ex);
                        mDBAccess.InsertLogEvent("File Read Error @ 2384", "" + ex);
                    }
                }
            }

            System.Threading.Thread.Sleep(1);
            return arrInfo;
        }

        private void PromisInfo(string waferID)
        {
            arrComponents = new ArrayList();
            Promis mPromisManager = new PromisSppSip02();
            mPromisManager.Init(Properties.Settings.Default.pUser, Properties.Settings.Default.pPassword);

            Lot tmplot;
            string err = "";

            try
            {
                if (mPromisManager.GetLotDetails(waferID, out tmplot, out err))
                {
                    inPartNumber = "" + tmplot.mMaterial.PartID;
                    inDeviceType = "" + tmplot.mMaterial.MaterialType;
                    inLotType = "" + tmplot.mSchedule.LotType;
                    inMask = "" + tmplot.mMaterial.MaskShort;
                    if (tmplot.mMaterial.Components.Count > 0)
                    {
                        for (int x = 0; x < tmplot.mMaterial.Components.Count; x++)
                        {
                            if (tmplot.mMaterial.Components[x].State.Equals("Present"))
                            {
                                arrComponents.Add("" + tmplot.mMaterial.Components[x].ID.ToString());
                            }
                        }
                        //arrComponents.Add("" + tmplot.mMaterial.Components[0].ID.ToString());
                    }
                }
                else
                {
                    inPartNumber = "UNKNOWNPN";
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("" + ex);
            }
        }

        private void ComegaConversion(string fileCreationTime, string fileFullname, string fileName, string fileLastWriteTime)
        {
            arrCSVInfo = new ArrayList();
            arrLogHeader = new ArrayList();
            arrLogCassette = new ArrayList();
            arrLogHeader = new ArrayList();
            arrLogWafer = new ArrayList();
            arrLogStep = new ArrayList();
            arrLogSet = new ArrayList();
            arrLogRec = new ArrayList();

            int x;
            sFileType = "equipData";

            testEndDate = fileCreationTime;
            oldFile = fileFullname;

            var csvFormat = fileName.Split('.');
            bool newFile = false;


            System.Threading.Thread.Sleep(1);
            var fs = new FileStream(fileFullname, FileMode.Open, FileAccess.Read);
            var countFS = new FileStream(fileFullname, FileMode.Open, FileAccess.Read);

            totalLines = File.ReadAllLines(fileFullname).Length;

            logFileName = fileName;
            logName = fileName.Split('.');
            logDate = fileCreationTime;
            //   logSummary = @"\\cas-sfl-01\CASFAB\ToolData\COMEGA02\" + fileCreationYear + "\\Logs\\DATALOG\\SUMMARY\\";

            logFileName = logName[1];
            var nFileName = Path.GetFileNameWithoutExtension(fileName);

            if (logName[1] != null && logName[1] != "")
            {
                if (logName[1].Contains("TEST"))
                {
                    ConvertToCSV(logName[1].ToString() + "." + logName[2].ToString(), "copy","csv"); //copy data only from csv and move
                }
                else
                {
                    ConvertToCSV(nFileName, "copy", "LOG"); //copy data only from csv and move
                }
                System.Threading.Thread.Sleep(1);
            }


            if (File.Exists(localProcessing + nFileName + ".LOG"))
            {
                var lines = File.ReadAllLines(localProcessing + nFileName + ".LOG").Select(line => line.Replace("\t", ","));

                lines = File.ReadAllLines(localProcessing + nFileName + ".LOG").Where(l => !string.IsNullOrWhiteSpace(l));


                using (var sr = new StreamReader(localProcessing + nFileName + ".LOG"))
                {
                    string currentLine;
                    fs = null;

                    while ((currentLine = sr.ReadLine()) != null)
                    {
                        HeaderOBJ headerInfo = new HeaderOBJ();
                        var values = currentLine.Split(',');

                        string info = Regex.Replace(values[0], "[@,\\\";'\\\\]", string.Empty);

                        x = values.Length;

                        for (int i = 0; i < x; i++)
                        {
                            string filterValues = Regex.Replace(values[i], "[@,\\\";'\\\\]", string.Empty);
                        }

                        switch (info)
                        {
                            case "Sequence":
                                for (int i = 0; i < x; i++)
                                {
                                    if (values[i] != "")
                                    {
                                        arrLogHeader.Add(Regex.Replace(values[i], "[@,\\\";'\\\\]", string.Empty));
                                    }
                                }
                                break;
                            case "Cassette":
                                for (int i = 0; i < x; i++)
                                {
                                    if (values[i] != "")
                                    {
                                        arrLogCassette.Add(Regex.Replace(values[i], "[@,\\\";'\\\\]", string.Empty));
                                    }
                                }
                                break;
                            case "Wafer":
                                for (int i = 0; i < x; i++)
                                {
                                    if (values[i] != "")
                                    {
                                        arrLogWafer.Add(Regex.Replace(values[i], "[@,\\\";'\\\\]", string.Empty));
                                    }
                                }
                                break;
                            case "Recipe":
                                for (int i = 0; i < x; i++)
                                {
                                    if (values[i] != "")
                                    {
                                        arrLogRec.Add(Regex.Replace(values[i], "[@,\\\";'\\\\]", string.Empty));
                                    }
                                }
                                break;
                            case "Step":
                                for (int i = 0; i < x; i++)
                                {
                                    if (values[i] != "")
                                    {
                                        arrLogStep.Add(Regex.Replace(values[i], "[@,\\\";'\\\\]", string.Empty));
                                    }
                                }
                                break;
                            case "Set":
                                for (int i = 0; i < x; i++)
                                {
                                    if (values[i] != "")
                                    {
                                        arrLogSet.Add(Regex.Replace(values[i], "[@,\\\";'\\\\]", string.Empty));
                                    }
                                }
                                break;
                            default:
                                break;
                        }
                        if (Regex.Replace(currentLine, "[@\\\";'\\\\]", string.Empty).Contains("CassStr") || Regex.Replace(currentLine, "[@\\\";'\\\\]", string.Empty).Contains("AttrStr"))
                        {
                            //ignore
                        }
                        else
                        {
                            arrCSVInfo.Add(Regex.Replace(currentLine, "[@\\\";'\\\\]", string.Empty));
                        }
                    }
                    arrCSVInfo.RemoveRange(0, 2);

                    int[] removeCol = new int[3] { 10, 11, 12 };

                    arrLogFileName = new ArrayList();

                    for (int n = 0; arrCSVInfo.Count > n; n++)
                    {
                        int nPlace = 0;
                        if (arrCSVInfo.Count <= 7)
                        {
                            nPlace = 6;
                        }
                        if (arrCSVInfo.Count > 8)
                        {
                            nPlace = 7;
                        }
                        System.Threading.Thread.Sleep(1);
                        if (arrCSVInfo.Count > 0)
                            if (arrCSVInfo[nPlace].ToString().Contains("Wafer"))
                            {
                                Random rnd = new Random();
                                int random = rnd.Next(1, 999999999);  // creates a number for #LIFTUP rec as the date is not recorded 
                                string[] nWafer = arrCSVInfo[0].ToString().Split(',');
                                string[] nRecipe = arrCSVInfo[1].ToString().Split(',');
                                string[] nStep = arrCSVInfo[2].ToString().Split(',');

                                string fileMove = localProcessing + "Wafer_" + nWafer[1] + "_" + nRecipe[0] + "_" + nRecipe[1] + "_" + nRecipe[2] + "_Step_" + nStep[1] + "_" + "StepName_" + nStep[2].ToString().Replace("#", "") + "_" + nRecipe[6].Replace("/", "_") + "_" + nRecipe[5].Replace(":", ".") + "_" + random + "_COMEGA01.csv";

                                arrLogFileName.Add(fileMove);
                                var dataCount = arrLogData.Count;

                                using (StreamWriter w = File.AppendText(fileMove)) //move file here
                                {
                                    for (int i = 2; i < 7; i++)
                                    {
                                        w.WriteLine(arrCSVInfo[i].ToString().Replace("Step", "DataType").ToUpper());
                                    }
                                    w.Close();
                                    arrCSVInfo.RemoveRange(0, 7);
                                }

                                var csv = File.ReadLines(fileMove) // not AllLines
                                    .Select((line, index) => index == 0
                                     ? "StepName," + line
                                     : nStep[2] + "," + line).Select((line, index) => index == 0
                                     ? "StepNumber," + line
                                     : nRecipe[3] + "," + line).Select((line, index) => index == 0
                                     ? "RecipeName," + line
                                     : nRecipe[2] + "," + line).Select((line, index) => index == 0
                                     ? "RecipeNumber," + line
                                     : nRecipe[1] + "," + line).Select((line, index) => index == 0
                                     ? "Date," + line
                                     : nRecipe[6] + " " + nRecipe[5] + "," + line).Select((line, index) => index == 0
                                     ? "Wafer," + line
                                     : nWafer[1] + "," + line).Select((line, index) => index == 0
                                         ? "Lotnumber," + line
                                         : arrLogCassette[2] + "," + line).Select((line, index) => index == 0
                                         ? "Sequence," + line
                                         : arrLogHeader[1] + "," + line).Select((line, index) => index == 0
                                         ? "Cassette," + line
                                         : arrLogCassette[1] + "," + line)
                                  .ToList(); // we should write into the same file, that´s why we materialize

                                System.Threading.Thread.Sleep(5);
                                File.WriteAllLines(fileMove, csv);

                                RemoveCol(fileMove, 11);
                                RemoveCol(fileMove, 10);
                            }

                        if (arrCSVInfo.Count > 0)
                            if (arrCSVInfo[0].ToString().Contains("Wafer") && arrCSVInfo.Count <= 7)
                            {
                                Random rnd = new Random();
                                int random = rnd.Next(1, 999999999);  // creates a number for #LIFTUP rec as the date is not recorded 
                                string[] nWafer = arrCSVInfo[0].ToString().Split(',');
                                string[] nRecipe = arrCSVInfo[1].ToString().Split(',');
                                string[] nStep = arrCSVInfo[2].ToString().Split(',');

                                string fileMove = localProcessing + "Wafer_" + nWafer[1] + "_" + nRecipe[0] + "_" + nRecipe[1] + "_" + nRecipe[2] + "_Step_" + nStep[1] + "_" + "StepName_" + nStep[2].ToString().Replace("#", "") + "_" + nRecipe[6].Replace("/", "_") + "_" + nRecipe[5].Replace(":", ".") + "_" + random + "_COMEGA01.csv";
                                arrLogFileName.Add(fileMove);

                                var dataCount = arrLogData.Count;

                                using (StreamWriter w = File.AppendText(fileMove)) //move file here
                                {
                                    //  var csv = new StringBuilder();
                                    for (int i = 2; i < 7; i++)
                                    {
                                        w.WriteLine(arrCSVInfo[i].ToString().Replace("Step", "DataType").ToUpper());
                                    }
                                    w.Close();

                                    arrCSVInfo.RemoveRange(0, 7);
                                }

                                var csv = File.ReadLines(fileMove) // not AllLines
                                    .Select((line, index) => index == 0
                                     ? "StepName," + line
                                     : nStep[2] + "," + line).Select((line, index) => index == 0
                                     ? "StepNumber," + line
                                     : nRecipe[3] + "," + line).Select((line, index) => index == 0
                                     ? "RecipeName," + line
                                     : nRecipe[2] + "," + line).Select((line, index) => index == 0
                                     ? "RecipeNumber," + line
                                     : nRecipe[1] + "," + line).Select((line, index) => index == 0
                                     ? "Date," + line
                                     : nRecipe[6] + " " + nRecipe[5] + "," + line).Select((line, index) => index == 0
                                     ? "Wafer," + line
                                     : nWafer[1] + "," + line).Select((line, index) => index == 0
                                         ? "Lotnumber," + line
                                         : arrLogCassette[2] + "," + line).Select((line, index) => index == 0
                                         ? "Sequence," + line
                                         : arrLogHeader[1] + "," + line).Select((line, index) => index == 0
                                         ? "Cassette," + line
                                         : arrLogCassette[1] + "," + line)
                                  .ToList(); // we should write into the same file, that´s why we materialize

                                System.Threading.Thread.Sleep(5);
                                File.WriteAllLines(fileMove, csv);

                                RemoveCol(fileMove, 11);
                                RemoveCol(fileMove, 10);
                            }

                        if (arrCSVInfo.Count > 0)
                            if (arrCSVInfo[nPlace].ToString().Contains("Step"))
                            {
                                Random rnd = new Random();
                                int random = rnd.Next(1, 999999999);  // creates a number for #LIFTUP rec as the date is not recorded 
                                string[] nWafer = arrCSVInfo[0].ToString().Split(',');
                                string[] nRecipe = arrCSVInfo[1].ToString().Split(',');
                                string[] nStep = arrCSVInfo[2].ToString().Split(',');

                                string fileMove = localProcessing + "Wafer_" + nWafer[1] + "_" + nRecipe[0] + "_" + nRecipe[1] + "_" + nRecipe[2] + "_Step_" + nStep[1] + "_" + "StepName_" + nStep[2].ToString().Replace("#", "") + "_" + nRecipe[6].Replace("/", "_") + "_" + nRecipe[5].Replace(":", ".") + "_" + random + "_COMEGA01.csv";
                                arrLogFileName.Add(fileMove);

                                var dataCount = arrLogData.Count;

                                using (StreamWriter w = File.AppendText(fileMove)) //move file here
                                {
                                    //  var csv = new StringBuilder();
                                    for (int i = 2; i < 7; i++)
                                    {
                                        w.WriteLine(arrCSVInfo[i].ToString().Replace("Step", "DataType").ToUpper());
                                    }
                                    w.Close();

                                    if (arrCSVInfo[2].ToString().Contains("Wafer"))
                                    {
                                        arrCSVInfo.RemoveRange(0, 1);
                                    }
                                    else
                                    {
                                        arrCSVInfo.RemoveRange(2, 5);
                                    }

                                    var csv = File.ReadLines(fileMove) // not AllLines
                                        .Select((line, index) => index == 0
                                         ? "StepName," + line
                                         : nStep[2] + "," + line).Select((line, index) => index == 0
                                         ? "StepNumber," + line
                                         : nRecipe[3] + "," + line).Select((line, index) => index == 0
                                         ? "RecipeName," + line
                                         : nRecipe[2] + "," + line).Select((line, index) => index == 0
                                         ? "RecipeNumber," + line
                                         : nRecipe[1] + "," + line).Select((line, index) => index == 0
                                         ? "Date," + line
                                         : nRecipe[6] + " " + nRecipe[5] + "," + line).Select((line, index) => index == 0
                                         ? "Wafer," + line
                                         : nWafer[1] + "," + line).Select((line, index) => index == 0
                                             ? "Lotnumber," + line
                                             : arrLogCassette[2] + "," + line).Select((line, index) => index == 0
                                             ? "Sequence," + line
                                             : arrLogHeader[1] + "," + line).Select((line, index) => index == 0
                                             ? "Cassette," + line
                                             : arrLogCassette[1] + "," + line)
                                      .ToList(); // we should write into the same file, that´s why we materialize

                                    System.Threading.Thread.Sleep(5);
                                    File.WriteAllLines(fileMove, csv);

                                    RemoveCol(fileMove, 11);
                                    RemoveCol(fileMove, 10);
                                }
                            }

                        if (arrCSVInfo.Count > 0)
                        {
                            if (arrCSVInfo.Count <= 7)
                            {
                                nPlace = 6;
                            }
                            if (arrCSVInfo[nPlace].ToString().Contains("Recipe") || arrCSVInfo[2].ToString().Contains("Recipe"))
                            {
                                Random rnd = new Random();
                                int random = rnd.Next(1, 999999999);  // creates a number for #LIFTUP rec as the date is not recorded 

                                string[] nWafer = arrCSVInfo[0].ToString().Split(',');
                                string[] nRecipe = arrCSVInfo[1].ToString().Split(',');
                                string[] nStep = arrCSVInfo[2].ToString().Split(',');

                                //    string fileMove = localProcessing + "Wafer_" + nWafer[1] + "_" + nRecipe[0] + "_" + nRecipe[1] + "_" + nRecipe[2] + "_Step_" + nStep[1] + "_" + nRecipe[6].Replace("/", "_") + "_" + nRecipe[5].Replace(":", ".") + "_" + random + "_COMEGA.csv";
                                string fileMove = localProcessing + "Wafer_" + nWafer[1] + "_" + nRecipe[0] + "_" + nRecipe[1] + "_" + nRecipe[2] + "_Step_" + nStep[1] + "_" + "StepName_" + nStep[2].ToString().Replace("#", "") + "_" + nRecipe[6].Replace("/", "_") + "_" + nRecipe[5].Replace(":", ".") + "_" + random + "_COMEGA01.csv";
                                arrLogFileName.Add(fileMove);

                                var dataCount = arrLogData.Count;

                                using (StreamWriter w = File.AppendText(fileMove)) //move file here
                                {
                                    for (int i = 2; i < 7; i++)
                                    {
                                        w.WriteLine(arrCSVInfo[i].ToString().Replace("Step", "DataType").ToUpper());
                                    }
                                    w.Close();

                                    if (arrCSVInfo[2].ToString().Contains("Recipe"))
                                    {
                                        arrCSVInfo.RemoveRange(1, 1);
                                    }
                                    if (arrCSVInfo[2].ToString().Contains("Wafer"))
                                    {
                                        arrCSVInfo.RemoveRange(0, 1);
                                    }
                                    if (arrCSVInfo[nPlace].ToString().Contains("Recipe,2") || arrCSVInfo[nPlace].ToString().Contains("Recipe,3") || arrCSVInfo[nPlace].ToString().Contains("Recipe,4") || arrCSVInfo[nPlace].ToString().Contains("Recipe,5"))
                                    {
                                        arrCSVInfo.RemoveRange(1, 6);
                                    }

                                    else
                                    {
                                        arrCSVInfo.RemoveRange(2, 5);
                                    }
                                }

                                var csv = File.ReadLines(fileMove) // not AllLines
                                    .Select((line, index) => index == 0
                                     ? "StepName," + line
                                     : nStep[2] + "," + line).Select((line, index) => index == 0
                                     ? "StepNumber," + line
                                     : nStep[1] + "," + line).Select((line, index) => index == 0
                                     ? "RecipeName," + line
                                     : nRecipe[2] + "," + line).Select((line, index) => index == 0
                                     ? "RecipeNumber," + line
                                     : nRecipe[1] + "," + line).Select((line, index) => index == 0
                                     ? "Date," + line
                                     : nRecipe[6] + " " + nRecipe[5] + "," + line).Select((line, index) => index == 0
                                     ? "Wafer," + line
                                     : nWafer[1] + "," + line).Select((line, index) => index == 0
                                         ? "Lotnumber," + line
                                         : arrLogCassette[2] + "," + line).Select((line, index) => index == 0
                                         ? "Sequence," + line
                                         : arrLogHeader[1] + "," + line).Select((line, index) => index == 0
                                         ? "Cassette," + line
                                         : arrLogCassette[1] + "," + line)
                                  .ToList(); // we should write into the same file, that´s why we materialize

                                System.Threading.Thread.Sleep(5);
                                File.WriteAllLines(fileMove, csv);

                                RemoveCol(fileMove, 11);
                                RemoveCol(fileMove, 10);
                            }
                        }
                    }
                }
            }
            // File.Move(nConverted + csvFormat[0] + ".csv", nTDSTable + csvFormat[0] + ".csv");
            System.Threading.Thread.Sleep(1);
            DateTime outDate = DateTime.ParseExact(arrLogCassette[5] + " " + arrLogCassette[6], "dd/MM/yy HH:mm:ss", null);

            //    DateTime outDate = Convert.ToDateTime(logItem[3]);
            //    startTime = outDate.ToString("yyyy-MM-ddTHH:mm:ss").Replace(":", ".");
            var headerTime = outDate.ToString("yyyy-MM-ddTHH:mm:ss").Replace(":", ".");

            destAttFile = "";
            destAttFile = nTDSAttachment + nFileName + "_SUMMARY.csv";

            if (!File.Exists(destAttFile))
            {
                File.Copy(localProcessing + nFileName + ".csv", destAttFile);
            }

            if (File.Exists(localProcessing + nFileName + ".csv"))
            {
                File.Delete(localProcessing + nFileName + ".csv");
            }
            attachmentLocation = "ATTACHMENT_" + nFileName + ".LOG";
            tableLocation = "TABLE_" + nFileName + ".LOG";

            logFileName = fileName;
            logName = fileName.Split('.');
            logDate = fileLastWriteTime.ToString();
            //    logSummary = @"\\cas-sfl-01\CASFAB\ToolData\COMEGA02\" + fileCreationYear.ToString() + "\\Logs\\DATALOG\\SUMMARY\\";

            logFileName = logName[0];

            if (nWaferID != "")
            {
                try
                {
                    ArrayList dbFileInfo = mDBAccess.GetFileInformation(nWaferID, fileName.ToString());
                }
                catch (Exception ex)
                {
                    Console.WriteLine("" + ex);
                    mDBAccess.InsertLogEvent("File Read Error @ 2384", "" + ex);
                }
            }
        }

        #endregion

        #endregion

        #region CSTEGA01
        public bool CreateXML_CSTEAG01(string srcFileFullName)
        {
            try
            {
                cls_tdsmodel xmlResultRoot = new cls_tdsmodel()
                {
                    Result = "Done"
                };
                var cSTEAG = this.GetCSTEAGInfo(srcFileFullName);
                if (cSTEAG.CSVDicList.Count() > 0)
                {
                    var tmpFolder = localProcessing;
                    var csv = this.GenerateCSTEAGCsv(cSTEAG);
                    var csvFileName = string.Format("{0}_{1}_{2}.csv", cSTEAG.SerialNumber, cSTEAG.Recipe,
                        cSTEAG.StartDateTime.ToString("MM.dd.yyyy.HHmmss"));
                    var localCsvFileName = localProcessing + csvFileName;
                    var netCsvFileName = nConverted + csvFileName;
                    if (File.Exists(localCsvFileName)) File.Delete(localCsvFileName);
                    File.WriteAllText(localCsvFileName, csv);

                    TestStep testStep = new TestStep()
                    {
                        Name = "SN_" + cSTEAG.SerialNumber,
                        startDateTime = cSTEAG.StartDateTime.ToString("s"),
                        endDateTime = cSTEAG.StartDateTime.ToString("s"),
                        Status = "Done"
                    };

                    testStep._DataTable.Add(new DataTable()
                    {
                        Name = "CSTEAG01 LOG",
                        Description = "SUMMARY DATA",
                        Value = netCsvFileName,
                        Status = "Done"
                    });
                    xmlResultRoot.TestStep.Add(testStep);

                    //Header                                
                    var xmlHeader = this.GenerateXmlHeader(cSTEAG.SerialNumber, "CSTEAG01", cSTEAG.StartDateTime, cSTEAG.StartDateTime);
                    xmlResultRoot.Header = xmlHeader;

                    //Build Xml Info
                    //    var objCfg = Program.Obj_config;
                    string xmlStr = string.Empty;

                    string xmlResultFileName = localProcessing
                            + string.Format(@"\Site={0},ProductFamily={1},Operation={2},PartNumber={3},SerialNumber={4},TestDate={5}.xml",
                            "250", "CAS FAB", "CSTEAG01", "NA",
                            cSTEAG.SerialNumber, cSTEAG.StartDateTime.ToString("yyyy-MM-ddTHH.mm.ss"));
                    if (File.Exists(xmlResultFileName))
                        File.Delete(xmlResultFileName);
                    File.WriteAllText(xmlResultFileName, xmlStr);
                    this._XmlAttachList.Add(xmlResultFileName);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            RemoveLogFile();
            return true;
        }
        private CSTEAGClass GetCSTEAGInfo(string srcFileFullName)
        {
            CSTEAGClass cSTEAG = new CSTEAGClass();
            cSTEAG.CSVDicList = new Dictionary<string, List<double>>();
            try
            {
                string lineStr = string.Empty;
                string[] cells = null;
                var summaryLines = File.ReadAllLines(srcFileFullName);
                //For Content
                double rate = 0;
                double number = 0;
                int addRowCount = 0;
                //All Content start from _samples key
                int contentStartRow = Array.IndexOf(summaryLines, summaryLines.Where(r => r.Contains("_samples")).FirstOrDefault());
                //Get Header
                for (int i = 0; i < summaryLines.Count(); i++)
                {
                    lineStr = summaryLines[i];
                    if (lineStr.TrimStart(' ').Equals("$"))
                    {
                        i++;
                        continue;
                    }
                    if (string.IsNullOrEmpty(lineStr) == false && lineStr.Contains("$"))
                    {
                        lineStr = lineStr.TrimStart(' ').TrimStart('$');
                        lineStr = lineStr.Replace("$", ",").Replace(", ,", ",");
                        cells = lineStr.Split(',');
                        switch (cells[0])
                        {
                            case "_matid":
                                cSTEAG.SerialNumber = cells[1];
                                break;
                            case "_recipe":
                                cSTEAG.Recipe = cells[1];
                                break;
                            case "_operator":
                                cSTEAG.Operator = cells[1];
                                break;
                            case "_time":
                                cSTEAG.StartDateTime = Convert.ToDateTime(cells[1]);
                                break;
                            default:
                                if (i > contentStartRow) //Content
                                {
                                    var resList = new List<double>();
                                    var timeList = new List<double>();
                                    var dList = new List<double>();
                                    rate = double.Parse(new string(cells[1].Where(c => char.IsDigit(c)).ToArray()));
                                    number = int.Parse(new string(cells[2].Where(c => char.IsDigit(c)).ToArray()));
                                    addRowCount = Convert.ToInt32(Math.Ceiling(number / 10)); //each row data has 10 columns
                                    i = i + 2; //add 2 rows to find the data content
                                    var unit = rate / 1000; //ms to sec
                                    for (double j = 0; j < number * unit; j = j + unit) //Get Timing by number * unit
                                    {
                                        if (j == 0)
                                            timeList.Add(j);
                                        timeList.Add(j + unit);
                                    }
                                    timeList.RemoveRange(Convert.ToInt32(number), 1); //Remove extra 1 data
                                    cSTEAG.CSVDicList.Add(cells[0].ToUpper().Replace(" ", "_") + "_TIMING", timeList);
                                    var data = summaryLines.ToList().GetRange(i, addRowCount);
                                    foreach (var item in data) //Get all data and combine them into one list
                                    {
                                        dList = item.TrimEnd(',').Split(',').Select(x => double.Parse(x)).ToList();
                                        resList.AddRange(dList);
                                    }
                                    cSTEAG.CSVDicList.Add(cells[0].ToUpper().Replace(" ", "_") + "_SAMPLING", resList);
                                    i = i + addRowCount; //Move to next Test Parameter by using row count
                                }
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return cSTEAG;
        }
        public bool EquipmentConversionMain(string fullFileName, string fileName, string eqpType)
        {
            List<string> toBeProcessedFileList = new List<string>();
            string tempFolder = localProcessing;
            try
            {
                EQPLogConversionHelper eqpHelper = new EQPLogConversionHelper();
                //bool result = false;
                //string eqpType = ConfigurationManager.AppSettings["Equipment Type"].ToString();
                //string summaryFolder = ConfigurationManager.AppSettings[string.Format("{0} Summary Source Folder", eqpType)].ToString();
                //Create Temp Folder if not exists
                if (Directory.Exists(tempFolder) == false) Directory.CreateDirectory(tempFolder);
                return eqpHelper.EQPMain(fullFileName, fileName, eqpType);
            }
            catch (Exception ex)
            {
                //    Error_Message = System.Reflection.MethodBase.GetCurrentMethod().Name + ": " + ex.Message;
                return false;
            }
        }
        private string GenerateCSTEAGCsv(CSTEAGClass cSTEAG)
        {
            var dic = cSTEAG.CSVDicList;
            StringBuilder csv = new StringBuilder();
            try
            {
                var maxRow = dic.Select(r => r.Value.Count()).Max();
                csv.Append("SerialNumber,");
                csv.AppendLine(String.Join(",", dic.Select(r => r.Key)));
                for (int i = 0; i < maxRow; i++)
                {
                    csv.Append(cSTEAG.SerialNumber + ",");
                    csv.AppendLine(String.Join(",", dic.Select(r => r.Value.Count > i ? r.Value[i].ToString() : "")));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return csv.ToString();
        }
        #endregion

        #region Utilities

        //This one is used for COMEGA log data structuring
        private Header GenerateXmlHeader(string serialNum, string testStation, DateTime startDate, DateTime overAllMaxDate)
        {
            Header header = new Header();
            try
            {
                header.Partnumber = "UNKNOWNPN";
                header.Devicetype = "LOT";
                header.Site = "250";
                header.Operation = "CAS FAB";
                header.Teststation = testStation;
                header.Serialnumber = serialNum;
                header.OperatorId = "NA";
                header.Result = "Done";
                header.Starttime = startDate.ToString("s");
                header.Endtime = overAllMaxDate.ToString("s");
            }
            catch (Exception)
            {
                throw;
            }
            return header;
        }
        #endregion
    }
}
