﻿namespace XMLConvertor
{
    partial class frmConverterMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnLaunch = new System.Windows.Forms.Button();
            this.btnAppend = new System.Windows.Forms.Button();
            this.btnWaferInfo = new System.Windows.Forms.Button();
            this.btnStrip = new System.Windows.Forms.Button();
            this.btnReadSpec = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnLaunch
            // 
            this.btnLaunch.Location = new System.Drawing.Point(16, 15);
            this.btnLaunch.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnLaunch.Name = "btnLaunch";
            this.btnLaunch.Size = new System.Drawing.Size(132, 41);
            this.btnLaunch.TabIndex = 10;
            this.btnLaunch.Text = "Convert";
            this.btnLaunch.UseVisualStyleBackColor = true;
            // 
            // btnAppend
            // 
            this.btnAppend.Location = new System.Drawing.Point(179, 15);
            this.btnAppend.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnAppend.Name = "btnAppend";
            this.btnAppend.Size = new System.Drawing.Size(121, 41);
            this.btnAppend.TabIndex = 11;
            this.btnAppend.Text = "Append";
            this.btnAppend.UseVisualStyleBackColor = true;
            // 
            // btnWaferInfo
            // 
            this.btnWaferInfo.Location = new System.Drawing.Point(16, 148);
            this.btnWaferInfo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnWaferInfo.Name = "btnWaferInfo";
            this.btnWaferInfo.Size = new System.Drawing.Size(137, 48);
            this.btnWaferInfo.TabIndex = 12;
            this.btnWaferInfo.Text = "Wafer Info";
            this.btnWaferInfo.UseVisualStyleBackColor = true;
            // 
            // btnStrip
            // 
            this.btnStrip.Location = new System.Drawing.Point(16, 79);
            this.btnStrip.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnStrip.Name = "btnStrip";
            this.btnStrip.Size = new System.Drawing.Size(137, 42);
            this.btnStrip.TabIndex = 13;
            this.btnStrip.Text = "Get Part";
            this.btnStrip.UseVisualStyleBackColor = true;
            // 
            // btnReadSpec
            // 
            this.btnReadSpec.Location = new System.Drawing.Point(306, 262);
            this.btnReadSpec.Name = "btnReadSpec";
            this.btnReadSpec.Size = new System.Drawing.Size(139, 43);
            this.btnReadSpec.TabIndex = 14;
            this.btnReadSpec.Text = "Read Spec";
            this.btnReadSpec.UseVisualStyleBackColor = true;
            this.btnReadSpec.Click += new System.EventHandler(this.btnReadSpec_Click);
            // 
            // frmConverterMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(457, 317);
            this.Controls.Add(this.btnReadSpec);
            this.Controls.Add(this.btnStrip);
            this.Controls.Add(this.btnWaferInfo);
            this.Controls.Add(this.btnAppend);
            this.Controls.Add(this.btnLaunch);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "frmConverterMain";
            this.Text = "Converter";
            this.Load += new System.EventHandler(this.frmConverterMain_Load);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnLaunch;
        private System.Windows.Forms.Button btnAppend;
        private System.Windows.Forms.Button btnWaferInfo;
        private System.Windows.Forms.Button btnStrip;
        private System.Windows.Forms.Button btnReadSpec;
    }
}

