﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace XMLConvertor
{
    class DBEpi
    {
        private static string sPassword = "Sw33per123";
        private static string sUsername = @"sweeper";
        private static string sServerName = @"96LT1Z2\SQLEXPRESS";
        private static string sDatabaseName = @"FileSweepEngine";

        private SqlConnection mSqlServerConnection = null;
        public ArrayList arrLocations = new ArrayList();

        public DBEpi()
        {

        }

        public void ConnectionOpen()
        {
            string CurrentDatabase = null;
            try
            {
                mSqlServerConnection = new SqlConnection(@"server =" + sServerName + "; database =" + sDatabaseName + ";  user=" + sUsername + "; password=" + sPassword + ";Connection Timeout=30; Trusted_Connection=False");
                CurrentDatabase = "Server=" + sServerName + "; Database=" + sDatabaseName + "; User=" + sUsername;
                mSqlServerConnection.Open();
            }
            catch (SqlException DBError)
            {

            }
        }

        public void ConnectionClose()
        {
            mSqlServerConnection.Close();
        }

        public ArrayList GetLocationInfo()
        {
            ConnectionOpen();
            ArrayList arrLocation = new ArrayList();

            StringBuilder SelectionCmnd = new StringBuilder("SELECT * FROM [" + sDatabaseName + "].[dbo].SweepLocation");
            SqlCommand GetData = new SqlCommand(SelectionCmnd.ToString(), mSqlServerConnection);

            SqlDataReader ReadData = null;

            try
            {
                ReadData = GetData.ExecuteReader();
                if (ReadData.HasRows)
                {
                    while (ReadData.Read())
                    {
                        SweepInfo ThisInfo = new SweepInfo();

                        ThisInfo.MaterialType = ReadData["Material"].ToString();
                        ThisInfo.FolderLocation = ReadData["Location"].ToString();
                        arrLocation.Add(ThisInfo);
                    }
                    return arrLocation;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception MyDBException)
            {
                throw MyDBException;
            }
        }
    }
}
