﻿using log4net;
using log4net.Appender;
using log4net.Repository.Hierarchy;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[assembly: log4net.Config.DOMConfigurator(Watch = true)]
[assembly: log4net.Config.XmlConfigurator()]
namespace SystemLibrary.Utility
{
    public class LogHelper
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static void WriteLine(string message, params object[] args)
        {
            log.Debug(string.Format(message, args));
            Console.WriteLine(message, args);
        }

        public static void WriteLine(string message)
        {
            log.Debug(message);
            Console.WriteLine(message);
        }

        public static void DeleteLog()
        {
            try
            {
                DateTime date = DateTime.Now.AddDays(-7);
                string filename = GetLatestLogFile();

                if (!string.IsNullOrEmpty(filename))
                {
                    FileInfo file = new FileInfo(filename);
                    DirectoryInfo dir = file.Directory;

                    FileInfo[] files = dir.GetFiles().Where(f => f.LastWriteTime <= date).ToArray();

                    foreach (var f in files)
                    {
                        f.Delete();
                    }
                }
            }
            catch (Exception e)
            {
                WriteLine(e.Message);
            }
        }

        public static string GetLatestLogFile()
        {
            var rootAppender = ((Hierarchy)LogManager.GetRepository())
             .Root.Appenders.OfType<FileAppender>()
             .FirstOrDefault();

            return rootAppender != null ? rootAppender.File : string.Empty;
        }
    }
}
