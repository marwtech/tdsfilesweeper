﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SystemLibrary.Utility
{
    public class UiHelper
    {
        TextBox _msgBox;

        public UiHelper(ref TextBox msgBox)
        {
            _msgBox = msgBox;
        }

        public void WriteLine(string msg, bool isResetText, bool needLoging)
        {
            if (_msgBox.InvokeRequired)
            {
                _msgBox.Invoke(new Action(() => SetTextBoxValue(msg, isResetText, needLoging)));
            }
            else
            {
                SetTextBoxValue(msg, isResetText, needLoging);
            }
        }

        public void WriteLine(string msg, bool isResetText)
        {
            WriteLine(msg, isResetText,false);
        }

        public void WriteLine(string msg, params object[] args)
        {
            WriteLine(string.Format(msg, args), true, false);
        }

        private void SetTextBoxValue(string msg, bool isResetText, bool needLoging)
        {
            if (isResetText)
                _msgBox.Text = msg;
            else
                _msgBox.Text += "\r\n" + msg;
            _msgBox.SelectionStart = _msgBox.Text.Length;
            _msgBox.ScrollToCaret();

            if (needLoging)
            {
                LogHelper.WriteLine(msg);
            }
        }

        private void SetTextBoxValue(string msg, bool isResetText)
        {
            SetTextBoxValue(msg, isResetText, false);
        }
    }
}
