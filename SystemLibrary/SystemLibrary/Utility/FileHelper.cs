﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemLibrary.Utility
{
    static public class FileHelper
    {
        static public void Copy(string sourceFileName, string destFileFolder, string destFileName)
        {
            DirectoryInfo dir = new DirectoryInfo(destFileFolder);
            if (!dir.Exists)
            {
                dir.Create();
            }

            File.Copy(sourceFileName, destFileFolder + "\\" + destFileName, true);
        }

        static public void Move(string sourceFileName, string destFileFolder, string destFileName)
        {
            string filePath = destFileFolder + "\\" + destFileName;
            FileInfo targetFile = new FileInfo(filePath);

            if (File.Exists(sourceFileName))
            {
                if (targetFile.Exists)
                {
                    File.Copy(filePath, destFileFolder + "\\" + targetFile.Name.Replace(targetFile.Extension, "_archive_") + DateTime.Now.ToString("yyyyMMdd_HHmmss") + targetFile.Extension);
                    targetFile.Delete();
                }

                File.Move(sourceFileName, destFileFolder + "\\" + destFileName);
            }
        }

        static public void WriteAllText(string folder, string fileName, string contents)
        {
            DirectoryInfo dir = new DirectoryInfo(folder);
            if (!dir.Exists)
            {
                dir.Create();
            }

            string filePath = folder + "\\" + fileName;
            FileInfo file = new FileInfo(filePath);
            if (file.Exists)
            {
                file.Delete();
            }

            File.WriteAllText(filePath, contents);
        }


    }
}
