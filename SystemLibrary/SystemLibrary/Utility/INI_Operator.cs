﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.IO;

namespace SystemLibrary.Utility
{
    public class INI_Operator
    {

        //INI_Operator.WriteIniData("Config", "user", "2", INI_TEMPLATE);//add a key "user" under section "Config" value "2"
        //INI_Operator.WriteIniData("Config", "user", null, INI_TEMPLATE);//delete "user" key under section "Config"
        //INI_Operator.WriteIniData("test", null, null, INI_TEMPLATE);// delete section "test"
        //INI_Operator.WriteIniData("Config", "user", "", INI_TEMPLATE);//add section "Config" with key "user"


        #region 
        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section, string key,
            string val, string filePath);

        [DllImport("kernel32")]
        private static extern long GetPrivateProfileString(string section, string key,
            string def, StringBuilder retVal, int size, string filePath);
        #endregion
        
        public static string ReadIniData(string Section, string Key, string NoText, string iniFilePath)
        {
            if (File.Exists(iniFilePath))
            {
                StringBuilder temp = new StringBuilder(1024);
                GetPrivateProfileString(Section, Key, NoText, temp, 1024, iniFilePath);
                return temp.ToString();
            }
            else
            {
                return String.Empty;
            }
        }

        public static bool WriteIniData(string Section, string Key, string Value, string iniFilePath)
        {
            if (File.Exists(iniFilePath))
            {
                long OpStation = WritePrivateProfileString(Section, Key, Value, iniFilePath);
                if (OpStation == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return false;
            }
        }
    }
}
