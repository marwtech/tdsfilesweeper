﻿/* function:send a mail easily by confige a configure file
 * date:2015.8.10
 * auto:wen anxu 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Net;
using System.Configuration;
using log4net;
using System.Net.Mime;
using System.Reflection;

namespace SystemLibrary.Utility
{
    public class MailUtility
    {
        string EmailServer = ConfigurationManager.AppSettings["EmailServer"];
        string EmailSender = ConfigurationManager.AppSettings["EmailSender"];
        string EmailSubject = ConfigurationManager.AppSettings["EmailSubject"];
        string SoftwareVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString();

        public bool SendMail(string subject, string message, string attachmentFile)
        {
            string emailTo = ConfigurationManager.AppSettings["EmailTo"];
            string emailCc = ConfigurationManager.AppSettings["EmailCc"];
            string emailBcc = ConfigurationManager.AppSettings["EmailBcc"];

            return SendMail(emailTo, emailCc, emailBcc, subject, message, attachmentFile);
        }

        public bool SendMail(string message)
        {
            string sysEmailTo = ConfigurationManager.AppSettings["SysEmailTo"];
            return SendMail(sysEmailTo, "", "", "System Exception Mail", message, "");
        }

        public bool SendMail(string emailTo, string emailCc, string emailBcc, string subject, string message, string attachmentFile)
        {
            return SendMail(emailTo, emailCc, emailBcc, subject, message, new string[] { attachmentFile });
        }

        public bool SendMail(string emailTo, string emailCc, string emailBcc, string subject, string message, string[] attachmentFile)
        {
            SmtpClient smtp = new SmtpClient(EmailServer);
            MailMessage mail = new MailMessage();

            try
            {
                mail.From = new MailAddress(EmailSender);
                emailTo = emailTo.Replace(";", ",");
                mail.To.Add(emailTo);
                mail.Subject = EmailSubject + ":" + subject;
                mail.Body = message + "\r\n\r\nVersion:" + SoftwareVersion + " ;Executed at:" + System.Net.Dns.GetHostName() + " ;Executed User:" + Environment.UserName;
                if (!string.IsNullOrEmpty(emailBcc))
                {
                    mail.Bcc.Add(emailBcc);
                }

                if (!string.IsNullOrEmpty(emailCc))
                {
                    mail.CC.Add(emailCc);
                }

                //if (!string.IsNullOrEmpty(attachmentFile))
                if (attachmentFile == null ? false : attachmentFile.Count() > 0)
                {
                    attachmentFile = attachmentFile.Where(x => !string.IsNullOrEmpty(x)).ToArray();
                    foreach (var file in attachmentFile)
                    {
                        System.Net.Mail.Attachment attachment;
                        attachment = new System.Net.Mail.Attachment(file);
                        mail.Attachments.Add(attachment);
                    }
                }

                smtp.Send(mail);
            }
            catch (Exception e)
            {
                string msg = "error " + e.Message + "\r\n";
                msg += "error " + e.StackTrace + "\r\n\r\n";
                msg += "SendMail";
                LogHelper.WriteLine(msg);
                throw;
            }
            finally
            {
                mail.Dispose();
                smtp.Dispose();
            }

            return true;
        }
    }
}
