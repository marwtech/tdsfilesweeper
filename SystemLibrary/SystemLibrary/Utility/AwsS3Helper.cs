﻿using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemLibrary.Utility
{
    public class AwsS3Helper
    {
        string baseDirectory = AppDomain.CurrentDomain.BaseDirectory + "App_Data";
        string _profilename;

        public AwsS3Helper(string sProfilename)
        {
            try
            {
                _profilename = sProfilename;
                StoredProfileAWSCredentials Con = new StoredProfileAWSCredentials(_profilename, baseDirectory);
                AmazonS3Client client = new AmazonS3Client(Con, Amazon.RegionEndpoint.USWest2);
            }
            catch (Exception e)
            {
                string msg = e.Message;
                throw;
            }
        }

        /// <summary>
        /// <param name="bucketName">bucket name.</param>
        /// <param name="keyName">key name when object is created.</param>
        /// <param name="filePath">Usedabsolute path to a sample file to upload.</param>
        /// </summary>
        /// 
        public void UploadToAWS(string bucketName, string keyName, string filePath)
        {
            try
            {
                StoredProfileAWSCredentials Con = new StoredProfileAWSCredentials(_profilename, baseDirectory);
                AmazonS3Client client = new AmazonS3Client(Con, Amazon.RegionEndpoint.USWest2);

                PutObjectRequest putRequest = new PutObjectRequest
                {
                    BucketName = bucketName,
                    Key = keyName,
                    FilePath = filePath,
                    ContentType = "text/plain"
                };
                //putRequest.Metadata.Add("x-amz-meta-title", "someTitle");

                PutObjectResponse response = client.PutObject(putRequest);
            }
            catch (Exception e)
            {
                string msg = e.Message;
                throw;
            }
        }
    }
}
