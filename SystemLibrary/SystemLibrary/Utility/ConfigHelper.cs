﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace SystemLibrary.Utility
{
    public class ConfigHelper
    {
        static string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
        public static void Update(string key, string value)
        {

            string configName = Assembly.GetCallingAssembly().ManifestModule.Name + ".config";
            string ConfigurePath = baseDirectory + configName;
            System.Configuration.ConfigurationManager.AppSettings[key] = value;

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(ConfigurePath);
            XmlNodeList nodeList = xmlDoc.SelectSingleNode("/configuration/appSettings").ChildNodes;
            foreach (XmlNode xn in nodeList)
            {
                if (xn is XmlElement)
                {
                    XmlElement xe = (XmlElement)xn;

                    if (xe.GetAttribute("key") == key)
                    {
                        xe.SetAttribute("value", value);
                        break;
                    }
                }
            }
            xmlDoc.Save(ConfigurePath);

        }
    }
}
