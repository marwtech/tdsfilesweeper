﻿using SystemLibrary.Utility;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.Odbc;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
//using System.Data.OracleClient;
using Oracle.ManagedDataAccess.Client;

namespace SystemLibrary.Utility
{
    public class DBHelper
    {
        int CommandTimeout = 60000;
        
        string RegexPattern = @"[\[\]\(\)\#\s\/]";

        public DBHelper()
        {
            try
            {
                string commandTimeoutSec = ConfigurationManager.AppSettings["CommandTimeoutSec"];
                CommandTimeout = int.Parse(commandTimeoutSec);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public DbConnection GetDBConn(string sDBAliasName)
        {
            DbProviderFactory dbpf = null;
            DbConnection dbcn = null;

            try
            {
                string providerName = ConfigurationManager.ConnectionStrings[sDBAliasName].ProviderName.ToString();
                dbpf = DbProviderFactories.GetFactory(providerName);
                dbcn = dbpf.CreateConnection();

                string connString = ConfigurationManager.ConnectionStrings[sDBAliasName].ToString();
                //LogHelper.WriteLine("connect:" + connString);
                dbcn.ConnectionString = connString;
                if (dbcn.State == ConnectionState.Closed)
                {
                    try
                    {
                        dbcn.Open();
                    }
                    catch (Exception ex)
                    {
                        throw (ex);
                    }
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            return dbcn;
        }

        public OracleConnection GetOraConn(string sDBAliasName)
        {
            string connString = ConfigurationManager.ConnectionStrings[sDBAliasName].ToString();
            OracleConnection conn = new OracleConnection(connString);
            try
            {
                conn.Open();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            return conn;
        }


        public DataTable GetDataTable(DbConnection dbcn, string sql)
        {
            return GetDataTable(dbcn, null, sql);
        }

        public DataTable GetDataTable(DbConnection dbcn, DbTransaction tran, string sql)
        {
            DataTable dt = new DataTable();
            try
            {
                using (DbCommand dbcm = dbcn.CreateCommand())
                {
                    dbcm.CommandText = sql;
                    dbcm.CommandType = CommandType.Text;
                    dbcm.CommandTimeout = CommandTimeout;
                    if (tran != null)
                        dbcm.Transaction = tran;
                    //using SqlCeCommand
                    using (DbDataReader Rdr = dbcm.ExecuteReader())
                    {
                        //Create datatable to hold schema and data seperately
                        //Get schema of our actual table
                        DataTable DTSchema = Rdr.GetSchemaTable();
                        //DataTable DT = new DataTable();
                        if (DTSchema != null)
                        {
                            if (DTSchema.Rows.Count > 0)
                            {
                                for (int i = 0; i < DTSchema.Rows.Count; i++)
                                {
                                    //Create new column for each row in schema table
                                    //Set properties that are causing errors and add it to our datatable
                                    //Rows in schema table are filled with information of columns in our actual table
                                    DataColumn Col = new DataColumn(DTSchema.Rows[i]["ColumnName"].ToString(), (Type)DTSchema.Rows[i]["DataType"]);
                                    Col.AllowDBNull = true;
                                    Col.Unique = false;
                                    Col.AutoIncrement = false;
                                    dt.Columns.Add(Col);
                                }
                            }
                        }

                        while (Rdr.Read())
                        {
                            //Read data and fill it to our datatable
                            DataRow Row = dt.NewRow();
                            for (int i = 0; i < dt.Columns.Count; i++)
                            {
                                Row[i] = Rdr[i];
                            }
                            dt.Rows.Add(Row);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            //This is our datatable filled with data
            return dt;
        }

        public DataTable GetDataTable(OracleConnection dbcn, string sql)
        {
            return GetDataTable(dbcn, null, sql);
        }

        public DataTable GetDataTable(OracleConnection dbcn, DbTransaction tran, string sql)
        {
            DataTable dt = new DataTable();
            try
            {
                using (DbCommand dbcm = dbcn.CreateCommand())
                {
                    dbcm.CommandText = sql;
                    dbcm.CommandType = CommandType.Text;
                    dbcm.CommandTimeout = CommandTimeout;
                    if (tran != null)
                        dbcm.Transaction = tran;
                    //using SqlCeCommand
                    using (DbDataReader Rdr = dbcm.ExecuteReader())
                    {
                        //Create datatable to hold schema and data seperately
                        //Get schema of our actual table
                        DataTable DTSchema = Rdr.GetSchemaTable();
                        //DataTable DT = new DataTable();
                        if (DTSchema != null)
                        {
                            if (DTSchema.Rows.Count > 0)
                            {
                                for (int i = 0; i < DTSchema.Rows.Count; i++)
                                {
                                    //Create new column for each row in schema table
                                    //Set properties that are causing errors and add it to our datatable
                                    //Rows in schema table are filled with information of columns in our actual table
                                    DataColumn Col = new DataColumn(DTSchema.Rows[i]["ColumnName"].ToString(), (Type)DTSchema.Rows[i]["DataType"]);
                                    Col.AllowDBNull = true;
                                    Col.Unique = false;
                                    Col.AutoIncrement = false;
                                    dt.Columns.Add(Col);
                                }
                            }
                        }

                        while (Rdr.Read())
                        {
                            //Read data and fill it to our datatable
                            DataRow Row = dt.NewRow();
                            for (int i = 0; i < dt.Columns.Count; i++)
                            {
                                Row[i] = Rdr[i];
                            }
                            dt.Rows.Add(Row);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            //This is our datatable filled with data
            return dt;
        }

        public int UpdateData(DbConnection dbcn, DbTransaction tran, string sql)
        {
            return ExecuteNonQuery(dbcn, tran, sql);
        }

        public int UpdateData(DbConnection dbcn, string sql)
        {
            return ExecuteNonQuery(dbcn, null, sql);
        }

        public bool InsertData(DbConnection dbcn, DbTransaction tran, string sql, DataColumnCollection targetColumns, DataRow sourceData)
        {
            bool result = false;
            string msg = "";
            try
            {
                DbCommand cmd = dbcn.CreateCommand();
                cmd.Parameters.Clear();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = sql;

                string tempSourceColumnName = sourceData.Table.Columns[0].ColumnName;
                int sourceAliasIdx = tempSourceColumnName.IndexOf(".");
                string sourceAliasName = tempSourceColumnName.Substring(0, sourceAliasIdx + 1);

                Regex regex = new Regex(RegexPattern);

                foreach (DataColumn col in targetColumns)
                {
                    var columnName = col.ColumnName;
                    if (columnName.ToUpper() != "ID")
                    {
                        var value = new object();
                        string tempColName = sourceAliasName + columnName;
                        if (sourceData.Table.Columns.Contains(tempColName))
                        {
                            if (col.DataType.Name == "DateTime")
                            {
                                DateTime tmp = DateTime.Now;
                                if (DateTime.TryParse(sourceData[tempColName].ToString(), out tmp))
                                {
                                    value = tmp;
                                }
                                else
                                {
                                    value = DateTime.Now;
                                }
                            }
                            else
                            {
                                value = sourceData[tempColName];
                            }
                        }
                        else
                        {
                            switch (col.DataType.Name)
                            {
                                case "DateTime":
                                    value = DateTime.Now;
                                    break;
                                case "String":
                                    value = "";
                                    break;
                                case "Double":
                                    value = 0.0;
                                    break;
                                case "Boolean":
                                    value = false;
                                    break;
                                default:
                                    value = null;
                                    break;
                            }
                        }

                        var parameter = cmd.CreateParameter();
                        parameter.ParameterName = "@" + regex.Replace(columnName, "");
                        parameter.Value = value;
                        cmd.Parameters.Add(parameter);

                        msg += columnName + "[" + value + "],";
                    }
                }
                cmd.Transaction = tran;
                cmd.ExecuteNonQuery();
                cmd.Dispose();

                result = true;
            }
            catch (Exception e1)
            {
                throw new Exception(e1.Message + "\r\n" + msg);
            }
            return result;
        }

        public int InsertData(DbConnection dbcn, DbTransaction tran, string sql)
        {
            return ExecuteNonQuery(dbcn, tran, sql);
        }

        public int DeleteData(DbConnection dbcn, DbTransaction tran, string sql)
        {
            return ExecuteNonQuery(dbcn, tran, sql);
        }

        private int ExecuteNonQuery(DbConnection dbcn, DbTransaction tran, string sql)
        {
            int iRows = 0;

            try
            {
                DbCommand dbcm = dbcn.CreateCommand();
                dbcm.CommandText = sql;
                if (tran != null)
                    dbcm.Transaction = tran;
                iRows = dbcm.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            return iRows;
        }

        public string List2InCond(List<string> list)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var str in list)
            {
                sb.AppendFormat("'{0}',", str);
            }
            if (sb.Length > 1) sb.Length--;
            return sb.ToString();
        }
    }
}