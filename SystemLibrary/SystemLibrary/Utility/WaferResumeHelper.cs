﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using SystemLibrary.EFModels;

namespace SystemLibrary.Utility
{
    public class WaferResumeHelper
    {
        string _url = ConfigurationManager.AppSettings["WaferResumeUrl"];

        public WaferResumeHelper()
        {
            if (string.IsNullOrEmpty(_url))
            {
                throw new Exception("cannot find the WaferResumeUrl in config file");
            }
        }

        public bool Logger(TBL_WAFER_RESUME header, Dictionary<string,string> waferData)
        {
            header.Created_By = header.Created_By ?? (System.Environment.MachineName);

            List<TBL_WAFER_RESUME_ITEM> wafer_item_list = new List<TBL_WAFER_RESUME_ITEM>();

            foreach (var key in waferData.Keys)
            {
                var value = waferData[key];
                if (value == null)
                    continue;

                wafer_item_list.Add(new TBL_WAFER_RESUME_ITEM() { Type = header.Type, Key = key, Value = value.ToString().Trim() });
            }

            var anonyObj = new { wafer_header = header, wafer_item_list };
            return SendJson(anonyObj, _url);
        }

        public bool Logger(List<object> anonyObjs)
        {
            return SendJson(anonyObjs, _url);
        }

        public bool Logger(TBL_WAFER_RESUME header, object waferData)
        {
            var properties = waferData.GetType().GetProperties();
            return Logger(header, waferData, properties);
        }

        public bool Logger(TBL_WAFER_RESUME header, object waferData, string[] propertyNames)
        {
            var properties = waferData.GetType().GetProperties().Where(x => propertyNames.Contains(x.Name)).ToArray();
            return Logger(header, waferData, properties);
        }

        private bool Logger(TBL_WAFER_RESUME header, object waferData, PropertyInfo[] properties)
        {
            header.Created_By = header.Created_By ?? (System.Environment.MachineName);

            List<TBL_WAFER_RESUME_ITEM> wafer_item_list = new List<TBL_WAFER_RESUME_ITEM>();

            foreach (var property in properties)
            {
                var value = property.GetValue(waferData);
                if (value == null)
                    continue;

                if (property.PropertyType == typeof(string) ? string.IsNullOrEmpty(value.ToString()) : false)
                    continue;

                if (property.PropertyType == typeof(DateTime) || property.PropertyType == typeof(DateTime?))
                {
                    value = ((DateTime)value).ToString("yyyy-MM-dd HH:mm:ss");
                }

                wafer_item_list.Add(new TBL_WAFER_RESUME_ITEM() { Type = header.Type, Key = property.Name, Value = value.ToString() });
            }

            var anonyObj = new { wafer_header = header, wafer_item_list };
            return SendJson(anonyObj, _url);
        }

        private bool SendJson(object anonyObj, string url)
        {
            WebClient webClient = new WebClient();
            byte[] resByte;
            string resString;
            byte[] reqString;

            try
            {
                Stopwatch sw = new Stopwatch();
                sw = Stopwatch.StartNew();

                webClient.Headers.Add("Content-Type", "application/json");
                reqString = Encoding.Default.GetBytes(JsonConvert.SerializeObject(anonyObj));
                resByte = webClient.UploadData(url, reqString);
                resString = Encoding.Default.GetString(resByte);
                Console.WriteLine(resString);
                webClient.Dispose();

                sw.Stop();
                //Console.WriteLine(" ElapsedMilliseconds:  " + sw.ElapsedMilliseconds);
                return true;
            }
            catch (Exception ex)
            {
                string errMsg = ex.Message + "\r\n" + ex.StackTrace;
                LogHelper.WriteLine(errMsg);                
                throw;
            }
        }
    }
}
