using System;
using System.Text;
using System.Collections.Generic;


namespace SystemLibrary.Models {
    
    public class TblSysServiceMaster {
        public virtual int? Id { get; set; }
        public virtual string SystemName { get; set; }
        public virtual string ServiceName { get; set; }
        public virtual string ControllerName { get; set; }
        public virtual string ActionName { get; set; }
        public virtual DateTime? ModifyDate { get; set; }
    }
}
