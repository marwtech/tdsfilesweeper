using System;
using System.Text;
using System.Collections.Generic;


namespace SystemLibrary.Models {
    
    public class VwSysAuthorization {
        public virtual string Id { get; set; }
        public virtual string UserId { get; set; }
        public virtual string SystemName { get; set; }
        public virtual string RoleName { get; set; }
        public virtual int? PageIndex { get; set; }
        public virtual string ControllerName { get; set; }
        public virtual string ActionName { get; set; }
        public virtual string ServiceName { get; set; }
        public virtual bool? ReadOnly { get; set; }
        public virtual string FunctionName { get; set; }
    }
}
