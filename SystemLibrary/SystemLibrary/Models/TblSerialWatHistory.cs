using System;
using System.Text;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace SystemLibrary.Models {

    public class TblSerialWatHistory
    {
        public virtual int? Id { get; set; }
        public virtual string IqeBouleid { get; set; }
        public virtual string EpiReactor { get; set; }
        public virtual string EpiType { get; set; }
        public virtual string WaferId { get; set; }
        public virtual DateTime? LoadedDate { get; set; }
        public virtual string DispoDate { get; set; }
        public virtual string PartId { get; set; }
        public virtual string FabWaferId { get; set; }
        public virtual string DispositionCode { get; set; }
        public virtual string RemapRequired { get; set; }
        public virtual string TestResult { get; set; }
        public virtual string BinThree { get; set; }
        public virtual string Comment { get; set; }
        public virtual string LineNumber { get; set; }
        public virtual string DieCount { get; set; }

        ////remove
        //[Display(Name = "spec failure")]
        //public virtual string SpecFailure { get; set; }

        //[Display(Name = "drift failure")]
        //public virtual string DriftFailure { get; set; }

        //[Display(Name = "t0 avg nf (dppm)")]
        //public virtual string T0AvgNfDppm { get; set; }

        //[Display(Name = "post bi avg nf (dppm)")]
        //public virtual string PostBiAvgNfDppm { get; set; }

        //[Display(Name = "post wat avg nf (dppm)")]
        //public virtual string PostWatAvgNfDppm { get; set; }

        //[Display(Name = "shippable zone (dppm)")]
        //public virtual string ShippableZoneDppm { get; set; }

        //[Display(Name = "exclusion zone (dppm)")]
        //public virtual string ExclusionZoneDppm { get; set; }

        //[Display(Name = "t0 max nf (#/worst die)")]
        //public virtual string T0MaxNfWorstDie { get; set; }

        //[Display(Name = "post bi max nf (#/worst die)")]
        //public virtual string PostBiMaxNfWorstDie { get; set; }

        //[Display(Name = "post wat max nf (#/worst die)")]
        //public virtual string PostWatMaxNfWorstDie { get; set; }

        //[Display(Name = "max po drift post wat (w)")]
        //public virtual string MaxPoDriftPostWatW { get; set; }

        //[Display(Name = "avg po drift post wat (w)")]
        //public virtual string AvgPoDriftPostWatW { get; set; }

        //[Display(Name = "min po drift post wat (w)")]
        //public virtual string MinPoDriftPostWatW { get; set; }

        //[Display(Name = "max vf drift post wat (v)")]
        //public virtual string MaxVfDriftPostWatV { get; set; }

        //[Display(Name = "avg vf drift post wat (v)")]
        //public virtual string AvgVfDriftPostWatV { get; set; }

        //[Display(Name = "min vf drift post wat (v)")]
        //public virtual string MinVfDriftPostWatV { get; set; }

        //[Display(Name = "max na drift post wat")]
        //public virtual string MaxNaDriftPostWat { get; set; }

        //[Display(Name = "avg na drift post wat")]
        //public virtual string AvgNaDriftPostWat { get; set; }

        //[Display(Name = "min na drift post wat")]
        //public virtual string MinNaDriftPostWat { get; set; }

        //[Display(Name = "dead emitter map radar ref")]
        //public virtual string DeadEmitterMapRadarRef { get; set; }

        //[Display(Name = "max wl l drift post wat (nm)")]
        //public virtual string MaxWlLDriftPostWatNm { get; set; }

        //[Display(Name = "avg wl l drift post wat (nm)")]
        //public virtual string AvgWlLDriftPostWatNm { get; set; }

        //[Display(Name = "min wl l drift post wat (nm)")]
        //public virtual string MinWlLDriftPostWatNm { get; set; }

        //[Display(Name = "max wl r drift post wat (nm)")]
        //public virtual string MaxWlRDriftPostWatNm { get; set; }

        //[Display(Name = "avg wl r drift post wat (nm)")]
        //public virtual string AvgWlRDriftPostWatNm { get; set; }

        //[Display(Name = "min wl r drift post wat (nm)")]
        //public virtual string MinWlRDriftPostWatNm { get; set; }

        //[Display(Name = "t0 neighbor failures (#)")]
        //public virtual string T0NeighborFailures { get; set; }

        //[Display(Name = "post bi neighbor failures (#)")]
        //public virtual string PostBiNeighborFailures { get; set; }

        //[Display(Name = "post wat neighbor failures (#)")]
        //public virtual string PostWatNeighborFailures { get; set; }

        //[Display(Name = "max wl drift post wat (nm)")]
        //public virtual string MaxWlDriftPostWatNm { get; set; }

        //[Display(Name = "avg wl drift post wat (nm)")]
        //public virtual string AvgWlDriftPostWatNm { get; set; }

        //[Display(Name = "min wl drift post wat (nm)")]
        //public virtual string MinWlDriftPostWatNm { get; set; }

        //[Display(Name = "max w0 drift post wat")]
        //public virtual string MaxW0DriftPostWat { get; set; }

        //[Display(Name = "avg w0 drift post wat")]
        //public virtual string AvgW0DriftPostWat { get; set; }

        //[Display(Name = "min w0 drift post wat")]
        //public virtual string MinW0DriftPostWat { get; set; }

        //[Display(Name = "max dense uni drift post wat")]
        //public virtual string MaxDenseUniDriftPostWat { get; set; }

        //[Display(Name = "max sparse uni drift post wat")]
        //public virtual string MaxSparseUniDriftPostWat { get; set; }

        //[Display(Name = "max T0 CBP r post wat")]
        //public virtual string MaxT0CbpRPostWat { get; set; }

        //[Display(Name = "avg T0 CBP r post wat")]
        //public virtual string AvgT0CbpRPostWat { get; set; }

        //[Display(Name = "min T0 CBP r post wat")]
        //public virtual string MinT0CbpRPostWat { get; set; }

        //[Display(Name = "max T0 CBP a post wat")]
        //public virtual string MaxT0CbpAPostWat { get; set; }

        //[Display(Name = "avg T0 CBP a post wat")]
        //public virtual string AvgT0CbpAPostWat { get; set; }

        //[Display(Name = "min T0 CBP a post wat")]
        //public virtual string MinT0CbpAPostWat { get; set; }

        //[Display(Name = "max T0 CBP q post wat")]
        //public virtual string MaxT0CbpQPostWat { get; set; }

        //[Display(Name = "avg T0 CBP q post wat")]
        //public virtual string AvgT0CbpQPostWat { get; set; }

        //[Display(Name = "min T0 CBP q post wat")]
        //public virtual string MinT0CbpQPostWat { get; set; }

        //[Display(Name = "max Te CBP r post wat")]
        //public virtual string MaxTeCbpRPostWat { get; set; }

        //[Display(Name = "avg Te CBP r post wat")]
        //public virtual string AvgTeCbpRPostWat { get; set; }

        //[Display(Name = "min Te CBP r post wat")]
        //public virtual string MinTeCbpRPostWat { get; set; }

        //[Display(Name = "max Te CBP a post wat")]
        //public virtual string MaxTeCbpAPostWat { get; set; }

        //[Display(Name = "avg Te CBP a post wat")]
        //public virtual string AvgTeCbpAPostWat { get; set; }

        //[Display(Name = "min Te CBP a post wat")]
        //public virtual string MinTeCbpAPostWat { get; set; }

        //[Display(Name = "max Te CBP q post wat")]
        //public virtual string MaxTeCbpQPostWat { get; set; }

        //[Display(Name = "avg Te CBP q post wat")]
        //public virtual string AvgTeCbpQPostWat { get; set; }

        //[Display(Name = "min Te CBP q post wat")]
        //public virtual string MinTeCbpQPostWat { get; set; }

        //[Display(Name = "Failure")]
        //public virtual string Failure { get; set; }

        //[DataType(DataType.MultilineText)]
        //[Display(Name = "Final Grading Result Remark")]
        //public virtual string FinalGradingResultRemark { get; set; }
        ////remove

        public virtual string Filename { get; set; }
        public virtual string SourceTable { get; set; }
        public virtual string SourceDate { get; set; }
        public virtual string LoadedMonth { get; set; }
        public virtual DateTime? InsertionDate { get; set; }
        public virtual string UpdateBy { get; set; }
        public virtual string WaitReleased { get; set; }
        public virtual DateTime? HistoryInsertionDate { get; set; }
        public virtual string HistoryType { get; set; }
        public virtual DateTime? ModifyDate { get; set; }

        public virtual string ParamCol { get; set; }
    }
}
