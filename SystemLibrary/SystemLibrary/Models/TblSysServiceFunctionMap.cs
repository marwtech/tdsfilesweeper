using System;
using System.Text;
using System.Collections.Generic;


namespace SystemLibrary.Models {
    
    public class TblSysServiceFunctionMap {
        public virtual int Id { get; set; }
        public virtual string UserId { get; set; }
        public virtual string ServiceName { get; set; }
        public virtual string FunctionName { get; set; }
    }
}
