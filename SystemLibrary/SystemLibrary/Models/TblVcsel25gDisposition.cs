using System;
using System.Text;
using System.Collections.Generic;


namespace SystemLibrary.Models {
    
    public class TblVcsel25gDisposition {
        public virtual string Id { get; set; }
        public virtual string LotId { get; set; }
        public virtual string WaferId { get; set; }
        public virtual string AutoGradingStatus { get; set; }
        public virtual DateTime? AutoGradingDate { get; set; }
        public virtual string AutoGradingBy { get; set; }
        public virtual float? Yield1X1 { get; set; }
        public virtual float? Yield1X4 { get; set; }
        public virtual string PreAoi1X1Testar { get; set; }
        public virtual string PreAoi1X4Testar { get; set; }
        public virtual string GradingExcelFile { get; set; }
        public virtual string IsFirstPass { get; set; }
        public virtual string SpecName { get; set; }
        public virtual string SpecVersion { get; set; }
        public virtual string GradingSoftwareVersion { get; set; }
        public virtual string EngDispositionStatus { get; set; }
        public virtual DateTime? EngDispositionDate { get; set; }
        public virtual string EngDispositionBy { get; set; }
        public virtual string Comment { get; set; }
        public virtual string SblSylResult { get; set; }
        public virtual string Sbl { get; set; }
        public virtual string Syl { get; set; }
        public virtual DateTime? PerAoiUploadDate { get; set; }
        public virtual bool? IsReleased { get; set; }
        public virtual DateTime? CreationDate { get; set; }
        public virtual DateTime? UpdatedDate { get; set; }
    }
}
