using System;
using System.Text;
using System.Collections.Generic;


namespace SystemLibrary.Models {
    
    public class TblBinCodeSummary {
        public virtual int? Id { get; set; }
        public virtual string NewBinCode { get; set; }
        public virtual string Device { get; set; }
        public virtual string WinPnpBinCode { get; set; }
        public virtual bool? IsFixed { get; set; }
        public virtual bool? CannotAppear { get; set; }
    }
}
