using System;
using System.Text;
using System.Collections.Generic;


namespace SystemLibrary.Models {
    
    public class TblSerialWatHoldWafer {
        public virtual int? Id { get; set; }
        public virtual string WaferId { get; set; }
        public virtual string HoldNote { get; set; }
        public virtual bool? Enabled { get; set; }
        public virtual string UpdateBy { get; set; }
        public virtual DateTime? ModifyDate { get; set; }
    }
}
