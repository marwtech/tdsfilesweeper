using System;
using System.Text;
using System.Collections.Generic;


namespace SystemLibrary.Models {

    public class TblSysRoleServiceMap
    {
        public virtual int? Id { get; set; }
        public virtual string SystemName { get; set; }
        public virtual string RoleName { get; set; }
        public virtual string ServiceName { get; set; }
        public virtual int? PageIndex { get; set; }
        public virtual DateTime? ModifyDate { get; set; }
    }
}
