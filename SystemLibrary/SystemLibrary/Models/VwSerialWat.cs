using System;
using System.Text;
using System.Collections.Generic;


namespace SystemLibrary.Models {
    
    public class VwSerialWat {
        public virtual string Id { get; set; }
        public virtual string WaferId { get; set; }
        public virtual string EpiType { get; set; }
        public virtual string Filename { get; set; }
        public virtual string SourceTable { get; set; }
        public virtual DateTime? LoadedDate { get; set; }
        public virtual string DispoDate { get; set; }
        public virtual string PartId { get; set; }
        public virtual string FabWaferId { get; set; }
        public virtual string DispositionCode { get; set; }
        public virtual string RemapRequired { get; set; }
        public virtual string TestResult { get; set; }
        public virtual string BinThree { get; set; }
        public virtual string Comment { get; set; }
        public virtual string SourceDate { get; set; }
        public virtual string LoadedMonth { get; set; }        
        public virtual DateTime? InsertionDate { get; set; }
        public virtual DateTime? HistoryInsertionDate { get; set; }
        public virtual string HistoryType { get; set; }
    }
}
