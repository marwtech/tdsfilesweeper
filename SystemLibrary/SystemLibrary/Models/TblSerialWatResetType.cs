using System;
using System.Text;
using System.Collections.Generic;


namespace SystemLibrary.Models {
    
    public class TblSerialWatResetType {
        public virtual int? Id { get; set; }
        public virtual string ResetType { get; set; }
        public virtual DateTime? ModifyDate { get; set; }
    }
}
