using System;
using System.Text;
using System.Collections.Generic;


namespace SystemLibrary.Models {
    
    public class TblProductColumn {
        public virtual int? Id { get; set; }
        public virtual string ProductType { get; set; }
        public virtual int? Priority { get; set; }
        public virtual string ColumnName { get; set; }
        public virtual string SourceTable { get; set; }
    }
}
