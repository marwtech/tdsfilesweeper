using System;
using System.Text;
using System.Collections.Generic;


namespace SystemLibrary.Models {
    
    public class TblHiveSynchronizeSetting {
        public virtual int? Id { get; set; }
        public virtual string TargetTableName { get; set; }
        public virtual string CompareDateColumn { get; set; }
        public virtual string TimeDifference { get; set; }
        public virtual string SourceSql { get; set; }
        public virtual bool? DeleteBeforeInsert { get; set; }
        public virtual string ExternalCriteria { get; set; }
    }
}
