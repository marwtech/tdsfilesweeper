using System;
using System.Text;
using System.Collections.Generic;


namespace SystemLibrary.Models {
    
    public class TblWinWipDispositionHistory {
        public virtual string WaferId { get; set; }
        public virtual DateTime? HistoryInsertionDate { get; set; }
        public virtual string AutoDispositionStatus { get; set; }
        public virtual DateTime? AutoDispositionDate { get; set; }
        public virtual string Yield { get; set; }
        public virtual string IsFirstPass { get; set; }
        public virtual string EngDispositionStatus { get; set; }
        public virtual DateTime? EngDispositionDate { get; set; }
        public virtual string EngDispositionBy { get; set; }
        public virtual string IsBiModal { get; set; }
        public virtual string DispositionResult { get; set; }
        public virtual string SblSylResult { get; set; }
        public virtual string Sbl { get; set; }
        public virtual string Syl { get; set; }
        public virtual string Comment { get; set; }
        //public virtual string IsNewWip { get; set; }
        public virtual string SpecVersion { get; set; }
        public virtual string GradingSoftwareVersion { get; set; }
        public virtual string AutoDispositionBy { get; set; }
        public virtual string LotId { get; set; }
        //public virtual string Quadrant { get; set; }
        //public virtual string CustomerCode { get; set; }
        //public virtual string CustomerName { get; set; }
        //public virtual string WinProdName { get; set; }
        //public virtual string CustProdName { get; set; }
        public virtual string CurrentStage { get; set; }
        //public virtual string FoundryComplete { get; set; }
        //public virtual string Type { get; set; }
        public virtual DateTime? StartDate { get; set; }
        //public virtual string CustomerHold { get; set; }
        //public virtual DateTime? SourceDate { get; set; }
        public virtual DateTime? WipFirstDate { get; set; }
        public virtual DateTime? InsertionDate { get; set; }
        public virtual string GradeSummaryFile { get; set; }
        //public virtual string ParetoTop1 { get; set; }
        //public virtual string ParetoTop2 { get; set; }
        //public virtual string ParetoTop3 { get; set; }
        public virtual string SpecName { get; set; }
        public virtual string HistoryType { get; set; }

        //add on revising of disposition system.
        //public virtual DateTime? WinMesFlagGenerationDate { get; set; }
        //public virtual DateTime? WinConsumeDate { get; set; }
        //public virtual DateTime? WinPreAoiTransferDate { get; set; }
        //public virtual string WinConsumeStatus { get; set; }
        //public virtual string WinFlagFilename { get; set; }
        //public virtual string PreAoiFilename { get; set; }
        //public virtual string IntegrationStatus { get; set; }
        public virtual string MrbApprovalFile { get; set; }

        #region NHibernate Composite Key Requirements
        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            var t = obj as TblWinWipDispositionHistory;
            if (t == null) return false;
            if (WaferId == t.WaferId
             && HistoryInsertionDate == t.HistoryInsertionDate)
                return true;

            return false;
        }
        public override int GetHashCode()
        {
            int hash = GetType().GetHashCode();
            hash = (hash * 397) ^ WaferId.GetHashCode();
            hash = (hash * 397) ^ HistoryInsertionDate.GetHashCode();

            return hash;
        }
        #endregion
    }
}
