using System;
using System.Text;
using System.Collections.Generic;


namespace SystemLibrary.Models {
    
    public class TblProductMask {
        public virtual int? Id { get; set; }
        public virtual string MaskId { get; set; }
        public virtual string ProductName { get; set; }
        public virtual DateTime? ModifyDate { get; set; }
    }
}
