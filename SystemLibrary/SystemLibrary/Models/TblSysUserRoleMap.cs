using System;
using System.Text;
using System.Collections.Generic;


namespace SystemLibrary.Models {
    
    public class TblSysUserRoleMap {
        public virtual int? Id { get; set; }
        public virtual string UserId { get; set; }
        public virtual string RoleName { get; set; }
        public virtual DateTime? ModifyDate { get; set; }
        //public virtual bool? ReadOnly { get; set; }
    }
}
