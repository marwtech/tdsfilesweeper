using System;
using System.Text;
using System.Collections.Generic;


namespace SystemLibrary.Models {
    
    public class TblBinCodeFileType {
        public virtual int? Id { get; set; }
        public virtual int? SequenceNo { get; set; }
        public virtual string FileType { get; set; }
        public virtual string ColumnX { get; set; }
        public virtual string ColumnY { get; set; }
        public virtual string ColumnBinCode { get; set; }
        public virtual string ColumnWaferId { get; set; }
        public virtual string FileFilter { get; set; }
    }
}
