using System;
using System.Text;
using System.Collections.Generic;


namespace SystemLibrary.Models {
    
    public class TblFileTrace {
        public virtual int? Id { get; set; }
        public virtual string WaferId { get; set; }
        public virtual string Percentage { get; set; }
        public virtual string Filetype { get; set; }
        public virtual string Temperature { get; set; }
        public virtual DateTime? Testtime { get; set; }
        public virtual string SourcePath { get; set; }
        public virtual string TargetPath { get; set; }
        public virtual DateTime? Inserttime { get; set; }
        public virtual decimal? Filesize { get; set; }
    }
}
