using System;
using System.Text;
using System.Collections.Generic;


namespace SystemLibrary.Models {
    
    public class TblBinCodeMaskPcm {
        public virtual int? Id { get; set; }
        public virtual string Mask { get; set; }
        public virtual string X { get; set; }
        public virtual string Y { get; set; }
        public virtual string Device { get; set; }
    }
}
