﻿using SystemLibrary.Utility;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemLibrary.Models
{
    public class ConfigInput
    {
        public virtual string FileType { get; set; }
        public virtual string ColumnX { get; set; }
        public virtual string ColumnY { get; set; }
        public virtual string ColumnBinCode { get; set; }
        public virtual string ColumnWaferId { get; set; }
        public virtual string FileFilter { get; set; }
        public virtual string WaferId { get; set; }
        public virtual string RwWaferId { get; set; }
        public virtual string MonitorFolder { get; set; }
        public virtual string EpiType { get; set; }
        public virtual string RemapRequired { get; set; }
        public virtual string ExclusionZoneColumn { get; set; }
        public virtual string ExclusionZoneValue { get; set; }
        public virtual string ExclusionMapPath { get; set; }
        public virtual string TargetBinCode { get; set; }
        public virtual string GoodDieBinCode { get; set; }
        public virtual string ResultFolder { get; set; }
        public virtual string NewFileNameSuffix { get; set; }
        public virtual string PickMode { get; set; }
        public virtual string AppMode { get; set; }
        public virtual string PickQty { get; set; }
        public virtual string BackupFolder { get; set; }
        public virtual bool IsAuto { get; set; }
        public virtual bool IsOnMonitor { get; set; }
        public virtual bool NeedSendValidMail { get; set; }

        private string _baseDirectory = AppDomain.CurrentDomain.BaseDirectory;

        public ConfigInput()
        {           
            var properties = this.GetType().GetProperties();

            foreach (var property in properties)
            {
                string value = ConfigurationManager.AppSettings[property.Name];
                if (string.IsNullOrEmpty(value))
                    continue;

                CheckFolder(property.Name, ref value);

                switch (property.PropertyType.Name)
                {
                    case "Boolean":
                        property.SetValue(this, value.ToLower() == "true");
                        break;
                    default:
                        property.SetValue(this, value);
                        break;
                }
            }

            if (string.IsNullOrEmpty(GoodDieBinCode))
                GoodDieBinCode = "1";
        }

        public void SetWaferId(string filePath)
        {            
            if (!File.Exists(filePath))
            {
                throw new Exception("cannot find the "+ filePath);
            }

            var fileInfo = new FileInfo(filePath);
            if (string.IsNullOrEmpty(ColumnWaferId))
            {
                WaferId = fileInfo.Name.Substring(0, 12);
            }
            else
            {
                WaferId = ExcelOperator.GetCsvDataToDic(filePath)[0][ColumnWaferId];
                RwWaferId = fileInfo.Name.Substring(0, 12);
            }            
        }

        private void CheckFolder(string propertyName, ref string value)
        {
            if (propertyName.Contains("Folder") && propertyName != "MapFolder")
            {
                if (!Directory.Exists(value))
                {
                    string subfolder = _baseDirectory + value.Split('\\').LastOrDefault();
                    if (!Directory.Exists(subfolder))
                    {
                        Directory.CreateDirectory(subfolder);
                    }
                    value = subfolder;
                }
            }
        }
    }
}
