using System;
using System.Text;
using System.Collections.Generic;


namespace SystemLibrary.Models
{

    public class TblBinCodeFileStatus
    {
        public virtual int? Id { get; set; }
        public virtual string AppMode { get; set; }
        public virtual string FileType { get; set; }
        public virtual string WaferId { get; set; }
        public virtual string RwWaferId { get; set; }
        public virtual string EpiType { get; set; }
        public virtual string FilePath { get; set; }
        public virtual string ResultPath { get; set; }
        public virtual bool? IsValid { get; set; }
        public virtual bool? IsCompleted { get; set; }
        public virtual string ErrorMessage { get; set; }
        public virtual DateTime? LoadedDate { get; set; }
        public virtual DateTime? UpdatedDate { get; set; }

        public TblBinCodeFileStatus()
        {

        }

        public TblBinCodeFileStatus(ConfigInput configInput, string filePath, bool isCompleted)
        {
            var properties =typeof(ConfigInput).GetProperties();

            foreach (var property in properties)
            {
                var target = this.GetType().GetProperty(property.Name);
                if (target != null)
                {
                    target.SetValue(this, property.GetValue(configInput));
                }
            }
            
            FilePath = filePath;
            IsCompleted = isCompleted;
        }
    }
}
