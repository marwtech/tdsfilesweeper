﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemLibrary.Enums
{
    public enum DispositionCode
    {
        P,
        F,
        R,
        H
    }
}
