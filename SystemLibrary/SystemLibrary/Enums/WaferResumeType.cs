﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemLibrary.Enums
{
    public enum WaferResumeType
    {
        eDoc,
        TMapRes,
        FaintLine,
        WIN,
        WAT
    }
}
