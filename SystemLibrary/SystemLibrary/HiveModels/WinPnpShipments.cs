﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemLibrary.HiveModels
{
    [Table("win.win_pnpshipments")]
    public class WinPnpShipments
    {
        [Column("wafer id")]
        [Display(Name = "fab wafer id")]
        public string FabWaferId { set; get; }

        [Column("loaded_date")]
        [Display(Name = "loaded_date")]
        public DateTime? LoadedDate { set; get; }

        [Column("po")]
        [Display(Name = "po")]
        public string Po { set; get; }

        [Column("ship date")]
        [Display(Name = "ship date")]
        public string ShipDate { set; get; }

        [Column("cell name")]
        [Display(Name = "cell name")]
        public string CellName { set; get; }

        [Column("qty")]
        [Display(Name = "qty")]
        public string Qty { set; get; }

        [Column("cust prod name")]
        [Display(Name = "cust prod name")]
        public string CustProdName { set; get; }

        [Column("ship to")]
        [Display(Name = "ship to")]
        public string ShipTo { set; get; }

        [Column("ship via")]
        [Display(Name = "ship via")]
        public string ShipVia { set; get; }

        [Column("tracking num")]
        [Display(Name = "tracking num")]
        public string TrackingNum { set; get; }

        [Column("source_date")]
        [Display(Name = "source_date")]
        public DateTime? SourceDate { set; get; }

        [Column("loaded_month")]
        [Display(Name = "loaded_month")]
        public string LoadedMonth { set; get; }

        [Column("filename")]
        [Display(Name = "filename")]
        public string Filename { set; get; }

        [Column("line_sequence")]
        [Display(Name = "line_sequence")]
        public Int64? LineSequence { set; get; }
    }
}
