﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemLibrary.HiveModels
{
    [Table("wafertest_agg.tbl_wafer_process_factor")]
    public class TblWaferProcessFactor
    {
        [Column("wafer_id")]
        public string WaferId { set; get; }

        [Column("wafer_type")]
        public string WaferType { set; get; }

        [Column("epi_wafer_id")]
        public string EpiWaferId { set; get; }

        [Column("epi_reactor")]
        public string EpiReactor { set; get; }

        [Column("epi_run")]
        public string EpiRun { set; get; }

        [Column("epi_group")]
        public string EpiGroup { set; get; }

        [Column("iqe_bouleid")]
        public string IqeBouleId { set; get; }

        [Column("q_type")]
        public string QType { set; get; }
    }
}
