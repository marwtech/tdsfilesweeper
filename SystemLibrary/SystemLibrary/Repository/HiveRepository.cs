﻿using NHibernate;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using SystemLibrary.Utility;

namespace SystemLibrary.Repository
{
    public class HiveRepository<T> : IRepository<T>
    {
        private DBHelper _dbHelper = new DBHelper();
        private Type _type = typeof(T);
        private string _dbName = ConfigurationManager.AppSettings["HiveName"];

        private string _assemblyFullName;
        private string _entityFullName;

        private PropertyInfo[] _properties;

        public HiveRepository()
        {
            _assemblyFullName = _type.Assembly.FullName;
            _entityFullName = _type.FullName;
            _properties = _type.GetProperties();
        }

        public IList<T> GetAll()
        {
            return Query("");
        }

        public IList<T> Get(T keyEntity)
        {
            string criteria = GetCriteria(keyEntity);

            return Query(criteria);
        }                

        private IList<T> Query(string criteria)
        {
            var tableName = (_type.GetCustomAttributes(typeof(TableAttribute), true).FirstOrDefault() as TableAttribute).Name;
            IList<T> list = new List<T>();
            //List<Dictionary<string, object>> dictionaries = new List<Dictionary<string, object>>();

            DataTable sourceDt = new DataTable();
            try
            {
                if (!criteria.Contains("get column only"))
                {
                    using (DbConnection conn = _dbHelper.GetDBConn(_dbName))
                    {
                        sourceDt = _dbHelper.GetDataTable(conn, "select * from " + tableName + " where 1=1 " + criteria);
                    }
                }

                //foreach (DataRow row in sourceDt.Rows)
                //{
                //    Dictionary<string, object> dictionary = Enumerable.Range(0, sourceDt.Columns.Count)
                //        .ToDictionary(i => sourceDt.Columns[i].ColumnName, i => row.ItemArray[i]);
                //    string keys = string.Join(",", dictionary.Keys);
                //    dictionaries.Add(dictionary);
                //}
                //return dictionaries;

                foreach (DataRow row in sourceDt.Rows)
                {
                    var obj = (T)Assembly.Load(_assemblyFullName).CreateInstance(_entityFullName);

                    foreach (var property in _properties)
                    {
                        var name = (property.GetCustomAttributes(typeof(ColumnAttribute), true).FirstOrDefault() as ColumnAttribute).Name;
                        var value = row[name];
                        var valTypeName = value.GetType().Name;

                        if (string.IsNullOrEmpty(value.ToString()))
                            continue;

                        switch (valTypeName)
                        {
                            case "DateTime":
                                value = Convert.ToDateTime(value);
                                break;
                            case "String":
                                value = value.ToString();
                                break;
                        }

                        property.SetValue(obj, value);
                    }
                    list.Add(obj);
                }
            }
            catch (Exception e)
            {                
                string ddd = "error:" + e.Message;
                throw;
            }

            return list;
        }

        private string GetCriteria(T keyEntity)
        {
            if (keyEntity == null)
                return "";

            string criteria = "";

            foreach (var property in _properties)
            {
                var value = property.GetValue(keyEntity);

                if (value == null ? false : !string.IsNullOrEmpty(value.ToString()))
                {
                    var name = (property.GetCustomAttributes(typeof(ColumnAttribute), true).FirstOrDefault() as ColumnAttribute).Name;
                    criteria += " and `" + name + "`";
                    string strValue = value.ToString();
                    if (strValue.Contains(","))
                    {
                        criteria += "in ('" + string.Join("','", strValue.Split(',')) + "')";
                    }
                    else if (strValue.Contains("%"))
                    {
                        criteria += " like '" + strValue + "'";
                    }
                    else
                    {
                        criteria += " = '" + strValue + "'";
                    }
                }
            }

            return criteria;
        }

        public void SaveOrUpdate(T entity)
        {
            throw new Exception("cannot use this function");
        }

        public void SaveOrUpdate(T entity, ISession session)
        {
            throw new Exception("cannot use this function");
        }

        public void Delete(T entity)
        {
            throw new Exception("cannot use this function");
        }

        public void Delete(T entity, ISession session)
        {
            throw new Exception("cannot use this function");
        }

        public IList<T> GetAll(ISession session)
        {            
            return new List<T>();
        }       

        public IList<T> Get(T keyEntity, ISession session)
        {
            return new List<T>();
        }

        public IList<T> Where(Expression<Func<T, bool>> predicate)
        {
            throw new NotImplementedException();
        }
    }
}
