﻿using SystemLibrary.Utility;
using NHibernate;
using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace SystemLibrary.Repository
{
    public class EntityRepository<T> : IRepository<T>
    {
        string[] filter = { "WD001-", "WD002-" };

        public void SaveOrUpdate(T entity)
        {
            string msg = "";
            try
            {
                using (ISession session = NHibernateHelper.OpenSession())
                {
                    using (ITransaction transaction = session.BeginTransaction())
                    {
                        session.SaveOrUpdate(entity);
                        transaction.Commit();
                    }
                }
            }
            catch (Exception e)
            {
                msg += e.Message + ";" + (e.InnerException == null ? "" : e.InnerException.Message) + ";";
                throw;
            }
        }

        public void SaveOrUpdate(T entity, ISession session)
        {
            string msg = "";
            try
            {
                session.SaveOrUpdate(entity);
            }
            catch (Exception e)
            {
                msg += e.Message + ";" + (e.InnerException == null ? "" : e.InnerException.Message) + ";";
                throw;
            }
        }

        public void Delete(T entity)
        {
            string msg = "";
            try
            {
                using (ISession session = NHibernateHelper.OpenSession())
                {
                    using (ITransaction transaction = session.BeginTransaction())
                    {
                        session.Delete(entity);
                        transaction.Commit();
                    }
                }
            }
            catch (Exception e)
            {
                msg += e.Message + ";" + (e.InnerException == null ? "" : e.InnerException.Message) + ";";
                throw;
            }
        }

        public void Delete(T entity, ISession session)
        {
            string msg = "";
            try
            {
                session.Delete(entity);
            }
            catch (Exception e)
            {
                msg += e.Message + ";" + (e.InnerException == null ? "" : e.InnerException.Message) + ";";
                throw;
            }
        }

        private IList<T> FilterByWaferId(IList<T> list)
        {
            return list;
            //PropertyInfo waferIdPro = typeof(T).GetProperty("WaferId");
            //if (waferIdPro == null)
            //    return list;
            //else
            //    return list.Where(t => !filter.Contains(waferIdPro.GetValue(t).ToString().Substring(0, 6))).ToList();
        }

        public IList<T> GetAll()
        {
            using (ISession session = NHibernateHelper.OpenSession())
            {
                return GetAll(session);
            }
        }

        public IList<T> GetAll(ISession session)
        {
            string msg = "";
            IList<T> list;
            try
            {
                ICriteria criteria = session.CreateCriteria(typeof(T));
                list = criteria.List<T>();
            }
            catch (Exception e)
            {
                msg += e.Message + ";" + (e.InnerException == null ? "" : e.InnerException.Message) + ";";
                throw;
            }

            return FilterByWaferId(list);
        }

        public IList<T> Get(T keyEntity)
        {
            using (ISession session = NHibernateHelper.OpenSession())
            {
                return Get(keyEntity, session);
            }
        }

        public IList<T> Where(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {
            try
            {
                using (ISession session = NHibernateHelper.OpenSession())
                {
                    QueryOver<T> query = QueryOver.Of<T>().Where(predicate);
                    return query.GetExecutableQueryOver(session).List();
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public IList<T> Get(T keyEntity, ISession session)
        {
            string msg = "";
            Type keyType = keyEntity.GetType();
            ICriteria criteria = session.CreateCriteria(keyType);
            try
            {
                var properties = keyType.GetProperties().Where(p => p.GetValue(keyEntity) != null && !p.Name.Contains("Stamp"));
                object value;

                foreach (PropertyInfo property in properties)
                {
                    value = property.GetValue(keyEntity);

                    if (value.ToString() != "")
                    {
                        switch (value.GetType().Name)
                        {
                            case "String":
                                if (value.ToString().Contains(","))
                                {
                                    criteria.Add(Expression.In(property.Name, value.ToString().Split(',')));
                                }
                                else
                                {
                                    criteria.Add(Expression.Like(property.Name, value));
                                }
                                break;
                            default:
                                criteria.Add(Expression.Eq(property.Name, value));
                                break;
                        }
                    }
                    else
                    {
                        if (property.Name != "Id")
                            criteria.Add(Expression.IsNull(property.Name));
                    }
                }
            }
            catch (Exception e)
            {
                msg += e.Message + ";" + (e.InnerException == null ? "" : e.InnerException.Message) + ";";
                throw;
            }

            return FilterByWaferId(criteria.List<T>());
        }
    }

}
