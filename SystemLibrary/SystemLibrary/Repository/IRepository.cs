﻿using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace SystemLibrary.Repository
{
    public interface IRepository<T>
    {
        void SaveOrUpdate(T entity);
        void SaveOrUpdate(T entity, ISession session);
        void Delete(T entiy);
        void Delete(T entity, ISession session);
        IList<T> GetAll();
        IList<T> GetAll(ISession session);
        /// <summary>
        /// It can get data by 'like' when key value contains '%'
        /// </summary>
        IList<T> Get(T keyEntity);
        /// <summary>
        /// It can get data by 'like' when key value contains '%'
        /// </summary>
        IList<T> Get(T keyEntity, ISession session);

        IList<T> Where(Expression<Func<T, bool>> predicate);
    }
}
