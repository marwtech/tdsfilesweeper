﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace Oclaro.Security
{
    class Encrypt
    {
        private static byte[] CryptoKey = { 186, 166, 11, 210, 130, 67, 122, 245, 5, 170, 25, 12, 148, 15, 249, 134 };
        private static byte[] CryptoIV = { 205, 92, 114, 41, 127, 94, 193, 116 };
        private static RC2 RC2Alg;
        private static MemoryStream mStream;
        private static bool mInitialised = false;

        private static void Init()
        {
            try
            {
                // Create a new RC2 object to generate a key 
                // and initialization vector (IV).  Specify one 
                // of the recognized simple names for this 
                // algorithm.
                mStream = new MemoryStream();

                RC2Alg = RC2.Create();
                RC2Alg.IV = CryptoIV;
                RC2Alg.Key = CryptoKey;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.ToString());
            }
        }

        internal static string Decode(string encodedText)
        {
            string ret = "";
            byte[] Data = new Byte[encodedText.Length / 2];
            if (!mInitialised) Init();
            MemoryStream mStream = new MemoryStream();
            CryptoStream cStream = new CryptoStream(mStream, RC2Alg.CreateDecryptor(), CryptoStreamMode.Write);

            for (int j = 0; j < Data.GetLength(0); ++j)
            {
                Data[j] = Convert.ToByte(encodedText.Substring(2 * j, 2), 16);
            }
            cStream.Write(Data, 0, Data.GetLength(0));
            cStream.FlushFinalBlock();
            byte[] DecryptedData = mStream.ToArray();
            ret = ASCIIEncoding.ASCII.GetString(DecryptedData);
            // we now have an array of bytes so
            return ret;
        }

        public static string Encode(string plainText)
        {
            byte[] EncodedPassword = Cypher(plainText);
            string strOut = "";
            for (int i = 0; i < EncodedPassword.GetLength(0); ++i)
            {
                strOut += EncodedPassword[i].ToString("X2");
            }
            return strOut;
        }

        private static byte[] Cypher(string plainText)
        {
            byte[] ret;
            MemoryStream mStream;
            CryptoStream cStream;
            // Create a MemoryStream.
            if (!mInitialised) Init();
            mStream = new MemoryStream();

            cStream = new CryptoStream(mStream,
                    RC2Alg.CreateEncryptor(), CryptoStreamMode.Write);

            byte[] toEncrypt = new ASCIIEncoding().GetBytes(plainText);

            try
            {

                // Write the byte array to the crypto stream and flush it.
                cStream.Write(toEncrypt, 0, toEncrypt.Length);
                cStream.FlushFinalBlock();

                // Get an array of bytes from the 
                // MemoryStream that holds the 
                // encrypted data.
                ret = mStream.ToArray();
            }
            catch (CryptographicException e)
            {
                System.Diagnostics.Debug.WriteLine(e.ToString());
                System.Diagnostics.Debug.Assert(false);
                ret = null;
            }
            finally
            {
                // Close the streams.
                cStream.Close();
                mStream.Close();

            }
            // Return the encrypted buffer.
            return ret;

        }

    }
}
