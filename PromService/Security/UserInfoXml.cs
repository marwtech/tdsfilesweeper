
using System;
using System.Xml;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Reflection;
using System.IO;

namespace Oclaro.Security
{
	/// <summary>
	/// Class holding cached user information and tools to load the file.
	/// </summary>
	public class UserInfoXml
	{
		/// <summary>
		/// Load user info from file. Any exceptions thrown in the process will be allowed to
		/// fall.
		/// </summary>
		public UserInfoXml(string userXmlLocation)
		{
            if (userXmlLocation != null)
            {
                this.userDataFileName = userXmlLocation;
            }
            else
            {
                userDataFileName = new FileInfo("UserInfo.xml" ?? EXE).FullName.ToString();
            }
            this.users = new Dictionary<string, UserInfo>();
            ReadXml();
		}

        /// <summary>
        /// Read xml file and add all users found to dictionary
        /// Also decrypt any decryptable items
        /// </summary>
        public void ReadXml()
        {
            /* Data for current user record loaded in from XML file. */
            string loadedUser = "";
            string loadedWinPass = "";
            string loadedPastieName = "";
            string loadedFullNameHash = "";
            string loadedPromPassHash = "";
            string loadedPromUserHash = "";
            string loadedAccessLevel = "";
            string loadedAccessLevelName = "";
            string loadedRigName = "";
            string loadedPromisRigName = "";

            /* Open user data XML file. This constructor call only conducts a scant validation of the
             * file name. Any expected error will not occur until the first call to Read. */
            XmlTextReader userData = new XmlTextReader(userDataFileName);

            try
            {
                /* Scan file, one element a time. */
                while (userData.Read())
                {
                    /* Found the start of an element. */
                    if (userData.NodeType == XmlNodeType.Element)
                    {
                        switch (userData.LocalName)
                        {
                            case "User":
                                {
                                    loadedUser = "";
                                    loadedWinPass = "";
                                    loadedPastieName = "";
                                    loadedFullNameHash = "";
                                    loadedPromPassHash = "";
                                    loadedPromUserHash = "";
                                    loadedAccessLevelName = "";
                                    loadedAccessLevel = "";
                                    loadedRigName = "";
                                    loadedPromisRigName = "";
                                }
                                break;
                            case "UserId":
                                {
                                    loadedUser = userData.ReadString();
                                }
                                break;
                            case "WinPass":
                                {
                                    loadedWinPass = userData.ReadString();
                                }
                                break;
                            case "PastieName":
                                {
                                    loadedPastieName = Encrypt.Decode(userData.ReadString());
                                }
                                break;
                            case "FullName":
                                {
                                    loadedFullNameHash = Encrypt.Decode(userData.ReadString());
                                }
                                break;
                            case "PromisPass":
                                {
                                    loadedPromPassHash = Encrypt.Decode(userData.ReadString());
                                }
                                break;
                            case "PromisUser":
                                {
                                    loadedPromUserHash = Encrypt.Decode(userData.ReadString());
                                }
                                break;
                            case "AccessLevel":
                                {
                                    loadedAccessLevel = userData.ReadString();
                                }
                                break;
                            case "AccessLevelName":
                                {
                                    loadedAccessLevelName = userData.ReadString();
                                }
                                break;
                            case "RigName":
                                {
                                    loadedRigName = userData.ReadString();
                                }
                                break;
                            case "PromisRigName":
                                {
                                    loadedPromisRigName = userData.ReadString();
                                }
                                break;
                            default:
                                break;
                        }
                    } /* End if found element. */

                        /* Found the </User> tag. */
                    else if ((userData.NodeType == XmlNodeType.EndElement) &&
                        (userData.LocalName == "User"))
                    {
                        this.users.Add(
                            loadedUser,
                            new UserInfo(loadedUser,loadedWinPass, loadedPastieName, loadedFullNameHash, loadedPromPassHash, loadedPromUserHash
                                , Convert.ToInt32(loadedAccessLevel), loadedAccessLevelName, loadedRigName, loadedPromisRigName));
                    }
                } /* End while */
            } /* End try */
            catch (Exception ex)
            {
                string tmp = ex.Message;
                MessageBox.Show("Error reading Xml File\n" + ex.Message);
            }
            finally
            {
                /* Close file. */
                userData.Close();
            }
        }

        /// <summary>
        /// Updates xml file with all the users stored in the dictionary
        /// stored some items ad encrypted strings
        /// </summary>
        private void WriteXml()
        {
            using (XmlTextWriter Output = new XmlTextWriter(userDataFileName, System.Text.Encoding.UTF8))
            {
                Output.Formatting = Formatting.Indented;
                Output.Indentation = 4;

                Output.WriteStartDocument();
                Output.WriteStartElement("Users");

                foreach (var key in this.users.Keys)
                {
                    Output.WriteStartElement("User");

                    Output.WriteElementString("UserId", users[key.ToString()].mWinName);
                    Output.WriteElementString("WinPass", users[key.ToString()].mWinPass);
                    Output.WriteElementString("PastieName", Encrypt.Encode( users[key.ToString()].mPastieName));
                    Output.WriteElementString("FullName", Encrypt.Encode(users[key.ToString()].mFullName));
                    Output.WriteElementString("PromisPass", Encrypt.Encode(users[key.ToString()].mPromisPassword));
                    Output.WriteElementString("PromisUser", Encrypt.Encode(users[key.ToString()].mPromisUsername));
                    Output.WriteElementString("AccessLevel", users[key.ToString()].mAccessLevel.ToString());
                    Output.WriteElementString("AccessLevelName", users[key.ToString()].mAccessLevelName);
                    Output.WriteElementString("RigName", users[key.ToString()].mRigName);
                    Output.WriteElementString("PromisRigName", users[key.ToString()].mPromisRigName);

                    Output.WriteEndElement();
                }

                Output.WriteEndElement();
                Output.WriteEndDocument();
            }
        }

        /// <summary>
        /// Adds a new user object to dictionary or overwrite it if key already exists
        /// Items in this dictionary are stored as plain text
        /// </summary>
        /// <param name="input">Validated user object</param>
        public void UpdateUser(UserInfo input)
        {
            if (this.users.ContainsKey(input.mWinName))
            {
                this.users.Remove(input.mWinName);
                this.users.Add(input.mWinName, input);
            }
            else
            {
                this.users.Add(input.mWinName, input);
            }

            WriteXml();
        }

        /// <summary>
        /// Return user info from dictionary
        /// </summary>
        /// <param name="id">Windows username</param>
        /// <returns>User infl calss with user data</returns>
        public UserInfo GetUser(string id)
        {
            if (users.ContainsKey(id))
            {
                return users[id];
            }
            else
            {
                return null;
            }

        }
				
		#region Private data
		/// <summary>
		/// Collection of UserRecord instances, key'd by user name.
		/// </summary>
        private Dictionary<string, UserInfo> users;

        private string EXE = Assembly.GetExecutingAssembly().GetName().Name;

		/// <summary>
		/// Location of username list.
		/// </summary>
        private string userDataFileName = "UserInfo.XML";

        
		#endregion

		#region Error messages
		/// <summary>
		/// Template for a Malformed privilege field error message.
		/// </summary>
		/// <remarks><list type="bullet">
		/// <item>{0} will be replaced with short file name.</item>
		/// <item>{1} will be replaced with line number where error was detected.</item>
		/// <item>{2} will be replaced with the poisition on the line where the error was detected.</item>
		/// <item>{3} will be replaved with the malformed data value.</item>
		/// </list></remarks>
		const string malformedPrivilegeFormat =
			"\"{0}\" line {1} position {2}: Privilege field contained \"{3}\", " +
			"instead of \"Operator\" or \"Technician\".";

		/// <summary>
		/// Template for a file access denied error.
		/// </summary>
		/// <remarks>{0} will be replaced with file name which failed to open for lack of privilege.</remarks>
		const string accessDeniedFormat = "Access to \"{0}\" denied.";
 
		/// <summary>
		/// Template for a file not found error message.
		/// </summary>
		/// <remarks>{0} will be replaced with full path of missing file.</remarks>
		const string fileNotFoundFormat = "File \"{0}\" not found. Contact your local technician.";
		
		/// <summary>
		/// Template for a general IO failure message.
		/// </summary>
		/// <remarks><list type="bullet">
		/// <item>{0} will be replaced with failed filename.</item>
		/// <item>{1} Will be replaced with specific error message.</item>
		/// </list></remarks>
		const string generalIoFailureFormat = "General IO failure reading \"{0}\". {1}";

		/// <summary>
		/// Template for a malformed XML error message.
		/// </summary>
		/// <remarks><list type="bullet">
		/// <item>(0} will be replaced with the short filename of the malformed XML file.</item>
		/// <item> {1} will be replaced withe the error generated by the XML parser.</item>
		/// </list></remarks>
		const string malformedXmlFormat = "Malformed XML in \"{0}\". {1}";																	 

		#endregion
		
	}
}
