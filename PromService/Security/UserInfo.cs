﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oclaro.Security
{
    /// <summary>
    /// User data storage class
    /// </summary>
    public class UserInfo
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="winName">Windows Username</param>
        /// <param name="winPass">Windows password (stored as an irriversble hash)</param>
        /// <param name="pastieName">Internal Uername</param>
        /// <param name="fullName">Full Name</param>
        /// <param name="promisPass">Promis TP password</param>
        /// <param name="promisName">Promis TP uername</param>
        /// <param name="accessLevel">Access level integer 1-6</param>
        /// <param name="accessLevelName">Access level identifier</param>
        /// <param name="rigName">Human readable rig name</param>
        /// <param name="promisRigName">Rig id on PROMIS</param>
        public UserInfo(string winName, string winPass, string pastieName, string fullName, string promisPass
            , string promisName, int accessLevel, string accessLevelName, string rigName, string promisRigName)
        {
            this.mWinName = winName;
            this.mWinPass = winPass;
            this.mPastieName = pastieName;
            this.mFullName = fullName;
            this.mPromisPassword = promisPass;
            this.mPromisUsername = promisName;
            this.mAccessLevel = accessLevel;
            this.mAccessLevelName = accessLevelName;
            this.mRigName = rigName;
            this.mPromisRigName = promisRigName;
        }

        #region Private Data
        /// <summary>
        /// Windows uername
        /// </summary>
        public readonly string mWinName;
        /// <summary>
        /// Encrypted windows password
        /// </summary>
        public readonly string mWinPass;
        /// <summary>
        /// PASTIE name
        /// </summary>
        public readonly string mPastieName;
        /// <summary>
        /// User's full nems
        /// </summary>
        public readonly string mFullName;
        /// <summary>
        /// Promis Password
        /// </summary>
        public readonly string mPromisPassword;
        /// <summary>
        /// Promis username
        /// </summary>
        public readonly string mPromisUsername;
        /// <summary>
        /// User's access level
        /// </summary>
        public readonly int mAccessLevel;
        /// <summary>
        /// User's access level name
        /// </summary>
        public readonly string mAccessLevelName;
        /// <summary>
        /// Rig name
        /// </summary>
        public readonly string mRigName;
        /// <summary>
        /// Rig ID on PROMIS
        /// </summary>
        public readonly string mPromisRigName;
        #endregion
    }
}
