
using System;
using System.Security.Cryptography;

namespace Oclaro.Security
{
	/// <summary>
	/// Utility class holding public static functions for hashing and checking passwords.
	/// </summary>
	public class PasswordHash
	{
		/// <summary>
		/// Secret salt added to all hashes. Do not reveal to operators.
		/// </summary>
        private const string salt = "IxeKLnkhuWa8UqnKC7&V^m)qmXL(v6guV1B0%6EDbuWGRZy_zLOii_iJrb7rAk3(";

		/// <summary>
		/// Transform user information into a salted hash.
		/// </summary>
		/// <param name="user">User name.</param>
		/// <param name="pass">Plain text password.</param>
		/// <returns>Base64 string of hash.</returns>
		public static string Hash(string user, string pass)
		{
			/* Check params. */
			if ((user == null) || (pass == null))
			{
				throw new Exception(
					"PasswordHash.Hash called with null string.");
			}

			/* Create plain text */
            string plainText =
				":" +
				user.Length + ":" +
				pass.Length + ":" +
				user + ":" +
				pass + ":" +
				PasswordHash.salt + ":";

			/* Convert string into byte array.
			 * UTF8 is an algorithm to encode a unicode array into a byte array. */
			byte[] plainArray = System.Text.Encoding.UTF8.GetBytes(plainText);

			/* Hash plainArray using SHA256.
			 * SHA256 is a secure one-way hash algorithm, turning any number of bytes into
			 * 256 bits of hash. In theory, the hash cannot be reversed. */
            HashAlgorithm hasher = SHA256.Create();

			byte[] hashArray = hasher.ComputeHash(plainArray);
			hasher.Clear();

			/* Encode the byte array into a friendly text string.
			 * Base64 is an encoding, transforming chunks of 6 bits into a character
			 * selected from letters, numerals and a couple of punctuation marks.
			 * A base64 string can be stored in an plain ascii text file. */
			return Convert.ToBase64String(hashArray);
		}

		/// <summary>
		/// Test login credentials against a loaded hash.
		/// </summary>
		/// <param name="user">User name.</param>
		/// <param name="pass">Typed in password plaintext.</param>
		/// <param name="hash">Hash loaded from user data.</param>
		/// <returns>True if password correct. False on no match.</returns>
		public static bool TestPassword(string user, string pass, string hash)
		{
            return (Hash(user, pass) == hash);
		}

		/// <summary>
		/// Private constructor to prevent construction.
		/// </summary>
		private PasswordHash()
		{}
	}
}
