﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.DirectoryServices.ActiveDirectory;
//Promis library
using Oclaro.PromService;
//Standard security library
using Oclaro.Security;

namespace Oclaro.PromisForms
{
    public partial class LogInForm : Form
    {
        /// <summary>
        /// Unique application name for database
        /// </summary>
        public static string mAppName = "BarTest";
        /// <summary>
        /// Unique application identifier for database
        /// </summary>
        public static string mAppGuid = "F8214893-45E1-48F9-A6C3-E216B9859D4D";

        private static string mDataBaseServer = "cas-spp-epi-01";
        private static string mDatabaseUsername = "E9D8ECD3D614220AA8BD60A9AAEAD5FC";
        private static string mDatabasePassword = "6FE3515A99707DEBDAE1B695B91AA4C6";

        private string mRigsTextFile;
        private string mLocalRig;

        private UserInfo mInfo;
        private UserInfoXml mXml;

        private Font mFont;
        private Color mBackColour;
        private Color mForeColour;

        private Color mTextBoxForeColour;
        private Color mTextBoxBackColour;

        #region Constructors

        /// <summary>
        /// Log in form to help get PROMIS information and verify a user's identity
        /// </summary>
        public LogInForm()
        {
            InitializeComponent();
            LogIn.InitPasswordSystem(mDataBaseServer, mDatabaseUsername, mDatabasePassword, mAppName, mAppGuid);
            mRigsTextFile = null;
            mLocalRig = null;
            mInfo = null;
            mFont = null;
            mBackColour = Color.Empty;
            mForeColour = Color.Empty;
            mTextBoxBackColour = Color.Empty;
            mTextBoxForeColour = Color.Empty;
        }

        /// <summary>
        /// Log in form to help get PROMIS information and verify a user's identity
        /// </summary>
        /// <param name="info">Initial ID</param>
        /// <param name="rigsTextFile">Location of the local rigs list</param>
        /// <param name="rigOption">If true, the rig text file acts as a list of local rigs, if false it acts as an individul rig id</param>
        public LogInForm(UserInfo info, string rigsTextFile, bool rigOption)
        {
            InitializeComponent();
            LogIn.InitPasswordSystem(mDataBaseServer, mDatabaseUsername, mDatabasePassword, mAppName, mAppGuid);
            if (rigOption)
            {
                mRigsTextFile = rigsTextFile;
                mLocalRig = null;
            }
            else
            {
                mRigsTextFile = null;
                mLocalRig = rigsTextFile;
            }
            mInfo = info;
            mFont = null;
            mBackColour = Color.Empty;
            mForeColour = Color.Empty;
            mTextBoxBackColour = Color.Empty;
            mTextBoxForeColour = Color.Empty;
        }

        /// <summary>
        /// Log in form to help get PROMIS information and verify a user's identity
        /// </summary>
        /// <param name="appName">Non-default appname</param>
        /// <param name="appGUID">Non-default appGUID</param>
        /// <param name="rigsTextFile">Location of the local rigs list</param>        
        /// <param name="rigOption">If true, the rig text file acts as a list of local rigs, if false it acts as an individul rig id</param>
        public LogInForm(string appName, string appGUID, string rigsTextFile, bool rigOption)
        {
            InitializeComponent();
            if (appName == null || appGUID == null)
            {
                LogIn.InitPasswordSystem(mDataBaseServer, mDatabaseUsername, mDatabasePassword, mAppName, mAppGuid);
            }
            else
            {
                LogIn.InitPasswordSystem(mDataBaseServer, mDatabaseUsername, mDatabasePassword, appName, appGUID);
            }
            if (rigOption)
            {
                mRigsTextFile = rigsTextFile;
                mLocalRig = null;
            }
            else
            {
                mRigsTextFile = null;
                mLocalRig = rigsTextFile;
            }
            mInfo = null;
            mFont = null;
            mBackColour = Color.Empty;
            mForeColour = Color.Empty;
            mTextBoxBackColour = Color.Empty;
            mTextBoxForeColour = Color.Empty;
        }

        /// <summary>
        /// Log in form to help get PROMIS information and verify a user's identity
        /// </summary>
        /// <param name="info">Initial ID</param>
        /// <param name="appName">Non-default appname</param>
        /// <param name="appGUID">Non-default appGUID</param>
        /// <param name="rigsTextFile">Location of the local rigs list</param>
        /// <param name="rigOption">If true, the rig text file acts as a list of local rigs, if false it acts as an individul rig id</param>
        public LogInForm(UserInfo info, string appName, string appGUID, string rigsTextFile, bool rigOption)
        {
            InitializeComponent();
            if (appName == null || appGUID == null)
            {
                LogIn.InitPasswordSystem(mDataBaseServer, mDatabaseUsername, mDatabasePassword, mAppName, mAppGuid);
            }
            else
            {
                LogIn.InitPasswordSystem(mDataBaseServer, mDatabaseUsername, mDatabasePassword, appName, appGUID);
            }
            if (rigOption)
            {
                mRigsTextFile = rigsTextFile;
                mLocalRig = null;
            }
            else
            {
                mRigsTextFile = null;
                mLocalRig = rigsTextFile;
            }
            mInfo = info;
            mFont = null;
            mBackColour = Color.Empty;
            mForeColour = Color.Empty;
            mTextBoxBackColour = Color.Empty;
            mTextBoxForeColour = Color.Empty;
        }

        /// <summary>
        /// Log in form to help get PROMIS information and verify a user's identity
        /// </summary>
        /// <param name="info">Initial ID</param>
        /// <param name="appName">Non-default appname</param>
        /// <param name="appGUID">Non-default appGUID</param>
        /// <param name="font">Default font</param>
        /// <param name="backColor">New background colour</param>
        /// <param name="foreColor">New foreground colour</param>
        /// <param name="textBox">True to change textbox</param>
        /// <param name="rigsTextFile">Location of the local rigs list</param>
        /// <param name="rigOption">If true, the rig text file acts as a list of local rigs, if false it acts as an individul rig id</param>
        public LogInForm(UserInfo info, string appName, string appGUID, Font font, Color backColor,
            Color foreColor, bool textBox, string rigsTextFile, bool rigOption)
        {
            InitializeComponent();
            if (appName == null || appGUID == null)
            {
                LogIn.InitPasswordSystem(mDataBaseServer, mDatabaseUsername, mDatabasePassword, mAppName, mAppGuid);
            }
            else
            {
                LogIn.InitPasswordSystem(mDataBaseServer, mDatabaseUsername, mDatabasePassword, appName, appGUID);
            }
            if (rigOption)
            {
                mRigsTextFile = rigsTextFile;
                mLocalRig = null;
            }
            else
            {
                mRigsTextFile = null;
                mLocalRig = rigsTextFile;
            }
            mInfo = info;
            mFont = font;
            if (textBox)
            {
                mBackColour = Color.Empty;
                mForeColour = Color.Empty;
                mTextBoxBackColour = backColor;
                mTextBoxForeColour = foreColor;
            }
            else
            {
                mBackColour = backColor;
                mForeColour = foreColor;
                mTextBoxBackColour = Color.Empty;
                mTextBoxForeColour = Color.Empty;
            }
            
        }

        /// <summary>
        /// Log in form to help get PROMIS information and verify a user's identity
        /// </summary>
        /// <param name="info">Initial ID</param>
        /// <param name="appName">Non-default appname</param>
        /// <param name="appGUID">Non-default appGUID</param>
        /// <param name="font">Default font</param>
        /// <param name="backColor">New background colour</param>
        /// <param name="foreColor">New foreground colour</param>
        /// <param name="txtBoxForeColour">TextBox forground colour</param>
        /// <param name="txtBoxBackColour">Textbox background colour</param>
        /// <param name="rigsTextFile">Location of the local rigs list</param>
        /// <param name="rigOption">If true, the rig text file acts as a list of local rigs, if false it acts as an individul rig id</param>
        public LogInForm(UserInfo info, string appName, string appGUID, Font font, Color backColor,
            Color foreColor, Color txtBoxForeColour, Color txtBoxBackColour, string rigsTextFile,bool rigOption)
        {
            InitializeComponent();
            if (appName == null || appGUID == null)
            {
                LogIn.InitPasswordSystem(mDataBaseServer, mDatabaseUsername, mDatabasePassword, mAppName, mAppGuid);
            }
            else
            {
                LogIn.InitPasswordSystem(mDataBaseServer, mDatabaseUsername, mDatabasePassword, appName, appGUID);
            }
            if (rigOption)
            {
                mRigsTextFile = rigsTextFile;
                mLocalRig = null;
            }
            else
            {
                mRigsTextFile = null;
                mLocalRig = rigsTextFile;
            }
            mInfo = info;
            mFont = font;
            mBackColour = backColor;
            mForeColour = foreColor;
            mTextBoxBackColour = txtBoxBackColour;
            mTextBoxForeColour = txtBoxForeColour;
        }

        /// <summary>
        /// Log in form to help get PROMIS information and verify a user's identity
        /// </summary>
        /// <param name="info">Initial ID</param>
        /// <param name="appName">Non-default appname</param>
        /// <param name="appGUID">Non-default appGUID</param>
        /// <param name="backColor">New background colour</param>
        /// <param name="foreColor">New foreground colour</param>
        /// <param name="textBox">True to change textbox</param>
        /// <param name="rigsTextFile">Location of the local rigs list</param>
        /// <param name="rigOption">If true, the rig text file acts as a list of local rigs, if false it acts as an individul rig id</param>
        public LogInForm(UserInfo info, string appName, string appGUID,  Color backColor,
            Color foreColor, bool textBox, string rigsTextFile, bool rigOption)
        {
            InitializeComponent();
            if (appName == null || appGUID == null)
            {
                LogIn.InitPasswordSystem(mDataBaseServer, mDatabaseUsername, mDatabasePassword, mAppName, mAppGuid);
            }
            else
            {
                LogIn.InitPasswordSystem(mDataBaseServer, mDatabaseUsername, mDatabasePassword, appName, appGUID);
            }
            if (rigOption)
            {
                mRigsTextFile = rigsTextFile;
                mLocalRig = null;
            }
            else
            {
                mRigsTextFile = null;
                mLocalRig = rigsTextFile;
            }
            mInfo = info;
            mFont = null;
            if (textBox)
            {
                mBackColour = Color.Empty;
                mForeColour = Color.Empty;
                mTextBoxBackColour = backColor;
                mTextBoxForeColour = foreColor;
            }
            else
            {
                mBackColour = backColor;
                mForeColour = foreColor;
                mTextBoxBackColour = Color.Empty;
                mTextBoxForeColour = Color.Empty;
            }

        }

        /// <summary>
        /// Log in form to help get PROMIS information and verify a user's identity
        /// </summary>
        /// <param name="info">Initial ID</param>
        /// <param name="appName">Non-default appname</param>
        /// <param name="appGUID">Non-default appGUID</param>
        /// <param name="backColor">New background colour</param>
        /// <param name="foreColor">New foreground colour</param>
        /// <param name="txtBoxForeColour">TextBox forground colour</param>
        /// <param name="txtBoxBackColour">Textbox background colour</param>
        /// <param name="rigsTextFile">Location of the local rigs list</param>
        /// <param name="rigOption">If true, the rig text file acts as a list of local rigs, if false it acts as an individul rig id</param>
        public LogInForm(UserInfo info, string appName, string appGUID, Color backColor,
            Color foreColor, Color txtBoxForeColour, Color txtBoxBackColour, string rigsTextFile, bool rigOption)
        {
            InitializeComponent();
            if (appName == null || appGUID == null)
            {
                LogIn.InitPasswordSystem(mDataBaseServer, mDatabaseUsername, mDatabasePassword, mAppName, mAppGuid);
            }
            else
            {
                LogIn.InitPasswordSystem(mDataBaseServer, mDatabaseUsername, mDatabasePassword, appName, appGUID);
            }
            if (rigOption)
            {
                mRigsTextFile = rigsTextFile;
                mLocalRig = null;
            }
            else
            {
                mRigsTextFile = null;
                mLocalRig = rigsTextFile;
            }
            mInfo = info;
            mFont = null;
            mBackColour = backColor;
            mForeColour = foreColor;
            mTextBoxBackColour = txtBoxBackColour;
            mTextBoxForeColour = txtBoxForeColour;
        }

        #endregion

        /// <summary>
        /// If the database location has changed, this function can be used to update it
        /// </summary>
        /// <param name="location">Server name</param>
        /// <param name="username">Database user name</param>
        /// <param name="password">Database password</param>
        public static void OverrideDatabaseDetails(string location, string username, string password)
        {
            if (string.IsNullOrEmpty(location) || string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password)) throw new ArgumentOutOfRangeException("Parametes can not be blank");
            mDataBaseServer = location;
            mDatabaseUsername = username;
            mDatabasePassword = password;
        }

        private void LogInForm_Load(object sender, EventArgs e)
        {
            mXml = new UserInfoXml(null);
            if (mInfo == null)
            {
                //CancelBtn.Enabled = false;
                //CancelBtn.Visible = false;
            }
            toolStripStatusLabel1.Text = "Waiting";
            List<string> rigs;
            if (mRigsTextFile != null)
            {
                rigs = Utilities.ReadTxtFile(mRigsTextFile);
            }
            else if (mLocalRig != null)
            {
                rigs = new List<string>();
                rigs.Add(mLocalRig);
            }
            else
            {
                rigs = Utilities.ReadTxtFile(@"LocalRigs.txt");
            }

            try
            {
                Rig.Items.AddRange(rigs.ToArray());
                Rig.Text = rigs[0];
            }
            catch { }

            try
            {
                using (var forest = Forest.GetCurrentForest())
                {
                    foreach (Domain domain in forest.Domains)
                    {
                        //string[] splitter = domain.Name.Split('.');

                        Console.WriteLine(domain.Name);
                        Domain.Items.Add(domain.Name);
                        //domain.Dispose();
                    }
                }
                Domain.Text = Domain.Items[0].ToString();
            }
            catch
            {
                Domain.Text = "int.oclaro.com";
            }

            if (mForeColour != Color.Empty)
            {
                toolStripStatusLabel1.ForeColor = mForeColour;
            }
            if (mBackColour != Color.Empty)
            {
                this.BackColor = mBackColour;
                this.statusStrip1.BackColor = mBackColour;
            }
            if (mFont != null)
            {
                toolStripStatusLabel1.Font = new Font(mFont.FontFamily, toolStripStatusLabel1.Font.SizeInPoints, FontStyle.Regular);
            }

            #region Loops
            //this.Font = new Font(mFont.FontFamily, 12);
            
            //Labels
            foreach (var cont in Utilities.GetAllControls(this, typeof(Label)))
            {
                //Font style
                if (mFont != null)
                    cont.Font = new Font(mFont.FontFamily, cont.Font.SizeInPoints, FontStyle.Regular);

                //Backout colour
                if (mBackColour != Color.Empty)
                    cont.BackColor = mBackColour;

                //Text colour
                if (mForeColour != Color.Empty)
                    cont.ForeColor = mForeColour;
            }

            //Buttons
            foreach (var cont in Utilities.GetAllControls(this, typeof(Button)))
            {
                //Font style
                if (mFont != null)
                    cont.Font = new Font(mFont.FontFamily, cont.Font.SizeInPoints, FontStyle.Regular);

                //Text colour
                if (mForeColour != Color.Empty)
                    cont.ForeColor = mForeColour;
            }

            //textboxes
            foreach (var cont in Utilities.GetAllControls(this, typeof(TextBox)))
            {
                //font style
                if (mFont != null)
                    cont.Font = new Font(mFont.FontFamily, cont.Font.SizeInPoints, FontStyle.Regular);

                //Text colour
                if (mTextBoxForeColour != Color.Empty)
                    cont.ForeColor = mTextBoxForeColour;

                //Back Colour
                if (mTextBoxBackColour != Color.Empty)
                    cont.BackColor = mTextBoxBackColour;

            }

            //Combo Boxes
            foreach (var cont in Utilities.GetAllControls(this, typeof(ComboBox)))
            {
                //font style
                if (mFont != null)
                    cont.Font = new Font(mFont.FontFamily, cont.Font.SizeInPoints, FontStyle.Regular);

                //Text colour
                if (mTextBoxForeColour != Color.Empty)
                    cont.ForeColor = mTextBoxForeColour;

                //Back Colour
                if (mTextBoxBackColour != Color.Empty)
                    cont.BackColor = mTextBoxBackColour;
            }

            #endregion

            this.AcceptButton = LogInBtn;

        }

        /// <summary>
        /// Perform the Log in
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LogInBtn_Click(object sender, EventArgs e)
        {

            if (radioButton1.Checked == true)
            {
                mInfo = WindowsLogIn();
            }
            else
            {
                mInfo = PASTIELogIn();
            }

            
        }

        private UserInfo WindowsLogIn()
        {
            UserInfo tmpUser = null;

            string mPastieName;
            string mFullName;
            string mPromisPassword;
            string mPromisUsername;
            int mAccessLevel;
            string mAccessLevelName;
            string mRigName;
            try
            {
                if (LogIn.IsValidLogin(Domain.Text, UserName.Text, Password.Text))
                {
                    int code;

                    if (LogIn.TestConnection())
                    {
                        if (LogIn.GetPromisIDRigWin(UserName.Text, Rig.Text, out mPastieName, out mFullName, out mPromisUsername, out mPromisPassword, out mAccessLevel, out mAccessLevelName, out mRigName, out code))
                        {
                            toolStripStatusLabel1.Text = "Success from database";
                            tmpUser = new UserInfo(UserName.Text, PasswordHash.Hash(UserName.Text, Pass.Text), mPastieName, mFullName, mPromisPassword, mPromisUsername, mAccessLevel, mAccessLevelName, mRigName, Rig.Text);
                            mXml.UpdateUser(tmpUser);
                            MessageBox.Show("Successfully logged in\nWelcome " + tmpUser.mFullName, "Log in result", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.DialogResult = System.Windows.Forms.DialogResult.OK;
                        }
                        else
                        {
                            MessageBox.Show("Error getting information from database", "Log in result", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    else
                    {
                        toolStripStatusLabel1.Text = "No database connection";
                        tmpUser = mXml.GetUser(UserName.Text);
                        if (tmpUser != null)
                        {
                            if (PasswordHash.TestPassword(UserName.Text, Pass.Text, tmpUser.mWinPass))
                            {
                                MessageBox.Show("Successfully logged in\nWelcome " + tmpUser.mFullName, "Log in result", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                toolStripStatusLabel1.Text += " - Success from local file";
                                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                                //successful
                            }
                            else
                            {
                                this.DialogResult = System.Windows.Forms.DialogResult.None;
                                toolStripStatusLabel1.Text += " - Incorrect log in information";
                                //unsuccessful
                            }
                        }
                        else
                        {
                            toolStripStatusLabel1.Text += " - User not found";
                            this.DialogResult = System.Windows.Forms.DialogResult.None;
                            //User not found
                        }
                    }
                }
                else
                {
                    this.DialogResult = System.Windows.Forms.DialogResult.None;
                    string message = toolStripStatusLabel1.Text = "Not a valid windows user";
                    MessageBox.Show(message);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return tmpUser;
        }

        private UserInfo PASTIELogIn()
        {
            UserInfo tmpUser = null;

            string mPastieName;
            string mFullName;
            string mPromisPassword;
            string mPromisUsername;
            int mAccessLevel;
            string mAccessLevelName;
            string mRigName;
            try
            {
                int code;

                if (LogIn.TestConnection())
                {
                    if (LogIn.GetPromsisIDRigPastie(UserName.Text, Password.Text, Rig.Text, out mPastieName, out mFullName, out mPromisUsername, out mPromisPassword, out mAccessLevel, out mAccessLevelName, out mRigName, out code))
                    {
                        toolStripStatusLabel1.Text = "Success from database";
                        tmpUser = new UserInfo(UserName.Text, PasswordHash.Hash(UserName.Text, Pass.Text), mPastieName, mFullName, mPromisPassword, mPromisUsername, mAccessLevel, mAccessLevelName, mRigName, Rig.Text);
                        mXml.UpdateUser(tmpUser);
                        MessageBox.Show("Successfully logged in\nWelcome " + tmpUser.mFullName, "Log in result", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.DialogResult = System.Windows.Forms.DialogResult.OK;
                    }
                    else
                    {
                        MessageBox.Show("Error getting information from database", "Log in result", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    toolStripStatusLabel1.Text = "No database connection";
                    tmpUser = mXml.GetUser(UserName.Text);
                    if (tmpUser != null)
                    {
                        if (PasswordHash.TestPassword(UserName.Text, Pass.Text, tmpUser.mWinPass))
                        {
                            MessageBox.Show("Successfully logged in\nWelcome " + tmpUser.mFullName, "Log in result", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            toolStripStatusLabel1.Text += " - Success from local file";
                            this.DialogResult = System.Windows.Forms.DialogResult.OK;
                            //successful
                        }
                        else
                        {
                            this.DialogResult = System.Windows.Forms.DialogResult.None;
                            toolStripStatusLabel1.Text += " - Incorrect log in information";
                            //unsuccessful
                        }
                    }
                    else
                    {
                        toolStripStatusLabel1.Text += " - User not found";
                        this.DialogResult = System.Windows.Forms.DialogResult.None;
                        //User not found
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return tmpUser;
        }

        /// <summary>
        /// Returns the user that is currently logged in, should be used with the dialogue result done in the main program
        /// </summary>
        /// <returns>Current user's information</returns>
        public UserInfo GetCurrentUser()
        {
            return mInfo;
        }

        private void CancelBtn_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Abort;
        }

        private void MoreBtn_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.None;
            if (Domain.Visible == true)
            {
                this.Size = new Size(385, 309);
                Domain.Visible = false;
                Rig.Visible = false;
                label2.Visible = false;
                label3.Visible = false;
                MoreBtn.Text = "vvvvv More vvvvv";
            }
            else
            {
                this.Size = new Size(385, 410);
                Domain.Visible = true;
                Rig.Visible = true;
                label2.Visible = true;
                label3.Visible = true;
                MoreBtn.Text = "^^^^^ Less ^^^^^";
            }
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked == true)
            {
                label1.Text = "Please log in with windows information";
            }
            else
            {
                label1.Text = "Please log in with PASTIE information";
            }
        }
    }
}
