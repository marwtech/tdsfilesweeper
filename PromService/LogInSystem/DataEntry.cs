﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Oclaro.PromService
{
    /// <summary>
    /// Auto generating data entry form to help with adding data to lots
    /// </summary>
    public partial class DataEntry : Form
    {
        Promis mPromisManager;
        List<Lot.DataCollectionOperations> mOps;
        Dictionary<string, string> values = new Dictionary<string, string>();
        string mtask, mEquip, mOpCount;
        Lot thisLot;

        /// <summary>
        /// Constructor for generic data entry
        /// </summary>
        /// <param name="ops">Operations to enter data for</param>
        /// <param name="manager">PROMIS manage</param>
        /// <param name="inLot">Lot to add data to</param>
        public DataEntry(List<Lot.DataCollectionOperations> ops, Promis manager,Lot inLot)
        {
            InitializeComponent();
            mOps = ops;
            mPromisManager = manager;
            mtask = null;
            mEquip = null;
            mOpCount = null;
            thisLot = inLot;
        }

        /// <summary>
        /// Constructor for TMS task data entry
        /// </summary>
        /// <param name="ops">Data collection operations to perform</param>
        /// <param name="manager">PROMIS manager</param>
        /// <param name="task">Tms task to complete</param>
        /// <param name="equip">Equipment the task is running on</param>
        /// <param name="opcount">operation number of DCOP on tms task</param>
        public DataEntry(List<Lot.DataCollectionOperations> ops, Promis manager, string task, string equip,string opcount )
        {
            InitializeComponent();
            mOps = ops;
            mPromisManager = manager;
            mtask = task;
            mEquip = equip;
            mOpCount = opcount;
            thisLot = null;
        }

        private void DataEntry_Load(object sender, EventArgs e)
        {
            int counter = 0;
            foreach (Lot.DataCollectionOperations ops in mOps)
            {
                #region testNum
                Label tmpLbl0 = new Label();
                tmpLbl0.Name = "Test" + counter.ToString();
                tmpLbl0.Location = new Point(0, 0 + (counter * 35));
                tmpLbl0.Text = ops.TestNumber;
                tmpLbl0.AutoSize = true;
                panel1.Controls.Add(tmpLbl0);
                #endregion
                #region name
                Label tmpLbl1 = new Label();
                tmpLbl1.Name = "Prompt" + counter.ToString();
                tmpLbl1.Location = new Point(10, 0 + (counter * 35));
                tmpLbl1.Text = ops.Prompt;
                tmpLbl1.AutoSize = true;
                panel1.Controls.Add(tmpLbl1);
                #endregion
                #region entry
                TextBox tmpLbl2 = new TextBox();
                tmpLbl2.LostFocus += new EventHandler(tmpLbl2_TextChanged);
                //tmpLbl2.TextChanged += new EventHandler(tmpLbl2_TextChanged);
                tmpLbl2.Name = "Entry" + counter.ToString();
                tmpLbl2.Location = new Point(190, 0 + (counter * 35));
                tmpLbl2.Width = 200;
                panel1.Controls.Add(tmpLbl2);
                if (ops.PickList != null)
                {
                    tmpLbl2.Text = ops.PickList[0];
                    if (ops.PickList.Length > 1)
                    {
                        ComboBox tmpBox = new ComboBox();
                        tmpBox.Name = "box" + counter.ToString();
                        tmpBox.Items.AddRange(ops.PickList);
                        tmpBox.Location = new Point(190, 0 + (counter * 35));
                        tmpBox.Width = 200;
                        tmpBox.SelectedIndexChanged += new EventHandler(tmpBox_SelectedIndexChanged);
                        panel1.Controls.Add(tmpBox);
                        tmpBox.BringToFront();
                    }
                }
                tmpLbl2.Text = "";
                #endregion
                #region Component
                Label tmpLbl7 = new Label();
                tmpLbl7.Name = "Component" + counter.ToString();
                tmpLbl7.Text = ops.Component;
                tmpLbl7.Location = new Point(650, 0 + (counter * 35));
                tmpLbl7.AutoSize = true;
                panel1.Controls.Add(tmpLbl7);
                #endregion
                #region Site
                Label tmpLbl6 = new Label();
                tmpLbl6.Name = "Site" + counter.ToString();
                tmpLbl6.Text = ops.Site;
                tmpLbl6.Location = new Point(590, 0 + (counter * 35));
                tmpLbl6.AutoSize = true;
                panel1.Controls.Add(tmpLbl6);
                #endregion
                #region Upper
                Label tmpLbl3 = new Label();
                tmpLbl3.Name = "Upper" + counter.ToString();
                tmpLbl3.Text = ops.UpLimit;
                tmpLbl3.Location = new Point(530, 0 + (counter * 35));
                tmpLbl3.AutoSize = true;
                //tmpLbl3.Width = (int)graphics.MeasureString(tmpLbl3.Text, tmpLbl3.Font).Width;
                panel1.Controls.Add(tmpLbl3);
                #endregion
                #region Lower
                Label tmpLbl4 = new Label();
                tmpLbl4.Name = "Lower" + counter.ToString();
                tmpLbl4.Text = ops.LowLimit;
                tmpLbl4.Location = new Point(470, 0 + (counter * 35));
                tmpLbl4.AutoSize = true;
                //tmpLbl4.Width = (int)graphics.MeasureString(tmpLbl4.Text, tmpLbl4.Font).Width;
                panel1.Controls.Add(tmpLbl4);
                #endregion
                #region Units
                Label tmpLbl5 = new Label();
                tmpLbl5.Name = "Units" + counter.ToString();
                tmpLbl5.Text = ops.Units;
                tmpLbl5.Location = new Point(410, 0 + (counter * 35));
                tmpLbl5.AutoSize = true;
                panel1.Controls.Add(tmpLbl5);
                #endregion

                counter++;
            }
            var l = Utilities.GetAllControls(panel1, typeof(Label));
            //Dictionary<string, string> values = new Dictionary<string, string>();
            foreach (Label lbl in l)
            {
                values.Add(lbl.Name, lbl.Text);
            }
        }

        void tmpLbl2_TextChanged(object sender, EventArgs e)
        {
            TextBox tmp = (TextBox)sender;
            char count = tmp.Name[tmp.Name.Length - 1];
            try
            {
                double current = Convert.ToDouble(tmp.Text);
                double upper = Convert.ToDouble(values["Upper" + count.ToString()]);
                double lower = Convert.ToDouble(values["Lower" + count.ToString()]);
                if (current > upper || current < lower)
                {
                    var message = MessageBox.Show("The value entered is out of range, do you want to continue with this value", "Out of range", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    switch (message)
                    {
                        case System.Windows.Forms.DialogResult.Yes:
                            break;
                        default: tmp.Text = "";
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                string test = ex.Message;
            }
        }

        void tmpBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox tmp = (ComboBox)sender;
            char count = tmp.Name[tmp.Name.Length - 1];
            panel1.Controls["Entry" + count.ToString()].Text = tmp.Text;
        }

        private void DataBtn_Click(object sender, EventArgs e)
        {

            if (thisLot != null)
            {
                //Generic data entry
            }
            else
            {
                //TMS data entry
                List<string> data = new List<string>();
                int counter = 0;
                var c = Utilities.GetAllControls(panel1, typeof(TextBox));
                foreach (TextBox txt in c)
                {
                    if (!string.IsNullOrWhiteSpace(txt.Text))
                    {
                        data.Add(txt.Text);
                        counter++;
                    }
                }

                string er, task, equip;
                List<Lot.DataCollectionOperations> Dcops = new List<Lot.DataCollectionOperations>();

                task = mtask;
                equip = mEquip;

                if (!mPromisManager.EnterTMSData(task, equip, mOpCount, data, textBox1.Text, out er))
                {
                    MessageBox.Show(er);
                    this.DialogResult = System.Windows.Forms.DialogResult.Abort;
                }
                else
                {
                    this.DialogResult = System.Windows.Forms.DialogResult.OK;
                }
            }

        }

        private void CancelBtn_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }
    }
}
