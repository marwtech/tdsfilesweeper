var class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slsp_set_row =
[
    [ "actionTriggerLevel", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slsp_set_row.html#ac83798802a546ed6ca582ca26486dc87", null ],
    [ "activeDate", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slsp_set_row.html#a7bba59a5bc47512bcb7506ab38c0ed7f", null ],
    [ "activeFlag", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slsp_set_row.html#a9be186891641a4a9c43b91bb54dc593b", null ],
    [ "changeDate", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slsp_set_row.html#a5c381e41ad150aa08f2d195767bcd44c", null ],
    [ "constraintExpression", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slsp_set_row.html#a52831f274a8740c563f54287fae524e6", null ],
    [ "createDate", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slsp_set_row.html#a29f7254f49d6317003a4674e400ec2df", null ],
    [ "description", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slsp_set_row.html#a64fc6835479712a2361c34cd1f771526", null ],
    [ "frozen", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slsp_set_row.html#acb8e431572e6fc6567da27e35e10137e", null ],
    [ "grpDocuId", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slsp_set_row.html#a57ada69b3cc542150806b30c226ffa93", null ],
    [ "NumQualificationTests", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slsp_set_row.html#a27b7c147612b36799156bcb88b8fadc3", null ],
    [ "NumReQualificationTests", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slsp_set_row.html#a907279a9c8f81503e1721766b0672249", null ],
    [ "owner", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slsp_set_row.html#a153833ea212485d7b796b68b1ce6fc88", null ],
    [ "prodStatus", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slsp_set_row.html#af029cede53f67d7c691d54a2a2ba4a0e", null ],
    [ "randomSampling", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slsp_set_row.html#a6e169d0c56745285fd04492810ee3cfb", null ],
    [ "sampleSize", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slsp_set_row.html#a45216e5e3b538658fff66b8b9262664e", null ],
    [ "skipPriorityThreshold", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slsp_set_row.html#a4e0c0d145f446594d902bb3a803c6839", null ],
    [ "slspActions", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slsp_set_row.html#a5f038796955fe226c3a9008368e58ea9", null ],
    [ "slspActionsExtern", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slsp_set_row.html#af6a616b72b7afb5c9f5025007c027c5b", null ],
    [ "slspId", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slsp_set_row.html#a9e9d04041c135448968ebab963669891", null ],
    [ "versionedRecpBased", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slsp_set_row.html#a4dbf69bb62dfa8ce52ad307d7c8f40d0", null ]
];