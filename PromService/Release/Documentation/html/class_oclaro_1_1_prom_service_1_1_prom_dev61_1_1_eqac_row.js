var class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_eqac_row =
[
    [ "activityID", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_eqac_row.html#a288f44212c5c6d424cce86745af440bc", null ],
    [ "activitySubType", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_eqac_row.html#a64727ee2c8050ad85626357ab48384ce", null ],
    [ "activityType", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_eqac_row.html#ad958e8fe9dc803dd4083e02416d0d1c3", null ],
    [ "description", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_eqac_row.html#a703f1dcbe20aec16e1addd93d351a3ae", null ],
    [ "isQual", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_eqac_row.html#a86c1326a86f15b99db4656ce11eb1ed6", null ],
    [ "isSupplierDelay", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_eqac_row.html#af54d33cae8c41047f72795ab6d780c77", null ],
    [ "isWaitingDownTime", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_eqac_row.html#ac6b4dd540d1a6abbd139db5c21814133", null ]
];