var class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_sub_lot =
[
    [ "comclass", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_sub_lot.html#a265b5906b0a3f2edfd96fa3b6b2ada8f", null ],
    [ "curmainqty", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_sub_lot.html#af7ddbf25c24d669e739614ac01d4c4be", null ],
    [ "curprcdcurinstnum", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_sub_lot.html#a6d7100006c88f42cf934dd5ead94fede", null ],
    [ "curPrcdId", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_sub_lot.html#a9401fa3e09ba1bfaa3222880b3f9ba14", null ],
    [ "curprcdname", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_sub_lot.html#ac1fce25e62f093d4f303326eda9f63f2", null ],
    [ "eqpId", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_sub_lot.html#a8b251837a60a2870692775ead852d66a", null ],
    [ "eqptype", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_sub_lot.html#a7507ef9af4f09290ecd9509c7c2cfe9b", null ],
    [ "location", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_sub_lot.html#ae8a15b9de4d6f0107dd7d663f6f61130", null ],
    [ "lotid", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_sub_lot.html#a5b6342052c37978965bd98bb18724e87", null ],
    [ "lottype", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_sub_lot.html#a9b7f9654dbd6b6704842d358c67eb062", null ],
    [ "mainmattype", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_sub_lot.html#a1fb1fc9d583dc30d4eec422d6a37f8e7", null ],
    [ "partid", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_sub_lot.html#adbd2a97873caff542ab23b4ac95242b1", null ],
    [ "planstarttime", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_sub_lot.html#a0381502ace42291aaf7a6fe11062ebee", null ],
    [ "priority", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_sub_lot.html#afe6275f7cce502c3e7ca3d38147923ec", null ],
    [ "procstate", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_sub_lot.html#a30a67f1006ab798b8bc07eeccda7ad3a", null ],
    [ "prodarea", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_sub_lot.html#acb3840108e9757d687c5536668e886f2", null ],
    [ "recpid", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_sub_lot.html#a165d62bba84255e5f8b36493f83ccb4d", null ],
    [ "stage", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_sub_lot.html#a40d3ddc641ad2fec267518369da3a9a2", null ],
    [ "state", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_sub_lot.html#abed858ed1ebfb423b88107d25e7895ca", null ],
    [ "stateentrytime", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_sub_lot.html#a1e3a18bf86a2650af3d6cab893f80240", null ],
    [ "testbatchid", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_sub_lot.html#a7abd029ac8daa25268e2aa992f268389", null ]
];