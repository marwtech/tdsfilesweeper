var indexSectionsWithContent =
{
  0: "abcdefghiklmnopqrstuvw",
  1: "abcdefghilmopqrstuw",
  2: "ot",
  3: "acdefghilmnopqrstuw",
  4: "abcdfhilmprs",
  5: "fp",
  6: "ipsu",
  7: "abcdefghiklmnopqrstuvw",
  8: "acdeglmpt"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "properties",
  8: "events"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Enumerator",
  7: "Properties",
  8: "Events"
};

