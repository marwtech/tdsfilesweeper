var class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_extended =
[
    [ "automatedRecipeId", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_extended.html#ab7cc6d7c867edbf6a38525fa2f6d4463", null ],
    [ "cr1", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_extended.html#a0c3627c811a888bf55b408973884b1e7", null ],
    [ "cr2", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_extended.html#a51bce3c1d0ef4bd6ac625ec8e957a1a5", null ],
    [ "cr3", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_extended.html#a7360ba3b4074f5eb6f872eda161c9fb1", null ],
    [ "createtime", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_extended.html#a820bacf6692ee89c69196ccd06100873", null ],
    [ "curprcdinsttype", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_extended.html#ad9b1f45e87a57b753565fb280aee2a11", null ],
    [ "curprcdkind", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_extended.html#a6d24e36479d8ae7195e8e54a1840e3da", null ],
    [ "curPrcdMainProdArea", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_extended.html#a05c62c7beb79f04ed99ea760afb38cc3", null ],
    [ "curprcdstage", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_extended.html#a90332df5edca4976f03dcc9b3a01327f", null ],
    [ "customername", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_extended.html#aff549db1deb67760a5165395938e5587", null ],
    [ "custordernumber", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_extended.html#a85fcb4379cf963da375a4846e96e1529", null ],
    [ "efflotyld", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_extended.html#a03af8a3520fa7a2852369b50df5ded68", null ],
    [ "empIdTrackIn", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_extended.html#a179b609c2a2c1d96a83d7d884da192ee", null ],
    [ "externalPriority", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_extended.html#a32b7e15128f5a873d47ec89590e5d7f7", null ],
    [ "holdcode", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_extended.html#a34e811ebb5b350d525344f457b6d1fae", null ],
    [ "holdreas", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_extended.html#a27323de8bc45c73570eeb12b3ce3cc85", null ],
    [ "holduser", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_extended.html#a4079d4d2659ed1aee7e7a5cf8d4ae1c9", null ],
    [ "lastevtime", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_extended.html#ad557edfd5f1eea2a052ba6b90639d83e", null ],
    [ "locationtype", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_extended.html#a212ff518e2f13e2ee6ebc116a5c050bd", null ],
    [ "lotinfo_auditreworkrequired", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_extended.html#a2640ef243137aa96f5ba43b72420b280", null ],
    [ "lotinfo_rejoininstnum", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_extended.html#a390f2d0ed585905e0b3d3f645c790721", null ],
    [ "lotinfo_rejoinprcdid", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_extended.html#a246a39ffc5690ed565ea910b45007a8b", null ],
    [ "lotinfo_repeatallpossible", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_extended.html#a0c89e66ccc2ed24757ae8cb16e16ed08", null ],
    [ "lotinfo_repeatsplitpossible", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_extended.html#a01211cd9aacf917699ba9508497f48bc", null ],
    [ "lotinfo_resumeinstnum", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_extended.html#a2479678899251a5103026522fb9695d6", null ],
    [ "lotinfo_reworkallpossible", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_extended.html#ae0b61a58069daaf74b417b94db497e91", null ],
    [ "lotinfo_reworkmaxcycle", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_extended.html#a2df72a7e8b29d2d33b05d231fd163ea9", null ],
    [ "lotinfo_reworkprcdid", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_extended.html#a2a1a69cbead35586dc9745512ed1db01", null ],
    [ "lotinfo_reworksplitpossible", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_extended.html#a128eac5ca1151ded428160c366105914", null ],
    [ "lotinfo_tempsplitpossible", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_extended.html#a32faeb2b68a30765d537f53092721b18", null ],
    [ "lotinfo_testreworkrequired", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_extended.html#a1b3e37e2d6f1246be78f78369e539325", null ],
    [ "mainStepYld", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_extended.html#a6c43decb0dee7cc62e07bda09ef0753a", null ],
    [ "maxreworkcyclecount", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_extended.html#a2e7b28dbf46c9c127c6014b0606d5803", null ],
    [ "mLotYld", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_extended.html#aab6fc7a2470f190e8e6277cfa2054207", null ],
    [ "planable", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_extended.html#aa99f7d1e04e00d3cbf6afc20b0631b84", null ],
    [ "planstarttime", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_extended.html#a18c60471ec632b8128464d7c3caa747d", null ],
    [ "queueTime", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_extended.html#a856dd6d90c619baf8a0063030674c2bb", null ],
    [ "reqdtime", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_extended.html#a1334d85a0691261fb629d74abe43b4f8", null ],
    [ "reworkcyclecount", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_extended.html#afcb32399b2479cd8b1ceebe7f01bfe17", null ],
    [ "startMainQty", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_extended.html#a86bc12ea72cc5d0b88049a4b2635318d", null ],
    [ "starttime", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_extended.html#abe96f18e0c98691c2ecfe170b478d861", null ],
    [ "stateentrytime", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_extended.html#ae9f3e89bce6e1c8032842d628b833e6f", null ],
    [ "stepstartmainmatident", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_extended.html#a0e6fd5a52d9108832a3d9442f235ff51", null ],
    [ "stepstartmainmattype", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_extended.html#a65b5b6ab61ded38110a0169f01dbbcea", null ],
    [ "stepstartprocstate", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_extended.html#ab773848423ec6019feebbddc5402c7a4", null ],
    [ "stockpos", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_extended.html#aaecdaeae4be6440bbdb2a5ab70c48bcf", null ],
    [ "substepyld", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_extended.html#ad4a556ece3b63a46044c8e27d9328e66", null ],
    [ "trackInMainQty", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_extended.html#ae60b10ef6d7162d08b18471a78bd34b4", null ],
    [ "trackInTime", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_extended.html#ab4d1e7105e6cfdffa0a6f9ddc7a30668", null ],
    [ "trackouttime", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_extended.html#ae354261e9348bb3f74aacacbb420c262", null ]
];