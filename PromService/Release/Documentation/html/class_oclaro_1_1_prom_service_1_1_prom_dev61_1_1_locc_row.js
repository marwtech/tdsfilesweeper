var class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_locc_row =
[
    [ "acceptLot", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_locc_row.html#a8092622ec8777a93f12c6c1a794e3de2", null ],
    [ "description", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_locc_row.html#a7964384e22760c000c17dee82bc1d792", null ],
    [ "dispatchType", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_locc_row.html#a6aac754c9949362285a9e7d4108aef0a", null ],
    [ "isControlledRejectDest", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_locc_row.html#a29578967094910ca12abeec46b53ebca", null ],
    [ "locationId", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_locc_row.html#a130dcf2493475db2c5d68f269f625531", null ],
    [ "locationType", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_locc_row.html#a6f1e5d6a660786b2392c1ad121bb5141", null ],
    [ "permitShip", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_locc_row.html#a2abc417faaaa798fe4ac236af67b908c", null ],
    [ "prodArea", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_locc_row.html#a6e91c761a46f080a9b9a89fb643700a9", null ],
    [ "usableParts", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_locc_row.html#a1c6a30e40fca869509a26ff67fc9dd98", null ]
];