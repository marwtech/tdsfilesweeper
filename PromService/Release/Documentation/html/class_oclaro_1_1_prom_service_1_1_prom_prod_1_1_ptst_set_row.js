var class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_ptst_set_row =
[
    [ "carrierId", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_ptst_set_row.html#a9b6d4fa008fc03eb33863a2f26522c9c", null ],
    [ "changeDt", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_ptst_set_row.html#a3d615fd54f94368dc9bf7d4939e23c76", null ],
    [ "changeReason", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_ptst_set_row.html#a99523a58f389bdf32e782fca46ee78fb", null ],
    [ "commentCount", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_ptst_set_row.html#a1db4af559d115a3c342f614b84da2c81", null ],
    [ "empId", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_ptst_set_row.html#a9cb14a8b2c582abe41b78c40e9c8e244", null ],
    [ "eqpId", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_ptst_set_row.html#aad8a2b214b7e12ca15e065003a066fd7", null ],
    [ "portAssociationState", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_ptst_set_row.html#a9c8174806d618a3f25a65b46ed5b4661", null ],
    [ "portId", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_ptst_set_row.html#ad692b2d31ad09bf15bb98817c308cc32", null ],
    [ "portReservationState", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_ptst_set_row.html#a0b938f423fb7842fbc3425b5bb5748ff", null ],
    [ "portTransferState", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_ptst_set_row.html#a512ef942e8a5802e24e4c2dfc7e9c245", null ]
];