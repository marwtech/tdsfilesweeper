var class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_docu =
[
    [ "activeDate", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_docu.html#ab62738674fb6657c696da26724646309", null ],
    [ "changeDate", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_docu.html#a52654f1b27a00c2c7f00a7a8234f3259", null ],
    [ "createDate", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_docu.html#ac4a6c39e95a9422b541a4b86c1ad8c5d", null ],
    [ "docuActiveFlag", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_docu.html#a016394d474c0f4559dd335a1ec405084", null ],
    [ "docuFrozen", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_docu.html#a95227c885abed845cdbb4a9087389527", null ],
    [ "docuGroup", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_docu.html#a8618b00d98dd4abdf5933d4c69b7ba7b", null ],
    [ "docuId", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_docu.html#aae045c725f41fbf091ae781f859bc474", null ],
    [ "docuTitle", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_docu.html#a92e61804251a747edce3fc8580cd1f37", null ],
    [ "owner", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_docu.html#a956fca27e4a71709284ebdeba38f68a2", null ],
    [ "prodStatus", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_docu.html#ad15206ae17017980ec0dd2ac5ee56032", null ],
    [ "seqNumber", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_docu.html#a44281e47f28ddf811ca21d1eee6a94ea", null ],
    [ "textLength", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_docu.html#a8ac1b3abc08750bb49f2fba37c1ef3a1", null ]
];