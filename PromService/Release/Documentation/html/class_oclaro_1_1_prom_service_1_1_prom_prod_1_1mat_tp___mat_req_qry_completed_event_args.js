var class_oclaro_1_1_prom_service_1_1_prom_prod_1_1mat_tp___mat_req_qry_completed_event_args =
[
    [ "actl", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1mat_tp___mat_req_qry_completed_event_args.html#acfb2d65e1bc594595f736bfa72f790e3", null ],
    [ "itemDoneCount", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1mat_tp___mat_req_qry_completed_event_args.html#abbcc371bd8c78105eba36afa3f32dc78", null ],
    [ "remReqDoneCount", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1mat_tp___mat_req_qry_completed_event_args.html#a1eb9f709ca869f4fa0cf6ef1cca961b5", null ],
    [ "reqItemsTable", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1mat_tp___mat_req_qry_completed_event_args.html#a58a03f83889060ad3dedba5c336d8899", null ],
    [ "reqPartsTable", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1mat_tp___mat_req_qry_completed_event_args.html#a70cd65f22c7ed7a143631d9bfc85fb8d", null ],
    [ "Result", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1mat_tp___mat_req_qry_completed_event_args.html#a2d6f58ebc73af06a4c7ce443a970fa75", null ],
    [ "tps_infoText", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1mat_tp___mat_req_qry_completed_event_args.html#a3642992f2be4b32bea20c97758367d92", null ]
];