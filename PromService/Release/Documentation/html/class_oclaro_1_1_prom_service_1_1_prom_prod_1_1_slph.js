var class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slph =
[
    [ "approvalTime", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slph.html#a4edfc40ade0eaa974a004fc541b92577", null ],
    [ "approver", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slph.html#a118258ff839f460a0ebc798547912c40", null ],
    [ "eqpId", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slph.html#a0fc3346ed7901abfd4ed727e87c164d0", null ],
    [ "lotId", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slph.html#a8511deada5279bbbf406848d56b56c8f", null ],
    [ "lotSkipState", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slph.html#a80eb58c89db22527bb2decc6115b0141", null ],
    [ "metrologyDisposition", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slph.html#a29f34dc16a5291db3d83820234a49084", null ],
    [ "prcdStackCount", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slph.html#a2d85896654f2160c3195f4a4e5da29da", null ],
    [ "processTime", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slph.html#aa03a46670524cf150f60463bbefea563", null ],
    [ "recpId", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slph.html#aae2c476f171698b40c4f889ae738385a", null ],
    [ "revTime", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slph.html#a43866dfc384bc2df3ba05b93a0f4ec96", null ],
    [ "slspId", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slph.html#a6ad609f80bf2f507efcc661d4b155d6d", null ],
    [ "testCount", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slph.html#a591be3cbff3c17e10d5aca62cefa4c55", null ],
    [ "wasAtRisk", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slph.html#af46f7ea202c73fd9a50d0bdaac61427e", null ]
];