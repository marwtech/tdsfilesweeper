var class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_fin_lots_extended_row =
[
    [ "curMainQty", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_fin_lots_extended_row.html#a0a5917e4baf56b06c4f0f26f51f7d813", null ],
    [ "location", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_fin_lots_extended_row.html#a3eae83a694ddd98257f53953b4ea965e", null ],
    [ "lotId", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_fin_lots_extended_row.html#a407d74b57fe779fdaa046e27dc23aa43", null ],
    [ "mainMatType", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_fin_lots_extended_row.html#ad88b2c9a1e565e5b3d43a1409db238d8", null ],
    [ "partId", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_fin_lots_extended_row.html#a3c9c4e6a0e4d69490436a9b589d04a12", null ],
    [ "priority", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_fin_lots_extended_row.html#a09a2ee6602b48472344ef1e8a51b542b", null ],
    [ "prodArea", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_fin_lots_extended_row.html#a6274ec8511724c1a59b0a5378992f21e", null ],
    [ "queueTime", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_fin_lots_extended_row.html#a079314ba5d9cbf82b69f576764bd9d00", null ],
    [ "stage", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_fin_lots_extended_row.html#a620859521b933ef7521d698015938fd0", null ],
    [ "state", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_fin_lots_extended_row.html#a5492e4536f5d87a954a40b34f680d271", null ]
];