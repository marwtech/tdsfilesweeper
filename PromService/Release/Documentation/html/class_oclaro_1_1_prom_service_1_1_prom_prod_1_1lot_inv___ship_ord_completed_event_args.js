var class_oclaro_1_1_prom_service_1_1_prom_prod_1_1lot_inv___ship_ord_completed_event_args =
[
    [ "actlSet", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1lot_inv___ship_ord_completed_event_args.html#a4aac753c632381dad51308fec8e8b023", null ],
    [ "lotInv_shipLotCount", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1lot_inv___ship_ord_completed_event_args.html#aab96ec2c697d1b2f30697f152456d2ad", null ],
    [ "lotInv_shippedSet", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1lot_inv___ship_ord_completed_event_args.html#aa6cd615329443f30e86672fd9a57c20e", null ],
    [ "lotInv_shipTolMainQty", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1lot_inv___ship_ord_completed_event_args.html#ad84b9f92cd59985ba49caf5e887d4533", null ],
    [ "lotInv_shipTolSubQty", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1lot_inv___ship_ord_completed_event_args.html#a2715d65a403d558f0e4c645ef0ef657d", null ],
    [ "Result", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1lot_inv___ship_ord_completed_event_args.html#a3bd0669e753bed0cd75d0e57fe0a29f9", null ],
    [ "tps_infoText", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1lot_inv___ship_ord_completed_event_args.html#afb68a7ee9f18ff917495a823c7239c0c", null ]
];