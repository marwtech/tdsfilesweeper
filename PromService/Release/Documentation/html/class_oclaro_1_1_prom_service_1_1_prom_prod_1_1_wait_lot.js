var class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_wait_lot =
[
    [ "comclass", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_wait_lot.html#a4117e7f8c524f99c57ced105d62fe9c2", null ],
    [ "curmainqty", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_wait_lot.html#a32d397f5cb9602db2b2b563a96d82095", null ],
    [ "curprcdcurinstnum", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_wait_lot.html#a6543e6ceec1cd86fb490419acf0f62e5", null ],
    [ "curPrcdId", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_wait_lot.html#abe64a93b17027e9dca4da2e57b44963c", null ],
    [ "curprcdname", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_wait_lot.html#a423996a5e59e60f2dc9c8d4145750bca", null ],
    [ "eqpId", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_wait_lot.html#a8a0df068fbe5de0b0d35c4a6c3699a7c", null ],
    [ "eqptype", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_wait_lot.html#a1508d8a3176410f2b1d306d609242091", null ],
    [ "location", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_wait_lot.html#a27a2e11dd23e8e4e1f7825351bcadcaa", null ],
    [ "lotid", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_wait_lot.html#a4f9de50850d05d0be4c8d67648055c53", null ],
    [ "lottype", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_wait_lot.html#af3359f6ac05cfabd2be25521e6157d33", null ],
    [ "mainmattype", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_wait_lot.html#a056dd658adba6e4cb470242f477aef80", null ],
    [ "partid", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_wait_lot.html#a32302f8d25ea7875772b8bc2f0c6f3b8", null ],
    [ "planstarttime", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_wait_lot.html#a540ea9c2fc24abdd079117d9a72c5713", null ],
    [ "priority", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_wait_lot.html#a7d3750d0c67b1e4df79fe07a27a1cd7a", null ],
    [ "procstate", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_wait_lot.html#a2a52e31e0a5077e538ca98b98f7a5488", null ],
    [ "prodarea", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_wait_lot.html#a4cc1a3ba5c06519fce5d3be15a97df3d", null ],
    [ "recpid", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_wait_lot.html#a6f941d776c9a89e81d9e199276e9145c", null ],
    [ "stage", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_wait_lot.html#a5f602793a72458e6538382e9f916279a", null ],
    [ "state", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_wait_lot.html#ac5ee7208276a9ec1793cfe3e4352e363", null ],
    [ "stateentrytime", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_wait_lot.html#a67246c294bf3eb310ad6e0ebaf27ce18", null ],
    [ "testbatchid", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_wait_lot.html#a9d7c17b6afdab712b05a1b5c50096c3f", null ]
];