var class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_docu =
[
    [ "activeDate", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_docu.html#ac6d6799afde74bfeed9bf148979e5e4d", null ],
    [ "changeDate", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_docu.html#a42de8431e215d545c482d08db64c996e", null ],
    [ "createDate", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_docu.html#afd2f9c2ec68718da702f4d5b18b8ff24", null ],
    [ "docuActiveFlag", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_docu.html#ad125020a313a458b14faeb2beb1191e0", null ],
    [ "docuFrozen", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_docu.html#a84bd4b1d199e3207bc433c8004fba626", null ],
    [ "docuGroup", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_docu.html#aee8537b4a7f1fbeeafce23115fff3529", null ],
    [ "docuId", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_docu.html#a42eca111fa4ec5b6bb68d6de672510ac", null ],
    [ "docuTitle", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_docu.html#a6eb18eeecc03bde3a2b74537d190d350", null ],
    [ "owner", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_docu.html#a630face45ae2b7225311980bd74917c5", null ],
    [ "prodStatus", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_docu.html#a47e20355da96a836693e1f01ca06df44", null ],
    [ "seqNumber", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_docu.html#ac1998302c9467b2ade64a0129e52c31f", null ],
    [ "textLength", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_docu.html#af3b913f187b3c1b84e3c25cf28ebee84", null ]
];