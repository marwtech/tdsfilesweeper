var class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_oper_set_row =
[
    [ "action", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_oper_set_row.html#aae80b6a54770c4408993f039dcff2e81", null ],
    [ "changeDate", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_oper_set_row.html#aed203c943a4aa7acab245cf7c7551086", null ],
    [ "createDate", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_oper_set_row.html#ac7c2bc53766409303459876e72b7e669", null ],
    [ "description", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_oper_set_row.html#a47b8cd8b0c9b8e59cb039e94b80d7007", null ],
    [ "docuGroup", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_oper_set_row.html#abd86abb4328d69519560f6d9a50a64ac", null ],
    [ "docuName", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_oper_set_row.html#ab981ee977a3fa8e298abba169b5ef70f", null ],
    [ "docuVersion", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_oper_set_row.html#a1451da6d6b94a72c613214b2e3a1e964", null ],
    [ "engOwner", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_oper_set_row.html#ac02a88fa429d6b2801df6971238b372b", null ],
    [ "frozen", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_oper_set_row.html#a7ce3a8f320dae0cac834d8326d2a8c17", null ],
    [ "operId", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_oper_set_row.html#aeb0582d758b1397160a5ff834c545947", null ],
    [ "operName", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_oper_set_row.html#ad683ef12bae4d8cef578ca56bf98ec27", null ],
    [ "operVersion", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_oper_set_row.html#aadeaef5fd41d2262feb5d6d9216983df", null ],
    [ "prodStatus", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_oper_set_row.html#af8a57d9232f1dad06d94ca83ef7f21ed", null ],
    [ "type", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_oper_set_row.html#a5d401afc50995c2b13b97b1b531ca7b7", null ]
];