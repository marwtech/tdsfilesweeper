var class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slsp =
[
    [ "actionTriggerLevel", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slsp.html#a26f2ec8a8e46dc94b8f86de822bb919c", null ],
    [ "activeDate", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slsp.html#a5d976495a663baefc24d5405e0a5e405", null ],
    [ "activeFlag", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slsp.html#ad8e61f13dd1a80d26defc7906e47867c", null ],
    [ "changeDate", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slsp.html#ae97ebffd115cb89d80193ab8122f37d2", null ],
    [ "constraintExpression", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slsp.html#aadecf0ebf05544fd2e3724c9aaf369ce", null ],
    [ "createDate", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slsp.html#a63dd33bc73ea627c9d9fdf5ce778c191", null ],
    [ "description", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slsp.html#a1ac1ae84e20eb6fe13f805a376e1164f", null ],
    [ "frozen", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slsp.html#a5c0d8e203fadae7599602358e2e866b0", null ],
    [ "grpDocuId", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slsp.html#a8ddd00a2144934422094c78c7bf105ac", null ],
    [ "NumQualificationTests", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slsp.html#a88874a68c7166a1bbc9c62e7354ac5c5", null ],
    [ "NumReQualificationTests", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slsp.html#a27f29e1f39c804f4b12c29a73a4f794b", null ],
    [ "owner", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slsp.html#a9e22adf289e3cdd08c5dcdb171f78d00", null ],
    [ "prodStatus", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slsp.html#a1663688bb49a7ccc3de8c8ecb0e60bb4", null ],
    [ "randomSampling", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slsp.html#a19bc645c876c2303346e3f6b34d7e770", null ],
    [ "sampleSize", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slsp.html#a6e2447bcdea6d3831fbcec1094e9034a", null ],
    [ "skipPriorityThreshold", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slsp.html#a9c241934799457fb2fcc04fff9ba6eb6", null ],
    [ "slspActions", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slsp.html#a4f835f90b4fe8fb35b0a737b5b1d0e10", null ],
    [ "slspActionsExtern", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slsp.html#a7e530227ed77093864cde0a958fe8378", null ],
    [ "slspId", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slsp.html#ad82b9d5f8a3d269448c71d3fb16b9127", null ],
    [ "versionedRecpBased", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slsp.html#a579c30c507a84cc17223d5d7dee9ddc3", null ]
];