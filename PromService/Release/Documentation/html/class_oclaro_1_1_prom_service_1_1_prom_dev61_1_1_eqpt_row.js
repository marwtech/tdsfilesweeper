var class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_eqpt_row =
[
    [ "accessMode", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_eqpt_row.html#a445cbaf2398c806a744797205c07d17c", null ],
    [ "bufferType", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_eqpt_row.html#a9fd2a4cf34959d4f87a62b74a757de17", null ],
    [ "carrierType", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_eqpt_row.html#a659a25dc93905b532fe06f8e8a8da11d", null ],
    [ "description", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_eqpt_row.html#a05de71bb3949a95e9e031dc6f48c7f96", null ],
    [ "eqpId", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_eqpt_row.html#a5d888216164c4da4301d3ca8fa25d706", null ],
    [ "ioType", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_eqpt_row.html#a8b91f3b1e024ecd4baa981c8c30abb6e", null ],
    [ "materialType", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_eqpt_row.html#a25620f6c5db6ea98fe45df975f870f62", null ],
    [ "portCapability", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_eqpt_row.html#ac76a8499e5950af0b1d526c68700f5a5", null ],
    [ "portId", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_eqpt_row.html#a70cd2ec163dcda55677c109df466b32b", null ]
];