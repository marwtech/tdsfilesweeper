var class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slph_set_row =
[
    [ "approvalTime", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slph_set_row.html#ad4a47aaedc2fda7b3f51d9c2a51ceee3", null ],
    [ "approver", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slph_set_row.html#a7efe2161ee021566188ce59dcf2b2097", null ],
    [ "eqpId", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slph_set_row.html#a8300598ecae39d916f76ccbff42308a2", null ],
    [ "lotId", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slph_set_row.html#a6e9d56f680ae4617267f1c94ff77d869", null ],
    [ "lotSkipState", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slph_set_row.html#aa2bd0510cec512c1610f50727f453730", null ],
    [ "metrologyDisposition", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slph_set_row.html#ad15cbf08775013213308b5c3201e777f", null ],
    [ "prcdStackCount", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slph_set_row.html#a73081dc82426a0265a23d6f118791cf7", null ],
    [ "processTime", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slph_set_row.html#a1d700fb61bd6289b15b2f19fb1153389", null ],
    [ "recpId", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slph_set_row.html#ab75386d155087137f139e1ffece69c0f", null ],
    [ "slspId", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slph_set_row.html#ab456bd24572e450884e9e38b06092633", null ],
    [ "testCount", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slph_set_row.html#a0a6510c35ac2ae4e1ff965b5c91b2f28", null ],
    [ "wasAtRisk", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slph_set_row.html#a0d14447c0dc3a76382f5924d64161e01", null ]
];