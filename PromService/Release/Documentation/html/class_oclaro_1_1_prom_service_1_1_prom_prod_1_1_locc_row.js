var class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_locc_row =
[
    [ "acceptLot", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_locc_row.html#ac0b24679fc0fcece792a848bb5e43544", null ],
    [ "description", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_locc_row.html#a94a220b1c80bbb5757a5b0b5c8a7dc43", null ],
    [ "dispatchType", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_locc_row.html#a35efa38e984095e2a5114d636cc86afb", null ],
    [ "isControlledRejectDest", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_locc_row.html#a5622b86405d654bc33d70c3e401ff119", null ],
    [ "locationId", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_locc_row.html#af2a190be57bd93fc62aa08dd88c5cc9e", null ],
    [ "locationType", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_locc_row.html#aa7c5d9cf644afc6247669589e7f1e6cf", null ],
    [ "permitShip", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_locc_row.html#a8b2347b92494b84f615a5cf290362564", null ],
    [ "prodArea", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_locc_row.html#a526de2d631d8e5d13726ef6f04b8517d", null ],
    [ "usableParts", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_locc_row.html#a062020411c262e79290de4966458d449", null ]
];