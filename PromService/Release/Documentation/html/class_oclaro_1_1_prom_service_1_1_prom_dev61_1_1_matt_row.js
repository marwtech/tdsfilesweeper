var class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_matt_row =
[
    [ "carrierName", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_matt_row.html#ab3112c37ef9e8e69dab6ba606bbf74e6", null ],
    [ "carrierNamePlural", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_matt_row.html#ae26ebfd268940398aa66d7232b425e8d", null ],
    [ "isDiscrete", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_matt_row.html#aebdd347fffaa7fa0bf1d6916e79f4341", null ],
    [ "matItemName", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_matt_row.html#aa2a23391278a52b78781eb7c5fb316a1", null ],
    [ "matItemNamePlural", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_matt_row.html#ad77cf69124b22ba1745687520b0b00e4", null ],
    [ "matTypeCode", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_matt_row.html#abe953f36643f582e6d97f28d83d0f07e", null ],
    [ "matTypeId", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_matt_row.html#aeec91930b5e3f707650e91762b4fc44b", null ],
    [ "unitID", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_matt_row.html#abdfc0ebf4af04bc08fce9d526d00aa25", null ]
];