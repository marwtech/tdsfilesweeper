var class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_fin_lots_extended_row =
[
    [ "curMainQty", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_fin_lots_extended_row.html#a37874019bce553d80720bbd6df977ede", null ],
    [ "location", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_fin_lots_extended_row.html#a0f1bbe5805acce4c01878c7f5e4e0dda", null ],
    [ "lotId", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_fin_lots_extended_row.html#ab9e12c6eb4788b2e87705dc6e00bd58d", null ],
    [ "mainMatType", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_fin_lots_extended_row.html#a57c80c4e3a26badcc601f9b94273df1e", null ],
    [ "partId", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_fin_lots_extended_row.html#ae5f1e12e4758ce6ef9be9147180e2576", null ],
    [ "priority", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_fin_lots_extended_row.html#a2a68c55713a66786177b6ce6c1809ecc", null ],
    [ "prodArea", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_fin_lots_extended_row.html#a818bba5d89cacc8d0876470e3ed4db72", null ],
    [ "queueTime", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_fin_lots_extended_row.html#aff1fc24a476a4f309fa38d14cdc4f8d7", null ],
    [ "stage", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_fin_lots_extended_row.html#a4061d0f2042a28e137e2d40b771c0cdd", null ],
    [ "state", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_fin_lots_extended_row.html#a334bc6753714093f15e13ee1667ec7c6", null ]
];