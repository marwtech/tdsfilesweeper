var class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_req_items_table_row =
[
    [ "absLowerLimit", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_req_items_table_row.html#a20cf6adc3b48e7fadae83bd15e033be1", null ],
    [ "absTargetQty", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_req_items_table_row.html#ae8860943419a9bb55f6315f5eff74d29", null ],
    [ "absUpperLimit", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_req_items_table_row.html#aedbf974825a70659ed28f6ee911e0710", null ],
    [ "balanceCode", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_req_items_table_row.html#a15530c00b9d77c7ce84b91be40d526b1", null ],
    [ "balanceToConsume", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_req_items_table_row.html#a00cfedb26fcb9e144e8f4ac3942f448c", null ],
    [ "docuGroup", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_req_items_table_row.html#a666e9b33df0d2ca694bdf9a524346da0", null ],
    [ "docuId", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_req_items_table_row.html#a29a6b214f00d256ca32ed05ac4f62593", null ],
    [ "holdIfOOS", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_req_items_table_row.html#a22980720da167a8009705d7f35d84ff1", null ],
    [ "itemAction", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_req_items_table_row.html#a9f9106782e08293b10c6e86e57a9d16f", null ],
    [ "itemDescription", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_req_items_table_row.html#a6cae7f1b179f5c42b5eebc4f9d9bef20", null ],
    [ "itemDone", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_req_items_table_row.html#a5f0fcd45a4895e717a62a1037dbab8f6", null ],
    [ "itemDoneUserId", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_req_items_table_row.html#a6e4bcfe0b40ab2772fd25cf576473070", null ],
    [ "itemNum", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_req_items_table_row.html#aa21a37bc8c7c4cea8a258dd263dedde5", null ],
    [ "matType", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_req_items_table_row.html#a09d54ade0e07f4e533f9102723d9212b", null ],
    [ "mistCount", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_req_items_table_row.html#a8a96fdc85be7a96a05ebe9094dc95da3", null ],
    [ "totalQtyConsumed", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_req_items_table_row.html#a494f06674f8b794ba28fbd194b9ea017", null ],
    [ "units", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_req_items_table_row.html#ac507c6d68b27c28fa2e6f2192cf88f2f", null ],
    [ "unsourcedMat", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_req_items_table_row.html#a160fc112c285e2c056dc0e7c5bd0224e", null ]
];