var class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_ptst =
[
    [ "carrierId", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_ptst.html#ab507ee288713ea767c31782d49215323", null ],
    [ "changeDt", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_ptst.html#a0763dbae0e5261c09caf5a6c52cbbda2", null ],
    [ "changeReason", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_ptst.html#af8c4a71ac93f57561d03331baebefc73", null ],
    [ "commentCount", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_ptst.html#a7f2da2c198557e669d85829dc27b2f2d", null ],
    [ "empId", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_ptst.html#aea675b76853b9d7d67cbce77afe71850", null ],
    [ "eqpId", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_ptst.html#aaa7b38c5461d7bbc510898b44011607d", null ],
    [ "portAssociationState", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_ptst.html#af0ddfc033a39746920525ed3b526269d", null ],
    [ "portId", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_ptst.html#a74140c5bc38f734d183722369ed853ce", null ],
    [ "portReservationState", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_ptst.html#a461f7bf9fcd28f7f0b4230f3f1e10f96", null ],
    [ "portTransferState", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_ptst.html#ae040259e80efa2d369a71695e0cb8401", null ]
];