var class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_result_lot =
[
    [ "comclass", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_result_lot.html#a7485c8a0b1aeb59c4c2188600d29ca33", null ],
    [ "curmainqty", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_result_lot.html#a78f5eb3f80cf3e0e10f1d14a4435018a", null ],
    [ "curprcdcurinstnum", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_result_lot.html#a7c3d470672abddad453dc06a0c07add9", null ],
    [ "curPrcdId", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_result_lot.html#a5b099b3893584a8cce7e8e23cab57c93", null ],
    [ "curprcdname", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_result_lot.html#af047b4ef2b9fa500449f6490e972e100", null ],
    [ "eqpId", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_result_lot.html#a2b930570bf25b53ee55fa7b74d1a2159", null ],
    [ "eqptype", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_result_lot.html#a8bc17589a7cd59a55bd4a8e6df3ac745", null ],
    [ "location", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_result_lot.html#ab93e70c2f7f3756b0415e1d25682625e", null ],
    [ "lotid", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_result_lot.html#ad318825d9d33765f705d4b5e58fd67bb", null ],
    [ "lottype", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_result_lot.html#ad7c028c00289a934572323eef8b716c9", null ],
    [ "mainmattype", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_result_lot.html#a250a4157b8acdf9f75acbddb55a625bb", null ],
    [ "partid", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_result_lot.html#adff3fc89a37b8ea33c67787afddf339e", null ],
    [ "planstarttime", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_result_lot.html#abc1fcafac6831d073588cbe2d4a3becd", null ],
    [ "priority", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_result_lot.html#a02d9cf51277fa5bb9ad314c5908456fc", null ],
    [ "procstate", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_result_lot.html#afe80cd9a3b2c86c0d4c01dc00b28aa92", null ],
    [ "prodarea", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_result_lot.html#a35e52010b1d52cc527a9113f7f41876a", null ],
    [ "recpid", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_result_lot.html#a16eaca8511d0025caa10cc211f3de761", null ],
    [ "stage", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_result_lot.html#a1520405d516319124b51522bfcc34dd5", null ],
    [ "state", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_result_lot.html#abe708284502724db6071a14fe918d73d", null ],
    [ "stateentrytime", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_result_lot.html#af94f56884e548bb0aa585270748603cf", null ],
    [ "testbatchid", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_result_lot.html#ad973cf64de78738a54f6da2963a7400c", null ]
];