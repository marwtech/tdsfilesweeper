var class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_actl_components_row =
[
    [ "chamberPPID", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_actl_components_row.html#a831585c03c193a88f6b1dd71b4c81082", null ],
    [ "compbatch", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_actl_components_row.html#ab2dfe36545527f702dc616ccc2dba354", null ],
    [ "compids", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_actl_components_row.html#acf98ac2d37c93a725a142ddea406cc49", null ],
    [ "complocation", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_actl_components_row.html#a8a026591922a937d9be44146ad59957d", null ],
    [ "complocationType", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_actl_components_row.html#a52f267e7d4cb2ee4c30078bfb99b8bdf", null ],
    [ "comprejcat", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_actl_components_row.html#ad462f45557b64704f4353cbc6dfe761b", null ],
    [ "comprejcat2", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_actl_components_row.html#aa7a55b5ac436c9d9aec863f22fbce17a", null ],
    [ "compreworkcodes", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_actl_components_row.html#a76ee74f2b9bb5f59f76ca29196b14362", null ],
    [ "compstate", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_actl_components_row.html#a17e894d63bb954e6f25651cec0510a45", null ],
    [ "compstateExtern", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_actl_components_row.html#a22c738f3965b4b2327765dd85ad55645", null ],
    [ "compsubqty", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_actl_components_row.html#a28e51f81a027d3712d912b307c2f49f4", null ],
    [ "compUsedLastTest", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_actl_components_row.html#aa232f0be02084e6bc948bb5f1265d7e9", null ],
    [ "controltype", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_actl_components_row.html#a0bc00952abdbbe905bb6e7ca88a67c53", null ],
    [ "curcompreworkcodes", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_actl_components_row.html#a5e955dd43340fe42e408db235adccb8d", null ]
];