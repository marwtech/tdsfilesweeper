var struct_oclaro_1_1_prom_service_1_1_lot_1_1_material =
[
    [ "ComponentParameters", "struct_oclaro_1_1_prom_service_1_1_lot_1_1_material.html#a8bdad233ae397355ce181df8a0a9e77e", null ],
    [ "Components", "struct_oclaro_1_1_prom_service_1_1_lot_1_1_material.html#a3e7ca6ff238d68adaa6c37050ffc9cb8", null ],
    [ "ControlComponents", "struct_oclaro_1_1_prom_service_1_1_lot_1_1_material.html#a05208e8f9a58524a065ab3eca8ab8d94", null ],
    [ "CurrentQuantity", "struct_oclaro_1_1_prom_service_1_1_lot_1_1_material.html#afc7629334952a578108ba328b880e96a", null ],
    [ "Mask", "struct_oclaro_1_1_prom_service_1_1_lot_1_1_material.html#ab786b4315dbc8be37b7ee43d695793f2", null ],
    [ "MaskShort", "struct_oclaro_1_1_prom_service_1_1_lot_1_1_material.html#a88c2b3be7f72bb9dfb9610c1c7c361f7", null ],
    [ "MaterialType", "struct_oclaro_1_1_prom_service_1_1_lot_1_1_material.html#abdd0fa501e7e8e3cc57044d95d95e198", null ],
    [ "Owner", "struct_oclaro_1_1_prom_service_1_1_lot_1_1_material.html#ace9c38ddacccc56a2efde896924314c8", null ],
    [ "Parameters", "struct_oclaro_1_1_prom_service_1_1_lot_1_1_material.html#a00c6efb65e60798197353d453660a46d", null ],
    [ "PartID", "struct_oclaro_1_1_prom_service_1_1_lot_1_1_material.html#a62fae3b7a69f2a782d6aa5e4d3e10f7a", null ]
];