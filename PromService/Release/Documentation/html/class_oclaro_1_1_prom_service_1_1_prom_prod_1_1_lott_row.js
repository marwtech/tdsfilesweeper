var class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_lott_row =
[
    [ "description", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_lott_row.html#a2b430418be5e6d201ebc2444b5fbbdf2", null ],
    [ "isControlledMaterial", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_lott_row.html#ace0cb21118e86cd4d4808a067a6717a8", null ],
    [ "isMaintLot", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_lott_row.html#a2b1bbd07cb603f9ac74e8956882044a1", null ],
    [ "isMergeAllowed", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_lott_row.html#a75548171c25a0dc53813f645e78f1e2b", null ],
    [ "isSplitAllowed", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_lott_row.html#af16bf6f5b26151fe789aa91f95afe848", null ],
    [ "lotTypeId", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_lott_row.html#a579296b95b1f6701924096345a605cda", null ]
];