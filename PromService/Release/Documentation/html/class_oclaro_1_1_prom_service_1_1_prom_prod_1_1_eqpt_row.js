var class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_eqpt_row =
[
    [ "accessMode", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_eqpt_row.html#ac4f5873c1e219fb73da173c57fed527d", null ],
    [ "bufferType", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_eqpt_row.html#a03031da65b4ce536815ff17f0caeb50c", null ],
    [ "carrierType", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_eqpt_row.html#a957073b7c1446460df79b5ae6c2f56f9", null ],
    [ "description", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_eqpt_row.html#ad67aa6de0ce401bffdd6f8405fd86620", null ],
    [ "eqpId", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_eqpt_row.html#a88a03401bd79ed7e8b19736d59229e3b", null ],
    [ "ioType", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_eqpt_row.html#a493ae08811ef5ecc3b04ed9c10e43475", null ],
    [ "materialType", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_eqpt_row.html#af6b018053a94e13a3eb53b22d73b580f", null ],
    [ "portCapability", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_eqpt_row.html#ae7939415c4d70bcc686b19e926b8082a", null ],
    [ "portId", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_eqpt_row.html#a182c0871b189e923709d966a2af91028", null ]
];