var class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_oper_set_row =
[
    [ "action", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_oper_set_row.html#ac35e95e77e049adccd25be5ac091e375", null ],
    [ "changeDate", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_oper_set_row.html#adabbb1487f140906a6a0403d4faa9f01", null ],
    [ "createDate", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_oper_set_row.html#a07a25dc9217c44936bb5dd55f6e567bf", null ],
    [ "description", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_oper_set_row.html#a663e1a218d8e2ef307428a92a4fd3f7a", null ],
    [ "docuGroup", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_oper_set_row.html#ac9610599404f8fac6bf830ca9c58695d", null ],
    [ "docuName", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_oper_set_row.html#a9664deb0a6605f3d7eaeb98ffbe6b191", null ],
    [ "docuVersion", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_oper_set_row.html#af2fb92fb4bc38c0559c77803feac48a0", null ],
    [ "engOwner", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_oper_set_row.html#a73cce7d3daa3dc715a09ca49e83ff84b", null ],
    [ "frozen", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_oper_set_row.html#ac2ff5e707573cbe6c6d6aed2beebd6a6", null ],
    [ "operId", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_oper_set_row.html#a90e9b8514ca7e3c182291d53fefd99e2", null ],
    [ "operName", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_oper_set_row.html#a088c087bc494f21244f800d659e012aa", null ],
    [ "operVersion", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_oper_set_row.html#a70238bfc85662604b11f74b7270399f8", null ],
    [ "prodStatus", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_oper_set_row.html#a529714fe1705d948893d2b7cbe2b74bf", null ],
    [ "type", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_oper_set_row.html#a58047c3da4d9f6a50a8e8a55c60405fe", null ]
];