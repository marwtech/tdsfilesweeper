var class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_ptst_set_row =
[
    [ "carrierId", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_ptst_set_row.html#a6c18ce6e3621dd6308bb16615e12c046", null ],
    [ "changeDt", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_ptst_set_row.html#a8f045eb2762589557707e7f448b43881", null ],
    [ "changeReason", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_ptst_set_row.html#a0f04273ab383aa4dc2cce94ed09e7735", null ],
    [ "commentCount", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_ptst_set_row.html#a570f5d4f62e88f73f987e6f8e3aef0c6", null ],
    [ "empId", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_ptst_set_row.html#ac4338d4b88cd7f9f4b0b9e975b02092b", null ],
    [ "eqpId", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_ptst_set_row.html#a6aa2efd020e6e54705191f73ab9afcfe", null ],
    [ "portAssociationState", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_ptst_set_row.html#a0984ed297c86a4ac06ac471860544efc", null ],
    [ "portId", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_ptst_set_row.html#abade9b0871a0bbdd7a86befa723b230b", null ],
    [ "portReservationState", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_ptst_set_row.html#a474b937efdbe21d62f3e6f9f5e49604e", null ],
    [ "portTransferState", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_ptst_set_row.html#a69cbe58389175a43deaa542e37152ecc", null ]
];