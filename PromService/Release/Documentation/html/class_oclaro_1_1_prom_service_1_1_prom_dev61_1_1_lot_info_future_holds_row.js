var class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_info_future_holds_row =
[
    [ "fileTime", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_info_future_holds_row.html#aaedcb7d753fdec810241cbe071929f41", null ],
    [ "holdCode", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_info_future_holds_row.html#a0aac136e11d472476f7759be7b562a03", null ],
    [ "holdNumber", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_info_future_holds_row.html#ac3497a7a0fd5aee36878e1bb92b2b35f", null ],
    [ "holdReas", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_info_future_holds_row.html#aa581eacbfd3ee9fa559b7902c85a740f", null ],
    [ "holdStage", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_info_future_holds_row.html#a84f0c64520d17432c482a9cd8240a57e", null ],
    [ "performanceCount", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_info_future_holds_row.html#aa1e6a2a5fa359bc69da9f683e7a7b858", null ],
    [ "stackInstNum", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_info_future_holds_row.html#a2b2f63684856addae1e007ebc4f307e5", null ],
    [ "stackPrcdName", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_info_future_holds_row.html#a1fd69f88eedbe048da26a8ce6d1fbd6e", null ],
    [ "stepPlacement", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_info_future_holds_row.html#ac06fd845b6284b500062a036bbcf80ca", null ],
    [ "userId", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_lot_info_future_holds_row.html#ae9c1e5cbd9591d499b301d8b08ac5c19", null ]
];