var class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_wait_lot =
[
    [ "comclass", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_wait_lot.html#ac025134314badfaf1c8db51983210226", null ],
    [ "curmainqty", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_wait_lot.html#a913a3ad5bbd40a5b932494eda3c52df8", null ],
    [ "curprcdcurinstnum", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_wait_lot.html#a1e246d8bb6de72b1445e77f0c4ec0b1a", null ],
    [ "curPrcdId", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_wait_lot.html#ad50c1a1a65037f7f83b9b28af25e1f3d", null ],
    [ "curprcdname", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_wait_lot.html#a513aac0707110ca2d4ec187c59155292", null ],
    [ "eqpId", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_wait_lot.html#abf39a1db5ed45fbce6faa6725a0a5928", null ],
    [ "eqptype", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_wait_lot.html#aebe1aa463c6063180ba385653e76b4d7", null ],
    [ "location", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_wait_lot.html#aefef52e3ea5e7228816bf5debd567bbc", null ],
    [ "lotid", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_wait_lot.html#aae906bc596c1a2610050f7da105749e5", null ],
    [ "lottype", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_wait_lot.html#a797933ab2013fef6caa213c30e358a83", null ],
    [ "mainmattype", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_wait_lot.html#ad24ba870354ab6a646e147456140039d", null ],
    [ "partid", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_wait_lot.html#a1c51bfcbe4f53a32fbadc3014e6316d7", null ],
    [ "planstarttime", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_wait_lot.html#aa5ff6a6680e88fa99d080674c91e4d06", null ],
    [ "priority", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_wait_lot.html#a81f103d9e8df3076acaf76a4c573a0bf", null ],
    [ "procstate", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_wait_lot.html#ad4b20862a55e7095f4e0f86a6c1df49c", null ],
    [ "prodarea", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_wait_lot.html#a6a6ab52f9bd2b9c712a1bc1e080df61a", null ],
    [ "recpid", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_wait_lot.html#ad0f306dd9a6ef63841c29892948dee64", null ],
    [ "stage", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_wait_lot.html#ad43e36c77aadfdcf65ad30e893be7ffc", null ],
    [ "state", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_wait_lot.html#a2094ffb229dbba5054dac464d4fba5c5", null ],
    [ "stateentrytime", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_wait_lot.html#ad28e53926d57be810cd5e2378af92e74", null ],
    [ "testbatchid", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_wait_lot.html#afdada21fee491bf0b99e0b727aa5ba40", null ]
];