var class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_constrans_table_row =
[
    [ "addSeqNum", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_constrans_table_row.html#a7463e4d6ba5b98bc31554e5322855da0", null ],
    [ "evTime", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_constrans_table_row.html#a41ba8b1e57368b24913db9899e6dbe0d", null ],
    [ "evType", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_constrans_table_row.html#a320e5ec87aac85b01ad1cf85f4ae761a", null ],
    [ "evUser", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_constrans_table_row.html#ac8a558eb03e759d37d6fa9cbd1a9cbc4", null ],
    [ "internalQty", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_constrans_table_row.html#af6e83f147798968dc14fd6a582189223", null ],
    [ "itemNum", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_constrans_table_row.html#ac59417e398b344da397e3fb8dae5a1c1", null ],
    [ "netRemainingQty", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_constrans_table_row.html#a98e62c0d6a285144e7a728f4e8f28258", null ],
    [ "partId", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_constrans_table_row.html#a24279391c472095c3f1fa5123d20435a", null ],
    [ "sourceLotId", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_constrans_table_row.html#aa5d0051813f2ec759e37dcf94a32fae0", null ],
    [ "transSeqNum", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_constrans_table_row.html#ad74a586188dbdd2e8425b0b87b490722", null ],
    [ "userQty", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_constrans_table_row.html#a41358016ddaf84198fbf35873d810c06", null ]
];