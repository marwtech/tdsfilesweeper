var class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slph_set_row =
[
    [ "approvalTime", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slph_set_row.html#a6bdb859b66e881c961ddeb72dde23396", null ],
    [ "approver", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slph_set_row.html#ae2ca0f4dff453d42c0126b6da87497a5", null ],
    [ "eqpId", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slph_set_row.html#ab9ea9180f1c7e286a0a6d92dd314e2ec", null ],
    [ "lotId", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slph_set_row.html#a381fbf1f78f3f01c7fc5f10cc3249c5a", null ],
    [ "lotSkipState", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slph_set_row.html#a95283340f5ccf69e82c0263289186af8", null ],
    [ "metrologyDisposition", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slph_set_row.html#a4beb97594c750cc14b1df8331a10d98d", null ],
    [ "prcdStackCount", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slph_set_row.html#a955a86d6c37fa698e9ab456fec42f793", null ],
    [ "processTime", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slph_set_row.html#aaa9b281280744f3e58a8fe6a84acdf0a", null ],
    [ "recpId", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slph_set_row.html#a32397382fe8c1bf381c61742522452c9", null ],
    [ "slspId", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slph_set_row.html#a37febe1b220d446a19978305da30b1e0", null ],
    [ "testCount", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slph_set_row.html#a82f39be9903faa6bee63b9d535710169", null ],
    [ "wasAtRisk", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slph_set_row.html#a06623e6fdfb061a6d2eb875c5e6e2d84", null ]
];