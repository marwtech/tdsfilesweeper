var class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_eqp_list_row =
[
    [ "activityId", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_eqp_list_row.html#a3ab79b1ac4c31ffbd6aaf412f88e608e", null ],
    [ "capabilityCount", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_eqp_list_row.html#a997bd0a25ae6649b9ed4494390724045", null ],
    [ "capabilityVal", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_eqp_list_row.html#ac0279b3d1dd1c0effdfdabfb60beb9ee", null ],
    [ "description", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_eqp_list_row.html#a2894b1fed5f761360860da739bcd3588", null ],
    [ "eqpId", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_eqp_list_row.html#a7e11d8fff08483d8315494cfe4df5e65", null ],
    [ "eqpType", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_eqp_list_row.html#afcf6fc7df4ef5790896c247116207df0", null ],
    [ "lastChangeDt", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_eqp_list_row.html#a2a0f975f42ca99529fadaaf88db6624e", null ],
    [ "locationId", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_eqp_list_row.html#acaaee90da255930e4ce4083a82c5352c", null ],
    [ "runcount", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_eqp_list_row.html#ad9d29e6b5c6d671dc7e66fde26678075", null ],
    [ "status", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_eqp_list_row.html#a8500d38962d3c940b5a2d781b66bf450", null ]
];