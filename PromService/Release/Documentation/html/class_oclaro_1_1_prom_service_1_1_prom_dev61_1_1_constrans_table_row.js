var class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_constrans_table_row =
[
    [ "addSeqNum", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_constrans_table_row.html#ad2aac1fd6739768e77feade84fb2185d", null ],
    [ "evTime", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_constrans_table_row.html#a0d9f087a9fc448ed14365cc606252c64", null ],
    [ "evType", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_constrans_table_row.html#ab39777c99b8cc6b464880388b0a9f8a9", null ],
    [ "evUser", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_constrans_table_row.html#a661e98422629a8f6867e392c24cedbd4", null ],
    [ "internalQty", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_constrans_table_row.html#a8511ec28fe9a81dca347a2f91ce51727", null ],
    [ "itemNum", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_constrans_table_row.html#a5a1a7225d103f1b0ab8759fd600d6888", null ],
    [ "netRemainingQty", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_constrans_table_row.html#af934fbd896d743b13206c81438c0b817", null ],
    [ "partId", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_constrans_table_row.html#a13c7e930798515211731c36373b11097", null ],
    [ "sourceLotId", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_constrans_table_row.html#ac096871b38419bbee0d1d1030102f11c", null ],
    [ "transSeqNum", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_constrans_table_row.html#a97b59501ada89b6769a3a95c8151bed6", null ],
    [ "userQty", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_constrans_table_row.html#a01142a1f6f53550c4f47b8aec36d1b70", null ]
];