var class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_sord =
[
    [ "curDateReqd", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_sord.html#a1ba90ddb0a79564b24b1de616917595f", null ],
    [ "curQtyReqd", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_sord.html#a565255cc47f16e6150ac23a9da36ed06", null ],
    [ "customerDateReqd", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_sord.html#ad52129487af3912bc4b2175508865e41", null ],
    [ "customerName", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_sord.html#aa5b65fc6fb8d06519e38de2e3150caca", null ],
    [ "customerQtyReqd", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_sord.html#a44953c17850ac695f039b1d054365eb3", null ],
    [ "custOrderNumber", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_sord.html#a764bfeeca74e56fab24757aa67c49ad9", null ],
    [ "dateChanged", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_sord.html#a7c0b335b9876123b4ba5f406bfa265ce", null ],
    [ "dateCreated", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_sord.html#a8d31a4d738f70c4ccb7f8c12013a114b", null ],
    [ "lineItem", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_sord.html#a4ce140c250b339b45c907629a8c2b2ba", null ],
    [ "orderState", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_sord.html#a402808f709f6d9781729b1907f745f6c", null ],
    [ "origDateReqd", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_sord.html#a078afaa35fa178782b781a44b06f232d", null ],
    [ "origQtyReqd", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_sord.html#aaeab5266d24db8da6363b1515a5a5b67", null ],
    [ "priceperPart", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_sord.html#aa310b0ea9f646b6f5fa8c5c042b31362", null ],
    [ "qtyShipped", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_sord.html#a760efee72964b30c205edea05ace3182", null ],
    [ "salesOrderNumber", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_sord.html#ab227c12667353c6171206314e0065412", null ],
    [ "seqNo", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_sord.html#a413c4d0739d5a2c161370c8c0f8f0307", null ],
    [ "sordPartId", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_sord.html#add30c525a69e7f4b8e33aa1a499e2524", null ]
];