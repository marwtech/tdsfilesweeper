var class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_req_items_table_row =
[
    [ "absLowerLimit", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_req_items_table_row.html#a221a490ad4ff31a1a3f33bdebf4983f6", null ],
    [ "absTargetQty", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_req_items_table_row.html#a9e59a1529d78d731d83e611595766d5a", null ],
    [ "absUpperLimit", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_req_items_table_row.html#ae9550bc8349126a8c3450d02e8d37adb", null ],
    [ "balanceCode", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_req_items_table_row.html#aafa1352c589e50d7287514947a7298c2", null ],
    [ "balanceToConsume", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_req_items_table_row.html#abf03c51de7ad976751e069b5cb1227c8", null ],
    [ "docuGroup", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_req_items_table_row.html#a783ea1e1b5a60d0743646364349fdc27", null ],
    [ "docuId", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_req_items_table_row.html#a519264e174ab76d3d7088d262352feae", null ],
    [ "holdIfOOS", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_req_items_table_row.html#ae8c17bbe75db39d4833b43c1dfa4729b", null ],
    [ "itemAction", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_req_items_table_row.html#a1271d842368e1c780b92fbc6f56d2722", null ],
    [ "itemDescription", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_req_items_table_row.html#a3ab4ed323bd12df9ad47037cc95376ec", null ],
    [ "itemDone", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_req_items_table_row.html#a6513415b3e53a89a4a4d6be8bcd922d0", null ],
    [ "itemDoneUserId", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_req_items_table_row.html#aec959d44061fe6e8ef9263a6bee5afd2", null ],
    [ "itemNum", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_req_items_table_row.html#a16c121262f8a99b0e024a687d1b5811f", null ],
    [ "matType", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_req_items_table_row.html#a1905ef6bd8e76c9ca1e880aeda4fc70d", null ],
    [ "mistCount", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_req_items_table_row.html#afaf3929a56e3448e24855334e0e9b407", null ],
    [ "totalQtyConsumed", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_req_items_table_row.html#a8c1e6789077ae0361b6b68ccffb2fcd8", null ],
    [ "units", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_req_items_table_row.html#ab13345db00e1b423738bff0c025a5979", null ],
    [ "unsourcedMat", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_req_items_table_row.html#a2c1c9d628f55e8a0fb517ac1e6b33c20", null ]
];