var class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1mat_tp___mat_req_qry_completed_event_args =
[
    [ "actl", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1mat_tp___mat_req_qry_completed_event_args.html#a36e81d71200ace310eab85f86e93cc96", null ],
    [ "itemDoneCount", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1mat_tp___mat_req_qry_completed_event_args.html#a4e5e552be2a23d4e249c856369129a69", null ],
    [ "remReqDoneCount", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1mat_tp___mat_req_qry_completed_event_args.html#aebe5ce1e77b9866f63eb3a74f1e20286", null ],
    [ "reqItemsTable", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1mat_tp___mat_req_qry_completed_event_args.html#a428ca29f63a0c0a406b8bc0795fbd565", null ],
    [ "reqPartsTable", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1mat_tp___mat_req_qry_completed_event_args.html#af251f9374101e502dac7aa4880bf356c", null ],
    [ "Result", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1mat_tp___mat_req_qry_completed_event_args.html#a4b3d0597a5da4626c12fc890eee5ba86", null ],
    [ "tps_infoText", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1mat_tp___mat_req_qry_completed_event_args.html#aff21611ed084c9fdfad30675a3f373e4", null ]
];