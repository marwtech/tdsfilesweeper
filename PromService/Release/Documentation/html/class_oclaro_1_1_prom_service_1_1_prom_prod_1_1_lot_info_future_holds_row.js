var class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_lot_info_future_holds_row =
[
    [ "fileTime", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_lot_info_future_holds_row.html#a7ae959c9fa596afb449109666302eb62", null ],
    [ "holdCode", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_lot_info_future_holds_row.html#a5e81ff81ea9de04140d6d4207d4cc604", null ],
    [ "holdNumber", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_lot_info_future_holds_row.html#aca395ad94e5db07cc25278528c98130a", null ],
    [ "holdReas", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_lot_info_future_holds_row.html#ae44e1264e51932e69c3e60228c3209fb", null ],
    [ "holdStage", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_lot_info_future_holds_row.html#a55bbc10d9819f16890956290b12146ba", null ],
    [ "performanceCount", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_lot_info_future_holds_row.html#a836a75aea705482693ed67ed764c0882", null ],
    [ "stackInstNum", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_lot_info_future_holds_row.html#a403937ad2b2921e2f6d4fba601b77f68", null ],
    [ "stackPrcdName", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_lot_info_future_holds_row.html#a9ca4e21bd4d633225caf45fe16d025e3", null ],
    [ "stepPlacement", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_lot_info_future_holds_row.html#a59b7288b58c9742fa843e04c7c1d7259", null ],
    [ "userId", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_lot_info_future_holds_row.html#a305b98b8c47a956a0df16f0fbc80902d", null ]
];