var class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slsp_set_row =
[
    [ "actionTriggerLevel", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slsp_set_row.html#a9b46a7878975d2ce70b30f13c39ab37e", null ],
    [ "activeDate", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slsp_set_row.html#a6b5acaa482447e84a23fd5170caaca9c", null ],
    [ "activeFlag", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slsp_set_row.html#a64b6c9fa3eb426fb94af1e786f53aaa8", null ],
    [ "changeDate", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slsp_set_row.html#add48c6b190c7b24fdc4a3e10eec0de3c", null ],
    [ "constraintExpression", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slsp_set_row.html#abdb33bee5ae58c2e138a34b423abaee5", null ],
    [ "createDate", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slsp_set_row.html#a27ca61f457c42ecde90363703220ceab", null ],
    [ "description", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slsp_set_row.html#aefe2003c12836b5e7fcc50a5eedc7f7b", null ],
    [ "frozen", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slsp_set_row.html#a39f73221f5af69c941b393c08e138b04", null ],
    [ "grpDocuId", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slsp_set_row.html#a3fc5789c16b399111e791a5912fd160c", null ],
    [ "NumQualificationTests", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slsp_set_row.html#a02e9d6b977d6e6f71945dd64c0122f4e", null ],
    [ "NumReQualificationTests", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slsp_set_row.html#a56a0c495dea3217499e079de8833f629", null ],
    [ "owner", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slsp_set_row.html#a84fbedbb4cf6b1fe87b8e046e656b9d1", null ],
    [ "prodStatus", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slsp_set_row.html#a33b14aae35ff5279cdd231a8d02d15a4", null ],
    [ "randomSampling", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slsp_set_row.html#a7a3168fa18de89671009a047ef603fd4", null ],
    [ "sampleSize", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slsp_set_row.html#a1df44b62637a6960944b01b0f6b85577", null ],
    [ "skipPriorityThreshold", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slsp_set_row.html#ac3c43723730feb976062c192d6bdc4b5", null ],
    [ "slspActions", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slsp_set_row.html#a66eb7b0a315364ac33653cefb3f91db6", null ],
    [ "slspActionsExtern", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slsp_set_row.html#a50c0ce839983e5e8952f76f366c216bb", null ],
    [ "slspId", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slsp_set_row.html#a79c4a61a6b15f68e00fd85027961d3d0", null ],
    [ "versionedRecpBased", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_slsp_set_row.html#af52359c6716a659d71a4f733f639d569", null ]
];