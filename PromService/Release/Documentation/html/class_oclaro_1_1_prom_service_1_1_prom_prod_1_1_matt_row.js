var class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_matt_row =
[
    [ "carrierName", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_matt_row.html#ac27a5d8c829c96e7be0a6d1be96ec7fb", null ],
    [ "carrierNamePlural", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_matt_row.html#a7f6c6b97bf0b5526a56a5935d4ba2331", null ],
    [ "isDiscrete", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_matt_row.html#a3b42e4002c824a9e4a0222eb832170a9", null ],
    [ "matItemName", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_matt_row.html#a309164c302ab40dc89d3e572a4290880", null ],
    [ "matItemNamePlural", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_matt_row.html#ac4f8a7b56fbfbc704a43bf7906b925d1", null ],
    [ "matTypeCode", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_matt_row.html#ac82119d59d8cc77cdd3003158a108b4c", null ],
    [ "matTypeId", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_matt_row.html#a802f12c8be84272a747a8d12afe436b6", null ],
    [ "unitID", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_matt_row.html#af1c9dc4367385dab8de4b76665bf6f88", null ]
];