var class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_src_lot_table_row =
[
    [ "curMainQty", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_src_lot_table_row.html#a97cb4c7e4cf62ee4d60f1b510a5e020b", null ],
    [ "endTime", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_src_lot_table_row.html#afefa5669230e70ca753ebadb6c7f1cd0", null ],
    [ "LotId", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_src_lot_table_row.html#a07a93e6f3065a781629a3949d149a285", null ],
    [ "mainMatIdent", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_src_lot_table_row.html#a88d63aead6f19f085301b015a05e8b03", null ],
    [ "mainMatType", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_src_lot_table_row.html#a03e8efce0409609ac39521bb172164d7", null ],
    [ "partId", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_src_lot_table_row.html#a31e67d5761d17f1604a1471c8f12ae8b", null ],
    [ "procState", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_src_lot_table_row.html#a61e6c5cf08cfb9db8d85238fc3203058", null ],
    [ "queueTime", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_src_lot_table_row.html#a5d8534e6544801d8f23f85e4db44b045", null ],
    [ "subMatType", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_src_lot_table_row.html#acddccf7a0da5b8a4d17e40a01fbdcd00", null ],
    [ "units", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_src_lot_table_row.html#a09666873b7400cd0270ef3797a585801", null ]
];