var class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slsp =
[
    [ "actionTriggerLevel", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slsp.html#acb48649b1d4eb57055f887718d8819be", null ],
    [ "activeDate", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slsp.html#addd454cf72686b2b1baed66bce05c183", null ],
    [ "activeFlag", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slsp.html#ae0685b6130cca69fd8b9862ccdc9aca3", null ],
    [ "changeDate", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slsp.html#a1e181a0953afde06e0ddf750aa9a44e3", null ],
    [ "constraintExpression", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slsp.html#abc150ef49371a5ac6a518b5d4e2642e7", null ],
    [ "createDate", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slsp.html#a60270d013ae09de50a7c742da658694f", null ],
    [ "description", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slsp.html#a435e23554bcf9e3eb57b526ac2b22939", null ],
    [ "frozen", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slsp.html#a340ec5a02326b2c210b2912c52d989fb", null ],
    [ "grpDocuId", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slsp.html#a5fa560599a41f2b19f312fbfee94fa8b", null ],
    [ "NumQualificationTests", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slsp.html#a3d715c233da23f073d709629eb82c2fb", null ],
    [ "NumReQualificationTests", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slsp.html#a03644384be88ed544dad66f45097a9ae", null ],
    [ "owner", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slsp.html#ac4a1aaeed27a772f358978427930db13", null ],
    [ "prodStatus", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slsp.html#a8cb866ac12db33a0abe87149d8d67b34", null ],
    [ "randomSampling", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slsp.html#a892890e0352552951254ecc288be0949", null ],
    [ "sampleSize", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slsp.html#a3b346518104d7d3f6b9d9009b6b63fe8", null ],
    [ "skipPriorityThreshold", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slsp.html#a04ad4c1fd6ef43f6bdafdf0bc9daa7df", null ],
    [ "slspActions", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slsp.html#a28ae8831d6e56bbba2bfc9fa20afb66b", null ],
    [ "slspActionsExtern", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slsp.html#aa02642843e64efa93c66854dddb969be", null ],
    [ "slspId", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slsp.html#a86f93d3ebc05e45385488b92c7733da5", null ],
    [ "versionedRecpBased", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_slsp.html#aeb53d3f0711df367e88b3dfebd56dd21", null ]
];