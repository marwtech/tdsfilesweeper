var class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_src_lot_table_row =
[
    [ "curMainQty", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_src_lot_table_row.html#a93ab6a0b3375e80900a534ab7fbaa785", null ],
    [ "endTime", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_src_lot_table_row.html#acb98aeeafebd1cd4cf1065259d076592", null ],
    [ "LotId", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_src_lot_table_row.html#a604338a8fdad8c2df6f44db6f5bf8b4f", null ],
    [ "mainMatIdent", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_src_lot_table_row.html#a6ec182e8a89e469ec60ec6de34692997", null ],
    [ "mainMatType", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_src_lot_table_row.html#a8c1743cee1f98e179aa9184527e24f93", null ],
    [ "partId", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_src_lot_table_row.html#a37ad1ec9c297e68e2ca64faeabedfc22", null ],
    [ "procState", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_src_lot_table_row.html#a619e11d59350b69a631d5fdb3802cc82", null ],
    [ "queueTime", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_src_lot_table_row.html#a76a7f47441e1cfe536777e1f7a6c6aaa", null ],
    [ "subMatType", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_src_lot_table_row.html#aa04e28dd3793eb3684e5e04c622bdb04", null ],
    [ "units", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_src_lot_table_row.html#a165e7628e46caeb9a07c7f41a993be3c", null ]
];