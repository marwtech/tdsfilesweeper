var class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1lot_inv___ship_ord_completed_event_args =
[
    [ "actlSet", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1lot_inv___ship_ord_completed_event_args.html#aa036fd51f3458a48c12cb27ac8704af5", null ],
    [ "lotInv_shipLotCount", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1lot_inv___ship_ord_completed_event_args.html#a38c09c4072e658441a696a9ee0ce5691", null ],
    [ "lotInv_shippedSet", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1lot_inv___ship_ord_completed_event_args.html#aca8bcfec69da9753a02d9c0b40b8f965", null ],
    [ "lotInv_shipTolMainQty", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1lot_inv___ship_ord_completed_event_args.html#a6e777dc96479bef55fc7c3201024f1d4", null ],
    [ "lotInv_shipTolSubQty", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1lot_inv___ship_ord_completed_event_args.html#a2c3e353342aeae0b325411286c0aff69", null ],
    [ "Result", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1lot_inv___ship_ord_completed_event_args.html#abe6c0cd2f2c8d944515015a63136bb02", null ],
    [ "tps_infoText", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1lot_inv___ship_ord_completed_event_args.html#a68cfaa1167eb8be4e649b69c28acbc76", null ]
];