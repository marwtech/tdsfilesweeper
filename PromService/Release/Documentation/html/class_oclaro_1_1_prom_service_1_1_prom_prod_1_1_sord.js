var class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_sord =
[
    [ "curDateReqd", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_sord.html#a43e0690cb69a1f072fe863c3a9b0324b", null ],
    [ "curQtyReqd", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_sord.html#a6a824c6c4dc36d28f9d9ce1340230ae8", null ],
    [ "customerDateReqd", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_sord.html#a582f63338c0b2cc383f313a980dddec7", null ],
    [ "customerName", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_sord.html#ad118605af54978979da3063c5d668168", null ],
    [ "customerQtyReqd", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_sord.html#ab15d38c026ac43a1c82feb26d860ee08", null ],
    [ "custOrderNumber", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_sord.html#a30276b0e78d7f61d12892cb3772b85d5", null ],
    [ "dateChanged", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_sord.html#ac0b04d77762e525c782fac9cbd89ad6d", null ],
    [ "dateCreated", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_sord.html#aba87b34b1c433d4f5b4e50615b5fc201", null ],
    [ "lineItem", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_sord.html#a8a119b16ac3cce3b6d1bda76ff33d120", null ],
    [ "orderState", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_sord.html#a80825c6c4b9707ea99f67248832a994b", null ],
    [ "origDateReqd", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_sord.html#a53ccc9aaa14cd8341ba2c967f4bb25f3", null ],
    [ "origQtyReqd", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_sord.html#a1c8bef77f0bf6fe1372212bc73771610", null ],
    [ "priceperPart", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_sord.html#a6be4168177156bab5a3446a8f67b8f31", null ],
    [ "qtyShipped", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_sord.html#a9d68dcd803d4c8254a111accd2eb29a4", null ],
    [ "salesOrderNumber", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_sord.html#ae6825cf868f3bfa5358c0c27f9b6ef0f", null ],
    [ "seqNo", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_sord.html#a821510c62f78113f45873761fe0c3ac9", null ],
    [ "sordPartId", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_sord.html#a81467dcb7e2f2924ff8410be304eaefc", null ]
];