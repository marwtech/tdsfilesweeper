var class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_actl_components_row =
[
    [ "chamberPPID", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_actl_components_row.html#a080f0b77a6e91eb69fcc3016c8ed578b", null ],
    [ "compbatch", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_actl_components_row.html#a5c4fbedcebf0fd0a240e0e8b2f0a87a3", null ],
    [ "compids", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_actl_components_row.html#afafde9eccab65ab9cc2b829ba035d5cd", null ],
    [ "complocation", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_actl_components_row.html#afe507aa64896e6caa5cc75158baefeea", null ],
    [ "complocationType", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_actl_components_row.html#a435749ee70c180933d23cca016c8312b", null ],
    [ "comprejcat", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_actl_components_row.html#ae5574e33c104126128ec972fbbf8fc92", null ],
    [ "comprejcat2", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_actl_components_row.html#a715ba43c001c035de89e725c89838aa0", null ],
    [ "compreworkcodes", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_actl_components_row.html#aa91bfeda5c839bb2de03c4d3ffc23d62", null ],
    [ "compstate", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_actl_components_row.html#ad042822e809ff0efbacdeeec7b44ef36", null ],
    [ "compstateExtern", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_actl_components_row.html#a934aab572b765f03a706ee157635854a", null ],
    [ "compsubqty", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_actl_components_row.html#a14c1b41f6a063fff17acb2ce1052ef32", null ],
    [ "compUsedLastTest", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_actl_components_row.html#a2627adb901a7f9c2d6378698d7bcc2a8", null ],
    [ "controltype", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_actl_components_row.html#a2e6791d9ed4c39afe4f0b9b9e89f1fb3", null ],
    [ "curcompreworkcodes", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_actl_components_row.html#a1e514afd674ce2b0244ffcb275674e3c", null ]
];