var class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_ptst =
[
    [ "carrierId", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_ptst.html#a4208c719ac7c6c65585492f437e6385f", null ],
    [ "changeDt", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_ptst.html#a5c742888c7ffc6e652d4b6ca15effb7f", null ],
    [ "changeReason", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_ptst.html#a3250a12d9a49dfe2d7e85cbfa08645d7", null ],
    [ "commentCount", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_ptst.html#ad4aa9440bf19b3fa5c5623f5f58a9b28", null ],
    [ "empId", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_ptst.html#a3953e2d53cfd7dfa1303cb8ddabadd00", null ],
    [ "eqpId", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_ptst.html#a12575165e15f159b231239cd7930cac2", null ],
    [ "portAssociationState", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_ptst.html#aa3f960cbd84ce67e2e8577ea6faff884", null ],
    [ "portId", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_ptst.html#afc66a7d2e355e25307d2ee7d7f332a07", null ],
    [ "portReservationState", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_ptst.html#a107bc94fe2046c2f7d9d86fb04e9191c", null ],
    [ "portTransferState", "class_oclaro_1_1_prom_service_1_1_prom_dev61_1_1_ptst.html#abb10e7f6b332bb5b11fb2607a4956dd1", null ]
];