var class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_result_lot =
[
    [ "comclass", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_result_lot.html#a4caa78f9144683f6cfc0fbee2357316e", null ],
    [ "curmainqty", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_result_lot.html#aa3d7133e7301edc0dcc5db8057493673", null ],
    [ "curprcdcurinstnum", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_result_lot.html#a114c78a9c5258b83ead54740cdb4464f", null ],
    [ "curPrcdId", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_result_lot.html#af213164901ed2c7e53b26b13e7691435", null ],
    [ "curprcdname", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_result_lot.html#a1ba2e3b3676c8deb8960cfda65019a31", null ],
    [ "eqpId", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_result_lot.html#af02105bfb4d93edd81cb1518d075b293", null ],
    [ "eqptype", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_result_lot.html#a321c51d335ede988429f5e78b39df5fd", null ],
    [ "location", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_result_lot.html#a72bd5521f952615ff6e6d3499030aff3", null ],
    [ "lotid", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_result_lot.html#a50ce4c9d80fcabc8fbbbfbaff2b03ac1", null ],
    [ "lottype", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_result_lot.html#a756dca1e737b0afe01670d5d4b54f498", null ],
    [ "mainmattype", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_result_lot.html#a0a05cede6585d060116ca31a6bae1760", null ],
    [ "partid", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_result_lot.html#ad316fb6d3ce0bea749351df2be63bff7", null ],
    [ "planstarttime", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_result_lot.html#a8c5b994484a05b5a71e2682e7c2b3df2", null ],
    [ "priority", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_result_lot.html#a7556b3ce3f98a144828a1bc21d5e2590", null ],
    [ "procstate", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_result_lot.html#a2d45a67165adc86ae706d49d53510b6d", null ],
    [ "prodarea", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_result_lot.html#aea187fc8a65848ac5187de27e5efaadb", null ],
    [ "recpid", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_result_lot.html#a118d3ffc5f0d3bef3d15236508f5a8ad", null ],
    [ "stage", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_result_lot.html#af65b13b1564fa335a2be452794c314e3", null ],
    [ "state", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_result_lot.html#ac1a29c2fbf8601f4388a1bd63461156f", null ],
    [ "stateentrytime", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_result_lot.html#a041fe1e245241079f6cfa03f8a10f520", null ],
    [ "testbatchid", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_result_lot.html#a7ca96d9f58c538d67f347ddfc3a78e71", null ]
];