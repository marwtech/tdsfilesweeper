var class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_eqac_row =
[
    [ "activityID", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_eqac_row.html#a6843d5d47c67a8efa439587b31dd1330", null ],
    [ "activitySubType", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_eqac_row.html#a6c15bc63abcb4bd27af9b5b5c7b41506", null ],
    [ "activityType", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_eqac_row.html#a455102633caf9738eb3c0d817d684a44", null ],
    [ "description", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_eqac_row.html#af1f76e7f7b04a66ab8ae613a582d8fbe", null ],
    [ "isQual", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_eqac_row.html#a7489f6793baf54ff4c405a7b8948e444", null ],
    [ "isSupplierDelay", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_eqac_row.html#a91b59238671ed5626adfa4dc0a0a54ea", null ],
    [ "isWaitingDownTime", "class_oclaro_1_1_prom_service_1_1_prom_prod_1_1_eqac_row.html#ade69609f10cbead3646e428cea06f16d", null ]
];