﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Oclaro.PromService;
using Oclaro.Security;
using Oclaro.PromisForms;

namespace Testing
{
    /// <summary>
    /// Form for reasing TMS reports
    /// </summary>
    public partial class TMSForm : Form
    {

        Promis mPromisManager;
        bool dev = true;
        UserInfo mUserInfo;
        Timer check = new Timer();

       /// <summary>
       /// Tms Form Constructor
       /// </summary>
        public TMSForm()
        {
            InitializeComponent();
            
            check.Interval = 45000;
            check.Tick += new EventHandler(check_Tick);
        }

        void check_Tick(object sender, EventArgs e)
        {
            string er, nextShed, early, late, isLate;
            List<Lot.Operations> TmsOps;

            if (mPromisManager.QueryTMSTask(null, textBox1.Text, textBox2.Text, "0", "0", "00",
                out TmsOps, out nextShed, out early, out late, out isLate, out er))
            {
                DateTime sched = Convert.ToDateTime(nextShed);
                double time = (sched - DateTime.Now).TotalSeconds;

                if (time > Convert.ToDouble(early))
                {
                    //Done;
                    label3.ForeColor = Color.Gray;
                }
                else if(time>0 && time < Convert.ToDouble(early))
                {
                    //Early Range
                    label3.ForeColor = Color.Green;
                }
                else if (time <= 0 && time > -Convert.ToDouble(late))
                {
                    //due
                    label3.ForeColor = Color.YellowGreen;
                }
                else
                {
                    //late
                    label3.ForeColor = Color.Red;
                }

                label3.Text = sched.ToString() + " " + ((int)((sched - DateTime.Now).TotalMinutes)).ToString() +" minutes";
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            DoLogIn();
            if (mUserInfo != null)
            {
                string er, nextShed, early, late, isLate;
                List<Lot.Operations> TmsOps;
                if (mPromisManager.QueryTMSTask(null, textBox1.Text, textBox2.Text, "0", "0", "00",
                   out TmsOps, out nextShed, out early, out late, out isLate, out er))
                {
                    foreach (Lot.Operations op in TmsOps)
                    {
                        listBox1.Items.Add(op.Desc);
                    }
                }
            }

        }

        void DoLogIn()
        {
        Start:
            LogInForm logIn = new LogInForm(mUserInfo, null, null, "C40GRX1", false);
            logIn.ShowDialog();
            mUserInfo = logIn.GetCurrentUser();
            if (mUserInfo != null)
            {
                InitPromManager();
                check.Start();
                check_Tick(null, null);
                LogFile.Init("Log.txt");
                LogFile.Write("");
            }
            else
            {
                var message = MessageBox.Show("No user logged in!\nWould you like to close the program?", "No user", MessageBoxButtons.YesNo, MessageBoxIcon.Stop);

                switch (message)
                {
                    case System.Windows.Forms.DialogResult.No:
                        goto Start;
                    default:
                        Application.Exit();
                        break;
                }
            }
           
        }

        private void InitPromManager()
        {
            if (dev == true)
            {
                mPromisManager = new PromisDev();
                mPromisManager.Init(mUserInfo.mPromisUsername, mUserInfo.mPromisPassword, "Log.txt");
            }
            else
            {
                mPromisManager = new PromisProd();
                mPromisManager.Init(mUserInfo.mPromisUsername, mUserInfo.mPromisPassword, "Log.txt");

                Lot thisLot;
                string newId, err;
                mPromisManager.GetLotAlias("cqrt0198E", out newId, out err);

                mPromisManager.GetLotDetails("cqrt0181A.1", out thisLot);

                //mPromisManager.ChangeEquipmentStatus(

                string test = thisLot.ID;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string er, task, equip, nextShed, early, late, isLate;
            List<Lot.Operations> TmsOps;
            List<Lot.DataCollectionOperations> Dcops = new List<Lot.DataCollectionOperations>(); ;
            bool finished = false;

            task = textBox1.Text;
            equip = textBox2.Text;

            if (mPromisManager.QueryTMSTask(null, textBox1.Text, textBox2.Text, "0", "0", "00",
                out TmsOps, out nextShed, out early, out late, out isLate, out er))
            {
                if (!mPromisManager.StartTMSTask(task, equip, null, null, "StartTask", out er))
                {
                    MessageBox.Show(er);
                }

                int opcount = 1;
                foreach (Lot.Operations op in TmsOps)
                {
                    if (op.Type == "T")
                    {
                        List<Lot.DataCollectionOperations> tst;
                        string err;
                        mPromisManager.GetDCOpInfo(op.ID, out tst,out err);
                        DataEntry dtaent = new DataEntry(tst, mPromisManager, task, equip, opcount.ToString());
                        var test = dtaent.ShowDialog();

                        switch (test)
                        {
                            case System.Windows.Forms.DialogResult.OK:
                                finished = true;
                                break;
                            default:
                                finished = false;
                                break;
                        }
                    }
                    else
                    {
                        if (!mPromisManager.PerformTMSTask(task, equip, opcount.ToString(), null, null, out er))
                        {
                            //MessageBox.Show(er);
                            finished = false;
                        }
                    }
                    opcount++;
                }
                //finished = true;

                if (finished == true)
                {
                    if (mPromisManager.FinishTMSTask(task, equip, null, "N", "FinishTask", out er))
                    {
                        MessageBox.Show(er);
                    }
                    else { MessageBox.Show(er); }
                }
            }
        }
    }
}
