﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oclaro.PromService
{
    public class SelectedConfigData
    {
        public string SelectedPromisInstance = null;
        public string SelectedEpiDatabase = null;
        public bool UpdateEpiDatabase = true;
        public bool UseDevelopmentSettings = false;
        public bool UseMRPDevelopmentInstance = false;
    }
}
