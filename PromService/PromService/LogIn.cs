﻿using System;
using System.Runtime.InteropServices;
using System.Security.Principal; // WindowsImpersonationContext
using System.Security.Permissions; // PermissionSetAttribute
using System.Security.Cryptography;
using System.IO;
using System.Text;
using System.Windows.Forms;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace Oclaro.PromService
{
    /// <summary>
    /// Log in management class
    /// </summary>
    public class LogIn
    {
        /// <summary>
        /// Unique application name for database
        /// </summary>
        private static string AppName = Properties.Settings.Default.AppName;
        /// <summary>
        /// Unique application identifier for database
        /// </summary>
        private static string AppGuid = Properties.Settings.Default.AppGUID;
        /// <summary>
        /// Impersonated windows user
        /// </summary>
        public static WindowsImpersonationContext impersonatedUser = null;
        /// <summary>
        /// Bool to select which instance of the Pastie database should be used.
        /// </summary>
        public static bool UseDevelopmentPastieDatabase = false;

        #region DllImports

        /// <summary>
        /// Tests user logon against the local windows directory
        /// </summary>
        /// <param name="lpszUsername">Windows username</param>
        /// <param name="lpszDomain">Domain Name</param>
        /// <param name="lpszPassword">Windows password</param>
        /// <param name="dwLogonType"></param>
        /// <param name="dwLogonProvider"></param>
        /// <param name="phToken">Log in token</param>
        /// <returns>True if valid windows user</returns>
        [DllImport("advapi32.dll", SetLastError = true)]
        public static extern bool LogonUser(
                String lpszUsername,
                String lpszDomain,
                String lpszPassword,
                int dwLogonType,
                int dwLogonProvider,
                ref IntPtr phToken);

        /// <summary>
        /// Close the Windows log in object
        /// </summary>
        /// <param name="handle"></param>
        /// <returns></returns>
        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        public extern static bool CloseHandle(IntPtr handle);

        /// <summary>
        /// Impersonate a user
        /// </summary>
        /// <param name="domainName">Domain</param>
        /// <param name="userName">Windows user to impersonate</param>
        /// <param name="password">Impersonated user password</param>
        [PermissionSetAttribute(SecurityAction.Demand, Name = "FullTrust")]
        public static void Impersonate(string domainName, string userName, string password)
        {
            try
            {
                // Use the unmanaged LogonUser function to get the user token for
                // the specified user, domain, and password.
                const int LOGON32_PROVIDER_DEFAULT = 0;
                // Passing this parameter causes LogonUser to create a primary token.
                const int LOGON32_LOGON_INTERACTIVE = 2;
                tokenHandle = IntPtr.Zero;

                // ---- Step - 1 
                // Call LogonUser to obtain a handle to an access token.
                bool returnValue = LogonUser(
                                    userName,
                                    domainName,
                                    password,
                                    LOGON32_LOGON_INTERACTIVE,
                                    LOGON32_PROVIDER_DEFAULT,
                                    ref tokenHandle);         // tokenHandle - new security token

                if (false == returnValue)
                {
                    int ret = Marshal.GetLastWin32Error();
                    Console.WriteLine("LogonUser call failed with error code : " + ret);
                    throw new System.ComponentModel.Win32Exception(ret);
                }

                // ---- Step - 2 
                WindowsIdentity newId = new WindowsIdentity(tokenHandle);

                // ---- Step - 3 
                impersonatedUser = newId.Impersonate();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception occurred. " + ex.Message);
            }
        }

        /// <summary>
        ///  Stops impersonation
        /// </summary>
        public static void Undo()
        {
            if (impersonatedUser != null)
                impersonatedUser.Undo();
            // Free the tokens.
            if (tokenHandle != IntPtr.Zero)
                CloseHandle(tokenHandle);
        }

        #endregion

        #region Decrypt
        private static string mDBServer = "", mDBUsername = "", mDBPassword = "";

        private static byte[] CryptoKey = { 159, 106, 145, 195, 49, 29, 161, 224, 68, 52, 196, 62, 243, 123, 250, 64 };
        private static byte[] CryptoIV = { 246, 153, 114, 41, 233, 94, 212, 116 };
        private static RC2 RC2Alg;
        private static MemoryStream mStream;
        private static bool mInitialised = false;

        private static void InitCrypto()
        {
            try
            {
                // Create a new RC2 object to generate a key 
                // and initialization vector (IV).  Specify one 
                // of the recognized simple names for this 
                // algorithm.
                mStream = new MemoryStream();

                RC2Alg = RC2.Create();
                RC2Alg.IV = CryptoIV;
                RC2Alg.Key = CryptoKey;

            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.ToString());
            }
        }

        /// <summary>
        /// Decode string
        /// </summary>
        /// <param name="encodedText">Encoded text to decode</param>
        /// <returns>Plain text result</returns>
        public static string Decode(string encodedText)
        {
            try
            {
                string ret = "";
                byte[] Data = new Byte[encodedText.Length / 2];

                MemoryStream mStream = new MemoryStream();
                CryptoStream cStream = new CryptoStream(mStream, RC2Alg.CreateDecryptor(), CryptoStreamMode.Write);

                for (int j = 0; j < Data.GetLength(0); ++j)
                {
                    Data[j] = Convert.ToByte(encodedText.Substring(2 * j, 2), 16);
                }
                cStream.Write(Data, 0, Data.GetLength(0));
                cStream.FlushFinalBlock();
                byte[] DecryptedData = mStream.ToArray();
                ret = ASCIIEncoding.ASCII.GetString(DecryptedData);
                // we now have an array of bytes so
                return ret;
            }
            catch
            {
                return null;
            }

        }

        /// <summary>
        /// Encode string
        /// </summary>
        /// <param name="plainText">Decoded text to encode</param>
        /// <returns>Encoded text that can be saved freely</returns>
        public static string Encode(string plainText)
        {
            byte[] EncodedPassword = Cypher(plainText);
            string strOut = "";
            for (int i = 0; i < EncodedPassword.GetLength(0); ++i)
            {
                strOut += EncodedPassword[i].ToString("X2");
            }
            return strOut;
        }

        private static byte[] Cypher(string plainText)
        {
            byte[] ret;
            MemoryStream mStream;
            CryptoStream cStream;
            // Create a MemoryStream.
            if (!mInitialised) InitCrypto();
            mStream = new MemoryStream();

            cStream = new CryptoStream(mStream,
                    RC2Alg.CreateEncryptor(), CryptoStreamMode.Write);

            byte[] toEncrypt = new ASCIIEncoding().GetBytes(plainText);

            try
            {

                // Write the byte array to the crypto stream and flush it.
                cStream.Write(toEncrypt, 0, toEncrypt.Length);
                cStream.FlushFinalBlock();

                // Get an array of bytes from the 
                // MemoryStream that holds the 
                // encrypted data.
                ret = mStream.ToArray();
            }
            catch (CryptographicException e)
            {
                System.Diagnostics.Debug.WriteLine(e.ToString());
                System.Diagnostics.Debug.Assert(false);
                ret = null;
            }
            finally
            {
                // Close the streams.
                cStream.Close();
                mStream.Close();

            }
            // Return the encrypted buffer.
            return ret;

        }


        #endregion

        #region DatabaseAccess
        private static string mCallInfo = "";
        private static MySqlConnection DBConnection = null;
        private static void OpenDatabase()
        {

            try
            {
                string ConnectionInfo = "Server=" + mDBServer + "; UID=" + Decode(mDBUsername) + "; pwd=" + Decode(mDBPassword) + "; Database=" + (UseDevelopmentPastieDatabase ? "pastiedev" : "pastie") + ";";
                DBConnection = new MySqlConnection(ConnectionInfo); // ("Server=Deepthought; UID=root; pwd=shotgun; Database=tams;");
                DBConnection.Open();
                //mInitialised = true;
            }
            catch (Exception OpenEx)
            {

                LogFile.Write(OpenEx.Message);
                throw new Exception("Can not connect to database", OpenEx);
            }
        }

        private static void CloseDatabase()
        {
            DBConnection.Close();
        }

        private static void GetPromisDetails(string username, out string promisUsername, out string promisPWord)
        {

            promisUsername = "";
            promisPWord = "";
            using (MySqlCommand GetPromisDetails = new MySqlCommand())
            {

                //PROCEDURE `DoAppLogin`(IN pCallInfo VARCHAR(50),IN pAppName VARCHAR(80), IN pAppGUID VARCHAR(100) , IN pWinUsername VARCHAR(20) )
                GetPromisDetails.Connection = DBConnection;
                GetPromisDetails.CommandText = "DoAppLogin";
                GetPromisDetails.CommandType = System.Data.CommandType.StoredProcedure;

                MySqlParameter paramCallInfo = new MySqlParameter("pCallInfo", MySqlDbType.VarChar);
                paramCallInfo.Direction = System.Data.ParameterDirection.Input;
                paramCallInfo.Value = mCallInfo;
                GetPromisDetails.Parameters.Add(paramCallInfo);

                MySqlParameter paramAppName = new MySqlParameter("pAppName", MySqlDbType.VarChar);
                paramAppName.Direction = System.Data.ParameterDirection.Input;
                paramAppName.Value = AppName;
                GetPromisDetails.Parameters.Add(paramAppName);

                MySqlParameter paramAppGUID = new MySqlParameter("pAppGUID", MySqlDbType.VarChar);
                paramAppGUID.Direction = System.Data.ParameterDirection.Input;
                paramAppGUID.Value = AppGuid;
                GetPromisDetails.Parameters.Add(paramAppGUID);

                MySqlParameter paramWinUsername = new MySqlParameter("pWinUsername", MySqlDbType.VarChar);
                paramWinUsername.Direction = System.Data.ParameterDirection.Input;
                paramWinUsername.Value = username;
                GetPromisDetails.Parameters.Add(paramWinUsername);

                using (MySqlDataReader drPromisDetails = GetPromisDetails.ExecuteReader())
                {
                    if (drPromisDetails.HasRows)
                    {
                        drPromisDetails.Read();
                        int RetStatus = drPromisDetails.GetInt32("RetStatus");
                        switch (RetStatus)
                        {
                            case 0:     //Success
                                promisUsername = drPromisDetails.GetString("PromisID");
                                promisPWord = drPromisDetails.GetString("PromisPassword");
                                break;
                            case -1:    //SQL Error
                                throw new Exception("SQL Error. Please seek assistance");
                            case -2:    // App not recognised
                                throw new Exception("Invalid application");
                            case -3:    // Windows User ID not recognised
                                throw new Exception("Invalid username");
                            case -4:    //This user does not have access from this App
                                throw new Exception("No access to selected appliction for this user");
                            case -5:    // User is locked out
                                throw new Exception("User is locked out");
                            default:
                                throw new Exception("Unknown return value (" + RetStatus.ToString() + ") from database. Please seek assistance");
                        }
                        drPromisDetails.Close();
                    }
                    else
                    {
                        throw new Exception("Stored procedure failed");
                    }
                }
            }
        }

        private static void GetPromisDetailsEx(string username, out string promisUsername, out string promisPWord, out int accessLevel)
        {

            promisUsername = "";
            promisPWord = "";
            accessLevel = 0;

            using (MySqlCommand GetPromisDetails = new MySqlCommand())
            {

                //PROCEDURE `DoAppLogin`(IN pCallInfo VARCHAR(50),IN pAppName VARCHAR(80), IN pAppGUID VARCHAR(100) , IN pWinUsername VARCHAR(20) )
                GetPromisDetails.Connection = DBConnection;
                GetPromisDetails.CommandText = "DoAppLoginEx";
                GetPromisDetails.CommandType = System.Data.CommandType.StoredProcedure;

                MySqlParameter paramCallInfo = new MySqlParameter("pCallInfo", MySqlDbType.VarChar);
                paramCallInfo.Direction = System.Data.ParameterDirection.Input;
                paramCallInfo.Value = mCallInfo;
                GetPromisDetails.Parameters.Add(paramCallInfo);

                MySqlParameter paramAppName = new MySqlParameter("pAppName", MySqlDbType.VarChar);
                paramAppName.Direction = System.Data.ParameterDirection.Input;
                paramAppName.Value = AppName;
                GetPromisDetails.Parameters.Add(paramAppName);

                MySqlParameter paramAppGUID = new MySqlParameter("pAppGUID", MySqlDbType.VarChar);
                paramAppGUID.Direction = System.Data.ParameterDirection.Input;
                paramAppGUID.Value = AppGuid;
                GetPromisDetails.Parameters.Add(paramAppGUID);

                MySqlParameter paramWinUsername = new MySqlParameter("pWinUsername", MySqlDbType.VarChar);
                paramWinUsername.Direction = System.Data.ParameterDirection.Input;
                paramWinUsername.Value = username;
                GetPromisDetails.Parameters.Add(paramWinUsername);

                using (MySqlDataReader drPromisDetails = GetPromisDetails.ExecuteReader())
                {
                    if (drPromisDetails.HasRows)
                    {
                        drPromisDetails.Read();
                        int RetStatus = drPromisDetails.GetInt32("RetStatus");
                        switch (RetStatus)
                        {
                            case 0:     //Success
                                promisUsername = drPromisDetails.GetString("PromisID");
                                promisPWord = drPromisDetails.GetString("PromisPassword");
                                accessLevel = drPromisDetails.GetInt32("AccessLevel");
                                break;
                            case -1:    //SQL Error
                                throw new Exception("SQL Error. Please seek assistance");
                            case -2:    // App not recognised
                                throw new Exception("Invalid application");
                            case -3:    // Windows User ID not recognised
                                throw new Exception("Invalid username");
                            case -4:    //This user does not have access from this App
                                throw new Exception("No access to selected appliction for this user");
                            case -5:    // User is locked out
                                throw new Exception("User is locked out");
                            default:
                                throw new Exception("Unknown return value (" + RetStatus.ToString() + ") from database. Please seek assistance");
                        }
                        drPromisDetails.Close();
                    }
                    else
                    {
                        throw new Exception("Stored procedure failed");
                    }
                }


            }
        }

        private static void GetPromisDetailsRigWin(string username, string rigId, out string pastieName, out string fullName,
            out string promisUsername, out string promisPassword, out int accessLevel, out string accessLevelName, out string rigName, out int status)
        {
            pastieName = "";
            fullName = "";
            promisPassword = "";
            promisUsername = "";
            accessLevel = 0;
            accessLevelName = "";
            rigName = "";
            status = 1;

            //PROCEDURE `DoAppRigLogin`(IN pCallInfo VARCHAR(50),IN pAppName VARCHAR(80), IN pAppGUID VARCHAR(100) , IN pWinUsername VARCHAR(20),IN pRigPromisID VARCHAR(10)
            using (MySqlCommand GetPromisDetails = new MySqlCommand())
            {
                GetPromisDetails.Connection = DBConnection;
                GetPromisDetails.CommandText = "DoAppRigLogin";
                GetPromisDetails.CommandType = System.Data.CommandType.StoredProcedure;

                MySqlParameter paramCallInfo = new MySqlParameter("pCallInfo", MySqlDbType.VarChar);
                paramCallInfo.Direction = System.Data.ParameterDirection.Input;
                paramCallInfo.Value = mCallInfo;
                GetPromisDetails.Parameters.Add(paramCallInfo);

                MySqlParameter paramAppName = new MySqlParameter("pAppName", MySqlDbType.VarChar);
                paramAppName.Direction = System.Data.ParameterDirection.Input;
                paramAppName.Value = AppName;
                GetPromisDetails.Parameters.Add(paramAppName);

                MySqlParameter paramAppGUID = new MySqlParameter("pAppGUID", MySqlDbType.VarChar);
                paramAppGUID.Direction = System.Data.ParameterDirection.Input;
                paramAppGUID.Value = AppGuid;
                GetPromisDetails.Parameters.Add(paramAppGUID);

                MySqlParameter paramWinUsername = new MySqlParameter("pWinUsername", MySqlDbType.VarChar);
                paramWinUsername.Direction = System.Data.ParameterDirection.Input;
                paramWinUsername.Value = username;
                GetPromisDetails.Parameters.Add(paramWinUsername);

                MySqlParameter paramRigPromisID = new MySqlParameter("pRigPromisID", MySqlDbType.VarChar);
                paramRigPromisID.Direction = System.Data.ParameterDirection.Input;
                paramRigPromisID.Value = rigId;
                GetPromisDetails.Parameters.Add(paramRigPromisID);

                using (MySqlDataReader drPromisInfo = GetPromisDetails.ExecuteReader())
                {
                    if (drPromisInfo.HasRows)
                    {
                        drPromisInfo.Read();
                        int RetStatus = drPromisInfo.GetInt32("RetStat");
                        status = RetStatus;
                        switch (RetStatus)
                        {
                            case 0://Success
                                pastieName = drPromisInfo.GetString("LoginID");
                                fullName = drPromisInfo.GetString("Fullname");
                                promisPassword = drPromisInfo.GetString("PromisPassword");
                                promisUsername = drPromisInfo.GetString("PromisUserName");
                                accessLevel = drPromisInfo.GetInt32("AccessLevel");
                                accessLevelName = drPromisInfo.GetString("AccessLevelName");
                                rigName = drPromisInfo.GetString("Rigname");
                                break;
                            case -1://Generic Sql Error
                                throw new Exception("SQL Error. Please seek assistance");
                            case -2://invalid username/password combo
                                throw new Exception("invalid username/password combo");
                            case -3://User is disabled
                                throw new Exception("User is disabled");
                            case -4://User is locked
                                throw new Exception("User is locked out");
                            case -5://Password expired
                                throw new Exception("Password expired");
                            case -6://invalid rigname
                                throw new Exception("Invalid rigname");
                            case -7://No Access for user on this rig
                                throw new Exception("No Access for user on this rig");
                            case -8://invalid app info
                                throw new Exception("Invalid application");
                            case -9://Invalid WindowsID
                                throw new Exception("Invalid WindowsID");
                            default:
                                throw new Exception("Unknown return value (" + RetStatus.ToString() + ") from database. Please seek assistance");
                        }
                    }
                    else
                    {
                        throw new Exception("Stored Procedure Failure");
                    }
                }
            }
        }

        private static void GetPromsisDetailsRigPastie(string username, string password, string rigId, out string pastieName, out string fullName,
            out string promisUsername, out string promisPassword, out int accessLevel, out string accessLevelName, out string rigName, out int status)
        {
            pastieName = "";
            fullName = "";
            promisUsername = "";
            promisPassword = "";
            accessLevel = 0;
            accessLevelName = "";
            rigName = "";
            status = 1;

            //use DoRigLogin(IN pCallInfo VARCHAR(50) , IN pUsername VARCHAR(40), IN pPassword VARCHAR(80), IN pRigPromisID VARCHAR(10))
            using (MySqlCommand GetPromisDetails = new MySqlCommand())
            {
                GetPromisDetails.Connection = DBConnection;
                GetPromisDetails.CommandText = "DoRigLogin";
                GetPromisDetails.CommandType = System.Data.CommandType.StoredProcedure;

                MySqlParameter paramCallInfo = new MySqlParameter("pCallInfo", MySqlDbType.VarChar);
                paramCallInfo.Direction = System.Data.ParameterDirection.Input;
                paramCallInfo.Value = mCallInfo;
                GetPromisDetails.Parameters.Add(paramCallInfo);

                MySqlParameter paramUserName = new MySqlParameter("pUsername", MySqlDbType.VarChar);
                paramUserName.Direction = System.Data.ParameterDirection.Input;
                paramUserName.Value = username;
                GetPromisDetails.Parameters.Add(paramUserName);

                MySqlParameter paramPassword = new MySqlParameter("pPassword", MySqlDbType.VarChar);
                paramPassword.Direction = System.Data.ParameterDirection.Input;
                paramPassword.Value = password;
                GetPromisDetails.Parameters.Add(paramPassword);

                MySqlParameter paramRigPromisID = new MySqlParameter("pRigPromisID", MySqlDbType.VarChar);
                paramRigPromisID.Direction = System.Data.ParameterDirection.Input;
                paramRigPromisID.Value = rigId;
                GetPromisDetails.Parameters.Add(paramRigPromisID);

                using (MySqlDataReader drPromisInfo = GetPromisDetails.ExecuteReader())
                {
                    if (drPromisInfo.HasRows)
                    {
                        drPromisInfo.Read();
                        int RetStatus = drPromisInfo.GetInt32("RetStat");
                        status = RetStatus;
                        switch (RetStatus)
                        {
                            case 0://Success
                                pastieName = drPromisInfo.GetString("LoginID");
                                fullName = drPromisInfo.GetString("Fullname");
                                promisPassword = drPromisInfo.GetString("PromisPassword");
                                promisUsername = drPromisInfo.GetString("PromisUserName");
                                accessLevel = drPromisInfo.GetInt32("AccessLevel");
                                accessLevelName = drPromisInfo.GetString("AccessLevelName");
                                rigName = drPromisInfo.GetString("Rigname");
                                break;
                            case -1://Generic Sql Error
                                throw new Exception("SQL Error. Please seek assistance");
                            case -2://invalid username/password combo
                                throw new Exception("Invalid username/password combo");
                            case -3://User is disabled
                                throw new Exception("User is disabled");
                            case -4://User is locked
                                throw new Exception("User is locked out");
                            case -5://Password expired
                                throw new Exception("Password expired");
                            case -6://invalid rigname
                                throw new Exception("Invalid rigname");
                            case -7://No Access for user on this rig
                                throw new Exception("No Access for user on this rig");
                            case -8://invalid app info
                                throw new Exception("Invalid application");
                            case -9://Invalid WindowsID
                                throw new Exception("Invalid WindowsID");
                            default:
                                throw new Exception("Unknown return value (" + RetStatus.ToString() + ") from database. Please seek assistance");
                        }
                    }
                    else
                    {
                        throw new Exception("Stored Procedure Failure");
                    }
                }
            }
        }

        /// <summary>
        /// Makes sure connection to database is open
        /// </summary>
        /// <returns>True if connected</returns>
        public static bool TestConnection()
        {
            bool worked = false;

            if (!mInitialised)
                worked = false;

            try
            {
                string ConnectionInfo = "Server=" + mDBServer + "; UID=" + Decode(mDBUsername) + "; pwd=" + Decode(mDBPassword) + "; Database=pastie;";
                DBConnection = new MySqlConnection(ConnectionInfo); // ("Server=Deepthought; UID=root; pwd=shotgun; Database=tams;");
                DBConnection.Open();
                worked = true;
            }
            catch
            {
                worked = false;
            }

            return worked;
        }

        #endregion

        private static IntPtr tokenHandle = new IntPtr(0);

        /// <summary>
        /// Starts the password system looking at the system managed by test
        /// </summary>
        /// <param name="dbServer">Database server</param>
        /// <param name="dbUsername">Database username</param>
        /// <param name="dbPassword">Database password</param>
        public static void InitPasswordSystem(string dbServer, string dbUsername, string dbPassword)
        {
            dbServer = Properties.Settings.Default.SQLServer;
            dbUsername = Properties.Settings.Default.dbUsername;
            dbPassword = Properties.Settings.Default.dbPassword;
            if (dbServer == "" || dbUsername == "" || dbPassword == "") throw new ArgumentOutOfRangeException("Parametes can not be blank");
            if (dbServer == "" || dbUsername == "" || dbPassword == "") throw new ArgumentOutOfRangeException("Parametes can not be blank");
            InitCrypto();

            string[] WinUsername = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Split(new char[] { '\\' });

            string WinMachineName = System.Net.Dns.GetHostName();
            mCallInfo = WinMachineName + ":" + WinUsername[WinUsername.GetUpperBound(0)];

            mDBServer = dbServer;
            mDBUsername = dbUsername;
            mDBPassword = dbPassword;
            mInitialised = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbServer">Database server hosting mySQL</param>
        /// <param name="dbUsername">Database username</param>
        /// <param name="dbPassword">Database password</param>
        /// <param name="applicationName">unique application GUID</param>
        /// <param name="applicationGuid">unique application name</param>
        /// <returns></returns>
        public static PastieConfigDetails InitPastieSystem(string dbServer, string dbUsername, string dbPassword, string applicationName, string applicationGuid)
        {
            string ErrorMessage = null;
            PastieConfigDetails SystemConfig = null;
            try
            {    

                InitPasswordSystem(dbServer, dbUsername, dbPassword, applicationName, applicationGuid);

                SystemConfig = new PastieConfigDetails();
                OpenDatabase();


                if (!GetLoginDomains(SystemConfig, ref ErrorMessage)) throw new Exception("Failed to get domains\n" + ErrorMessage);

                if (!GetPromisInstanceInfo(SystemConfig, ref ErrorMessage)) throw new Exception("Failed to get Promis Instances\n" + ErrorMessage);

                if (!GetMRPInfo(SystemConfig, ref ErrorMessage)) throw new Exception("Failed to get MRP connection data\n" + ErrorMessage);

            }
            catch (Exception Error)
            {
                SystemConfig = null;
                throw Error;
            }
            finally
            {
                CloseDatabase();
            }
            return SystemConfig;

        }


        /// <summary>
        /// Starts the password system looking at the system managed by test and overrides the appname and GUID
        /// </summary>
        /// <param name="databaseServer">Database server</param>
        /// <param name="databaseUsername">Database username</param>
        /// <param name="databasePassword">Database password</param>
        /// <param name="appGuid">unique application GUID</param>
        /// <param name="appName">unique application name</param>
        private static void InitPasswordSystem(string databaseServer, string databaseUsername, string databasePassword, string appName, string appGuid)
        {

            //dbServer = Properties.Settings.Default.SQLServer;
            //dbUsername = Properties.Settings.Default.dbUsername;
            //dbPassword = Properties.Settings.Default.dbPassword;

            AppName = appName;
            AppGuid = appGuid;
            if (databaseServer == "" || databaseUsername == "" || databasePassword == "") throw new ArgumentOutOfRangeException("Parameters can not be blank");
            //if (databaseServer == "" || dbUsername == "" || dbPassword == "") throw new ArgumentOutOfRangeException("Parametes can not be blank");
            InitCrypto();

            string[] WinUsername = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Split(new char[] { '\\' });
            //string[] WinUsername = Environment.UserName.Split(new char[] { '\\' });

            string WinMachineName = System.Net.Dns.GetHostName();
            mCallInfo = WinMachineName + ":" + WinUsername[WinUsername.GetUpperBound(0)];

            mDBServer = databaseServer;
            mDBUsername = databaseUsername;
            mDBPassword = databasePassword;
            mInitialised = true;
        }

        /// <summary>
        /// Check if the windows username and password match the active directory
        /// </summary>
        /// <param name="domainName">Domain ID</param>
        /// <param name="username">Windows username</param>
        /// <param name="password">Windows password</param>
        /// <returns>True if log in was successful</returns>
        public static bool IsValidLogin(string domainName, string username, string password)
        {

            // Use the unmanaged LogonUser function to get the user token for
            // the specified user, domain, and password.
            const int LOGON32_PROVIDER_DEFAULT = 0;
            // Passing this parameter causes LogonUser to create a primary token.
            const int LOGON32_LOGON_INTERACTIVE = 2;
            tokenHandle = IntPtr.Zero;

            // ---- Step - 1 
            // Call LogonUser to obtain a handle to an access token.
            bool returnValue = LogonUser(
                                username,
                                domainName,
                                password,
                                LOGON32_LOGON_INTERACTIVE,
                                LOGON32_PROVIDER_DEFAULT,
                                ref tokenHandle);         // tokenHandle - new security token

            if (!returnValue) return false;
            return true;
        }

        /// <summary>
        /// Returns the promis username and password
        /// </summary>
        /// <param name="winUsername">Windows username used as an identifier</param>
        /// <param name="promisID">Promis User ID</param>
        /// <param name="promisPassword">PROMIS password</param>
        /// <returns>True is successful</returns>
        public static bool GetPromisID(string winUsername, out string promisID, out string promisPassword)
        {
            promisID = "";
            promisPassword = "";
            bool worked = false;

            if (!mInitialised) throw new Exception("Password system not initialised");
            try
            {
                string PPassword = "";
                OpenDatabase();
                GetPromisDetails(winUsername, out promisID, out PPassword);
                promisPassword = Decode(PPassword);
                CloseDatabase();
                worked = true;
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Can not connect to database"))
                {
                    MessageBox.Show("Error connecting to the database", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                worked = false;
            }

            return worked;
        }

        /// <summary>
        /// Get PROMIS username and password along with the user's access level
        /// </summary>
        /// <param name="winUsername">Windows username</param>
        /// <param name="promisID">PROMIS username</param>
        /// <param name="promisPassword">PROMIS password</param>
        /// <param name="accessLevel">Access level to be managed by application</param>
        /// <returns></returns>
        public static bool GetPromisIDEx(string winUsername, out string promisID, out string promisPassword, out int accessLevel)
        {
            promisID = "";
            promisPassword = "";
            bool worked = false;

            if (!mInitialised) throw new Exception("Password system not initialised");
            try
            {
                string PPassword = "";
                OpenDatabase();
                GetPromisDetailsEx(winUsername, out promisID, out PPassword, out accessLevel);
                promisPassword = Decode(PPassword);
                CloseDatabase();
                worked = true;
            }
            catch (Exception ex)
            {
                worked = false;
                throw ex;
            }

            return worked;
        }

        /// <summary>
        /// Get PROMIS username and password along with the user's access level based on the rig they are logging into
        /// </summary>
        /// <param name="winUsername">Windows username</param>
        /// <param name="rigId">Equipment the user is logging into</param>
        /// <param name="pastieName">PASTIE username</param>
        /// <param name="fullName">User's full name</param>
        /// <param name="promisUsername">PROMIS username</param>
        /// <param name="promisPassword">PROMIS password</param>
        /// <param name="accessLevel">Access level to be managed by application</param>
        /// <param name="accessLevelName">Identifier for the access level</param>
        /// <param name="rigName">Human understandable rig name</param>
        /// <param name="status">Status of the database connection</param>
        /// <returns>True if successful</returns>
        public static bool GetPromisIDRigWin(string winUsername, string rigId, out string pastieName, out string fullName,
            out string promisUsername, out string promisPassword, out int accessLevel, out string accessLevelName, out string rigName, out int status)
        {
            pastieName = "";
            fullName = "";
            promisPassword = "";
            promisUsername = "";
            accessLevel = 0;
            accessLevelName = "";
            rigName = "";
            status = 1;

            bool worked = false; ;

            if (!mInitialised) throw new Exception("Password system not initialised");
            try
            {
                string PPasword = "";
                OpenDatabase();
                GetPromisDetailsRigWin(winUsername, rigId, out pastieName, out fullName, out promisUsername, out PPasword, out accessLevel, out accessLevelName, out rigName, out status);
                promisPassword = Decode(PPasword);
                CloseDatabase();
                worked = true;
            }
            catch (Exception ex)
            {
                worked = false;
                throw ex;
            }
            return worked;
        }

        /// <summary>
        /// Uses the Oclaro PASTIE log in system to get a users PROMIS_TP information as well as access level for a given Rig 
        /// </summary>
        /// <param name="username">PASTIE user name</param>
        /// <param name="password">PASTIE password</param>
        /// <param name="rigId">Equipment user is logging inot</param>
        /// <param name="pastieName">PASTIE username</param>
        /// <param name="fullName">User's full name</param>
        /// <param name="promisUsername">PROMIS username</param>
        /// <param name="promisPassword">PROMIS password</param>
        /// <param name="accessLevel">Access level to be managed by application</param>
        /// <param name="accessLevelName">Identifier for the access level</param>
        /// <param name="rigName">Human understandable rig name</param>
        /// <param name="status">Status of the database connection</param>
        /// <returns>True if successful</returns>
        public static bool GetPromsisIDRigPastie(string username, string password, string rigId, out string pastieName, out string fullName,
            out string promisUsername, out string promisPassword, out int accessLevel, out string accessLevelName, out string rigName, out int status)
        {
            pastieName = "";
            fullName = "";
            promisUsername = "";
            promisPassword = "";
            accessLevel = 0;
            accessLevelName = "";
            rigName = "";
            status = 1;

            bool worked = false;

            if (!mInitialised) throw new Exception("Password system not initialised");
            try
            {
                string PPassword = "";
                OpenDatabase();
                GetPromsisDetailsRigPastie(username, Encode(password), rigId, out pastieName, out fullName, out promisUsername, out PPassword, out accessLevel, out accessLevelName, out rigName, out status);
                promisPassword = Decode(PPassword);
                CloseDatabase();
                worked = true;
            }
            catch (Exception ex)
            {
                worked = false;
                throw ex;
            }
            return worked;
        }

        /// <summary>
        /// Get Promis Login details including Fullname 
        /// Handled Lumentum Windows ID
        /// </summary>
        /// <param name="winUsername"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public static PastieUserDetails GetPromisIDExtended(string winUsername, ref string status)
        {
            //bool RetValue = false;

            PastieUserDetails ThisUser = null;
            if (!mInitialised) throw new Exception("Password system not initialised");
            try
            {
                OpenDatabase();

                using (MySqlCommand GetPastieDetails = new MySqlCommand())
                {
                    GetPastieDetails.Connection = DBConnection;
                    GetPastieDetails.CommandText = "DoAppLoginLumentum";
                    GetPastieDetails.CommandType = System.Data.CommandType.StoredProcedure;

                    MySqlParameter paramCallInfo = new MySqlParameter("pCallInfo", MySqlDbType.VarChar);
                    paramCallInfo.Direction = System.Data.ParameterDirection.Input;
                    paramCallInfo.Value = mCallInfo;
                    GetPastieDetails.Parameters.Add(paramCallInfo);

                    MySqlParameter paramAppName = new MySqlParameter("pAppName", MySqlDbType.VarChar);
                    paramAppName.Direction = System.Data.ParameterDirection.Input;
                    paramAppName.Value = AppName;
                    GetPastieDetails.Parameters.Add(paramAppName);

                    MySqlParameter paramAppGUID = new MySqlParameter("pAppGUID", MySqlDbType.VarChar);
                    paramAppGUID.Direction = System.Data.ParameterDirection.Input;
                    paramAppGUID.Value = AppGuid;
                    GetPastieDetails.Parameters.Add(paramAppGUID);

                    MySqlParameter paramWinUsername = new MySqlParameter("pWinUsername", MySqlDbType.VarChar);
                    paramWinUsername.Direction = System.Data.ParameterDirection.Input;
                    paramWinUsername.Value = winUsername;
                    GetPastieDetails.Parameters.Add(paramWinUsername);


                    using (MySqlDataReader drPastieInfo = GetPastieDetails.ExecuteReader())
                    {
                        if (drPastieInfo.HasRows)
                        {
                            drPastieInfo.Read();
                            int RetStatus = drPastieInfo.GetInt32("RetStatus");
                            //status = RetStatus;
                            switch (RetStatus)
                            {
                                case 0://Success
                                    string PastieName = drPastieInfo.GetString("Username");
                                    string Fullname = drPastieInfo.GetString("Fullname");
                                    string PromisPassword = drPastieInfo.GetString("PromisPassword");
                                    string PromisUsername = drPastieInfo.GetString("PromisID");
                                    int AccessLevel = drPastieInfo.GetInt32("AccessLevel");
                                    ThisUser = new PastieUserDetails(winUsername, PastieName, PromisUsername, Decode(PromisPassword), Fullname, AccessLevel);
                                    status = "Success";
                                    break;
                                case -1://Generic Sql Error
                                    status = ("SQL Error. Please seek assistance");
                                    break;
                                case -2://invalid app info
                                    status = ("Invalid application");
                                    break;
                                case -3://Invalid WindowsID
                                    status = ("Invalid WindowsID");
                                    break;
                                case -4://No Access for user on this rig
                                    status = ("No Access for user to this Application");
                                    break;
                                case -5://User is disabled
                                    status = ("User is disabled");
                                    break;
                                default:
                                    throw new Exception("Unknown return value (" + RetStatus.ToString() + ") from database. Please seek assistance");
                            }
                        }
                        else
                        {
                            throw new Exception("Stored Procedure Failure");
                        }
                    }
                }
            }
            catch (Exception Error)
            {
                throw Error;
            }
            finally
            {
                CloseDatabase();
            }


            return ThisUser;
        }

        /** Gets promis instance names from DB **/
        private static bool GetPromisInstanceInfo(PastieConfigDetails sysConf, ref string errorMsg)
        {
            bool ReturnStatus = false;
            try
            {
                // This assumes that the database is still open after getting the User details
                using (MySqlCommand GetPromisDetails = new MySqlCommand())
                {
                    GetPromisDetails.Connection = DBConnection;
                    GetPromisDetails.CommandText = "SELECT * FROM `pastiesystemdata` WHERE `ParameterName` LIKE 'XMLGateway_%';";
                    GetPromisDetails.CommandType = System.Data.CommandType.Text;

                    using (MySqlDataReader drPromisInfo = GetPromisDetails.ExecuteReader())
                    {
                        if (drPromisInfo.HasRows)
                        {
                            while (drPromisInfo.Read())
                            {
                                string InstanceName = drPromisInfo.GetString("ParamValue");
                                string GatewayInstance = drPromisInfo.GetString("ParameterName").Substring(11);
                                if (GatewayInstance == "ProductionDefault")
                                {
                                    sysConf.DefaultProductionInstance = InstanceName;
                                }
                                else sysConf.AddPromisInstance(InstanceName, GatewayInstance);
                            }

                        }


                    }
                    ReturnStatus = true;
                }
            }
            catch (Exception Error)
            {
                errorMsg = Error.Message;
            }

            return ReturnStatus;
        }

        private static bool GetLoginDomains(PastieConfigDetails conf, ref string errorMsg)
        {
            bool ReturnStatus = false;
            try
            {
                // This assumes that the database is still open after getting the User details
                using (MySqlCommand GetPastieDetails = new MySqlCommand())
                {
                    GetPastieDetails.Connection = DBConnection;
                    GetPastieDetails.CommandText = "SELECT * FROM `pastiesystemdata` WHERE `ParameterName` LIKE 'LoginDomains_%';";
                    GetPastieDetails.CommandType = System.Data.CommandType.Text;

                    using (MySqlDataReader drLoginDomains = GetPastieDetails.ExecuteReader())
                    {
                        if (drLoginDomains.HasRows)
                        {
                            while (drLoginDomains.Read())
                            {
                                switch (drLoginDomains.GetString("ParameterName").Substring(13))
                                {
                                    case "Lumentum":
                                        conf.AvailableLumentumLoginDomains = drLoginDomains.GetString("ParamValue").Split('|');
                                        break;
                                    case "Oclaro":
                                        conf.AvailableOclaroLoginDomains = drLoginDomains.GetString("ParamValue").Split('|');
                                        break;

                                    default:
                                        throw new Exception("Unknown Login Domain List");
                                }
                            }

                        }
                    }
                    ReturnStatus = true;
                }
            }
            catch (Exception Error)
            {
                errorMsg = Error.Message;
            }

            return ReturnStatus;
        }

        private static bool GetMRPInfo(PastieConfigDetails conf, ref string errorMsg)
        {
            bool ReturnStatus = false;
            try
            {
                // This assumes that the database is still open after getting the User details
                using (MySqlCommand GetMRPDetails = new MySqlCommand())
                {
                    GetMRPDetails.Connection = DBConnection;
                    GetMRPDetails.CommandText = "SELECT * FROM `pastiesystemdata` WHERE `ParameterName` LIKE 'MRP__%';";
                    GetMRPDetails.CommandType = System.Data.CommandType.Text;

                    using (MySqlDataReader drMRPData = GetMRPDetails.ExecuteReader())
                    {
                        if (drMRPData.HasRows)
                        {
                            while (drMRPData.Read())
                            {
                                conf.AddMRPDataItem(drMRPData.GetString("ParameterName").Substring(4), drMRPData.GetString("ParamValue"));
                            }

                        }
                    }
                    ReturnStatus = true;
                }
            }
            catch (Exception Error)
            {
                errorMsg = Error.Message;
            }

            return ReturnStatus;
        }
    }
}
