﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.VisualBasic;

namespace Oclaro.PromService
{
    /// <summary>
    /// Abstract definitions that PromisProd and PromisDev should impliment on their own
    /// </summary>
    abstract public class Promis
    {
        /// <summary>
        /// Legacy Promis Class
        /// </summary>
        public class DropFileException : Exception
        {
            private string mDetails = "";
            /// <summary>
            /// Exception thrown by the dropfile system
            /// </summary>
            /// <param name="msg">Error message</param>
            public DropFileException(string msg)
                : base(msg)
            {

            }

            /// <summary>
            /// Second exception thrown by the dropfile system
            /// </summary>
            /// <param name="msg">Error message</param>
            /// <param name="previousException">The last excpection thrown</param>
            public DropFileException(string msg, Exception previousException)
                : base(msg, previousException)
            {
            }

            /// <summary>
            /// Exception thrown by the dropfile system
            /// </summary>
            /// <param name="msg">Error message</param>
            /// <param name="details">Details of what caused the exception</param>
            public DropFileException(string msg, string details)
                : base(msg)
            {
                mDetails = details;
            }

            /// <summary>
            /// Full message of the thrown exception
            /// </summary>
            public string FullMsg
            {
                get { return base.Message + "\n" + mDetails; }
            }
        }

        /// <summary>
        /// Legacy Promis Class
        /// </summary>
        public enum PromisStatus
        {
            /// <summary>
            /// Succesfull function
            /// </summary>
            Success,
            /// <summary>
            /// Lot ID was not reconised
            /// </summary>
            InvalidLotID,
            /// <summary>
            /// User was not reconised
            /// </summary>
            InvalidUser,
            /// <summary>
            /// Failed to connect to PROMIS server
            /// </summary>
            PromisConnectionFailed,
            /// <summary>
            /// Invalid lot information
            /// </summary>
            InvalidBurninCartForLot,
            /// <summary>
            /// Generic error
            /// </summary>
            UnknownError
        }

        /// <summary>
        /// Initilises the Promis Object and stores the username and password to be used later
        /// </summary>
        /// <param name="UserID">PROMIS TP username</param>
        /// <param name="Password">PROMIS TP password</param>
        /// <returns>True if successful</returns>
        abstract public bool Init(string UserID, string Password);

        /// <summary>
        /// Get information about future stages for a lot
        /// </summary>
        /// <param name="lot">Input lot object</param>
        /// <param name="processCount">Future steps to look ahead</param>
        /// <param name="detailLot">Lot object with all the future recipe</param>
        /// <returns>True if successful</returns>
        public abstract bool GetFutureRecp(string lot, int processCount, out Lot detailLot);
        /// <summary>
        /// Get information about future stages for a lot
        /// </summary>
        /// <param name="lot">Lot ID</param>
        /// <param name="processCount">Number of steps to look ahead for</param>
        /// <param name="futRecpies">List of recipe details</param>
        /// <param name="err">Error message</param>
        /// <returns>True if successful</returns>
        public abstract bool GetFutureRecp(string lot, int processCount, out List<Lot.Recipe> futRecpies, out string err);

        /// <summary>
        /// Get location that a lot should be tracked into
        /// </summary>
        /// <param name="LotID">Lot id</param>
        /// <param name="Location">Location to be tracked into</param>
        /// <returns>True if successful</returns>
        /// <remarks>Unused</remarks>
        abstract public bool GetTrackingLocation(string LotID, out string Location);

        /// <summary>
        /// Returns a lot full of information based on a Lot ID, can take a while to call but is the go-to for lot information
        /// </summary>
        /// <param name="LotID">Lot Id</param>
        /// <param name="lot">Lot object full of information</param>
        /// <returns>True if successful</returns>
        abstract public bool GetLotDetails(string LotID, out Lot lot);
        /// <summary>
        /// Returns a lot full of information based on a Lot ID, can take a while to call but is the go-to for lot information
        /// </summary>
        /// <param name="LotID">Lot Id</param>
        /// <param name="lot">Lot object full of information</param>
        /// <param name="err">String of errors thrown by the function</param>
        /// <returns>True if successful</returns>
        abstract public bool GetLotDetails(string LotID, out Lot lot, out string err);

        /// <summary>
        /// Flat out adds a comment to a lot no matter what state it is in
        /// </summary>
        /// <param name="LotID">Lot ID</param>
        /// <param name="cmnt">Comment to add</param>
        /// <returns>True if successful</returns>
        abstract public bool AddComment(string LotID, string cmnt);

        /// <summary>
        /// Lists all the pieces of equipment in a given area.
        /// </summary>
        /// <param name="Location">Location to look at</param>
        /// <param name="equip">List of equipment ids</param>
        /// <returns>True if successful</returns>
        abstract public bool GetEquipmentList(string Location, out List<string> equip);

        /// <summary>
        /// Gets a detailed lot information for all lots running on a piece of equipment
        /// </summary>
        /// <param name="Location">Location of Equipment</param>
        /// <param name="Equip">Equipment ID</param>
        /// <param name="Lots">List of lot objects running on equipment</param>
        /// <returns>True if successful</returns>
        abstract public bool GetLotsOnEquip(string Location, string Equip, out List<Lot> Lots);
        /// <summary>
        /// Gets just the IDs for all lots running on a piece of equipment
        /// </summary>
        /// <param name="Equip">Equipment to look at</param>
        /// <param name="Lots">List of lot ids running on equipment</param>
        /// <returns>True if successful</returns>
        abstract public bool GetLotsOnEquip(string Equip, out List<string> Lots);

        /// <summary>
        /// Get lots waiting for a piece of equipment
        /// </summary>
        /// <param name="equipment">Equipment to look at</param>
        /// <param name="lots">List of lots</param>
        /// <returns>True if successful</returns>
        abstract public bool GetLotsAtEquip(string equipment, out List<string> lots);

        /// <summary>
        /// Dumb tracks a lot into a given piece of equipment.
        /// </summary>
        /// <param name="Lot">Lot to track in</param>
        /// <param name="Equip">Equipment to track it into</param>
        /// <param name="location">Location of equipment</param>
        /// <param name="message">Error message</param>
        /// <returns>True if successful</returns>
        abstract public bool TrackInLots(Lot Lot, string Equip, string location, out string message);
        /// <summary>
        /// Track in to alternate recpie
        /// </summary>
        /// <param name="inLot">Lot to track in</param>
        /// <param name="Equip">Equipment to track it into</param>
        /// <param name="Recpie">Recipe to track into</param>
        /// <param name="location">Location of equipment</param>
        /// <param name="comment">Additional comments</param>
        /// <param name="message">Error message</param>
        /// <returns>True if successful</returns>
        abstract public bool TrackInLots(Lot inLot, string Equip, string Recpie, string location,string comment, out string message);

        /// <summary>
        /// Tracks out a lot with given rejects and reworks
        /// </summary>
        /// <param name="Lot"></param>
        /// <param name="inIdentRejects">List of identified rejected components {Comp Id, Rejection catagory}</param>
        /// <param name="inUnidentRejects">List of unidentified rejected components {Rejection catagory, Quantity}</param>
        /// <param name="inIdentReworks">List of idetified reworks (Not implimented)</param>
        /// <param name="inUnidentReworks">List of unidetified reworks (Not implimented)</param>
        /// <param name="comment">Additional comment</param>
        /// <param name="err">Error message</param>
        /// <returns>True if successful</returns>
        abstract public bool TrackOutLots(Lot Lot, List<string[]> inIdentRejects, List<string[]> inUnidentRejects, List<string[]> inIdentReworks, List<string[]> inUnidentReworks, string comment, out string err);

        /// <summary>
        /// returns a list of DCOP details based on a lot input.
        /// </summary>
        /// <param name="inLot">Lot to get current DCOPs for</param>
        /// <param name="DCOps">DCOP Objects</param>
        /// <param name="err">Error message</param>
        /// <returns>True if successful</returns>
        abstract public bool GetDCOpInfo(Lot inLot, out List<Lot.DataCollectionOperations> DCOps, out string err);
        /// <summary>
        /// Returns the DCOP details based on a operation ID
        /// </summary>
        /// <param name="opID">Operation ID</param>
        /// <param name="DCOp">Dcop Objects</param>
        /// <param name="err">Error Message</param>
        /// <returns>True if successful</returns>
        abstract public bool GetDCOpInfo(string opID, out  List<Lot.DataCollectionOperations> DCOp, out string err);

        /// <summary>
        /// Aborts a running lot instantly with the ability to add a comment
        /// </summary>
        /// <param name="inlot">Lot to abort step</param>
        /// <param name="Comment">Comment for abort</param>
        /// <returns>True if successful</returns>
        abstract public bool AbortStep(Lot inlot, string Comment);
        /// <summary>
        /// Aborts a running lot instantly with the ability to add a comment
        /// </summary>
        /// <param name="inlot">Lot to abort step</param>
        /// <param name="comment">Comment for abort</param>
        /// <param name="err">Error message</param>
        /// <returns>True if successful</returns>
        abstract public bool AbortStep(Lot inlot, string comment, out string err);

        /// <summary>
        /// Puts a lot on hold based on a hold reason and a comment
        /// </summary>
        /// <param name="inLot">Lot to hold</param>
        /// <param name="Comment">Comment for the hold</param>
        /// <param name="holdCode">Hold code</param>
        /// <param name="holdReason">Reason for hold (limited to 50 characters)</param>
        /// <param name="Message">Error Message</param>
        /// <returns>True if successful</returns>
        abstract public bool HoldLot(Lot inLot, string Comment, string holdCode, string holdReason, out string Message);

        /// <summary>
        /// Adds data to a running lot
        /// Data entered in the form {data, component, site, testnum}
        /// </summary>
        /// <param name="inlot">Lot to add Data to</param>
        /// <param name="Data">Array of data for input</param>
        /// <param name="message">Error Message</param>
        /// <param name="comment">Comment to add to Lot</param>
        /// <returns>True if successful</returns>
        /// <remarks>"should be used in congunction with bool GetDCOpInfo()"</remarks>
        abstract public bool AddData(Lot inlot, List<string[]> Data, string comment, out string message);

        /// <summary>
        /// Adds data to a running lot
        /// Data entered in the form {data, component, site, testnum}
        /// </summary>
        /// <param name="inlot">Lot to add Data to</param>
        /// <param name="Data">Array of data for input</param>
        /// <param name="message">Error Message</param>
        /// <param name="comment">Comment to add to Lot</param>
        /// <returns>True if successful</returns>
        /// <remarks>"should be used in congunction with bool GetDCOpInfo()"</remarks>
        abstract public bool CorrectLotData(Lot inlot, List<string[]> Data, string comment, out string message);

        /// <summary>
        /// Scraps materials from a lot with given rejects
        /// </summary>
        /// <param name="inLot">Lot to scrap from</param>
        /// <param name="rejectList">List of identified rejected components {Comp Id, Rejection catagory}</param>
        /// <param name="unidentRejectList">List of unidentified rejected components {Rejection catagory, Quantity}</param>
        /// <param name="comment">Comment</param>
        /// <param name="message">Error message</param>
        /// <returns>True if successful</returns>
        abstract public bool ScrapLot(Lot inLot, List<string[]> rejectList, List<string[]> unidentRejectList, string comment, out string message);

        /// <summary>
        /// Gets all the equipment that a lot can be tracked into
        /// </summary>
        /// <param name="LotID">Lot ID</param>
        /// <param name="Equip">List of equipment that the lot can be tracked into</param>
        /// <param name="Err">Error message</param>
        /// <returns>True if successful</returns>
        abstract public bool GetEquipForLot(string LotID, out List<string> Equip, out string Err);

        /// <summary>
        /// Marks a piece of equipment as down
        /// </summary>
        /// <param name="Equip">Equipment to make as down</param>
        /// <param name="reason">Reason for down</param>
        /// <param name="comment">Additional comment</param>
        /// <param name="err">Error message</param>
        /// <returns>True if successful</returns>
        abstract public bool LockEquipment(string Equip, string reason, string comment, out string err);

        /// <summary>
        /// List of the past recipies up to a set history count
        /// </summary>
        /// <param name="lot">Lot ID</param>
        /// <param name="processCount">Number of history steps to look at</param>
        /// <param name="pastRecpies">Output list</param>
        /// <param name="err">Error message to be passed back to main form</param>
        /// <returns>True if successful</returns>
        abstract public bool GetPastRecp(string lot, int processCount, out List<Lot.Recipe> pastRecpies, out string err);

        /// <summary>
        /// Move lot along when in transit state
        /// </summary>
        /// <param name="inLot">Lot to move</param>
        /// <param name="location">Location to move to</param>
        /// <param name="Comment">Additional comment</param>
        /// <param name="err">Error message</param>
        /// <returns>True if successful</returns>
        abstract public bool MoveLot(Lot inLot, string location, string Comment, out string err);

        /// <summary>
        /// Move a Lot to the next location if in transit state
        /// </summary>
        /// <param name="inLot">Lot to move</param>
        /// <param name="Comment">Additional comment</param>
        /// <param name="err">Error message</param>
        /// <returns>True if successful</returns>
        abstract public bool ContinueLot(Lot inLot,string Comment, out string err);

        /// <summary>
        /// Get all the lot commments for a given lot
        /// </summary>
        /// <param name="lot">Lot ID</param>
        /// <param name="comments">List of Comments</param>
        /// <param name="err">Error Message</param>
        /// <returns>True if successful</returns>
        abstract public bool GetLotComments(string lot, out List<string> comments, out string err);

        /// <summary>
        /// Gets the True lot Id from an alias
        /// </summary>
        /// <param name="lotAlias">Alias Lot</param>
        /// <param name="lotId">True lot ID</param>
        /// <param name="err">Error message</param>
        /// <returns>True if successful</returns>
        abstract public bool GetLotAlias(string lotAlias, out string lotId, out string err);

        /// <summary>
        /// Add control components to a lot
        /// </summary>
        /// <param name="inLot">Lot to add to</param>
        /// <param name="compIds">Control component Ids</param>
        /// <param name="comment">Additional comment</param>
        /// <param name="err">Error message</param>
        /// <returns>True if successful</returns>
        abstract public bool AddControlComponents(Lot inLot, List<string> compIds, string comment, out string err);

        /// <summary>
        /// Remove control components from a lot
        /// </summary>
        /// <param name="inLot">Lot to remove components from</param>
        /// <param name="compIds">Control component Ids</param>
        /// <param name="comment">Additional comment</param>
        /// <param name="err">Error message</param>
        /// <returns>True if successful</returns>
        abstract public bool RemoveControlComponents(Lot inLot, List<string> compIds, string comment, out string err);

        /// <summary>
        /// Changes the state of a piece of equipment
        /// </summary>
        /// <param name="equipment">Equipment to change the status of</param>
        /// <param name="state">New state</param>
        /// <param name="comment">Additional comment</param>
        /// <param name="err">Error message</param>
        /// <returns>True if successful</returns>
        abstract public bool ChangeEquipmentStatus(string equipment, string state, string comment, out string err);

        /// <summary>
        /// Get information about a piece of equipment
        /// </summary>
        /// <param name="equipment">Equipment ID</param>
        /// <param name="equipDet">Equipment details object</param>
        /// <param name="err">Error message</param>
        /// <returns>True if successful</returns>
        abstract public bool GetEquipDetails(string equipment, out Lot.EquipmentDet equipDet, out string err);

        /// <summary>
        /// Set a lot Parameter
        /// </summary>
        /// <param name="lotId">Lot to add to</param>
        /// <param name="paramName">Parameter name</param>
        /// <param name="paramValue">New parameter value</param>
        /// <param name="componentId">Component parameter ID (if null, parameter will be a general lot parameter)</param>
        /// <param name="comment">Additional comment</param>
        /// <param name="err">Error message</param>
        /// <returns>True if successful</returns>
        abstract public bool SetLotParameter(Lot lotId, string paramName, string paramValue, string componentId, string comment, out string err);

        /// <summary>
        /// Returns all the data entered on a lot for a given DCOP
        /// </summary>
        /// <param name="lotID">Batch ID</param>
        /// <param name="opID">Data Collection Operation ID</param>
        /// <param name="histNumSearch">Number of steps to look back to find operation (null for full history)</param>
        /// <param name="opNumSearch">Number of occurences of the operation to return values from (null for all occurences)</param>
        /// <param name="DCOPs">List of DCOP information</param>
        /// <param name="err">Error message</param>
        /// <returns>True if successful</returns>
        abstract public bool GetDCOPData(string lotID, string opID, string histNumSearch, string opNumSearch, out List<Lot.DataCollectionOperations> DCOPs, out string err);

        /// <summary>
        /// Gets all the operations that are performed during a given recipe
        /// </summary>
        /// <param name="lotID">Lot to query</param>
        /// <param name="recipeID">Recipie to find operations for</param>
        /// <param name="ops">List of operations</param>
        /// <param name="err">Error message</param>
        /// <returns>True if successful</returns>
        abstract public bool GetRecipeOperations(string lotID, string recipeID, out List<Lot.Operations> ops, out string err);

        /// <summary>
        /// Releases the current holds put on a lot
        /// </summary>
        /// <param name="inLot">Lot to release</param>
        /// <param name="reason">Reason for release</param>
        /// <param name="comment">Additional comments</param>
        /// <param name="holdNumber">Hold number to release</param>
        /// <param name="err">Error message</param>
        /// <returns>True if successful</returns>
        abstract public bool UnholdLot(Lot inLot, string reason, string comment, string holdNumber, out string err);
        /// <summary>
        /// Releases the current holds put on a lot
        /// </summary>
        /// <param name="inLot">Lot to release</param>
        /// <param name="reason">Reason for release</param>
        /// <param name="comment">Additional comments</param>
        /// <param name="holdNumbers">List of holds to release</param>
        /// <param name="err">Error message</param>
        /// <returns>True if successful</returns>
        abstract public bool UnholdLot(Lot inLot, string reason, string comment, List<string> holdNumbers, out string err);

        /// <summary>
        /// Run adhoc rework for the entire lot
        /// </summary>
        /// <param name="inLot">Lot to rework</param>
        /// <param name="identifiedReworks">List of identified materials to be reworked with corresponding rework reasons </param>
        /// <param name="unIdentifiedReworks">List of unidentified materials to be reworked with corresponding rework reasons</param>
        /// <param name="recoveryProcedure">Procedure stack containing the recovery procedure (cannot be empty?)</param>
        /// <param name="comment">Comment to add to the lot</param>
        /// <param name="err">Error Message</param>
        /// <returns>True if successful</returns>
        abstract public bool ReworkLot(Lot inLot, List<string[]> identifiedReworks, List<string[]> unIdentifiedReworks, string recoveryProcedure, string comment, out string err);
        /// <summary>
        /// Run special branch rework for the entire lot
        /// </summary>
        /// <param name="inLot">Lot to rework</param>
        /// <param name="identifiedReworks">List of identified materials to be reworked with corresponding rework reasons </param>
        /// <param name="unIdentifiedReworks">List of unidentified materials to be reworked with corresponding rework reasons</param>
        /// <param name="recoveryProcedure">Procedure stack containing the recovery procedure (cannot be empty?)</param>
        /// <param name="comment">Comment to add to the lot</param>
        /// <param name="reworkRunLot">Bool flag (T or F) whether the transaction should recover if the lot is running</param>
        /// <param name="removeFutureAction">Boolean flag. If it is set to T, the command removes all scheduled future actions before starting the recovery. If it is set to F (the default), the command fails if any future actions are scheduled.</param>
        /// <param name="startProcStack">A procedure stack that represents the first instruction of the recovery procedure.</param>
        /// <param name="finishProcStack">A procedure stack that represents the last instruction of the recovery procedure</param>
        /// <param name="resumeProcStack">A procedure stack that represents the starting instruction after the recovery is finished.</param>
        /// <param name="err">Error Message</param>
        /// <returns>True if successful</returns>
        abstract public bool ReworkLot(Lot inLot, List<string[]> identifiedReworks, List<string[]> unIdentifiedReworks, string recoveryProcedure, string comment, string reworkRunLot, 
            string removeFutureAction, string startProcStack, string finishProcStack, string resumeProcStack, out string err);

        #region TMS functions

        /// <summary>
        /// Deletes a TMS task
        /// </summary>
        /// <param name="taskName">Task name</param>
        /// <param name="err">Error message</param>
        /// <returns>True if successful</returns>
        abstract public bool DeleteTMSTask(string taskName, out string err);

        /// <summary>
        /// Starts a TMS task off
        /// </summary>
        /// <param name="taskName">Task name</param>
        /// <param name="equipID">Equipment id that the task should run on</param>
        /// <param name="changeChild">Set "T" to also start off any child tasks</param>
        /// <param name="predictedAvaliableTime">Predicted time that the task will be run (optional)</param>
        /// <param name="comment">Aditional comments</param>
        /// <param name="err">Error message</param>
        /// <returns>True if successful</returns>
        abstract public bool StartTMSTask(string taskName, string equipID, string changeChild, string predictedAvaliableTime, string comment, out string err);

        /// <summary>
        /// Performs a TMS task
        /// </summary>
        /// <param name="taskName">Task name</param>
        /// <param name="equipID">Equipment that the task is running on</param>
        /// <param name="stepNum">Step of task that has been completed. If a 
        /// DC op exists within this range for which no data 
        /// has been entered, the range of steps is considered 
        /// complete only up to this DC op.</param>
        /// <param name="predictedAvaliableTime">The estimated time of task completion and therefore equipment availability</param>
        /// <param name="comment">Additional comment</param>
        /// <param name="err">Error message</param>
        /// <returns>True if successful</returns>
        abstract public bool PerformTMSTask(string taskName, string equipID, string stepNum, string predictedAvaliableTime, string comment, out string err);

        /// <summary>
        /// Finish a TMS task 
        /// </summary>
        /// <param name="taskName">Task name</param>
        /// <param name="equipID">Equipment the task is running on</param>
        /// <param name="changeChild">Determines if the status change should be propagated to child equipment units (optional)</param>
        /// <param name="completeCascTask">Set to Y if you want to finish all cascaded tasks. The default is N (optional)</param>
        /// <param name="comment">Additional comments</param>
        /// <param name="err">Error message</param>
        /// <returns>True if successful</returns>
        abstract public bool FinishTMSTask(string taskName, string equipID, string changeChild, string completeCascTask, string comment, out string err);

        /// <summary>
        /// This command lists generic tasks and any associated scheduled tasks. You can filter them by equipment ID.
        /// </summary>
        /// <param name="endTask">Ending task name (optional)</param>
        /// <param name="startTask">Starting task name</param>
        /// <param name="equipId">Equipment to query(optional</param>
        /// <param name="nextGtsk">Next generic task to read (usually 0)</param>
        /// <param name="nextSchd">Next scheduled task to read(usually 0)</param>
        /// <param name="nextReadFlag">The control flag for the read streams(usually 00)</param>
        /// <param name="ops">List of operations for the tasks</param>
        /// <param name="nextSched">Next scheduled date</param>
        /// <param name="early">Seconds early the task can be</param>
        /// <param name="late">Seconds late the task can be</param>
        /// <param name="isLate">True if task is late</param>
        /// <param name="err">Error message</param>
        /// <returns>True if successful</returns>
        abstract public bool QueryTMSTask(string endTask, string startTask, string equipId, string nextGtsk, string nextSchd, string nextReadFlag
            , out List<Lot.Operations> ops, out string nextSched, out string early, out string late, out string isLate, out string err);

        /// <summary>
        /// Run data entry on a tms task
        /// </summary>
        /// <param name="taskName">Task name</param>
        /// <param name="equipId">Equipmentg the task is running on</param>
        /// <param name="stepNumber">Step in the task that the data entry is for</param>
        /// <param name="data">Data to add to tms</param>
        /// <param name="comment">Additional comments</param>
        /// <param name="err">Error message</param>
        /// <returns>True if successful</returns>
        abstract public bool EnterTMSData(string taskName, string equipId, string stepNumber, List<string> data, string comment, out string err);

        #endregion

        #region Old

        /// <summary>
        /// Legacy initilisation to allow for PROMIS to control the log file
        /// </summary>
        /// <param name="UserID">Promis user name</param>
        /// <param name="Password">PROMIS password</param>
        /// <param name="LogFile">Logfile location</param>
        /// <returns>True if successful</returns>
        abstract public bool Init(string UserID, string Password, string LogFile);

        #region Get Lots

        /// <summary>
        /// Get lots based on query built in the abstact class
        /// </summary>
        /// <param name="query">Query strings</param>
        /// <param name="SelectedLots">Lots to return</param>
        /// <param name="futureStepQuantity">Number of steps to look ahead</param>
        /// <param name="percent">Number of completed lot queries as a percentage</param>
        /// <returns>True if successful</returns>
        /// <remarks>Depreciated</remarks>
        protected abstract bool GetLots(string query, out Lot[] SelectedLots, string futureStepQuantity, out int percent);
        /// <summary>
        /// Get all Lots
        /// </summary>
        /// <param name="location">Location to look at</param>
        /// <param name="ArrayOfLots">List of lot objects</param>
        /// <param name="futureStepQuantity">Number of steps to look ahead</param>
        /// <param name="percent">Number of completed lot queries as a percentage</param>
        /// <returns>True if successful</returns>
        public bool GetLotsAll(string location, out Lot[] ArrayOfLots, string futureStepQuantity, out int percent)
        {
            percent = 0;
            try
            {
                ArrayOfLots = null;
                bool test = false;
                test = GetLots("LOCATION = '" + location.ToUpper() + "'", out ArrayOfLots, futureStepQuantity, out percent);
                return test;
                //GetWIPlots(username, password, "LOCATION = '" + location.ToUpper() + "'");
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                ArrayOfLots = null;
                return false;
            }
        }
        /// <summary>
        /// Get all waiting lots
        /// </summary>
        /// <param name="location">Location to look at</param>
        /// <param name="ArrayOfLots">List of lot objects</param>
        /// <param name="futureStepQuantity">Number of steps to look ahead</param>
        /// <param name="percent">Number of completed lot queries as a percentage</param>
        /// <returns>True if successful</returns>
        public bool GetLotsWaiting(string location, out Lot[] ArrayOfLots, string futureStepQuantity, out int percent)
        {
            ArrayOfLots = null;
            bool test = false;
            test = GetLots("LOCATION = '" + location.ToUpper() + "' AND STATE='WAIT'", out ArrayOfLots, futureStepQuantity, out percent);
            return test;
            //return GetWIPlots(username, password, "LOCATION = '" + location.ToUpper() + "' AND STATE='WAIT'");
        }
        /// <summary>
        /// Get all running lots
        /// </summary>
        /// <param name="location">Location to look at</param>
        /// <param name="ArrayOfLots">List of lot objects</param>
        /// <param name="futureStepQuantity">Number of steps to look ahead</param>
        /// <param name="percent">Number of completed lot queries as a percentage</param>
        /// <returns>True if successful</returns>
        public bool GetLotsRunning(string location, out Lot[] ArrayOfLots, string futureStepQuantity, out int percent)
        {
            ArrayOfLots = null;
            bool test = false;
            test = GetLots("LOCATION = '" + location.ToUpper() + "' AND STATE='RUN'", out ArrayOfLots, futureStepQuantity, out percent);
            return test;
            //return GetWIPlots(username, password, "LOCATION = '" + location.ToUpper() + "' AND STATE='RUN'");
        }
        /// <summary>
        /// Get lots running at a specific stage
        /// </summary>
        /// <param name="location">Location to look at</param>
        /// <param name="stage">Stage to find lots at</param>
        /// <param name="ArrayOfLots">List of lot objects</param>
        /// <param name="futureStepQuantity">Number of steps to look ahead</param>
        /// <param name="percent">Number of completed lot queries as a percentage</param>
        /// <returns>True if successful</returns>
        public bool GetLotsAtStage(string location, string stage, out Lot[] ArrayOfLots, string futureStepQuantity, out int percent)
        {
            ArrayOfLots = null;
            bool test = false;
            test = GetLots("LOCATION = '" + location.ToUpper() + "' AND STAGE='" + stage + "'", out ArrayOfLots, futureStepQuantity, out percent);
            return test;
            //return GetWIPlots(username, password, "LOCATION = '" + location.ToUpper() + "' AND STAGE='" + stage + "'");
        }

        /// <summary>
        /// Get lots based on query built in the abstact class
        /// </summary>
        /// <param name="query">Query string</param>
        /// <param name="SelectedLots">Lots to return</param>
        /// <returns>True if successful</returns>
        /// <remarks>Depreciated</remarks>
        protected abstract bool GetLots(string query, out Lot[] SelectedLots);
        /// <summary>
        /// Get all lots
        /// </summary>
        /// <param name="location">Location to look at</param>
        /// <param name="ArrayOfLots">List of lot objects</param>
        /// <returns>True if successful</returns>
        public bool GetLotsAll(string location, out Lot[] ArrayOfLots)
        {
            try
            {
                ArrayOfLots = null;
                bool test = false;
                test = GetLots("LOCATION = '" + location.ToUpper() + "'", out ArrayOfLots);
                return test;
                //GetWIPlots(username, password, "LOCATION = '" + location.ToUpper() + "'");
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                ArrayOfLots = null;
                return false;
            }
        }
        /// <summary>
        /// Get waiting lots
        /// </summary>
        /// <param name="location">Location to look at</param>
        /// <param name="ArrayOfLots">List of lot objects</param>
        /// <returns>True if successful</returns>
        public bool GetLotsWaiting(string location, out Lot[] ArrayOfLots)
        {
            ArrayOfLots = null;
            bool test = false;
            test = GetLots("LOCATION = '" + location.ToUpper() + "' AND STATE='WAIT'", out ArrayOfLots);
            return test;
            //return GetWIPlots(username, password, "LOCATION = '" + location.ToUpper() + "' AND STATE='WAIT'");
        }

        /// <summary>
        /// Get running lots
        /// </summary>
        /// <param name="location">Location to look at</param>
        /// <param name="ArrayOfLots">List of lot objects</param>
        /// <returns>True if successful</returns>
        public bool GetLotsRunning(string location, out Lot[] ArrayOfLots)
        {
            ArrayOfLots = null;
            bool test = false;
            test = GetLots("LOCATION = '" + location.ToUpper() + "' AND STATE='RUN'", out ArrayOfLots);
            return test;
            //return GetWIPlots(username, password, "LOCATION = '" + location.ToUpper() + "' AND STATE='RUN'");
        }
        /// <summary>
        /// Get lots that are in a location at a certain stage
        /// </summary>
        /// <param name="location">Location to look at</param>
        /// <param name="stage">Stage we want</param>
        /// <param name="ArrayOfLots">List of lot objects</param>
        /// <returns>True if successful</returns>
        public bool GetLotsAtStage(string location, string stage, out Lot[] ArrayOfLots)
        {
            ArrayOfLots = null;
            bool test = false;
   
            test = GetLots("LOCATION = '" + location.ToUpper() + "' AND STAGE='" + stage + "'", out ArrayOfLots);
 
            return test;
            //return GetWIPlots(username, password, "LOCATION = '" + location.ToUpper() + "' AND STAGE='" + stage + "'");
        }

        #endregion

        #region LabelPrinter
        /// <summary>
        /// Massive Lot query function
        /// </summary>
        /// <param name="lotID">Lot ID</param>
        /// <returns>true if successful</returns>
        /// <remarks>Depricated, only for use with Label printer</remarks>
        abstract public bool GetPromiseLot(string lotID);
        /// <summary>
        /// Returns DCOP information
        /// </summary>
        /// <param name="username">Promis username</param>
        /// <param name="password">PROMIS password</param>
        /// <param name="lotID">Lot ID</param>
        /// <param name="dcOpname">DCOP ID</param>
        /// <returns>True if successful</returns>
        /// <remarks>Not used, not even sure if it works</remarks>
        abstract public bool GetDCOP(string username, string password, string lotID, string dcOpname);
        /// <summary>
        /// List of lot information
        /// </summary>
        /// <returns>All lot information collected</returns>
        abstract public ArrayList GetLabels();
        #endregion

        #region LotStart

        /// <summary>
        /// Schedules a new Lot
        /// </summary>
        /// <param name="LotPartNumber">Lot Part</param>
        /// <param name="LotType">Production or engineering lot</param>
        /// <param name="Priority">Lot Priority</param>
        /// <param name="Comment">Overall lot comment</param>
        /// <param name="StartSize">Number of wafer in Lot</param>
        /// <param name="StartDate">Lot start date</param>
        /// <param name="EndDate">Expected finish date</param>
        /// <param name="Cr1">Customer reference 1</param>
        /// <param name="Cr2">Customer reference 2</param>
        /// <param name="Cr3">Customer reference 3</param>
        /// <param name="CustomerName">Customer name</param>
        /// <param name="CustomerOrderNo">Customer order number</param>
        /// <param name="Engineer">Part managing engineer</param>
        /// <param name="NoOfLots">Nuber of lots with these specifications to start</param>
        /// <param name="LotNumber">Id of the new lot</param>
        /// <param name="Source">Source lot</param>
        /// <returns>True if successful</returns>
        abstract public bool ScheduleLot(string LotPartNumber, string LotType, string Priority, string Comment, string StartSize, string StartDate,
            string EndDate, string Cr1, string Cr2, string Cr3, string CustomerName, string CustomerOrderNo, string Engineer, string NoOfLots,
            string LotNumber, string Source);
        /// <summary>
        /// Start a scheduled Lot
        /// </summary>
        /// <param name="LotNumber">Lot ID to start</param>
        /// <param name="mats">List of Part ids of the consumed wafers</param>
        /// <param name="lots">List of lot ids that the consumed wafers come from</param>
        /// <param name="ids">List of consumed wafer ids</param>
        /// <param name="letters">List of letters to identify the wafers on the first step</param>
        /// <param name="TrkIn">Equipment to track into(usually CINPGROW)</param>
        /// <param name="TrkOut">Equipment to track out to (not used)</param>
        /// <returns>True if successful</returns>
        abstract public bool StartLot(string LotNumber, string[] mats, string[] lots, string[] ids, string[] letters, string TrkIn, string TrkOut);
        /// <summary>
        /// Add the lettes to a lot during the first dcop
        /// </summary>
        /// <param name="LotNumber">Lot ID to run dcop on</param>
        /// <param name="ids">List of wafer ids</param>
        /// <param name="letters">Lois of identifying labels</param>
        /// <param name="TrkIn">Location to track into</param>
        /// <param name="TrkOut">Location to track out to (not used)</param>
        /// <remarks>This step only happens on some of </remarks>
        abstract public void AddLetters(string LotNumber, string[] ids, string[] letters, string TrkIn, string TrkOut);
        #endregion

        #region Facet Coat
        /// <summary>
        /// Facet coat stages
        /// </summary>
        public enum Func
        {
            /// <summary>
            /// Front Coat
            /// </summary>
            Stage1Coat,
            /// <summary>
            /// Front test coat
            /// </summary>
            Stage1Test,
            /// <summary>
            /// Results of front test coat
            /// </summary>
            Stage1Results,
            /// <summary>
            /// Back Coat
            /// </summary>
            Stage2Coat,
            /// <summary>
            /// Back test coat
            /// </summary>
            Stage2Test,
            /// <summary>
            /// Results of back test coat
            /// </summary>
            Stage2Results
        };
        /// <summary>
        /// Add the lettes to a lot during the first dcop
        /// </summary>
        /// <param name="LotNumber">Lot ID to run dcop on</param>
        /// <param name="ids">List of wafer ids</param>
        /// <param name="letters">Lois of identifying labels</param>
        /// <remarks>Depricated</remarks>
        abstract public void AddLetters(string LotNumber, string[] ids, string[] letters);

        //abstract protected void PrintInfo(string LotNumber, out LotBasic basicTest, out LotExtended extTest);

        /// <summary>
        /// Run various functions associated with facet coat
        /// </summary>
        /// <param name="Lot">Lot to track</param>
        /// <param name="Ref1">Reference 1 (for DCOP)</param>
        /// <param name="Ref2">Reference 2 (for DCOP)</param>
        /// <param name="Ref3">Reference 3 (for DCOP)</param>
        /// <param name="coating">Coating number (for DCOP)</param>
        /// <param name="facet">Facet front or back (for DCOP)</param>
        /// <param name="held">True or false whether the lot will be held after the dcop (data out of range ect)</param>
        /// <param name="CurrentFunc">Current facetcoat state</param>
        /// <returns>True if successful</returns>
        abstract public bool RunFacetCoat(string Lot, string Ref1, string Ref2, string Ref3, string coating, string facet, bool held, Func CurrentFunc);


        #endregion

        #region BatchViewer
        /// <summary>
        /// Update progress percentage
        /// </summary>
        public abstract double ProgressPercent { get; set; }
        /// <summary>
        /// Get information about future stages for a lot
        /// </summary>
        /// <param name="lot">Input lot object</param>
        /// <param name="processCount">Future steps to look ahead</param>
        /// <param name="detailLot">Lot object with all the future recipe</param>
        /// <returns>True if successful</returns>
        public abstract bool GetFutureRecp(Lot lot, int processCount, out Lot detailLot);
        #endregion

        /// <summary>
        /// Get information about a given DCOP
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="lotID"></param>
        /// <param name="dcOpname"></param>
        /// <param name="DCOPInfo"></param>
        /// <returns></returns>
        public abstract bool GetDCOP(string username, string password, string lotID, string dcOpname, out Dictionary<string, string> DCOPInfo);

        /// <summary>
        /// Get lots waiting for a piece of equipment
        /// </summary>
        /// <param name="equipment">Equipment to look at</param>
        /// <param name="lots">List of lots</param>
        /// <param name="err">Error message</param>
        /// <returns>True if successful</returns>
        abstract public bool GetLotsAtEquip(string equipment, out List<string> lots, out string err);

        /// <summary>
        /// Get source lots with a specific material
        /// </summary>
        /// <param name="material">Material to find source lots for</param>
        /// <param name="Source">List of source lots</param>
        /// <param name="location">Location to look</param>
        /// <returns>True if successful</returns>
        abstract public bool GetSourceLots(string material, out Dictionary<string, string[]> Source, string location);

        /// <summary>
        /// Get materals that could be consumed by a lot
        /// </summary>
        /// <param name="LotID">Lot ID</param>
        /// <param name="Materials">List of material Ids</param>
        /// <returns>True if successful</returns>
        abstract public bool GetPossibleMaterials(string LotID, out string[] Materials);

        /// <summary>
        /// Get the list of components in a lot
        /// </summary>
        /// <param name="lotID"></param>
        /// <returns>Array of component Ids</returns>
        abstract public string[] GetLotComponentIDs(string lotID);

        /// <summary>
        /// Get the IDs of the source lots for a specific material type in a set location
        /// </summary>
        /// <param name="material"></param>
        /// <param name="sourceLotIDs"></param>
        /// <param name="location"></param>
        /// <param name="filter"></param>
        /// <returns>Array of Lot IDs</returns>
        abstract public bool GetSourceLotIDs(string material, ref string[] sourceLotIDs, string location, string filter);


        #endregion
    }
}
