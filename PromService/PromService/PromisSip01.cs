﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.IO;
using Oclaro.PromService.CasSip01;


namespace Oclaro.PromService
{
    /// <summary>
    /// Implimentation of the PROMIS abstract class for the cas-sip-01 XMLGateway instance
    /// </summary>
    public class PromisSip01: Promis
    {
        #region Copy area
        /// <summary>
        /// 
        /// </summary>
        public class PromisLotSupportData
        {
            /// <summary>
            /// List of commands for lot
            /// </summary>
            public CommandListRow[] CmdList = null;
            /// <summary>
            /// List of equipment the lot can be tracked into
            /// </summary>
            public LotInfoEqpAvailRow[] AvailEqpt = null;
            /// <summary>
            /// Dcops associted with lot at current step
            /// </summary>
            public ActlTestRow[] DCOps = null;
            /// <summary>
            /// List of components currently in lot
            /// </summary>
            public ActlComponentsRow[] ComponentsList = null;
            /// <summary>
            /// List of component rejects
            /// </summary>
            public ActlRejectsRow[] Rejects = null;
            /// <summary>
            /// List of lot oerations
            /// </summary>
            public LotInfoOperationsRow[] LotOps = null;
            /// <summary>
            /// History events at current step
            /// </summary>
            public HistEventsRow[] HistoryEvents = null;
            /// <summary>
            /// List of notes on lot for current step
            /// </summary>
            public LotInfoStepNotesRow[] StepNotes = null;
            /// <summary>
            /// List of commands allowed
            /// </summary>
            public LotInfoCmdAllowedRow[] CmdAllowed = null;
            /// <summary>
            /// list of future hold information
            /// </summary>
            public LotInfoFutureHoldsRow[] FutureHolds = null;
            /// <summary>
            /// list of component reworks
            /// </summary>
            public HistReworkRow[] Rework = null;
            /// <summary>
            /// List of Lot parameters
            /// </summary>
            public ParmTableRow[] Parameters = null;
            /// <summary>
            /// List of future commands
            /// </summary>
            public FutaSetRow[] FutureSet = null;
            /// <summary>
            /// List of current procedure information
            /// </summary>
            public HistPrcdStackRow[] PrcdStack = null;
            /// <summary>
            /// Current rework information
            /// </summary>
            public ActlCurReworkRow[] CurrentRework = null;
            /// <summary>
            /// Information timer details
            /// </summary>
            public LotInfoTimersRow[] InfoTimers = null;
            /// <summary>
            /// List of component parameters
            /// </summary>
            public LotInfoCompParmsRow[] ComponentParams = null;
            /// <summary>
            /// Current hold information
            /// </summary>
            public LotInfoCurrentHoldsRow[] CurrentHolds = null;
            /// <summary>
            /// Bin information
            /// </summary>
            public LotInfoBinsRow[] Bins = null;
            /// <summary>
            /// Current stage information
            /// </summary>
            public PstaRow[] Stage = null;
        }

        PromisLotSupportData SupportInfo = null;
        CTestFullPromisService mPromiseManager = null;
        string mUserId = null;
        string mPassword = null;
        InfoTextRow[] mInfo = null;
        string mCurrentLot = "";

        /// <summary>
        /// Convert a long string into a PROMIS compliant comment object
        /// </summary>
        /// <param name="comment">Comment to convert</param>
        /// <returns>PROMIS comment array</returns>
        private CommentListRow[] ConvertComment(string comment)
        {
            try
            {
                if (comment == null)
                    return null;

                if (comment.Length > 730)
                    comment = comment.Remove(730);
                if (!string.IsNullOrEmpty(comment))
                {
                    List<string> comm = Utilities.SplitLines(comment, 50);
                    CommentListRow[] promComment = null;
                    if (comm.Count > 0)
                    {
                        promComment = new CommentListRow[comm.Count];

                        for (int i = 0; i < promComment.Length; i++)
                        {
                            promComment[i] = new CommentListRow();
                            promComment[i].comment = comm[i];
                        }
                    }
                    return promComment;
                }
                else
                {
                    return null;
                }
            }
            catch
            { return null; }

        }

        /// <summary>
        /// Initilises the Promis Object and stores the username and password to be used later
        /// </summary>
        /// <param name="UserID">PROMIS TP username</param>
        /// <param name="Password">PROMIS TP password</param>
        /// <returns>True if successful</returns>
        override public bool Init(string UserID, string Password)
        {
            SupportInfo = new PromisLotSupportData();
            mPromiseManager = new CTestFullPromisService();
            mUserId = UserID;
            mPassword = Password;

            return true;
        }

        /// <summary>
        /// Returns a lot full of information based on a Lot ID, can take a while to call but is the go-to for lot information
        /// </summary>
        /// <param name="LotID">Lot Id</param>
        /// <param name="lot">Lot object full of information</param>
        /// <returns>True if successful</returns>
        public override bool GetLotDetails(string LotID, out Lot lot)
        {
            LotBasic basic;
            LotExtended extend;
            PromisLotSupportData Other;
            LotQuery_HistListByStepOutputElementRow[] Hist;


            lot = new Lot(LotID);
            try
            {
                PrintInfo(LotID, out basic, out extend, out Other, out Hist);
                //rintInfo(LotID, out basic, out extend, out Other);
                lot.mMaterial.CurrentQuantity = basic.curmainqty;
                lot.mMaterial.MaterialType = basic.mainMatTypeExtern;
                lot.mMaterial.PartID = basic.partid;
                lot.mMaterial.Owner = basic.engineer;
                lot.mEquipment.EquipmentID = basic.eqpid;
                lot.mEquipment.EquipmentType = basic.eqpType;
                lot.mProcedure.Stage = basic.stage;
                lot.mState.Location = basic.location;
                lot.mState.CurrentState = basic.stateExtern;
                lot.mState.HoldReason = extend.holdreas;
                lot.mSchedule.Priority = basic.priority.Trim();
                lot.mSchedule.ReqDate = extend.reqdtime.Trim();
                lot.mSchedule.LastChange = extend.lastevtime.Trim();
                lot.mSchedule.LotType = basic.lottype.Trim();
                lot.mProcedure.RecipeID = basic.recpid;
                lot.mProcedure.RecipeTitle = basic.recpTitle;
                lot.mProcedure.AltRecipe = basic.altRecpid;
                try
                {
                    lot.mProcedure.CurrentProcedure = basic.curprcdid.Remove(basic.curprcdid.IndexOf('.'));
                }
                catch { }
                lot.mProcedure.CurrentProcedureStage = basic.curprcdcurinstnum;
                #region Avaliable Equipment
                if (Other.AvailEqpt != null)
                {
                    foreach (LotInfoEqpAvailRow Equip in Other.AvailEqpt)
                    {
                        //if (Equip.constOK == "F")
                        //{
                        //    try
                        //    {
                        //        LotIdListRow[] TmpLot = new LotIdListRow[1];
                        //        TmpLot[0] = new LotIdListRow();
                        //        TmpLot[0].lotId = LotID;
                        //        Cexp ig1;
                        //        TestResultsRow[] ig2;
                        //        Eqps tmp = mPromiseManager.eqpConst_Test(mUserId, mPassword, Equip.eqpAvail, null
                        //            , TmpLot, "EQPUNIT", Equip.eqpAvail, null, null, "AC", "ACTLFLD('EMPIDTRACKIN') NOT IN ('61AKG', '61CDS')",out ig1, out ig2);
                        //        string test = ig1.actionType;
                        //    }
                        //    catch (Exception ex) { }
                        //}
                        Lot.EquipmentDet tmpEq = new Lot.EquipmentDet();
                        tmpEq.ID = Equip.eqpAvail;
                        tmpEq.Capability = Equip.capability;
                        tmpEq.Location = Equip.eqpLoc;
                        lot.mEquipment.AvaliableEquipment.Add(tmpEq);
                    }
                }

                #endregion
                #region Mask;
                try
                {
                    string removeType = basic.partid;
                    string[] splitter = removeType.Split('-');
                    string mask = splitter[0] + "-OR" + splitter[2];
                    splitter = mask.Split('.');
                    mask = splitter[0];
                    if (mask[mask.Length - 1].ToString().ToUpper() == "X")
                        mask = mask.Remove(mask.Length - 1);
                    lot.mMaterial.Mask = mask;
                    splitter = mask.Split('-');
                    lot.mMaterial.MaskShort = splitter[1];
                }
                catch
                {
                    lot.mMaterial.Mask = "No Mask";
                    lot.mMaterial.MaskShort = "OR3xx";
                }
                #endregion
                #region LotOps
                if (Other.LotOps != null)
                {
                    foreach (LotInfoOperationsRow op in Other.LotOps)
                    {
                        Lot.Operations tmpOp = new Lot.Operations();
                        tmpOp.ID = op.opId;
                        tmpOp.Desc = op.opDesc;
                        tmpOp.Type = op.opType;
                        lot.mProcedure.Ops.Add(tmpOp);
                    }
                }
                #endregion
                #region HistoryEvents
                if (Other.HistoryEvents != null)
                {
                    foreach (HistEventsRow hist in Other.HistoryEvents)
                    {
                        Lot.History tmpHist = new Lot.History();
                        tmpHist.Desc = hist.evVariant;
                        tmpHist.User = hist.evUser;
                        tmpHist.Type = hist.evTypeExtern;
                        lot.mState.HistoryEvents.Add(tmpHist);
                    }
                }
                #endregion
                #region ComponentsList
                if (Other.ComponentsList != null)
                {
                    foreach (ActlComponentsRow comp in Other.ComponentsList)
                    {
                        Lot.Component tmpComp = new Lot.Component();
                        tmpComp.ID = comp.compids;
                        tmpComp.State = comp.compstateExtern;
                        tmpComp.ControlType = comp.controltype;
                        lot.mMaterial.Components.Add(tmpComp);
                    }
                }
                #endregion
                #region PastRecipies
                if (Hist != null)
                {
                    foreach (LotQuery_HistListByStepOutputElementRow thisHist in Hist)
                    {
                        Lot.Recipe tmpPast = new Lot.Recipe();
                        tmpPast.ID = thisHist.recp.recpId;
                        tmpPast.Title = thisHist.recp.recpTitle;
                        tmpPast.Location = thisHist.recp.capability;
                        tmpPast.Procedure = thisHist.hist.curprcdid.Replace(" ", "");
                        tmpPast.Operations = new List<Lot.Operations>();
                        for (int i = 0; i < thisHist.recpOperations.Length; i++)
                        {
                            Lot.Operations tmpOp = new Lot.Operations();
                            tmpOp.ID = thisHist.recpOperations[i].opids;
                            tmpOp.Type = thisHist.recpOperations[i].optypes;
                            tmpOp.Desc = thisHist.recpOperdesc[i].opdescs;
                            tmpPast.Operations.Add(tmpOp);
                        }

                        lot.mProcedure.PastRecipe.Add(tmpPast);
                    }
                }
                #endregion
                #region FutureRecipes
                try
                {
                    RecpSetRow[] ig16;
                    MitmSetRow[] ig18;
                    MatrSetRow[] ig19;
                    PiarSetRow[] ig20;
                    FutaSetRow[] ig21;
                    FutaMapRow[] ig22;
                    LotInfoStepCmdAllowedRow[] ig23;
                    InfoTextRow[] ig24;
                    ActlSetRow[] temp = mPromiseManager.lotQuery_Multistep(mUserId, mPassword, LotID, "10", null, out ig16, out ig18, out ig19, out ig20, out ig21, out ig22, out ig23, out ig24);
                    foreach (ActlSetRow fut in temp)
                    {
                        //mPromiseManager.pro
                        Lot.Recipe tmpFut = new Lot.Recipe();
                        tmpFut.ID = fut.recpid;
                        tmpFut.Location = fut.location;
                        foreach (RecpSetRow recp in ig16)
                        {
                            if (fut.recpid == recp.recpId)
                            {
                                tmpFut.Capability = recp.capability;
                                tmpFut.Title = recp.title;
                            }
                        }
                        //tmpFut.Description = fut.curprcdname;
                        tmpFut.Procedure = fut.curPrcdId;
                        lot.mProcedure.FutureRecipe.Add(tmpFut);
                    }
                }
                catch
                {

                }
                #endregion
                #region Step notes
                if (Other.StepNotes != null)
                {
                    foreach (LotInfoStepNotesRow step in Other.StepNotes)
                    {
                        lot.mProcedure.Notes.Add(step.noteLine);
                    }
                }
                #endregion
                #region Comments
                if (Hist != null)
                {
                    foreach (LotQuery_HistListByStepOutputElementRow thisHist in Hist)
                    {
                        foreach (HistEventsRow events in thisHist.events)
                        {
                            try
                            {
                                if (events.evType == "COMM")
                                {
                                    lot.mState.Comments.Add("*");
                                    lot.mState.Comments.Add(events.evTime + " - " + events.evUser);
                                    lot.mState.Comments.Add(events.evVariant); ;
                                }
                                if (events.evType == "CONT")
                                {
                                    lot.mState.Comments[lot.mState.Comments.Count - 1] += " " + events.evVariant;
                                }
                            }
                            catch { }
                        }
                    }
                }
                #endregion
                #region Control Components
                try
                {
                    CtlCompInstRow[] controlComps;
                    Actl tmp = mPromiseManager.lotCtlComp_ShowInst(mUserId, mPassword, LotID, null, null, out controlComps);

                    if (controlComps != null)
                    {
                        foreach (CtlCompInstRow comp in controlComps)
                        {
                            Lot.Component tmpControl = new Lot.Component();
                            tmpControl.Action = comp.action;
                            tmpControl.ControlType = comp.controlType;
                            tmpControl.Description = comp.description;
                            tmpControl.Quantity = comp.quantity;
                            lot.mMaterial.ControlComponents.Add(tmpControl);
                        }
                    }
                }
                catch { }
                #endregion
                #region Parameters
                if (Other.Parameters != null)
                {
                    foreach (ParmTableRow param in Other.Parameters)
                    {
                        Lot.Parameters tmpParam = new Lot.Parameters();
                        tmpParam.Name = param.parmName;
                        tmpParam.Type = param.parmType;
                        tmpParam.Value = param.parmVal;
                        lot.mMaterial.Parameters.Add(tmpParam);
                    }
                }

                if (Other.ComponentParams != null)
                {
                    foreach (LotInfoCompParmsRow param in Other.ComponentParams)
                    {
                        Lot.Parameters tmpParam = new Lot.Parameters();
                        tmpParam.Name = param.parmName;
                        tmpParam.Type = param.parmType;
                        tmpParam.Value = param.parmVal;
                        tmpParam.Component = param.compId;
                        tmpParam.Kind = param.parmKind;
                        lot.mMaterial.ComponentParameters.Add(tmpParam);
                    }
                }
                #endregion
                #region DCOPs
                List<Lot.DataCollectionOperations> tmpDCOPs;
                string er = "";
                if (GetDCOpInfo(lot, out tmpDCOPs, out er))
                {
                    lot.mProcedure.DCOps = tmpDCOPs;
                }
                else
                {
                    throw new Exception(er);
                }
                #endregion
                #region Alternate Recpie
                try
                {
                    string ig1, ig2;
                    ig1 = lot.mProcedure.CurrentProcedure;
                    ig2 = basic.curprcdid.Remove(0, basic.curprcdid.IndexOf('.'));

                    ProQuery_PiarListOutputElementRow[] alt = mPromiseManager.proQuery_PiarList(mUserId, mPassword, ig1, ig2, basic.curprcdcurinstnum);

                    Piar boo = alt[0].piar;
                    Recp boo2 = alt[0].recp;



                    PromisLotSupportData SupportOut = new PromisLotSupportData();
                    LotBasic basicTest = mPromiseManager.lotQuery_LotBasic(mUserId, mPassword, LotID, null, boo.altRecpId, null, null, SupportOut.CmdList,
                                        null, out SupportOut.AvailEqpt, out SupportOut.DCOps, out SupportOut.ComponentsList, out SupportOut.Rejects, out SupportOut.LotOps,
                                        out SupportOut.HistoryEvents, out SupportOut.StepNotes, out SupportOut.CmdAllowed, out SupportOut.FutureHolds);

                    lot.mProcedure.AltRecipe = basicTest.recpid;
                    Lot.AltRecipe tmpRecp = new Lot.AltRecipe();
                    tmpRecp.ID = basicTest.recpid;
                    tmpRecp.Description = boo.altRecpTitle;
                    tmpRecp.Capability = boo2.capability;
                    tmpRecp.Procedure = boo.prcdId;
                    tmpRecp.State = basicTest.state;
                    tmpRecp.Stage = basicTest.stage;
                    tmpRecp.Operations = new List<Lot.Operations>();
                    foreach (LotInfoOperationsRow op in SupportOut.LotOps)
                    {
                        Lot.Operations tmpOp = new Lot.Operations();
                        tmpOp.Desc = op.opDesc;
                        tmpOp.ID = op.opId;
                        tmpOp.Type = op.opType;
                        tmpRecp.Operations.Add(tmpOp);
                    }
                    tmpRecp.Equipment = new List<Lot.EquipmentDet>();
                    foreach (LotInfoEqpAvailRow eqp in SupportOut.AvailEqpt)
                    {
                        Lot.EquipmentDet tmpEq = new Lot.EquipmentDet();
                        tmpEq.ID = eqp.eqpAvail;
                        tmpEq.Capability = eqp.capability;
                        tmpEq.Location = eqp.eqpLoc;
                        tmpEq.Type = eqp.constOK;
                        tmpRecp.Equipment.Add(tmpEq);
                    }

                    lot.mProcedure.FullAltRecipe = tmpRecp;

                }
                catch { }
                #endregion

                return true;

            }
            catch (Exception e)
            {
                var message = System.Windows.Forms.MessageBox.Show(e.Message, "Error",
                      System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                return false;
            }
        }

        /// <summary>
        /// Returns a lot full of information based on a Lot ID, can take a while to call but is the go-to for lot information
        /// </summary>
        /// <param name="LotID">Lot Id</param>
        /// <param name="lot">Lot object full of information</param>
        /// <param name="err">String of errors thrown by the function</param>
        /// <returns>True if successful</returns>
        public override bool GetLotDetails(string LotID, out Lot lot, out string err)
        {
            err = "";
            LotBasic basic;
            LotExtended extend;
            PromisLotSupportData Other;
            LotQuery_HistListByStepOutputElementRow[] Hist;

            lot = new Lot(LotID);
            try
            {
                PrintInfo(LotID, out basic, out extend, out Other, out Hist);
                //rintInfo(LotID, out basic, out extend, out Other);
                lot.mMaterial.CurrentQuantity = basic.curmainqty;
                lot.mMaterial.MaterialType = basic.mainMatTypeExtern;
                lot.mMaterial.PartID = basic.partid;
                lot.mMaterial.Owner = basic.engineer;
                lot.mEquipment.EquipmentID = basic.eqpid;
                lot.mEquipment.EquipmentType = basic.eqpType;
                lot.mProcedure.Stage = basic.stage;
                lot.mState.Location = basic.location;
                lot.mState.CurrentState = basic.stateExtern;
                lot.mState.HoldReason = extend.holdreas;
                lot.mSchedule.Priority = basic.priority.Trim();
                lot.mSchedule.ReqDate = extend.reqdtime.Trim();
                lot.mSchedule.LastChange = extend.lastevtime.Trim();
                lot.mSchedule.LotType = basic.lottype.Trim();
                lot.mProcedure.RecipeID = basic.recpid;
                lot.mProcedure.RecipeTitle = basic.recpTitle;
                lot.mProcedure.AltRecipe = basic.altRecpid;
                lot.mProcedure.ReworkAll = extend.lotinfo_reworkallpossible;
                try
                {
                    lot.mProcedure.CurrentProcedure = basic.curprcdid.Remove(basic.curprcdid.IndexOf('.'));
                }
                catch (Exception ex) { err += (ex.Message + "\n"); }
                lot.mProcedure.CurrentProcedureStage = basic.curprcdcurinstnum;
                #region Avaliable Equipment
                if (Other.AvailEqpt != null)
                {
                    foreach (LotInfoEqpAvailRow Equip in Other.AvailEqpt)
                    {
                        //if (Equip.constOK == "F")
                        //{
                        //    try
                        //    {
                        //        LotIdListRow[] TmpLot = new LotIdListRow[1];
                        //        TmpLot[0] = new LotIdListRow();
                        //        TmpLot[0].lotId = LotID;
                        //        Cexp ig1;
                        //        TestResultsRow[] ig2;
                        //        Eqps tmp = mPromiseManager.eqpConst_Test(mUserId, mPassword, Equip.eqpAvail, null
                        //            , TmpLot, "EQPUNIT", Equip.eqpAvail, null, null, "AC", "ACTLFLD('EMPIDTRACKIN') NOT IN ('61AKG', '61CDS')",out ig1, out ig2);
                        //        string test = ig1.actionType;
                        //    }
                        //    catch (Exception ex) { }
                        //}
                        Lot.EquipmentDet tmpEq = new Lot.EquipmentDet();
                        tmpEq.ID = Equip.eqpAvail;
                        tmpEq.Capability = Equip.capability;
                        tmpEq.Location = Equip.eqpLoc;
                        lot.mEquipment.AvaliableEquipment.Add(tmpEq);
                    }
                }

                #endregion
                #region Mask;
                try
                {
                    string removeType = basic.partid;
                    string[] splitter = removeType.Split('-');
                    string mask = splitter[0] + "-OR" + splitter[2];
                    splitter = mask.Split('.');
                    mask = splitter[0];
                    if (mask[mask.Length - 1].ToString().ToUpper() == "X")
                        mask = mask.Remove(mask.Length - 1);
                    lot.mMaterial.Mask = mask;
                    splitter = mask.Split('-');
                    lot.mMaterial.MaskShort = splitter[1];
                }
                catch (Exception ex)
                {
                    err += (ex.Message + "\n");
                    lot.mMaterial.Mask = "No Mask";
                    lot.mMaterial.MaskShort = "OR3xx";
                }
                #endregion
                #region LotOps
                if (Other.LotOps != null)
                {
                    foreach (LotInfoOperationsRow op in Other.LotOps)
                    {
                        Lot.Operations tmpOp = new Lot.Operations();
                        tmpOp.ID = op.opId;
                        tmpOp.Desc = op.opDesc;
                        tmpOp.Type = op.opType;
                        lot.mProcedure.Ops.Add(tmpOp);
                    }
                }
                #endregion
                #region HistoryEvents
                if (Other.HistoryEvents != null)
                {
                    foreach (HistEventsRow hist in Other.HistoryEvents)
                    {
                        Lot.History tmpHist = new Lot.History();
                        tmpHist.Desc = hist.evVariant;
                        tmpHist.User = hist.evUser;
                        tmpHist.Type = hist.evTypeExtern;
                        tmpHist.Time = hist.evTime;
                        lot.mState.HistoryEvents.Add(tmpHist);
                    }
                }
                #endregion
                #region ComponentsList
                if (Other.ComponentsList != null)
                {
                    int counter = 0;
                    foreach (ActlComponentsRow comp in Other.ComponentsList)
                    {
                        Lot.Component tmpComp = new Lot.Component();
                        tmpComp.ID = comp.compids;
                        tmpComp.State = comp.compstateExtern;
                        tmpComp.ControlType = comp.controltype;
                        //comp.
                        lot.mMaterial.Components.Add(tmpComp);

                        if (!string.IsNullOrWhiteSpace(comp.controltype) && comp.compstate == "1")
                            counter++;
                    }
                    lot.mMaterial.ControlComponentQuantity = counter.ToString();
                }
                #endregion
                #region PastRecipies
                if (Hist != null)
                {
                    foreach (LotQuery_HistListByStepOutputElementRow thisHist in Hist)
                    {
                        Lot.Recipe tmpPast = new Lot.Recipe();
                        tmpPast.ID = thisHist.recp.recpId;
                        tmpPast.Title = thisHist.recp.recpTitle;
                        tmpPast.Location = thisHist.recp.capability;
                        tmpPast.Procedure = thisHist.hist.curprcdid.Replace(" ", "");
                        tmpPast.Operations = new List<Lot.Operations>();
                        for (int i = 0; i < thisHist.recpOperations.Length; i++)
                        {
                            Lot.Operations tmpOp = new Lot.Operations();
                            tmpOp.ID = thisHist.recpOperations[i].opids;
                            tmpOp.Type = thisHist.recpOperations[i].optypes;
                            tmpOp.Desc = thisHist.recpOperdesc[i].opdescs;
                            tmpPast.Operations.Add(tmpOp);
                        }

                        lot.mProcedure.PastRecipe.Add(tmpPast);
                    }
                }
                #endregion
                #region FutureRecipes
                try
                {
                    RecpSetRow[] ig16;
                    MitmSetRow[] ig18;
                    MatrSetRow[] ig19;
                    PiarSetRow[] ig20;
                    FutaSetRow[] ig21;
                    FutaMapRow[] ig22;
                    LotInfoStepCmdAllowedRow[] ig23;
                    InfoTextRow[] ig24;
                    ActlSetRow[] temp = mPromiseManager.lotQuery_Multistep(mUserId, mPassword, LotID, "10", null, out ig16, out ig18, out ig19, out ig20, out ig21, out ig22, out ig23, out ig24);
                    foreach (ActlSetRow fut in temp)
                    {
                        //mPromiseManager.pro
                        Lot.Recipe tmpFut = new Lot.Recipe();
                        tmpFut.ID = fut.recpid;
                        tmpFut.Location = fut.location;
                        foreach (RecpSetRow recp in ig16)
                        {
                            if (fut.recpid == recp.recpId)
                            {
                                tmpFut.Capability = recp.capability;
                                tmpFut.Title = recp.title;
                            }
                        }
                        //tmpFut.Description = fut.curprcdname;
                        tmpFut.Procedure = fut.curPrcdId;
                        lot.mProcedure.FutureRecipe.Add(tmpFut);
                    }
                }
                catch (Exception ex) { err += (ex.Message + "\n"); }
                #endregion
                #region Step notes
                if (Other.StepNotes != null)
                {
                    foreach (LotInfoStepNotesRow step in Other.StepNotes)
                    {
                        lot.mProcedure.Notes.Add(step.noteLine);
                    }
                }
                #endregion
                #region Comments
                if (Hist != null)
                {
                    foreach (LotQuery_HistListByStepOutputElementRow thisHist in Hist)
                    {
                        foreach (HistEventsRow events in thisHist.events)
                        {
                            try
                            {
                                if (events.evType == "COMM")
                                {
                                    lot.mState.Comments.Add("*");
                                    lot.mState.Comments.Add(events.evTime + " - " + events.evUser);
                                    lot.mState.Comments.Add(events.evVariant); ;
                                }
                                if (events.evType == "CONT")
                                {
                                    lot.mState.Comments[lot.mState.Comments.Count - 1] += " " + events.evVariant;
                                }
                            }
                            catch (Exception ex) { err += (ex.Message + "\n"); }
                        }
                    }
                }
                #endregion
                #region Control Components
                try
                {
                    CtlCompInstRow[] controlComps;
                    Actl tmp = mPromiseManager.lotCtlComp_ShowInst(mUserId, mPassword, LotID, null, null, out controlComps);

                    if (controlComps != null)
                    {
                        foreach (CtlCompInstRow comp in controlComps)
                        {
                            Lot.Component tmpControl = new Lot.Component();
                            tmpControl.Action = comp.action;
                            tmpControl.ControlType = comp.controlType;
                            tmpControl.Description = comp.description;
                            tmpControl.Quantity = comp.quantity;
                            lot.mMaterial.ControlComponents.Add(tmpControl);
                        }
                    }
                }
                catch (Exception ex) { err += (ex.Message + "\n"); }
                #endregion
                #region Parameters
                if (Other.Parameters != null)
                {
                    foreach (ParmTableRow param in Other.Parameters)
                    {
                        Lot.Parameters tmpParam = new Lot.Parameters();
                        tmpParam.Name = param.parmName;
                        tmpParam.Type = param.parmType;
                        tmpParam.Value = param.parmVal;
                        lot.mMaterial.Parameters.Add(tmpParam);
                    }
                }

                if (Other.ComponentParams != null)
                {
                    foreach (LotInfoCompParmsRow param in Other.ComponentParams)
                    {
                        Lot.Parameters tmpParam = new Lot.Parameters();
                        tmpParam.Name = param.parmName;
                        tmpParam.Type = param.parmType;
                        tmpParam.Value = param.parmVal;
                        tmpParam.Component = param.compId;
                        tmpParam.Kind = param.parmKind;
                        lot.mMaterial.ComponentParameters.Add(tmpParam);
                    }
                }
                #endregion
                #region DCOPs
                if (basic.stateExtern == "RUN")
                {
                    List<Lot.DataCollectionOperations> tmpDCOPs;
                    string er = "";
                    if (GetDCOpInfo(lot, out tmpDCOPs, out er))
                    {
                        lot.mProcedure.DCOps = tmpDCOPs;
                    }
                    else
                    {
                        if (er != "")
                            err += er + "\n";
                    }
                }
                #endregion
                #region Alternate Recpie
                try
                {
                    string ig1, ig2;
                    ig1 = lot.mProcedure.CurrentProcedure;
                    ig2 = basic.curprcdid.Remove(0, basic.curprcdid.IndexOf('.'));

                    ProQuery_PiarListOutputElementRow[] alt = mPromiseManager.proQuery_PiarList(mUserId, mPassword, ig1, ig2, basic.curprcdcurinstnum);

                    Piar boo = alt[0].piar;
                    Recp boo2 = alt[0].recp;



                    PromisLotSupportData SupportOut = new PromisLotSupportData();
                    LotBasic basicTest = mPromiseManager.lotQuery_LotBasic(mUserId, mPassword, LotID, null, boo.altRecpId, null, null, SupportOut.CmdList,
                                        null, out SupportOut.AvailEqpt, out SupportOut.DCOps, out SupportOut.ComponentsList, out SupportOut.Rejects, out SupportOut.LotOps,
                                        out SupportOut.HistoryEvents, out SupportOut.StepNotes, out SupportOut.CmdAllowed, out SupportOut.FutureHolds);

                    lot.mProcedure.AltRecipe = basicTest.recpid;
                    Lot.AltRecipe tmpRecp = new Lot.AltRecipe();
                    tmpRecp.ID = basicTest.recpid;
                    tmpRecp.Description = boo.altRecpTitle;
                    tmpRecp.Capability = boo2.capability;
                    tmpRecp.Procedure = boo.prcdId;
                    tmpRecp.State = basicTest.state;
                    tmpRecp.Stage = basicTest.stage;
                    tmpRecp.Operations = new List<Lot.Operations>();
                    foreach (LotInfoOperationsRow op in SupportOut.LotOps)
                    {
                        Lot.Operations tmpOp = new Lot.Operations();
                        tmpOp.Desc = op.opDesc;
                        tmpOp.ID = op.opId;
                        tmpOp.Type = op.opType;
                        tmpRecp.Operations.Add(tmpOp);
                    }
                    tmpRecp.Equipment = new List<Lot.EquipmentDet>();
                    foreach (LotInfoEqpAvailRow eqp in SupportOut.AvailEqpt)
                    {
                        Lot.EquipmentDet tmpEq = new Lot.EquipmentDet();
                        tmpEq.ID = eqp.eqpAvail;
                        tmpEq.Capability = eqp.capability;
                        tmpEq.Location = eqp.eqpLoc;
                        tmpEq.Type = eqp.constOK;
                        tmpRecp.Equipment.Add(tmpEq);
                    }

                    lot.mProcedure.FullAltRecipe = tmpRecp;

                }
                catch { }
                #endregion
                #region Holds
                if (Other.CurrentHolds != null)
                {
                    foreach (LotInfoCurrentHoldsRow hold in Other.CurrentHolds)
                    {
                        Lot.Hold tmpHold = new Lot.Hold();
                        tmpHold.Code = hold.holdCode;
                        tmpHold.HoldNumber = hold.holdNumber;
                        tmpHold.Reason = hold.holdReason;
                        tmpHold.Time = hold.holdTime;
                        tmpHold.Type = hold.holdType;
                        tmpHold.User = hold.holdUser;
                        lot.mState.CurrentHolds.Add(tmpHold);
                    }
                }
                #endregion

                return true;

            }
            catch (Exception e)
            {
                err = e.Message;
                return false;
            }
        }

        /// <summary>
        /// Flat out adds a comment to a lot no matter what state it is in
        /// </summary>
        /// <param name="LotID">Lot ID</param>
        /// <param name="cmnt">Comment to add</param>
        /// <returns>True if successful</returns>
        public override bool AddComment(string LotID, string cmnt)
        {
            CommentListRow[] input = ConvertComment(cmnt);

            if (input == null)
                return false;

            try
            {
                Actl Test = mPromiseManager.lotComment_Add(mUserId, mPassword, LotID, input, out mInfo);
                return true;
            }
            catch (Exception e)
            {
                var message = System.Windows.Forms.MessageBox.Show(e.Message, "Error",
                        System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                return false;
            }

        }

        /// <summary>
        /// Lists all the pieces of equipment in a given area.
        /// </summary>
        /// <param name="Location">Location to look at</param>
        /// <param name="equip">List of equipment ids</param>
        /// <returns>True if successful</returns>
        public override bool GetEquipmentList(string Location, out List<string> equip)
        {
            equip = new List<string>();
            string filter = "LOCATIONID = '" + Location + "'";

            try
            {
                EqpListRow[] promequ = mPromiseManager.getEqpList(mUserId, mPassword, filter, null);

                foreach (EqpListRow ep in promequ)
                {
                    equip.Add(ep.eqpId);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Gets a detailed lot information for all lots running on a piece of equipment
        /// </summary>
        /// <param name="Location">Location of Equipment</param>
        /// <param name="Equip">Equipment ID</param>
        /// <param name="Lots">List of lot objects running on equipment</param>
        /// <returns>True if successful</returns>
        public override bool GetLotsOnEquip(string Location, string Equip, out List<Lot> Lots)
        {
            string filter = "EQPID = '" + Equip + "'";
            Lots = new List<Lot>();

            try
            {
                WipLotsRow[] LotsOfInfo = mPromiseManager.getWipLots(mUserId, mPassword, filter, null);
                foreach (WipLotsRow wip in LotsOfInfo)
                {
                    Lot tmpLot = new Lot(wip.lotId);
                    GetLotDetails(wip.lotId, out tmpLot);
                    Lots.Add(tmpLot);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Gets just the IDs for all lots running on a piece of equipment
        /// </summary>
        /// <param name="Equip">Equipment to look at</param>
        /// <param name="Lots">List of lot ids running on equipment</param>
        /// <returns>True if successful</returns>
        public override bool GetLotsOnEquip(string Equip, out List<string> Lots)
        {
            string filter = "EQPID = '" + Equip + "'";
            Lots = new List<string>();

            try
            {
                WipLotsRow[] LotsOfInfo = mPromiseManager.getWipLots(mUserId, mPassword, filter, null);
                foreach (WipLotsRow wip in LotsOfInfo)
                {
                    Lots.Add(wip.lotId);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Get lots waiting for a piece of equipment
        /// </summary>
        /// <param name="equipment">Equipment to look at</param>
        /// <param name="lots">List of lots</param>
        /// <param name="err">Error message</param>
        /// <returns>True if successful</returns>
        public override bool GetLotsAtEquip(string equipment, out List<string> lots, out string err)
        {
            err = "";
            string firstlot = null;
            lots = new List<string>();
            ActlSetRow[] fromprms;
            try
            {
                mPromiseManager.lotQuery_EqpLotList(mUserId, mPassword, equipment, null, null, out firstlot, out fromprms);
                foreach (ActlSetRow tmp in fromprms)
                {
                    lots.Add(tmp.lotid);
                }
                return true;
            }
            catch (Exception e)
            {
                err = e.Message;
                return false;
            }
        }

        /// <summary>
        /// Dumb tracks a lot into a given piece of equipment.
        /// </summary>
        /// <param name="Lot">Lot to track in</param>
        /// <param name="Equip">Equipment to track it into</param>
        /// <param name="location">Location of equipment</param>
        /// <param name="message">Error message</param>
        /// <returns>True if successful</returns>
        public override bool TrackInLots(Lot Lot, string Equip, string location, out string message)
        {
            message = "";
            string cap = null;
            foreach (Lot.EquipmentDet det in Lot.mEquipment.AvaliableEquipment)
            {
                if (det.ID == Equip)
                {
                    cap = det.Capability;
                    break;
                }
            }
            try
            {
                Recp Ig6;
                TallyTaskArrayRow[] Ig7;
                InfoTextRow[] ig8;
                mPromiseManager.lotTrack_IntoStep(mUserId, mPassword, Lot.ID, Equip, null, null, Lot.mProcedure.RecipeID, null, location, null, out Ig6, out Ig7, out ig8);
                return true;
            }
            catch (Exception e)
            {
                message = e.Message;
                return false;
            }
        }

        /// <summary>
        /// Track in to alternate recpie
        /// </summary>
        /// <param name="inLot">Lot to track in</param>
        /// <param name="Equip">Equipment to track it into</param>
        /// <param name="Recpie">Recipe to track into</param>
        /// <param name="location">Location of equipment</param>
        /// <param name="comment">Additional comments</param>
        /// <param name="message">Error message</param>
        /// <returns>True if successful</returns>
        public override bool TrackInLots(Lot inLot, string Equip, string Recpie, string location, string comment, out string message)
        {
            message = "";
            try
            {
                Recp Ig6;
                TallyTaskArrayRow[] Ig7;
                InfoTextRow[] ig8;
                mPromiseManager.lotTrack_IntoStep(mUserId, mPassword, inLot.ID, Equip, null, null, Recpie, null, location, ConvertComment(comment), out Ig6, out Ig7, out ig8);
                return true;
            }
            catch (Exception ex)
            {
                message = ex.Message;
                return false;
            }
        }

        /// <summary>
        /// Tracks out a lot with given rejects and reworks
        /// </summary>
        /// <param name="Lot"></param>
        /// <param name="inIdentRejects">List of identified rejected components {Comp Id, Rejection catagory}</param>
        /// <param name="inUnidentRejects">List of unidentified rejected components {Rejection catagory, Quantity}</param>
        /// <param name="inIdentReworks">List of idetified reworks (Not implimented)</param>
        /// <param name="inUnidentReworks">List of unidetified reworks (Not implimented)</param>
        /// <param name="comment">Additional comment</param>
        /// <param name="err">Error message</param>
        /// <returns>True if successful</returns>
        public override bool TrackOutLots(Lot Lot, List<string[]> inIdentRejects, List<string[]> inUnidentRejects, List<string[]> inIdentReworks, List<string[]> inUnidentReworks, string comment, out string err)
        {
            err = null;

            CommentListRow[] input = ConvertComment(comment);

            FutaSetRow[] ignore;
            ActlSetRow[] ignore2;
            Recp Ig6;
            SubLot Ig8;
            ActlSetRow[] Ig9;
            ClassOutGoodBinsRow[] ig10;
            ClassOutBadBinsRow[] ig11;
            FutaSetRow[] ig12;
            RecpSetRow[] ig13;
            InfoTextRow[] info;
            IdentRejectListRow[] identRejects = null;
            UnidentRejectListRow[] unidentRejects = null;
            IdentReworkListRow[] identReworks = null;
            UnidentReworkListRow[] unidentReworks = null;

            if (inIdentRejects != null)
            {
                if (inIdentRejects.Count > 0)
                {
                    identRejects = new IdentRejectListRow[inIdentRejects.Count];

                    for (int i = 0; i < identRejects.Length; i++)
                    {
                        identRejects[i] = new IdentRejectListRow();
                        identRejects[i].compId = inIdentRejects[i][0];
                        identRejects[i].rejCat = inUnidentRejects[i][1];
                    }
                }
            }

            if (inUnidentRejects != null)
            {
                if (inUnidentRejects.Count > 0)
                {
                    unidentRejects = new UnidentRejectListRow[inUnidentRejects.Count];
                    for (int i = 0; i < unidentRejects.Length; i++)
                    {
                        unidentRejects[i] = new UnidentRejectListRow();
                        unidentRejects[i].rejCat = inUnidentRejects[i][0];
                        unidentRejects[i].qty = inUnidentRejects[i][1];
                    }
                }
            }

            if (inIdentReworks != null)
            {
                if (inIdentReworks.Count > 0)
                {
                    identReworks = new IdentReworkListRow[inIdentReworks.Count];
                    for (int i = 0; i < identReworks.Length; i++)
                    {
                        identReworks[i] = new IdentReworkListRow();
                        identReworks[i].compId = inIdentReworks[i][0];
                        identReworks[i].rewkCode = inIdentReworks[i][1];
                    }
                }
            }

            if (inUnidentReworks != null)
            {
                if (inUnidentReworks.Count > 0)
                {
                    unidentReworks = new UnidentReworkListRow[inUnidentReworks.Count];
                    for (int i = 0; i < identReworks.Length; i++)
                    {
                        unidentReworks[i] = new UnidentReworkListRow();
                        unidentReworks[i].rewkCode = inIdentReworks[i][0];
                        unidentReworks[i].qty = inIdentReworks[i][1];
                    }
                }
            }

            try
            {
                mPromiseManager.lotTrack_OutOfStep(mUserId, mPassword, Lot.ID, null, Lot.mProcedure.FutureRecipe[0].Location, identRejects, unidentRejects, identReworks, unidentReworks, input, out Ig6, out Ig8, out Ig9, out ig10, out ig11, out ignore, out ig12, out ignore2, out ig13, out info);
                return true;
            }
            catch (Exception e)
            {
                err = e.Message;
                return false;
            }
        }

        /// <summary>
        /// returns a list of DCOP details based on a lot input.
        /// </summary>
        /// <param name="inLot">Lot to get current DCOPs for</param>
        /// <param name="DCOps">DCOP Objects</param>
        /// <param name="err">Error message</param>
        /// <returns>True if successful</returns>
        public override bool GetDCOpInfo(Lot inLot, out List<Lot.DataCollectionOperations> DCOps, out string err)
        {
            err = "";
            DCOps = new List<Lot.DataCollectionOperations>();
            bool found = false;
            int tstCounter = 1;
            for (int i = 0; i < inLot.mProcedure.Ops.Count; i++)
            {
                if (inLot.mProcedure.Ops[i].Type == "T")
                {
                    found = true;
                    Test ig1;
                    CalcSetRow[] ig2;
                    TestAvailCompTableRow[] ig3;
                    TestChrtTableRow[] ig4;
                    TestCompTableRow[] ig5;
                    Recp Ig6;
                    TestItemTableRow[] ig7;
                    TestSiteTableRow[] ig8;
                    TestParmTableRow[] ig9;

                    try
                    {
                        Actl Test = mPromiseManager.tstShow_InputFmt(mUserId, mPassword, inLot.mProcedure.Ops[i].ID, inLot.ID, out Ig6, out ig1, out ig2, out ig3, out ig4, out ig5, out ig7, out ig8, out ig9);
                        //Tres tres = mPromiseManager.tstShow_Data(mUserId, mPassword, inLot.mProcedure.Ops[i].ID, inLot.mSchedule.LastChange, null, null, out ig10, out ig11, out ig12, out ig13, out ig14, out ig15, out ig16, out ig17, out ig18, out ig19, out ig5, out ig7, out ig9, out ig8, out ig3, out ig20, out ig21);
                        if (ig3.Length > 0)
                        {
                            for (int k = 0; k < ig3.Length; k++)
                            {
                                if (ig8.Length > 0)
                                {
                                    for (int l = 0; l < ig8.Length; l++)
                                    {
                                        for (int j = 0; j < ig7.Length; j++)
                                        {
                                            Lot.DataCollectionOperations tmpDc = new Lot.DataCollectionOperations();
                                            tmpDc.Component = ig3[k].compId;
                                            tmpDc.Tested = ig3[k].compTested;
                                            tmpDc.Site = ig8[l].siteId;
                                            tmpDc.SiteNum = (l + 1).ToString();
                                            tmpDc.LowLimit = ig7[j].lowlim;
                                            tmpDc.PickList = ig7[j].picklist.Split('/');
                                            tmpDc.Prompt = ig7[j].prompt;
                                            tmpDc.UpLimit = ig7[j].UprLim;
                                            tmpDc.Units = ig7[j].units;
                                            tmpDc.TestNumber = tstCounter.ToString();
                                            DCOps.Add(tmpDc);
                                        }
                                    }
                                }
                                else
                                {
                                    for (int j = 0; j < ig7.Length; j++)
                                    {
                                        Lot.DataCollectionOperations tmpDc = new Lot.DataCollectionOperations();
                                        tmpDc.Component = ig3[k].compId;
                                        tmpDc.Tested = ig3[k].compTested;
                                        tmpDc.LowLimit = ig7[j].lowlim;
                                        tmpDc.PickList = ig7[j].picklist.Split('/');
                                        tmpDc.Prompt = ig7[j].prompt;
                                        tmpDc.UpLimit = ig7[j].UprLim;
                                        tmpDc.Units = ig7[j].units;
                                        tmpDc.TestNumber = tstCounter.ToString();
                                        DCOps.Add(tmpDc);
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (ig8.Length > 0)
                            {
                                for (int l = 0; l < ig8.Length; l++)
                                {
                                    for (int j = 0; j < ig7.Length; j++)
                                    {
                                        Lot.DataCollectionOperations tmpDc = new Lot.DataCollectionOperations();
                                        tmpDc.Site = ig8[l].siteId;
                                        tmpDc.SiteNum = (l + 1).ToString();
                                        tmpDc.LowLimit = ig7[j].lowlim;
                                        tmpDc.PickList = ig7[j].picklist.Split('/');
                                        tmpDc.Prompt = ig7[j].prompt;
                                        tmpDc.UpLimit = ig7[j].UprLim;
                                        tmpDc.Units = ig7[j].units;
                                        tmpDc.TestNumber = tstCounter.ToString();
                                        DCOps.Add(tmpDc);
                                    }
                                }
                            }
                            else
                            {
                                for (int j = 0; j < ig7.Length; j++)
                                {
                                    Lot.DataCollectionOperations tmpDc = new Lot.DataCollectionOperations();
                                    tmpDc.LowLimit = ig7[j].lowlim;
                                    tmpDc.PickList = ig7[j].picklist.Split('/');
                                    tmpDc.Prompt = ig7[j].prompt;
                                    tmpDc.UpLimit = ig7[j].UprLim;
                                    tmpDc.Units = ig7[j].units;
                                    tmpDc.TestNumber = tstCounter.ToString();
                                    DCOps.Add(tmpDc);
                                }
                            }
                        }


                        found = true;
                    }
                    catch (Exception e)
                    {
                        err = e.Message;
                        return false;
                    }

                    tstCounter++;
                }
            }
            return found;
        }

        /// <summary>
        /// Returns the DCOP details based on a operation ID
        /// </summary>
        /// <param name="opID">Operation ID</param>
        /// <param name="DCOps">Dcop Objects</param>
        /// <param name="err">Error Message</param>
        /// <returns>True if successful</returns>
        public override bool GetDCOpInfo(string opID, out List<Lot.DataCollectionOperations> DCOps, out string err)
        {
            err = "";
            int testCounter = 1;
            DCOps = new List<Lot.DataCollectionOperations>();
            Test ig1;
            CalcSetRow[] ig2;
            TestAvailCompTableRow[] ig3;
            TestChrtTableRow[] ig4;
            TestCompTableRow[] ig5;
            Recp Ig6;
            TestItemTableRow[] ig7;
            TestSiteTableRow[] ig8;
            TestParmTableRow[] ig9;

            try
            {
                Actl Test = mPromiseManager.tstShow_InputFmt(mUserId, mPassword, opID, null
                    , out Ig6, out ig1, out ig2, out ig3, out ig4, out ig5, out ig7, out ig8, out ig9);
                //Tres tres = mPromiseManager.tstShow_Data(mUserId, mPassword, inLot.mProcedure.Ops[i].ID, inLot.mSchedule.LastChange, null, null, out ig10, out ig11, out ig12, out ig13, out ig14, out ig15, out ig16, out ig17, out ig18, out ig19, out ig5, out ig7, out ig9, out ig8, out ig3, out ig20, out ig21);

                foreach (TestItemTableRow test in ig7)
                {
                    Lot.DataCollectionOperations tmpDC = new Lot.DataCollectionOperations();
                    tmpDC.Prompt = test.prompt;
                    tmpDC.LowLimit = test.lowlim;
                    tmpDC.UpLimit = test.UprLim;
                    tmpDC.PickList = test.picklist.Split('/');
                    tmpDC.TestNumber = testCounter.ToString();
                    DCOps.Add(tmpDC);
                    testCounter++;
                }
                return true;
            }
            catch (Exception e)
            {
                err = e.Message;
                return false;
            }


        }

        /// <summary>
        /// Aborts a running lot instantly with the ability to add a comment
        /// </summary>
        /// <param name="inlot">Lot to abort step</param>
        /// <param name="Comment">Comment for </param>
        /// <returns>True if successful</returns>
        public override bool AbortStep(Lot inlot, string Comment)
        {
            CommentListRow[] input = ConvertComment(Comment);
            try
            {
                Recp ig1;
                mPromiseManager.lotSpec_AbortStep(mUserId, mPassword, inlot.ID, inlot.mEquipment.EquipmentID, input, out ig1, out mInfo);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Aborts a running lot instantly with the ability to add a comment
        /// </summary>
        /// <param name="inlot">Lot to abort step</param>
        /// <param name="comment">Comment for abort</param>
        /// <param name="err">Error message</param>
        /// <returns>True if successful</returns>
        public override bool AbortStep(Lot inlot, string comment, out string err)
        {
            err = "";
            try
            {
                Recp ig1;
                mPromiseManager.lotSpec_AbortStep(mUserId, mPassword, inlot.ID, inlot.mEquipment.EquipmentID, ConvertComment(comment), out ig1, out mInfo);
                return true;
            }
            catch (Exception ex)
            {
                err = ex.Message;
                return false;
            }
        }

        /// <summary>
        /// Adds data to a running lot
        /// Data entered in the form {data, component, site, testnum}
        /// </summary>
        /// <param name="inlot">Lot to add Data to</param>
        /// <param name="Data">Array of data for input</param>
        /// <param name="message">Error Message</param>
        /// <param name="comment">Comment to add to Lot</param>
        /// <returns>True if successful</returns>
        /// <remarks>"should be used in congunction with bool GetDCOpInfo()"</remarks>

        /*
    public override bool AddData(Lot inlot, List<string[]> Data, string comment, out string message)
    {
        message = null;
        //List<string[]> compDataList = new List<string[]>();
        //List<string[]> siteDataList = new List<string[]>();

        Dictionary<string, List<string[]>> Tests = new Dictionary<string, List<string[]>>();

        foreach (string[] test in Data)
        {
            if (!Tests.ContainsKey(test[3]))
            {
                Tests.Add(test[3], new List<string[]>());
                Tests[test[3]].Add(test);
            }
            else
            {
                Tests[test[3]].Add(test);
            }
        }

        var doTests = Tests.Keys.ToArray();

        foreach (string testnum in doTests)
        {
            #region Create Lists

            //DataList()
            List<string> dataList = new List<string>();
            //Componentlist(DataList())
            Dictionary<string, List<string>> compDataList = new Dictionary<string, List<string>>();
            //ComponentList(SiteList(Datalist()))
            Dictionary<string, Dictionary<string, List<string>>> siteDataList = new Dictionary<string, Dictionary<string, List<string>>>();
            foreach (string[] inData in Tests[testnum])
            {
                if (!string.IsNullOrEmpty(inData[0]))
                {
                    if (!string.IsNullOrEmpty(inData[1]))
                    {
                        if (!string.IsNullOrEmpty(inData[2]))
                        {
                            //data components and site
                            if (!siteDataList.ContainsKey(inData[1]))
                            {
                                //comp doesnt exist
                                siteDataList.Add(inData[1], new Dictionary<string, List<string>>());
                                siteDataList[inData[1]].Add(inData[2], new List<string>());
                                siteDataList[inData[1]][inData[2]].Add(inData[0]);
                            }
                            else
                            {
                                if (!siteDataList[inData[1]].ContainsKey(inData[2]))
                                {
                                    //does contain comp but not site
                                    siteDataList[inData[1]].Add(inData[2], new List<string>());
                                    siteDataList[inData[1]][inData[2]].Add(inData[0]);
                                }
                                else
                                {
                                    //contains comp and site
                                    siteDataList[inData[1]][inData[2]].Add(inData[0]);
                                }
                            }
                        }
                        else
                        {
                            //data and components
                            if (!compDataList.ContainsKey(inData[1]))
                            {
                                compDataList.Add(inData[1], new List<string>());
                                compDataList[inData[1]].Add(inData[0]);
                            }
                            else
                            {
                                compDataList[inData[1]].Add(inData[0]);
                            }
                        }
                    }
                    else
                    {
                        //just data
                        dataList.Add(inData[0]);
                    }

                }
            }
            #endregion
            try
            {

                #region create data entry lists
                DataValueListRow[] data = null;
                CompValueListRow[] compData = null;
                SiteValueListRow[] siteData = null;

                if (dataList.Count > 0)
                {
                    data = new DataValueListRow[dataList.Count];

                    for (int i = 0; i < data.Length; i++)
                    {
                        data[i] = new DataValueListRow();
                        data[i].data = dataList[i];
                    }
                }
                if (compDataList.Count > 0)
                {
                    var comps = compDataList.Keys.ToArray();
                    compData = new CompValueListRow[compDataList.Count];
                    for (int i = 0; i < compData.Length; i++)
                    {
                        DataValueListRow[] tmpData = new DataValueListRow[compDataList[comps[i]].Count];
                        for (int j = 0; j < tmpData.Length; j++)
                        {
                            tmpData[j] = new DataValueListRow();
                            tmpData[j].data = compDataList[comps[i]][j];
                        }
                        compData[i] = new CompValueListRow();
                        compData[i].data = tmpData;
                        compData[i].compId = comps[i];
                    }
                }
                if (siteDataList.Count > 0)
                {
                    int counter = 0;
                    foreach (string str in siteDataList.Keys)
                    {
                        Dictionary<string, List<string>> tmp = siteDataList[str];
                        foreach (string str2 in tmp.Keys)
                        {
                            counter++;
                        }
                    }
                    siteData = new SiteValueListRow[counter];
                    counter = 0;
                    var comps = siteDataList.Keys.ToArray();
                    for (int i = 0; i < comps.Length; i++)
                    {
                        var sites = siteDataList[comps[i]].Keys.ToArray();
                        for (int j = 0; j < sites.Length; j++)
                        {
                            siteData[counter] = new SiteValueListRow();
                            siteData[counter].compId = comps[i];
                            siteData[counter].number = (j + 1).ToString();
                            DataValueListRow[] tmpData = new DataValueListRow[siteDataList[comps[i]][sites[j]].Count];
                            for (int k = 0; k < tmpData.Length; k++)
                            {
                                tmpData[k] = new DataValueListRow();
                                tmpData[k].data = siteDataList[comps[i]][sites[j]][k];
                            }
                            siteData[counter].data = tmpData;
                            counter++;
                        }

                    }
                }
                #endregion

                Actl ig1;
                CalcSetRow[] ig2;
                TresDresTableRow[] ig3;
                ChrtTableRow[] ig4;
                WarningsRow[] ig5;

                mPromiseManager.tstEnter_LotData(mUserId, mPassword, inlot.ID, testnum, null, "PARTIAL", data, compData, siteData, out ig1, out ig2, out ig3, out ig4, out ig5, out mInfo);
            }
            catch (Exception e)
            {
                message += "\n" + e.Message;    
                return false;
            }
        }

        return true;
    }
    */
        public override bool AddData(Lot inlot, List<string[]> Data, string comment, out string message)
        {
            message = null;
            //List<string[]> compDataList = new List<string[]>();
            //List<string[]> siteDataList = new List<string[]>();

            Dictionary<string, List<string[]>> Tests = new Dictionary<string, List<string[]>>();

            foreach (string[] test in Data)
            {
                if (!Tests.ContainsKey(test[3]))
                {
                    Tests.Add(test[3], new List<string[]>());
                    Tests[test[3]].Add(test);
                }
                else
                {
                    Tests[test[3]].Add(test);
                }
            }

            var doTests = Tests.Keys.ToArray();

            foreach (string testnum in doTests)
            {
                #region Create Lists

                //DataList()
                List<string> dataList = new List<string>();
                //Componentlist(DataList())
                Dictionary<string, List<string>> compDataList = new Dictionary<string, List<string>>();
                //ComponentList(SiteList(Datalist()))
                Dictionary<string, Dictionary<string, List<string>>> siteDataList = new Dictionary<string, Dictionary<string, List<string>>>();
                foreach (string[] inData in Tests[testnum])
                {
                    if (!string.IsNullOrEmpty(inData[0]))
                    {
                        if (!string.IsNullOrEmpty(inData[1]))
                        {
                            if (!string.IsNullOrEmpty(inData[2]))
                            {
                                //data components and site
                                if (!siteDataList.ContainsKey(inData[1]))
                                {
                                    //comp doesnt exist
                                    siteDataList.Add(inData[1], new Dictionary<string, List<string>>());
                                    siteDataList[inData[1]].Add(inData[2], new List<string>());
                                    siteDataList[inData[1]][inData[2]].Add(inData[0]);
                                }
                                else
                                {
                                    if (!siteDataList[inData[1]].ContainsKey(inData[2]))
                                    {
                                        //does contain comp but not site
                                        siteDataList[inData[1]].Add(inData[2], new List<string>());
                                        siteDataList[inData[1]][inData[2]].Add(inData[0]);
                                    }
                                    else
                                    {
                                        //contains comp and site
                                        siteDataList[inData[1]][inData[2]].Add(inData[0]);
                                    }
                                }
                            }
                            else
                            {
                                //data and components
                                if (!compDataList.ContainsKey(inData[1]))
                                {
                                    compDataList.Add(inData[1], new List<string>());
                                    compDataList[inData[1]].Add(inData[0]);
                                }
                                else
                                {
                                    compDataList[inData[1]].Add(inData[0]);
                                }
                            }
                        }
                        else
                        {
                            //just data
                            dataList.Add(inData[0]);
                        }

                    }
                }
                #endregion
                try
                {

                    #region create data entry lists
                    DataValueListRow[] data = null;
                    CompValueListRow[] compData = null;
                    SiteValueListRow[] siteData = null;

                    if (dataList.Count > 0)
                    {
                        data = new DataValueListRow[dataList.Count];

                        for (int i = 0; i < data.Length; i++)
                        {
                            data[i] = new DataValueListRow();
                            data[i].data = dataList[i];
                        }
                    }
                    if (compDataList.Count > 0)
                    {
                        var comps = compDataList.Keys.ToArray();
                        compData = new CompValueListRow[compDataList.Count];
                        for (int i = 0; i < compData.Length; i++)
                        {
                            DataValueListRow[] tmpData = new DataValueListRow[compDataList[comps[i]].Count];
                            for (int j = 0; j < tmpData.Length; j++)
                            {
                                tmpData[j] = new DataValueListRow();
                                tmpData[j].data = compDataList[comps[i]][j];
                            }
                            compData[i] = new CompValueListRow();
                            compData[i].data = tmpData;
                            compData[i].compId = comps[i];
                        }
                    }
                    if (siteDataList.Count > 0)
                    {
                        int counter = 0;
                        foreach (string str in siteDataList.Keys)
                        {
                            Dictionary<string, List<string>> tmp = siteDataList[str];
                            foreach (string str2 in tmp.Keys)
                            {
                                counter++;
                            }
                        }
                        siteData = new SiteValueListRow[counter];
                        counter = 0;
                        var comps = siteDataList.Keys.ToArray();
                        for (int i = 0; i < comps.Length; i++)
                        {
                            var sites = siteDataList[comps[i]].Keys.ToArray();
                            for (int j = 0; j < sites.Length; j++)
                            {
                                siteData[counter] = new SiteValueListRow();
                                siteData[counter].compId = comps[i];
                                siteData[counter].number = (j + 1).ToString();
                                DataValueListRow[] tmpData = new DataValueListRow[siteDataList[comps[i]][sites[j]].Count];
                                for (int k = 0; k < tmpData.Length; k++)
                                {
                                    tmpData[k] = new DataValueListRow();
                                    tmpData[k].data = siteDataList[comps[i]][sites[j]][k];
                                }
                                siteData[counter].data = tmpData;
                                counter++;
                            }

                        }
                    }
                    #endregion

                    Actl ig1;
                    CalcSetRow[] ig2;
                    TresDresTableRow[] ig3;
                    ChrtTableRow[] ig4;
                    WarningsRow[] ig5;

                    mPromiseManager.tstEnter_LotData(mUserId, mPassword, inlot.ID, testnum, null, "PARTIAL", data, compData, siteData, out ig1, out ig2, out ig3, out ig4, out ig5, out mInfo);
                }
                catch (Exception e)
                {
                    message += "\n" + e.Message;
                    return false;
                }
            }

            return true;
        }


        public override bool CorrectLotData(Lot inlot, List<string[]> Data, string comment, out string message)
        {
            message = null;
            //List<string[]> compDataList = new List<string[]>();
            //List<string[]> siteDataList = new List<string[]>();

            Dictionary<string, List<string[]>> Tests = new Dictionary<string, List<string[]>>();

            foreach (string[] test in Data)
            {
                if (!Tests.ContainsKey(test[3]))
                {
                    Tests.Add(test[3], new List<string[]>());
                    Tests[test[3]].Add(test);
                }
                else
                {
                    Tests[test[3]].Add(test);
                }
            }

            var doTests = Tests.Keys.ToArray();

            foreach (string testnum in doTests)
            {
                #region Create Lists

                //DataList()
                List<string> dataList = new List<string>();
                //Componentlist(DataList())
                Dictionary<string, List<string>> compDataList = new Dictionary<string, List<string>>();
                //ComponentList(SiteList(Datalist()))
                Dictionary<string, Dictionary<string, List<string>>> siteDataList = new Dictionary<string, Dictionary<string, List<string>>>();
                foreach (string[] inData in Tests[testnum])
                {
                    if (!string.IsNullOrEmpty(inData[0]))
                    {
                        if (!string.IsNullOrEmpty(inData[1]))
                        {
                            if (!string.IsNullOrEmpty(inData[2]))
                            {
                                //data components and site
                                if (!siteDataList.ContainsKey(inData[1]))
                                {
                                    //comp doesnt exist
                                    siteDataList.Add(inData[1], new Dictionary<string, List<string>>());
                                    siteDataList[inData[1]].Add(inData[2], new List<string>());
                                    siteDataList[inData[1]][inData[2]].Add(inData[0]);
                                }
                                else
                                {
                                    if (!siteDataList[inData[1]].ContainsKey(inData[2]))
                                    {
                                        //does contain comp but not site
                                        siteDataList[inData[1]].Add(inData[2], new List<string>());
                                        siteDataList[inData[1]][inData[2]].Add(inData[0]);
                                    }
                                    else
                                    {
                                        //contains comp and site
                                        siteDataList[inData[1]][inData[2]].Add(inData[0]);
                                    }
                                }
                            }
                            else
                            {
                                //data and components
                                if (!compDataList.ContainsKey(inData[1]))
                                {
                                    compDataList.Add(inData[1], new List<string>());
                                    compDataList[inData[1]].Add(inData[0]);
                                }
                                else
                                {
                                    compDataList[inData[1]].Add(inData[0]);
                                }
                            }
                        }
                        else
                        {
                            //just data
                            dataList.Add(inData[0]);
                        }

                    }
                }
                #endregion
                try
                {

                    #region create data entry lists
                    DataValueListRow[] data = null;
                    CompValueListRow[] compData = null;
                    SiteValueListRow[] siteData = null;

                    if (dataList.Count > 0)
                    {
                        data = new DataValueListRow[dataList.Count];

                        for (int i = 0; i < data.Length; i++)
                        {
                            data[i] = new DataValueListRow();
                            data[i].data = dataList[i];
                        }
                    }
                    if (compDataList.Count > 0)
                    {
                        var comps = compDataList.Keys.ToArray();
                        compData = new CompValueListRow[compDataList.Count];
                        for (int i = 0; i < compData.Length; i++)
                        {
                            DataValueListRow[] tmpData = new DataValueListRow[compDataList[comps[i]].Count];
                            for (int j = 0; j < tmpData.Length; j++)
                            {
                                tmpData[j] = new DataValueListRow();
                                tmpData[j].data = compDataList[comps[i]][j];
                            }
                            compData[i] = new CompValueListRow();
                            compData[i].data = tmpData;
                            compData[i].compId = comps[i];
                        }
                    }
                    if (siteDataList.Count > 0)
                    {
                        int counter = 0;
                        foreach (string str in siteDataList.Keys)
                        {
                            Dictionary<string, List<string>> tmp = siteDataList[str];
                            foreach (string str2 in tmp.Keys)
                            {
                                counter++;
                            }
                        }
                        siteData = new SiteValueListRow[counter];
                        counter = 0;
                        var comps = siteDataList.Keys.ToArray();
                        for (int i = 0; i < comps.Length; i++)
                        {
                            var sites = siteDataList[comps[i]].Keys.ToArray();
                            for (int j = 0; j < sites.Length; j++)
                            {
                                siteData[counter] = new SiteValueListRow();
                                siteData[counter].compId = comps[i];
                                siteData[counter].number = (j + 1).ToString();
                                DataValueListRow[] tmpData = new DataValueListRow[siteDataList[comps[i]][sites[j]].Count];
                                for (int k = 0; k < tmpData.Length; k++)
                                {
                                    tmpData[k] = new DataValueListRow();
                                    tmpData[k].data = siteDataList[comps[i]][sites[j]][k];
                                }
                                siteData[counter].data = tmpData;
                                counter++;
                            }

                        }
                    }
                    #endregion

                    Actl ig1;
                    CalcSetRow[] ig2;
                    TresDresTableRow[] ig3;
                    ChrtTableRow[] ig4;
                    WarningsRow[] ig5;
                    InfoTextRow[] ig6;


                 //   Schd ig7;
                 //   ActlSetRow[] ig8;

                    mPromiseManager.tstCorrect_LotData(mUserId, mPassword, inlot.mProcedure.Ops[inlot.mProcedure.Ops.Count - 1].ID.ToString(), "7-NOV-2019 12:45:06.00", null, testnum, data, compData, siteData, out ig1, out ig2, out ig3, out ig4, out ig5, out ig6);

                    //       mPromiseManager.tstCorrect_OldData(mUserId, mPassword, inlot.mProcedure.Ops[inlot.mProcedure.Ops.Count - 1].ID.ToString(), "7-NOV-2019 09:51:55.00", "L", "DONE", inlot.ID.ToString(), null, null, null, null, testnum, data, compData, siteData, out ig1, out ig7, out ig8, out ig2, out ig3, out ig4, out ig5, out ig6);


                }
                catch (Exception e)
                {
                    message += "\n" + e.Message;
                    return false;
                }
            }

            return true;
        }
        /// <summary>
        /// Puts a lot on hold based on a hold reason and a comment
        /// </summary>
        /// <param name="inLot">Lot to hold</param>
        /// <param name="Comment">Comment for the hold</param>
        /// <param name="holdCode">Hold code</param>
        /// <param name="holdReason">Reason for hold (limited to 50 characters)</param>
        /// <param name="Message">Error Message</param>
        /// <returns>True if successful</returns>
        public override bool HoldLot(Lot inLot, string Comment, string holdCode, string holdReason, out string Message)
        {
            Message = null;

            CommentListRow[] input = ConvertComment(Comment);

            List<string> reson = Utilities.SplitLines(holdReason, 50);
            HoldReasonListRow[] Reason = null;
            if (reson.Count > 0)
            {
                Reason = new HoldReasonListRow[reson.Count];
                for (int i = 0; i < Reason.Length; i++)
                {
                    Reason[i] = new HoldReasonListRow();
                    Reason[i].reason = reson[i];
                }
            }
            try
            {
                mPromiseManager.lotHold_ImmSet(mUserId, mPassword, inLot.ID, null, holdReason, null, input, out mInfo);
                return true;
            }
            catch (Exception e)
            {
                Message = e.Message;
                return false;
            }
        }

        /// <summary>
        /// Scraps materials from a lot with given rejects
        /// </summary>
        /// <param name="inLot">Lot to scrap from</param>
        /// <param name="rejectList">List of identified rejected components {Comp Id, Rejection catagory}</param>
        /// <param name="unidentRejectList">List of unidentified rejected components {Rejection catagory, Quantity}</param>
        /// <param name="comment">Comment</param>
        /// <param name="message">Error message</param>
        /// <returns>True if successful</returns>
        public override bool ScrapLot(Lot inLot, List<string[]> rejectList, List<string[]> unidentRejectList, string comment, out string message)
        {
            message = null;

            CommentListRow[] input = ConvertComment(comment);

            IdentRejectListRow[] identRejects = null;
            UnidentRejectListRow[] unidentRejects = null;

            if (rejectList != null)
            {
                if (rejectList.Count > 0)
                {
                    identRejects = new IdentRejectListRow[rejectList.Count];

                    for (int i = 0; i < identRejects.Length; i++)
                    {
                        identRejects[i] = new IdentRejectListRow();
                        identRejects[i].compId = rejectList[i][0];
                        identRejects[i].rejCat = rejectList[i][1];
                    }
                }
            }

            if (unidentRejectList != null)
            {
                if (unidentRejectList.Count > 0)
                {
                    unidentRejects = new UnidentRejectListRow[unidentRejectList.Count];
                    for (int i = 0; i < unidentRejects.Length; i++)
                    {
                        unidentRejects[i] = new UnidentRejectListRow();
                        unidentRejects[i].rejCat = unidentRejectList[i][0];
                        unidentRejects[i].qty = unidentRejectList[i][1];
                    }
                }
            }

            FutaSetRow[] ig1;
            FutaSetRow[] ig2;

            try
            {
                mPromiseManager.lotSpec_Scrap(mUserId, mPassword, inLot.ID, identRejects, unidentRejects, input, out ig1, out ig2, out mInfo);
                return true;
            }
            catch (Exception e)
            {
                message = e.Message;
                return false;
            }
        }

        /// <summary>
        /// Gets all the equipment that a lot can be tracked into
        /// </summary>
        /// <param name="LotID">Lot ID</param>
        /// <param name="Equip">List of equipment that the lot can be tracked into</param>
        /// <param name="Err">Error message</param>
        /// <returns>True if successful</returns>
        public override bool GetEquipForLot(string LotID, out List<string> Equip, out string Err)
        {
            Equip = new List<string>();
            Err = "";
            try
            {
                PromisLotSupportData tmp1;
                LotBasic tmp2;
                LotExtended tmp3;
                PrintInfo(LotID, out tmp2, out tmp3, out tmp1);
                if (tmp1.AvailEqpt != null)
                {
                    foreach (LotInfoEqpAvailRow equip in tmp1.AvailEqpt)
                    {
                        Equip.Add(equip.eqpAvail);
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                Err = e.Message;
                return false;
            }
        }

        /// <summary>
        /// Marks a peice of equipment as down
        /// </summary>
        /// <param name="Equip">Equipment to make as down</param>
        /// <param name="reason">Reason for down</param>
        /// <param name="comment">Additional comment</param>
        /// <param name="err">Error message</param>
        /// <returns>True if successful</returns>
        public override bool LockEquipment(string Equip, string reason, string comment, out string err)
        {
            err = "";

            EqpcSetRow[] ig1;
            EqprSetRow[] ig2;
            EqpsCommentsRow[] ig3;
            try
            {
                mPromiseManager.eqpStatus_DownEqp(mUserId, mPassword, Equip, "DOWN", ConvertComment(comment), null, null, null, null, out ig1, out ig2, out ig3, out mInfo);
                return true;
            }
            catch (Exception e)
            {
                err = e.Message;
                return false;
            }
        }

        /// <summary>
        /// Get information about future stages for a lot
        /// </summary>
        /// <param name="lot">Lot ID</param>
        /// <param name="processCount">Number of steps to look ahead for</param>
        /// <param name="futRecpies">List of recipe details</param>
        /// <param name="err">Error message</param>
        /// <returns>True if successful</returns>
        public override bool GetFutureRecp(string lot, int processCount, out List<Lot.Recipe> futRecpies, out string err)
        {
            futRecpies = new List<Lot.Recipe>();
            err = "";
            try
            {
                RecpSetRow[] ig16;
                MitmSetRow[] ig18;
                MatrSetRow[] ig19;
                PiarSetRow[] ig20;
                FutaSetRow[] ig21;
                FutaMapRow[] ig22;
                LotInfoStepCmdAllowedRow[] ig23;
                InfoTextRow[] ig24;
                ActlSetRow[] temp = mPromiseManager.lotQuery_Multistep(mUserId, mPassword, lot, processCount.ToString(), null, out ig16, out ig18, out ig19, out ig20, out ig21, out ig22, out ig23, out ig24);

                foreach (ActlSetRow fut in temp)
                {
                    //mPromiseManager.pro
                    Lot.Recipe tmpFut = new Lot.Recipe();
                    tmpFut.ID = fut.recpid;
                    tmpFut.Location = fut.location;
                    foreach (RecpSetRow recp in ig16)
                    {
                        if (fut.recpid == recp.recpId)
                        {
                            tmpFut.Title = recp.title;
                            tmpFut.Capability = recp.capability;
                        }
                    }
                    //tmpFut.Description = fut.curprcdname;
                    tmpFut.Procedure = fut.curPrcdId;
                    futRecpies.Add(tmpFut);
                }

                return true;
            }
            catch (Exception e)
            {
                err = e.Message;
                return false;
            }
        }

        /// <summary>
        /// List of the past recipies up to a set history count
        /// </summary>
        /// <param name="lot">Lot ID</param>
        /// <param name="processCount">Number of history steps to look at</param>
        /// <param name="pastRecpies">Output list</param>
        /// <param name="err">Error message to be passed back to mainform</param>
        /// <returns>True if successful</returns>
        public override bool GetPastRecp(string lot, int processCount, out List<Lot.Recipe> pastRecpies, out string err)
        {
            pastRecpies = new List<Lot.Recipe>();
            err = "";
            try
            {
                LotQuery_HistListByStepOutputElementRow[] Hist = mPromiseManager.lotQuery_HistListByStep(mUserId, mPassword, lot, "1", processCount.ToString(), null, null);
                if (Hist != null)
                {
                    foreach (LotQuery_HistListByStepOutputElementRow thisHist in Hist)
                    {
                        Lot.Recipe tmpPast = new Lot.Recipe();
                        tmpPast.ID = thisHist.recp.recpId;
                        tmpPast.Title = thisHist.recp.recpTitle;
                        tmpPast.Location = thisHist.recp.capability;
                        tmpPast.Procedure = thisHist.hist.curprcdid.Replace(" ", "");
                        tmpPast.Operations = new List<Lot.Operations>();
                        for (int i = 0; i < thisHist.recpOperations.Length; i++)
                        {
                            Lot.Operations tmpOp = new Lot.Operations();
                            tmpOp.ID = thisHist.recpOperations[i].opids;
                            tmpOp.Type = thisHist.recpOperations[i].optypes;
                            tmpOp.Desc = thisHist.recpOperdesc[i].opdescs;
                            tmpPast.Operations.Add(tmpOp);
                        }

                        pastRecpies.Add(tmpPast);
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                err = e.Message;
                return false;
            }
        }

        /// <summary>
        /// Move lot along when in transit state
        /// </summary>
        /// <param name="inLot">Lot to move</param>
        /// <param name="location">Location to move to</param>
        /// <param name="Comment">Additional comment</param>
        /// <param name="err">Error message</param>
        /// <returns>True if successful</returns>
        public override bool MoveLot(Lot inLot, string location, string Comment, out string err)
        {
            err = "";

            try
            {
                mPromiseManager.lotInv_MoveToLoc(mUserId, mPassword, inLot.ID, location, null, null, null, null, null, ConvertComment(Comment), out mInfo);
                return true;
            }
            catch (Exception e)
            {
                err = e.Message;
                return false;
            }

        }

        /// <summary>
        /// Move a Lot to the next location if in transit state
        /// </summary>
        /// <param name="inLot">Lot to move</param>
        /// <param name="Comment">Additional comment</param>
        /// <param name="err">Error message</param>
        /// <returns>True if successful</returns>
        public override bool ContinueLot(Lot inLot, string Comment, out string err)
        {
            err = "";

            FutaSetRow[] ig1;

            try
            {
                mPromiseManager.lotInv_Continue(mUserId, mPassword, inLot.ID, null, null, null, null, null, ConvertComment(Comment), out ig1, out mInfo);
                return true;
            }
            catch (Exception e)
            {
                err = e.Message;
                return false;
            }
        }

        /// <summary>
        /// Get all the lot commments for a given lot
        /// </summary>
        /// <param name="lot">Lot ID</param>
        /// <param name="comments">List of Comments</param>
        /// <param name="err">Error Message</param>
        /// <returns>True if successful</returns>
        public override bool GetLotComments(string lot, out List<string> comments, out string err)
        {
            comments = new List<string>();
            err = "";
            try
            {
                LotQuery_HistListByStepOutputElementRow[] Hist = mPromiseManager.lotQuery_HistListByStep(mUserId, mPassword, lot, "1", null, null, null);
                if (Hist != null)
                {
                    foreach (LotQuery_HistListByStepOutputElementRow thisHist in Hist)
                    {
                        foreach (HistEventsRow events in thisHist.events)
                        {
                            try
                            {
                                if (events.evType == "COMM")
                                {
                                    comments.Add("*");
                                    comments.Add(events.evTime + " - " + events.evUser);
                                    comments.Add(events.evVariant);
                                }
                                if (events.evType == "CONT")
                                {
                                    comments[comments.Count - 1] += " " + events.evVariant;
                                }
                            }
                            catch { }
                        }
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                err = e.Message;
                return false;
            }
        }

        /// <summary>
        /// Gets the True lot Id from an alias
        /// </summary>
        /// <param name="lotAlias">Alias Lot</param>
        /// <param name="lotId">True lot ID</param>
        /// <param name="err">Error message</param>
        /// <returns>True if successful</returns>
        public override bool GetLotAlias(string lotAlias, out string lotId, out string err)
        {
            err = "";
            lotId = "";
            try
            {
                ActlSetRow[] ig1;
                mPromiseManager.alsTp_AliasInfo(mUserId, mPassword, lotAlias, out ig1);
                lotId = ig1[0].lotid;
                return true;
            }
            catch (Exception e)
            {
                err = e.Message;
                return false;
            }
        }

        /// <summary>
        /// Add control components to a lot
        /// </summary>
        /// <param name="inLot">Lot to add to</param>
        /// <param name="compIds">Control component Ids</param>
        /// <param name="comment">Additional comment</param>
        /// <param name="err">Error message</param>
        /// <returns>True if successful</returns>
        public override bool AddControlComponents(Lot inLot, List<string> compIds, string comment, out string err)
        {
            err = "";

            try
            {
                CompIdListRow[] ids = new CompIdListRow[compIds.Count];
                for (int i = 0; i < ids.Length; i++)
                {
                    ids[i] = new CompIdListRow();
                    ids[i].compId = compIds[i];
                }

                mPromiseManager.lotCtlComp_Add(mUserId, mPassword, inLot.ID, ids, ConvertComment(comment), out mInfo);

                return true;
            }
            catch (Exception ex)
            {
                err = ex.Message;
                return false;
            }
        }

        /// <summary>
        /// Remove control components from a lot
        /// </summary>
        /// <param name="inLot">Lot to remove components from</param>
        /// <param name="compIds">Control component Ids</param>
        /// <param name="comment">Additional comment</param>
        /// <param name="err">Error message</param>
        /// <returns>True if successful</returns>
        public override bool RemoveControlComponents(Lot inLot, List<string> compIds, string comment, out string err)
        {
            err = "";

            try
            {
                CompIdListRow[] ids = new CompIdListRow[compIds.Count];
                for (int i = 0; i < ids.Length; i++)
                {
                    ids[i] = new CompIdListRow();
                    ids[i].compId = compIds[i];
                }

                mPromiseManager.lotCtlComp_Remove(mUserId, mPassword, inLot.ID, ids, ConvertComment(comment), out mInfo);

                return true;
            }
            catch (Exception ex)
            {
                err = ex.Message;
                return false;
            }
        }

        /// <summary>
        /// Changes the state of a piece of equipment
        /// </summary>
        /// <param name="equipment">Equipment to change the status of</param>
        /// <param name="state">New state</param>
        /// <param name="comment">Additional comment</param>
        /// <param name="err">Error message</param>
        /// <returns>True if successful</returns>
        public override bool ChangeEquipmentStatus(string equipment, string state, string comment, out string err)
        {
            err = "";
            try
            {
                EqpcSetRow[] ig1;
                EqprSetRow[] ig2;
                EqpsCommentsRow[] ig3;
                Eqps test = mPromiseManager.eqpStatus_Update(mUserId, mPassword, equipment, state, ConvertComment(comment), null, null, null, null, null, out ig1, out ig2, out ig3, out mInfo);
                return true;
            }
            catch (Exception ex)
            {
                err = ex.Message;
                return false;
            }
        }

        /// <summary>
        /// Get information about a piece of equipment
        /// </summary>
        /// <param name="equipment">Equipment ID</param>
        /// <param name="equipDet">Equipment details object</param>
        /// <param name="err">Error message</param>
        /// <returns>True if successful</returns>
        public override bool GetEquipDetails(string equipment, out Lot.EquipmentDet equipDet, out string err)
        {
            err = "";
            equipDet = new Lot.EquipmentDet();
            try
            {
                EqpListRow[] tst = mPromiseManager.getEqpList(mUserId, mPassword, "EQPID = '" + equipment + "'", null);
                equipDet.ID = tst[0].eqpId;
                equipDet.Type = tst[0].eqpType;
                equipDet.Capability = tst[0].capabilityVal;
                equipDet.Location = tst[0].locationId;
                equipDet.Description = tst[0].description;
                equipDet.Avaliable = tst[0].status;
                equipDet.Activity = tst[0].activityId;
                return true;
            }
            catch (Exception ex)
            {
                err = ex.Message;
                return false;
            }
        }

        /// <summary>
        /// Set a lot Parameter
        /// </summary>
        /// <param name="inLot">Lot to add to</param>
        /// <param name="paramName">Parameter name</param>
        /// <param name="paramValue">New parameter value</param>
        /// <param name="componentId">Component parameter ID (if null, parameter will be a general lot parameter)</param>
        /// <param name="comment">Additional comment</param>
        /// <param name="err">Error message</param>
        /// <returns>True if successful</returns>
        public override bool SetLotParameter(Lot inLot, string paramName, string paramValue, string componentId, string comment, out string err)
        {
            err = "";
            try
            {
                LotIdListRow[] ig1 = new LotIdListRow[1];
                ig1[0] = new LotIdListRow();
                ig1[0].lotId = inLot.ID;
                ParmListRow[] ig2 = new ParmListRow[1];
                ig2[0] = new ParmListRow();
                ig2[0].compId = componentId;
                ig2[0].parmName = paramName;
                ig2[0].parmVal = paramValue;

                mPromiseManager.lotParm_Update(mUserId, mPassword, ig1, ig2, ConvertComment(comment), out mInfo);
                return true;
            }
            catch (Exception e)
            {
                err = e.Message;
                return false;
            }
        }

        /// <summary>
        /// Returns all the data entered on a lot for a given DCOP
        /// </summary>
        /// <param name="lotID">Batch ID</param>
        /// <param name="opID">Data Collection Operation ID</param>
        /// <param name="histNumSearch">Number of steps to look back to find operation (null for full history)</param>
        /// <param name="opNumSearch">Number of occurences of the operation to return values from (null for all occurences)</param>
        /// <param name="DCOPs">List of DCOP information</param>
        /// <param name="err">Error message</param>
        /// <returns>True if successful</returns>
        public override bool GetDCOPData(string lotID, string opID, string histNumSearch, string opNumSearch, out List<Lot.DataCollectionOperations> DCOPs, out string err)
        {
            DCOPs = new List<Lot.DataCollectionOperations>();
            err = "";
            LotQuery_HistListByStepOutputElementRow[] Hist;

            Test ig1;
            TresDimensionsRow[] ig2;
            TresComponentsRow[] ig3;
            CalcSetRow[] ig4;
            string ig5, ig6, ig7, ig8, ig9, ig10;
            TestCompTableRow[] ig11;
            TestItemTableRow[] ig12;
            TestParmTableRow[] ig13;
            TestSiteTableRow[] ig14;
            TestAvailCompTableRow[] ig15;
            TresDataTableRow[] ig16;
            TresDresTableRow[] ig17;
            try
            {

                Hist = mPromiseManager.lotQuery_HistListByStep(mUserId, mPassword, lotID, "1", histNumSearch, null, null);
                int counter = 0;
                bool finished = false;
                foreach (LotQuery_HistListByStepOutputElementRow step in Hist)
                {
                    foreach (HistEventsRow histEvent in step.events)
                    {
                        if (histEvent.evType == "ETST")
                        {
                            string[] splitter = histEvent.evVariant.Split("&#7;".ToCharArray());
                            List<string> splitter2 = new List<string>();
                            foreach (string str in splitter)
                            {
                                if (!string.IsNullOrEmpty(str))
                                    splitter2.Add(str);
                            }
                            if (splitter[0] == opID)
                            {
                                //GetDCOpInfo(opID, out DCOPs);
                                Tres test = mPromiseManager.tstShow_Data(mUserId, mPassword, opID, histEvent.evTime, null, null,
                                    out ig1, out ig2, out ig3, out ig4, out ig5, out ig6, out ig7, out ig8, out ig9, out ig10,
                                    out ig11, out ig12, out ig13, out ig14, out ig15, out ig16, out ig17);
                                for (int i = 0; i < ig16.Length; i++)
                                {
                                    Lot.DataCollectionOperations tmpDC = new Lot.DataCollectionOperations();
                                    if (ig12.Length > 0)
                                    {
                                        tmpDC.Prompt = ig12[Convert.ToInt32(ig16[i].itemNo) - 1].prompt;
                                        tmpDC.LowLimit = ig12[Convert.ToInt32(ig16[i].itemNo) - 1].lowlim;
                                        tmpDC.UpLimit = ig12[Convert.ToInt32(ig16[i].itemNo) - 1].UprLim;
                                        tmpDC.PickList = ig12[Convert.ToInt32(ig16[i].itemNo) - 1].picklist.Split('/');
                                    }
                                    if (ig15.Length > 0)
                                    {
                                        tmpDC.Component = ig15[Convert.ToInt32(ig16[i].compNo) - 1].compId;
                                        tmpDC.Tested = ig15[Convert.ToInt32(ig16[i].compNo) - 1].compTested;
                                    }
                                    if (ig14.Length > 0)
                                    {
                                        tmpDC.Site = ig14[Convert.ToInt32(ig16[i].siteNo) - 1].siteId;
                                        tmpDC.SiteNum = ig16[i].siteNo;
                                    }
                                    tmpDC.TestNumber = (i + 1).ToString();
                                    tmpDC.DataEntered = ig16[i].item.Trim();
                                    tmpDC.TimeEntered = splitter2[1];
                                    DCOPs.Add(tmpDC);
                                }
                                counter++;
                                if (opNumSearch != null)
                                {
                                    if (counter >= Convert.ToInt32(opNumSearch))
                                    {
                                        finished = true;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    if (finished)
                        break;
                }
                return true;
            }
            catch (Exception ex)
            {
                err = ex.Message;
                return false;
            }
        }

        /// <summary>
        /// Gets all the operations that are performed during a given recipe
        /// </summary>
        /// <param name="lotID">Lot to query</param>
        /// <param name="recipeID">Recipie to find operations for</param>
        /// <param name="ops">List of operations</param>
        /// <param name="err">Error message</param>
        /// <returns>True if successful</returns>
        public override bool GetRecipeOperations(string lotID, string recipeID, out List<Lot.Operations> ops, out string err)
        {
            err = "";
            ops = new List<Lot.Operations>();

            try
            {
                PromisLotSupportData tmpData = new PromisLotSupportData();

                LotBasic tmpBas;
                //LotExtended tmpExt;

                tmpBas = mPromiseManager.lotQuery_LotBasic(mUserId, mPassword, lotID, null, recipeID, null, null, tmpData.CmdList,
                null, out tmpData.AvailEqpt, out tmpData.DCOps, out tmpData.ComponentsList, out tmpData.Rejects, out tmpData.LotOps,
                out tmpData.HistoryEvents, out tmpData.StepNotes, out tmpData.CmdAllowed, out tmpData.FutureHolds);
                //tmpExt = mPromiseManager.lotQuery_LotExtended(mUserId, mPassword, lotID, null, recipeID, null, null, null, out tmpData.Rework,
                //out tmpData.Parameters, out tmpData.FutureSet, out tmpData.PrcdStack, out tmpData.CurrentRework, out tmpData.InfoTimers,
                //out tmpData.ComponentParams, out tmpData.CurrentHolds, out tmpData.Bins);

                if (tmpData.LotOps != null)
                {
                    foreach (LotInfoOperationsRow op in tmpData.LotOps)
                    {
                        Lot.Operations tmpOp = new Lot.Operations();
                        tmpOp.ID = op.opId;
                        tmpOp.Desc = op.opDesc;
                        tmpOp.Type = op.opType;
                        ops.Add(tmpOp);
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                err = ex.Message;
                return false;
            }
        }

        /// <summary>
        /// Releases the current holds put on a lot
        /// </summary>
        /// <param name="inLot">Lot to release</param>
        /// <param name="reason">Reason for release</param>
        /// <param name="comment">Additional comments</param>
        /// <param name="holdNumber">Hold Id to be released</param>
        /// <param name="err">Error message</param>
        /// <returns>True if successful</returns>
        public override bool UnholdLot(Lot inLot, string reason, string comment, string holdNumber, out string err)
        {
            err = "";
            try
            {
                FutaSetRow[] ig1;

                HoldListRow[] holdList = new HoldListRow[1];
                holdList[0] = new HoldListRow();
                holdList[0].holdNumber = holdNumber;

                mPromiseManager.lotHold_CurClear(mUserId, mPassword, inLot.ID, holdList, null, reason, ConvertComment(comment), out ig1, out mInfo);
                return true;
            }
            catch (Exception ex)
            {
                err = ex.Message;
                return false;
            }
        }
        /// <summary>
        /// Releases the current holds put on a lot
        /// </summary>
        /// <param name="inLot">Lot to release</param>
        /// <param name="reason">Reason for release</param>
        /// <param name="comment">Additional comments</param>
        /// <param name="holdNumbers">List of holds to release</param>
        /// <param name="err">Error message</param>
        /// <returns>True if successful</returns>
        public override bool UnholdLot(Lot inLot, string reason, string comment, List<string> holdNumbers, out string err)
        {
            err = "";
            try
            {
                FutaSetRow[] ig1;

                HoldListRow[] holdList = new HoldListRow[holdNumbers.Count];
                for (int i = 0; i < holdNumbers.Count; i++)
                {
                    holdList[i] = new HoldListRow();
                    holdList[i].holdNumber = holdNumbers[i];
                }

                mPromiseManager.lotHold_CurClear(mUserId, mPassword, inLot.ID, holdList, null, reason, ConvertComment(comment), out ig1, out mInfo);
                return true;
            }
            catch (Exception ex)
            {
                err = ex.Message;
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lotID"></param>
        /// <returns></returns>
        public override string[] GetLotComponentIDs(string lotID)
        {

            PromisLotSupportData SupportOut = new PromisLotSupportData();
            LotBasic BasicInfo = mPromiseManager.lotQuery_LotBasic(mUserId, mPassword, lotID, null, null, null, null, SupportOut.CmdList,
                null, out SupportOut.AvailEqpt, out SupportOut.DCOps, out SupportOut.ComponentsList, out SupportOut.Rejects, out SupportOut.LotOps,
                out SupportOut.HistoryEvents, out SupportOut.StepNotes, out SupportOut.CmdAllowed, out SupportOut.FutureHolds);

            string[] CompIds = new string[SupportOut.ComponentsList.Length];
            for (int i = 0; i < CompIds.Length; i++)
            {
                CompIds[i] = SupportOut.ComponentsList[i].compids;
            }

            return CompIds;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="material"></param>
        /// <param name="sourceLotIDs"></param>
        /// <param name="location"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public override bool GetSourceLotIDs(string material, ref string[] sourceLotIDs, string location, string filter)
        {
            SrcLotTableRow[] mSource;
            InfoTextRow[] mInfo;

            try
            {

                mSource = mPromiseManager.matTp_ShwSrcLot(mUserId, mPassword, material, location, filter, out mInfo);
                if (mInfo.Length != 0)
                {
                    if (mInfo.GetLength(0) == 2 && mInfo[1].text.StartsWith("may have been truncated")) { }
                    else
                    {
                        if (filter != null)
                        {
                            return false;
                        }
                        else
                        {
                            var message = System.Windows.Forms.MessageBox.Show("No source lots found for " + material, "Error",
                              System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        }
                        return false;
                    }
                }
                sourceLotIDs = new string[mSource.GetLength(0)];
                for (int iLotNum = 0; iLotNum < mSource.GetLength(0); ++iLotNum)
                {
                    sourceLotIDs[iLotNum] = mSource[iLotNum].LotId;
                }
                return true;

            }
            catch (Exception e)
            {
                string lines = "GetSourceLotIDs" + e.Message;
                System.IO.StreamWriter file = new System.IO.StreamWriter(LegacyLogFile, true);
                file.WriteLine(lines);
                file.Close();
                var message = System.Windows.Forms.MessageBox.Show("Unknown error getting source lots\n" + e.Message, "Error",
                      System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                return false;
            }


        }


        #region TMS functions

        /// <summary>
        /// Deletes a TMS task
        /// </summary>
        /// <param name="taskName">Task name</param>
        /// <param name="err">Error message</param>
        /// <returns>True if successful</returns>
        public override bool DeleteTMSTask(string taskName, out string err)
        {
            err = "";
            try
            {
                GtskActionsRow[] ig1;
                GtskCatsRow[] ig2;
                GtskItemTypesRow[] ig3;
                mPromiseManager.tmsMaint_DeleteTask(mUserId, mPassword, taskName, out ig1, out ig2, out ig3);
                return true;
            }
            catch (Exception ex)
            {
                err = ex.Message;
                return false;
            }
        }

        /// <summary>
        /// Starts a TMS task off
        /// </summary>
        /// <param name="taskName">Task name</param>
        /// <param name="equipID">Equipment id that the task should run on</param>
        /// <param name="changeChild">Set "T" to also start off any child tasks</param>
        /// <param name="predictedAvaliableTime">Predicted time that the task will be run (optional)</param>
        /// <param name="comment">Aditional comments</param>
        /// <param name="err">Error message</param>
        /// <returns>True if successful</returns>
        public override bool StartTMSTask(string taskName, string equipID, string changeChild, string predictedAvaliableTime, string comment, out string err)
        {
            err = "";
            try
            {
                Schd ig1;
                SchdActionsRow[] ig2;
                mPromiseManager.tmsPerform_Start(mUserId, mPassword, taskName, equipID, changeChild
                    , predictedAvaliableTime, ConvertComment(comment), out ig1, out ig2, out mInfo);
                return true;
            }
            catch (Exception ex)
            {
                err = ex.Message;
                return false;
            }

        }

        /// <summary>
        /// Performs a TMS task
        /// </summary>
        /// <param name="taskName">Task name</param>
        /// <param name="equipID">Equipment that the task is running on</param>
        /// <param name="stepNum">Step of task that has been completed. If a 
        /// DC op exists within this range for which no data 
        /// has been entered, the range of steps is considered 
        /// complete only up to this DC op.</param>
        /// <param name="predictedAvaliableTime">The estimated time of task completion and therefore equipment availability</param>
        /// <param name="comment">Additional comment</param>
        /// <param name="err">Error message</param>
        /// <returns>True if successful</returns>
        public override bool PerformTMSTask(string taskName, string equipID, string stepNum, string predictedAvaliableTime, string comment, out string err)
        {
            err = "";
            try
            {
                Schd ig1;
                SchdActionsRow[] ig2;
                mPromiseManager.tmsPerform_Task(mUserId, mPassword, taskName, equipID, stepNum
                    , predictedAvaliableTime, ConvertComment(comment), out ig1, out ig2, out mInfo);
                return true;
            }
            catch (Exception ex)
            {
                err = ex.Message;
                return false;
            }
        }

        /// <summary>
        /// Finish a TMS task 
        /// </summary>
        /// <param name="taskName">Task name</param>
        /// <param name="equipID">Equipment the task is running on</param>
        /// <param name="changeChild">Determines if the status change should be propagated to child equipment units (optional)</param>
        /// <param name="completeCascTask">Set to Y if you want to finish all cascaded tasks. The default is N (optional)</param>
        /// <param name="comment">Additional comments</param>
        /// <param name="err">Error message</param>
        /// <returns>True if successful</returns>
        public override bool FinishTMSTask(string taskName, string equipID, string changeChild, string completeCascTask, string comment, out string err)
        {
            err = "";
            try
            {
                Schd ig1;
                mPromiseManager.tmsPerform_Finish(mUserId, mPassword, taskName, equipID, changeChild
                    , completeCascTask, ConvertComment(comment), out ig1);
                return true;
            }
            catch (Exception ex)
            {
                err = ex.Message;
                return false;
            }

        }

        /// <summary>
        /// This command lists generic tasks and any associated scheduled tasks. You can filter them by equipment ID.
        /// </summary>
        /// <param name="endTask">Ending task name (optional)</param>
        /// <param name="startTask">Starting task name</param>
        /// <param name="equipId">Equipment to query(optional</param>
        /// <param name="nextGtsk">Next generic task to read (usually 0)</param>
        /// <param name="nextSchd">Next scheduled task to read(usually 0)</param>
        /// <param name="nextReadFlag">The control flag for the read streams(usually 00)</param>
        /// <param name="ops">List of operations for the tasks</param>
        /// <param name="nextSched">Next scheduled date</param>
        /// <param name="early">Seconds early the task can be</param>
        /// <param name="late">Seconds late the task can be</param>
        /// <param name="isLate">True if task is late</param>
        /// <param name="err">Error message</param>
        /// <returns>True if successful</returns>
        public override bool QueryTMSTask(string endTask, string startTask, string equipId, string nextGtsk, string nextSchd, string nextReadFlag
            , out List<Lot.Operations> ops, out string nextSched, out string early, out string late, out string isLate, out string err)
        {
            err = "";
            nextSched = "";
            early = "";
            late = "";
            isLate = "";
            ops = new List<Lot.Operations>();
            try
            {
                Schd ig1;
                Stsk ig2;
                SchdMessagesRow[] ig3;
                SchdVolAccHistRow[] ig4;
                SchdActionsRow[] ig5;
                GtskActionsRow[] ig6;
                GtskItemTypesRow[] ig7;
                Gtsk tst = mPromiseManager.tmsQuery_Task(mUserId,
                                                        mPassword,
                                                        startTask,
                                                        endTask,
                                                        equipId,
                                                        ref nextGtsk,
                                                        ref nextSchd,
                                                        ref nextReadFlag,
                                                        out ig1, out ig2, out ig3, out ig4, out ig5, out ig6, out ig7);

                foreach (GtskActionsRow task in ig6)
                {
                    Lot.Operations op = new Lot.Operations();
                    op.ID = task.actionOperId;
                    op.Type = task.operationType;
                    op.Desc = task.operDescription;
                    ops.Add(op);
                }
                nextSched = ig1.scheduledTime;
                early = ig1.earlyTimeAllow;
                late = ig1.lateTimeAllow;
                isLate = ig1.isLate;

                return true;
            }
            catch (Exception ex)
            {
                err = ex.Message;
                return false;
            }
        }

        /// <summary>
        /// Run data entry on a tms task
        /// </summary>
        /// <param name="taskName">Task name</param>
        /// <param name="equipId">Equipmentg the task is running on</param>
        /// <param name="stepNumber">Step in the task that the data entry is for</param>
        /// <param name="data">Data to add to tms</param>
        /// <param name="comment">Additional comments</param>
        /// <param name="err">Error message</param>
        /// <returns>True if successful</returns>
        public override bool EnterTMSData(string taskName, string equipId, string stepNumber, List<string> data, string comment, out string err)
        {
            err = "";
            try
            {
                DataValueListRow[] promData = new DataValueListRow[data.Count];
                for (int i = 0; i < promData.Length; i++)
                {
                    promData[i] = new DataValueListRow();
                    promData[i].data = data[i];
                }

                Schd ig1;
                CalcSetRow[] ig2;
                TresDresTableRow[] ig3;
                ChrtTableRow[] ig4;
                WarningsRow[] ig5;
                mPromiseManager.tstEnter_TmsData(mUserId, mPassword, taskName, equipId, stepNumber, null, promData, null, null
                    , out ig1, out ig2, out ig3, out ig4, out ig5, out mInfo);
                return true;
            }
            catch (Exception ex)
            {
                err = ex.Message;
                return false;
            }
        }

        #endregion

        #region PrintInfo
        /// <summary>
        /// Information Collectection
        /// </summary>
        /// <param name="LotNumber">Lot id</param>
        /// <param name="basicTest">Basic lot information</param>
        /// <param name="extTest">Extended lot information</param>
        protected void PrintInfo(string LotNumber, out LotBasic basicTest, out LotExtended extTest)
        {
            basicTest = mPromiseManager.lotQuery_LotBasic(mUserId, mPassword, LotNumber, null, null, null, null, SupportInfo.CmdList,
                null, out SupportInfo.AvailEqpt, out SupportInfo.DCOps, out SupportInfo.ComponentsList, out SupportInfo.Rejects, out SupportInfo.LotOps,
                out SupportInfo.HistoryEvents, out SupportInfo.StepNotes, out SupportInfo.CmdAllowed, out SupportInfo.FutureHolds);
            //basicTest = null;
            extTest = mPromiseManager.lotQuery_LotExtended(mUserId, mPassword, LotNumber, null, null, null, null, null, out SupportInfo.Rework,
                out SupportInfo.Parameters, out SupportInfo.FutureSet, out SupportInfo.PrcdStack, out SupportInfo.CurrentRework, out SupportInfo.InfoTimers,
                out SupportInfo.ComponentParams, out SupportInfo.CurrentHolds, out SupportInfo.Bins);
        }
        /// <summary>
        /// Information Collectection
        /// </summary>
        /// <param name="LotNumber">Lot id</param>
        protected void PrintInfo(string LotNumber)
        {
            LotBasic basicTest = mPromiseManager.lotQuery_LotBasic(mUserId, mPassword, LotNumber, null, null, null, null, SupportInfo.CmdList,
                null, out SupportInfo.AvailEqpt, out SupportInfo.DCOps, out SupportInfo.ComponentsList, out SupportInfo.Rejects, out SupportInfo.LotOps,
                out SupportInfo.HistoryEvents, out SupportInfo.StepNotes, out SupportInfo.CmdAllowed, out SupportInfo.FutureHolds);
            LotExtended extTest = mPromiseManager.lotQuery_LotExtended(mUserId, mPassword, LotNumber, null, null, null, null, null, out SupportInfo.Rework,
                out SupportInfo.Parameters, out SupportInfo.FutureSet, out SupportInfo.PrcdStack, out SupportInfo.CurrentRework, out SupportInfo.InfoTimers,
                out SupportInfo.ComponentParams, out SupportInfo.CurrentHolds, out SupportInfo.Bins);


            foreach (var prop in basicTest.GetType().GetProperties())
            {
                if (prop != null)
                {
                    string[] tmpFields = new string[2];
                    tmpFields[0] = prop.ToString().Remove(0, 14);
                    tmpFields[1] = prop.GetValue(basicTest, null).ToString();
                    Console.WriteLine(tmpFields[0] + " = " + tmpFields[1]);
                }
            }

            foreach (var prop in extTest.GetType().GetProperties())
            {
                if (prop != null)
                {
                    string[] tmpFields = new string[2];
                    tmpFields[0] = prop.ToString().Remove(0, 14);
                    tmpFields[1] = prop.GetValue(extTest, null).ToString();
                    Console.WriteLine(tmpFields[0] + " = " + tmpFields[1]);
                }
            }
        }
        /// <summary>
        /// Information Collection
        /// </summary>
        /// <param name="LotNumber">Lot ID</param>
        /// <param name="basicTest">Basic lot information</param>
        /// <param name="extTest">Extended lot information</param>
        /// <param name="SupportOut">Additional lot information</param>
        protected void PrintInfo(string LotNumber, out LotBasic basicTest, out LotExtended extTest, out PromisLotSupportData SupportOut)
        {
            SupportOut = new PromisLotSupportData();
            basicTest = mPromiseManager.lotQuery_LotBasic(mUserId, mPassword, LotNumber, null, null, null, null, SupportOut.CmdList,
                null, out SupportOut.AvailEqpt, out SupportOut.DCOps, out SupportOut.ComponentsList, out SupportOut.Rejects, out SupportOut.LotOps,
                out SupportOut.HistoryEvents, out SupportOut.StepNotes, out SupportOut.CmdAllowed, out SupportOut.FutureHolds);
            extTest = mPromiseManager.lotQuery_LotExtended(mUserId, mPassword, LotNumber, null, null, null, null, null, out SupportOut.Rework,
                out SupportOut.Parameters, out SupportOut.FutureSet, out SupportOut.PrcdStack, out SupportOut.CurrentRework, out SupportOut.InfoTimers,
                out SupportOut.ComponentParams, out SupportOut.CurrentHolds, out SupportOut.Bins);
        }
        /// <summary>
        /// Information Collection
        /// </summary>
        /// <param name="LotNumber">Lot ID</param>
        /// <param name="basicTest">Basic lot information</param>
        /// <param name="extTest">Extended lot information</param>
        /// <param name="SupportOut">Additional lot information</param>
        /// <param name="Hist">Lot history</param>
        protected void PrintInfo(string LotNumber, out LotBasic basicTest, out LotExtended extTest, out PromisLotSupportData SupportOut, out LotQuery_HistListByStepOutputElementRow[] Hist)
        {
            SupportOut = new PromisLotSupportData();
            basicTest = mPromiseManager.lotQuery_LotBasic(mUserId, mPassword, LotNumber, null, null, "T", null, SupportOut.CmdList,
                null, out SupportOut.AvailEqpt, out SupportOut.DCOps, out SupportOut.ComponentsList, out SupportOut.Rejects, out SupportOut.LotOps,
                out SupportOut.HistoryEvents, out SupportOut.StepNotes, out SupportOut.CmdAllowed, out SupportOut.FutureHolds);
            extTest = mPromiseManager.lotQuery_LotExtended(mUserId, mPassword, LotNumber, null, null, "T", null, null, out SupportOut.Rework,
                out SupportOut.Parameters, out SupportOut.FutureSet, out SupportOut.PrcdStack, out SupportOut.CurrentRework, out SupportOut.InfoTimers,
                out SupportOut.ComponentParams, out SupportOut.CurrentHolds, out SupportOut.Bins);
            Hist = mPromiseManager.lotQuery_HistListByStep(mUserId, mPassword, LotNumber, "1", "5", null, null);
        }
        #endregion

        #region Archive

        /// <summary>
        /// Get lots based on query built in the abstact class
        /// </summary>
        /// <param name="query">Query strings</param>
        /// <param name="SelectedLots">Lots to return</param>
        /// <param name="futureStepQuantity">Number of steps to look ahead</param>
        /// <param name="percent">Number of completed lot queries as a percentage</param>
        /// <returns>True if successful</returns>
        /// <remarks>Depreciated</remarks>
        protected override bool GetLots(string query, out Lot[] SelectedLots, string futureStepQuantity, out int percent)
        {
            percent = 0;
            SelectedLots = null;

            try
            {
                RecpSetRow[] ig16;
                MitmSetRow[] ig18;
                MatrSetRow[] ig19;
                PiarSetRow[] ig20;
                FutaSetRow[] ig21;
                FutaMapRow[] ig22;
                LotInfoStepCmdAllowedRow[] ig23;
                InfoTextRow[] ig24;

                WipLotsRow[] LotsOfInfo = mPromiseManager.getWipLots(mUserId, mPassword, query, null);
                if (LotsOfInfo.GetLength(0) != 0)
                {
                    int NumLots = LotsOfInfo.GetLength(0);
                    SelectedLots = new Lot[NumLots];
                    for (int iLoop = 0; iLoop < NumLots + 1; ++iLoop)
                    {
                        if (LotsOfInfo[iLoop].state != "SCHED" && LotsOfInfo[iLoop].state != "COMPl")
                        {
                            Lot TmpLot = new Lot(LotsOfInfo[iLoop].lotId);
                            TmpLot.mProcedure.CurrentProcedure = LotsOfInfo[iLoop].curPrcdId;
                            TmpLot.mProcedure.CurrentProcedure = TmpLot.mProcedure.CurrentProcedure.Replace(" ", "");
                            TmpLot.mMaterial.PartID = LotsOfInfo[iLoop].partId;
                            TmpLot.mMaterial.PartID = TmpLot.mMaterial.PartID.Replace(" ", "");
                            TmpLot.mSchedule.Priority = LotsOfInfo[iLoop].priority.Trim();
                            TmpLot.mSchedule.LastChange = LotsOfInfo[iLoop].queueTime.Trim();
                            TmpLot.mProcedure.RecipeID = LotsOfInfo[iLoop].recpId;
                            TmpLot.mProcedure.Stage = LotsOfInfo[iLoop].stage;
                            TmpLot.mState.CurrentState = LotsOfInfo[iLoop].state;
                            TmpLot.mState.Location = LotsOfInfo[iLoop].location;
                            TmpLot.mEquipment.EquipmentID = LotsOfInfo[iLoop].eqpId.Trim();
                            TmpLot.mMaterial.CurrentQuantity = LotsOfInfo[iLoop].curMainQty.Trim();

                            ActlSetRow[] temp = mPromiseManager.lotQuery_Multistep(mUserId, mPassword, LotsOfInfo[iLoop].lotId, futureStepQuantity, null, out ig16, out ig18, out ig19, out ig20, out ig21, out ig22, out ig23, out ig24);
                            foreach (ActlSetRow Rec in temp)
                            {
                                Lot.Recipe resp = new Lot.Recipe();
                                resp.ID = Rec.recpid;
                                resp.Location = Rec.location;
                                resp.State = Rec.state;
                                resp.Stage = Rec.stage;
                                TmpLot.mProcedure.FutureRecipe.Add(resp);
                            }
                            TmpLot.mSchedule.LotType = temp[0].lottype;
                            LotBasic bas;
                            LotExtended ext;
                            PrintInfo(LotsOfInfo[iLoop].lotId, out bas, out ext);
                            TmpLot.mSchedule.ReqDate = ext.reqdtime;
                            TmpLot.mSchedule.ReqDate = TmpLot.mSchedule.ReqDate.Remove(TmpLot.mSchedule.ReqDate.Length - 12);
                            TmpLot.mSchedule.ReqDate = TmpLot.mSchedule.ReqDate.Trim();
                            //if (DateTime.TryParse(LotsOfInfo[iLoop].queueTime, out tmp)) TmpLot.QueueTime = tmp;
                            SelectedLots[iLoop] = TmpLot;
                        }
                    }
                }    // Successful but no lots in this location
                return true;
            }
            catch (System.Web.Services.Protocols.SoapException SoapErr)
            {
                MessageBox.Show(SoapErr.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        /// <summary>
        /// Get information about future stages for a lot
        /// </summary>
        /// <param name="lot">Lot id</param>
        /// <param name="processCount">Number of steps to look ahead</param>
        /// <param name="detailLot">Detailed lot object</param>
        /// <returns>True if successful</returns>
        /// <remarks>Depreciated</remarks>
        public override bool GetFutureRecp(string lot, int processCount, out Lot detailLot)
        {
            detailLot = new Lot(lot);
            try
            {
                RecpSetRow[] ig16;
                MitmSetRow[] ig18;
                MatrSetRow[] ig19;
                PiarSetRow[] ig20;
                FutaSetRow[] ig21;
                FutaMapRow[] ig22;
                LotInfoStepCmdAllowedRow[] ig23;
                InfoTextRow[] ig24;
                ActlSetRow[] temp = mPromiseManager.lotQuery_Multistep(mUserId, mPassword, lot, processCount.ToString(), null, out ig16, out ig18, out ig19, out ig20, out ig21, out ig22, out ig23, out ig24);
                return true;
            }
            catch (Exception e)
            {
                var message = System.Windows.Forms.MessageBox.Show("Error occured adding components\n" + e.Message, "Error",
                       System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                return false;
            }
        }

        /// <summary>
        /// Get information about a given DCOP
        /// </summary>
        /// <param name="username">Promis Username</param>
        /// <param name="password">Promis Password</param>
        /// <param name="lotID">Lot ID</param>
        /// <param name="dcOpname">DCOP id</param>
        /// <param name="DCOPInfo">Dcop information</param>
        /// <returns>True if successful</returns>
        /// <remarks>Deprecated</remarks>
        override public bool GetDCOP(string username, string password, string lotID, string dcOpname, out Dictionary<string, string> DCOPInfo)
        {
            char[] SplitChars = { ';' };
            string entstatedata, entstateredo, enstatus, enstatetype, enstatewarn, enstatewhen;
            Test T1;
            TresDimensionsRow[] TDR;
            CalcSetRow[] CSR;
            TestCompTableRow[] TCTR;
            TestItemTableRow[] TITR;
            TestParmTableRow[] TPTR;
            TestSiteTableRow[] TSTR;
            TestAvailCompTableRow[] TACTR;
            TresDresTableRow[] TDres;
            TresDataTableRow[] mTDTR;
            TresComponentsRow[] mTCR;
            Tres mDCOPInfo = new Tres();
            DCOPInfo = new Dictionary<string, string>();

            // lotQuery_HistListByStep with a value of 1 for StartSTep will return all steps for the selected lot
            // Need to look through Events.EvType for "ETST"  & Events.evVariants starts with required DCOpName
            // Events.evVariant contains the timestamp for the DCOp
            // Use this timestamp to search tstShowData for the entry for this data.
            // Data will be in one of the output parametrs bdepending up DCOp type. 
            bool found = false;
            // Need to replace the user details 
            try
            {
                //if (mService == null) OpenPromisConnection();

                LotQuery_HistListByStepOutputElementRow[] DOut = mPromiseManager.lotQuery_HistListByStep(username, password, lotID, "1", null, null, null);  // Gets all entries, in time reverse order, in history file starting a record 1 - Current one

                if (DOut != null)
                {

                    for (int iCnt = 0; iCnt < DOut.GetLength(0); ++iCnt)
                    {
                        LotQuery_HistListByStepOutputElementRow ThisRecord = DOut[iCnt];
                        for (int iEventLoop = 0; iEventLoop < ThisRecord.events.GetLength(0); ++iEventLoop)
                        {
                            if (ThisRecord.events[iEventLoop].evType.ToUpper() == "ETST")
                            {
                                System.Diagnostics.Debug.WriteLine(ThisRecord.events[iEventLoop].evVariant);
                                if (ThisRecord.events[iEventLoop].evVariant.Substring(0, dcOpname.Length) == dcOpname)
                                {
                                    string SearchRecordDate = ThisRecord.events[iEventLoop].evVariant.Substring(ThisRecord.events[iEventLoop].evVariant.IndexOf(';') + 1, 20); //ThisOne.events[iEventLoop].evVariant.Split(SplitChars)[1];
                                    // This is the record we're looking for  
                                    mDCOPInfo = mPromiseManager.tstShow_Data(username, password, dcOpname, SearchRecordDate, null, null,
                                        out T1, out TDR, out mTCR, out CSR, out entstatedata, out entstateredo, out enstatus, out enstatetype,
                                        out enstatewarn, out enstatewhen, out TCTR, out TITR, out TPTR, out TSTR, out TACTR, out mTDTR, out TDres);

                                    found = true;
                                    //var message = System.Windows.Forms.MessageBox.Show("Congratz, a DCOP has been found");
                                    break;
                                }
                            }
                        }
                        if (found) break;

                    }

                    System.Diagnostics.Debug.WriteLine("Found some records");

                }


                foreach (var prop in mDCOPInfo.GetType().GetProperties())
                {
                    if (prop != null)
                    {
                        DCOPInfo.Add(prop.ToString().Remove(0, 14), prop.GetValue(mDCOPInfo, null).ToString());
                        Console.WriteLine(prop.ToString().Remove(0, 14) + " = " + prop.GetValue(mDCOPInfo, null).ToString());
                    }
                }
                return found;
            }
            catch (DropFileException NormalError)
            {
                throw NormalError;
            }
            catch (Exception OopsErr)
            {
                throw new DropFileException("Unexpected Errr", OopsErr);
            }

        }

        /// <summary>
        /// Log file location
        /// </summary>
        private string LegacyLogFile;
        /// <summary>
        /// Legacy initilisation to allow for PROMIS to control the log file
        /// </summary>
        /// <param name="UserID">Promis user name</param>
        /// <param name="Password">PROMIS password</param>
        /// <param name="LogFile">Logfile location</param>
        /// <returns>True if successful</returns>
        override public bool Init(string UserID, string Password, string LogFile)
        {
            SupportInfo = new PromisLotSupportData();
            mPromiseManager = new CTestFullPromisService();
            mUserId = UserID;
            mPassword = Password;
            LegacyLogFile = LogFile;
            return true;
        }

        #region Label Printer
        private ArrayList mAllFields = new ArrayList();
        Tres mDCOPInfo;
        TresDataTableRow[] mTDTR;// SUPER BAD WAY OF STORING DCOP DATA
        TresComponentsRow[] mTCR; //dcop information is stored here then used later at very specific times. THIS IS BAD, BUT I NEEDED IT QUICKLY
        LotBasic CurrentState;
        LotExtended ExtCurrentState;
        /// <summary>
        /// List of lot information
        /// </summary>
        /// <returns>All lot information collected</returns>
        public override ArrayList GetLabels()
        {
            return mAllFields;
        }

        /// <summary>
        /// Massive Lot query function
        /// </summary>
        /// <param name="lotID">Lot ID</param>
        /// <returns>true if successful</returns>
        /// <remarks>Depricated, only for use with Label printer</remarks>
        override public bool GetPromiseLot(string lotID)
        {
            string[] lotFields = new string[2];
            mAllFields = new ArrayList();
            lotFields[0] = "lotID";
            lotFields[1] = lotID.ToUpper();
            Console.WriteLine(lotFields[0] + " = " + lotFields[1]);
            mAllFields.Add(lotFields);


            try
            {
                CurrentState = mPromiseManager.lotQuery_LotBasic(mUserId, mPassword, lotID, null, null, null, null, SupportInfo.CmdList, null, out SupportInfo.AvailEqpt, out SupportInfo.DCOps, out SupportInfo.ComponentsList, out SupportInfo.Rejects, out SupportInfo.LotOps,
                        out SupportInfo.HistoryEvents, out SupportInfo.StepNotes, out SupportInfo.CmdAllowed, out SupportInfo.FutureHolds);
                ExtCurrentState = mPromiseManager.lotQuery_LotExtended(mUserId, mPassword, lotID, null, null, null, null, null, out SupportInfo.Rework, out SupportInfo.Parameters, out SupportInfo.FutureSet, out SupportInfo.PrcdStack,
                       out SupportInfo.CurrentRework, out SupportInfo.InfoTimers, out SupportInfo.ComponentParams, out SupportInfo.CurrentHolds, out SupportInfo.Bins);

                //itterate through lot basic adding data to all fields
                foreach (var prop in CurrentState.GetType().GetProperties())
                {
                    string[] tmpFields = new string[2];
                    if (prop != null)
                    {
                        tmpFields = new string[2];
                        tmpFields[0] = prop.ToString().Remove(0, 14);
                        tmpFields[1] = prop.GetValue(CurrentState, null).ToString();
                        //Console.WriteLine(tmpFields[0] + " = " + tmpFields[1]);

                        mAllFields.Add(tmpFields);

                    }
                    if (prop.ToString().Contains("partid") && prop.GetValue(CurrentState, null).ToString().Contains('-'))
                    {
                        string[] mskFields = new string[2];
                        string removeType = prop.GetValue(CurrentState, null).ToString();
                        string[] splitter = removeType.Split('-');

                        //check if part id contains "C75" prefix
                        if (prop.GetValue(CurrentState, null).ToString().Contains("C75"))
                        {
                            string mask = splitter[1] + "-OR" + splitter[2];
                            splitter = mask.Split('.');
                            mask = splitter[0];
                            if (mask[mask.Length - 1].ToString().ToUpper() == "X")
                                mask = mask.Remove(mask.Length - 1);

                            mskFields[0] = "MaskID";
                            mskFields[1] = mask;
                        }
                        else
                        {
                            string mask = splitter[0] + "-OR" + splitter[2];
                            splitter = mask.Split('.');
                            mask = splitter[0];
                            if (mask[mask.Length - 1].ToString().ToUpper() == "X")
                                mask = mask.Remove(mask.Length - 1);

                            mskFields[0] = "MaskID";
                            mskFields[1] = mask;
                        }
                        // Console.WriteLine(mskFields[0] + " = " + mskFields[1]);
                        mAllFields.Add(mskFields);
                    }
                }
                //itterate through lot extended adding data to all fields
                foreach (var prop in ExtCurrentState.GetType().GetProperties())
                {
                    if (prop != null)
                    {
                        string[] tmpFields = new string[2];
                        tmpFields[0] = prop.ToString().Remove(0, 14);
                        tmpFields[1] = prop.GetValue(ExtCurrentState, null).ToString();
                        Console.WriteLine(tmpFields[0] + " = " + tmpFields[1]);
                        mAllFields.Add(tmpFields);
                    }
                }

                //detailed Wafer information is found here
                //this has the full history of a lot so we can loop through this to find out everything that has ever happened to a lot
                //which is nice
                LotQuery_HistListByStepOutputElementRow[] tmpHist = mPromiseManager.lotQuery_HistListByStep(mUserId, mPassword, lotID, "1", null, null, null);
                LotQuery_HistListByStepOutputElementRow workingStep = null;
                LotQuery_HistListByStepOutputElementRow solidStep = null;
                bool waveFound = false;
                bool found = false;
                try
                {
                    for (int i = tmpHist.Length - 1; i > -1; i--)
                    {
                        workingStep = tmpHist[i];
                        if (workingStep.hist.partid.ToString().Contains('-') && waveFound == false)
                        {
                            string[] wavebandField = new string[2];
                            wavebandField[0] = "waveBand";
                            //wavebandField[1] = solidStep.hist.partid.ToString();
                            string[] splitter = workingStep.hist.partid.ToString().Split('-');
                            wavebandField[1] = splitter[1] + "-Band";
                            waveFound = true;
                            Console.WriteLine(wavebandField[0] + " = " + wavebandField[1]);
                            mAllFields.Add(wavebandField);
                        }
                        for (int j = 0; j < workingStep.events.Length; j++) //loop through all the steps to find the one where the lot was created
                        {
                            if (workingStep.events[j].evTypeExtern.ToUpper() == "CREATELOT" ||
                                workingStep.events[j].evType.ToString() == "MIDC")
                            {
                                solidStep = workingStep;
                                Console.WriteLine(workingStep.events[j].evVariant.ToString());
                                found = true;
                            }
                            if (found == true)
                                break;
                        }
                    }
                }
                catch
                {
                }
                //found = true;
                int compNum = 0;
                if (found == true)
                {
                    #region get growth materials from full history
                    if (GetDCOP(mUserId, mPassword, lotID, "CAW02.04"))
                    {

                        for (int i = 0; i < mTCR.Length; i++)
                        {
                            //wafer ID
                            string[] tmpFields = new string[2];
                            tmpFields[0] = "components" + i.ToString();
                            tmpFields[1] = mTCR[i].compIds;
                            Console.WriteLine(tmpFields[0] + " = " + tmpFields[1]);
                            mAllFields.Add(tmpFields);

                            //Wafer notation (3 numbers
                            string[] tmpFields2 = new string[2];
                            string[] SplitComponents = tmpFields[1].Split(new string[] { "." }, System.StringSplitOptions.None);
                            tmpFields2[0] = "waferID" + i.ToString();
                            tmpFields2[1] = SplitComponents[1];
                            Console.WriteLine(tmpFields2[0] + " = " + tmpFields2[1]);
                            mAllFields.Add(tmpFields2);

                            //wafer Letter
                            string[] tmpFields3 = new string[2];
                            tmpFields3[0] = "waferLetter" + i.ToString();
                            tmpFields3[1] = mTDTR[i].item.Trim();
                            Console.WriteLine(tmpFields3[0] + " = " + tmpFields3[1]);
                            mAllFields.Add(tmpFields3);
                        }
                    }
                    else
                    {
                        try
                        {
                            if (solidStep != null)
                            {
                                List<string> varient = new List<string>();
                                for (int j = 0; j < solidStep.events.Length; j++)
                                {
                                    //gets all the ad material steps in slot start, this is where we get the information for the following voodoo
                                    if (solidStep.events[j].evType == "MIDC")
                                    {
                                        varient.Add(solidStep.events[j].evVariant);
                                    }
                                }
                                int count = 0;
                                foreach (string var in varient)
                                {

                                    string[] splitter = var.Split(new string[] { "&#7;" }, System.StringSplitOptions.None);
                                    //"SU048641.136   &#7;CMZ07L5430.1 &#7;CCAL00568.1  &#7; &#7; &#7; &#7;           0" 

                                    //wafer ID
                                    string[] tmpFields = new string[2];
                                    tmpFields[0] = "components" + count.ToString();
                                    tmpFields[1] = splitter[0].Trim();
                                    Console.WriteLine(tmpFields[0] + " = " + tmpFields[1]);
                                    mAllFields.Add(tmpFields);

                                    //Wafer notation (3 numbers
                                    string[] tmpFields2 = new string[2];
                                    string[] SplitComponents = tmpFields[1].Split(new string[] { "." }, System.StringSplitOptions.None);
                                    tmpFields2[0] = "waferID" + count.ToString();
                                    tmpFields2[1] = SplitComponents[1];
                                    Console.WriteLine(tmpFields2[0] + " = " + tmpFields2[1]);
                                    mAllFields.Add(tmpFields2);

                                    //wafer Letter
                                    string[] tmpFields3 = new string[2];
                                    tmpFields3[0] = "waferLetter" + count.ToString();
                                    tmpFields3[1] = Utilities.NumberToLetter(count + 1);
                                    Console.WriteLine(tmpFields3[0] + " = " + tmpFields3[1]);
                                    mAllFields.Add(tmpFields3);

                                    count++;
                                }
                            }

                        }
                        catch
                        {
                            //var message = System.Windows.Forms.MessageBox.Show("ERROR material details\n" + e.Message, "Lot Error", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                        }
                    }

                    if (GetDCOP(mUserId, mPassword, lotID, "CAW02.05")) //getting growth number here doesnt always work
                    {
                        for (int i = 0; i < SupportInfo.ComponentsList.Length - 1; i++)
                        {
                            //growth
                            string[] tmpFields0 = new string[2];
                            tmpFields0[0] = "GrowthNumber" + compNum.ToString();
                            tmpFields0[1] = mTDTR[i].item.Trim();
                            Console.WriteLine(tmpFields0[0] + " = " + tmpFields0[1]);
                            mAllFields.Add(tmpFields0);
                        }
                    }
                    else
                    {
                        try
                        {
                            if (solidStep != null)
                            {
                                List<string> varient = new List<string>();
                                for (int j = 0; j < solidStep.events.Length; j++)
                                {
                                    //gets all the ad material steps in slot start, this is where we get the information for the following voodoo
                                    if (solidStep.events[j].evType == "ADMA")
                                    {
                                        varient.Add(solidStep.events[j].evVariant);
                                    }
                                }
                                int count = 0;
                                foreach (string var in varient)
                                {
                                    //basically voodoo that turns wafer ID into a growth number
                                    //hope that this never breaks
                                    string[] tmpFields0 = new string[2];
                                    tmpFields0[0] = "GrowthNumber" + count.ToString();
                                    int split = var.IndexOf('&');
                                    string validation = var.Remove(split);
                                    tmpFields0[1] = GrowthNumberVoodoo(validation);
                                    Console.WriteLine(tmpFields0[0] + " = " + tmpFields0[1]);
                                    mAllFields.Add(tmpFields0);
                                    count++;
                                }
                            }
                        }
                        catch
                        {
                            //var message = System.Windows.Forms.MessageBox.Show("ERROR getting growth numbers\n" + e.Message, "Lot Error", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                        }
                    }
                    #endregion

                    #region get Rx materials

                    try
                    {
                        if (solidStep != null)
                        {
                            int counter = 0;
                            for (int i = 0; i < solidStep.events.Length; i++)
                            {
                                if (solidStep.events[i].evType == "ADMA")
                                {
                                    string value = solidStep.events[i].evVariant.Split(new string[] { "&#7;" }, System.StringSplitOptions.None)[0];
                                    if (value.Contains("STX"))
                                    {
                                        string[] STXstring = new string[2];
                                        STXstring[0] = "TXValue" + counter.ToString();
                                        STXstring[1] = value;
                                        Console.WriteLine(STXstring[0] + " = " + STXstring[1]);
                                        mAllFields.Add(STXstring);
                                        counter++;
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        string test = ex.Message;
                    }

                    #endregion
                }
                else
                {
                    #region get materials from local history

                    int count = 0;
                    foreach (HistEventsRow events in SupportInfo.HistoryEvents)
                    {
                        if (events.evType == "MIDC")
                        {
                            string[] splitter = events.evVariant.Split(new string[] { "&#7;" }, System.StringSplitOptions.None);
                            //"SU048641.136   &#7;CMZ07L5430.1 &#7;CCAL00568.1  &#7; &#7; &#7; &#7;           0" 

                            //wafer ID
                            string[] tmpFields = new string[2];
                            tmpFields[0] = "components" + count.ToString();
                            tmpFields[1] = splitter[0].Trim();
                            Console.WriteLine(tmpFields[0] + " = " + tmpFields[1]);
                            mAllFields.Add(tmpFields);

                            //Wafer notation (3 numbers
                            string[] tmpFields2 = new string[2];
                            string[] SplitComponents = tmpFields[1].Split(new string[] { "." }, System.StringSplitOptions.None);
                            tmpFields2[0] = "waferID" + count.ToString();
                            tmpFields2[1] = SplitComponents[1];
                            Console.WriteLine(tmpFields2[0] + " = " + tmpFields2[1]);
                            mAllFields.Add(tmpFields2);

                            //wafer Letter
                            string[] tmpFields3 = new string[2];
                            tmpFields3[0] = "waferLetter" + count.ToString();
                            tmpFields3[1] = Utilities.NumberToLetter(count + 1);
                            Console.WriteLine(tmpFields3[0] + " = " + tmpFields3[1]);
                            mAllFields.Add(tmpFields3);

                            count++;
                        }

                    }

                    System.Windows.Forms.MessageBox.Show("There was an error getting some material information!\nSome data will need to be entered manually\nSorry for the inconvenience");
                    #endregion
                }

                int RXcounter = 0;
                foreach (LotQuery_HistListByStepOutputElementRow hi in tmpHist)
                {
                    foreach (HistEventsRow hevent in hi.events)
                    {
                        if (hevent.evType == "ADMA")
                        {
                            string value = hevent.evVariant.Split(new string[] { "&#7;" }, System.StringSplitOptions.None)[0];
                            if (value.Contains("SRG"))
                            {
                                string[] RXstring = new string[2];
                                RXstring[0] = "RXValue" + RXcounter.ToString();
                                RXstring[1] = value;
                                Console.WriteLine(RXstring[0] + " = " + RXstring[1]);
                                mAllFields.Add(RXstring);
                                RXcounter++;
                            }
                        }
                    }
                }

                //GetDCOP(mUserID, mPassword, lotID, "CAW02.04");
            }
            catch (System.Web.Services.Protocols.SoapException SoapError)
            {
                var message = System.Windows.Forms.MessageBox.Show("Lot not found\n Please check the lot ID\n" + SoapError.Message, "Lot Error"
                    , System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Returns DCOP information
        /// </summary>
        /// <param name="username">Promis username</param>
        /// <param name="password">PROMIS password</param>
        /// <param name="lotID">Lot ID</param>
        /// <param name="dcOpname">DCOP ID</param>
        /// <returns>True if successful</returns>
        /// <remarks>Not used, not even sure if it works</remarks>
        override public bool GetDCOP(string username, string password, string lotID, string dcOpname)
        {
            char[] SplitChars = { ';' };
            string entstatedata, entstateredo, enstatus, enstatetype, enstatewarn, enstatewhen;
            Test T1;
            TresDimensionsRow[] TDR;
            CalcSetRow[] CSR;
            TestCompTableRow[] TCTR;
            TestItemTableRow[] TITR;
            TestParmTableRow[] TPTR;
            TestSiteTableRow[] TSTR;
            TestAvailCompTableRow[] TACTR;
            TresDresTableRow[] TDres;

            // lotQuery_HistListByStep with a value of 1 for StartSTep will return all steps for the selected lot
            // Need to look through Events.EvType for "ETST"  & Events.evVariants starts with required DCOpName
            // Events.evVariant contains the timestamp for the DCOp
            // Use this timestamp to search tstShowData for the entry for this data.
            // Data will be in one of the output parametrs bdepending up DCOp type. 
            bool found = false;
            // Need to replace the user details 
            try
            {
                //if (mService == null) OpenPromisConnection();

                LotQuery_HistListByStepOutputElementRow[] DOut = mPromiseManager.lotQuery_HistListByStep(username, password, lotID, "1", null, null, null);  // Gets all entries, in time reverse order, in history file starting a record 1 - Current one

                if (DOut != null)
                {

                    for (int iCnt = 0; iCnt < DOut.GetLength(0); ++iCnt)
                    {
                        LotQuery_HistListByStepOutputElementRow ThisRecord = DOut[iCnt];
                        for (int iEventLoop = 0; iEventLoop < ThisRecord.events.GetLength(0); ++iEventLoop)
                        {
                            if (ThisRecord.events[iEventLoop].evType.ToUpper() == "ETST")
                            {
                                System.Diagnostics.Debug.WriteLine(ThisRecord.events[iEventLoop].evVariant);
                                if (ThisRecord.events[iEventLoop].evVariant.Substring(0, dcOpname.Length) == dcOpname)
                                {
                                    string SearchRecordDate = ThisRecord.events[iEventLoop].evVariant.Substring(ThisRecord.events[iEventLoop].evVariant.IndexOf(';') + 1, 20); //ThisOne.events[iEventLoop].evVariant.Split(SplitChars)[1];
                                    // This is the record we're looking for  
                                    mDCOPInfo = mPromiseManager.tstShow_Data(username, password, dcOpname, SearchRecordDate, null, null,
                                        out T1, out TDR, out mTCR, out CSR, out entstatedata, out entstateredo, out enstatus, out enstatetype,
                                        out enstatewarn, out enstatewhen, out TCTR, out TITR, out TPTR, out TSTR, out TACTR, out mTDTR, out TDres);

                                    found = true;
                                    //var message = System.Windows.Forms.MessageBox.Show("Congratz, a DCOP has been found");
                                    break;
                                }
                            }
                        }
                        if (found) break;

                    }

                    System.Diagnostics.Debug.WriteLine("Found some records");

                }

                return found;
            }
            catch (DropFileException NormalError)
            {
                throw NormalError;
            }
            catch (Exception OopsErr)
            {
                throw new DropFileException("Unexpected Errr", OopsErr);
            }

        }

        /// <summary>
        /// Takes the component id and works out the growth number from in
        /// </summary>
        /// <param name="input">Component ID</param>
        /// <returns>Growth number</returns>
        private string GrowthNumberVoodoo(string input)
        {
            string output;

            string validation = input;
            var edit = validation.IndexOfAny("0123456789".ToArray());
            validation = validation.Remove(0, edit);
            edit = validation.IndexOfAny("ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToArray());
            var end = Int32.Parse(validation.Remove(edit));
            validation = validation.Remove(0, edit);
            if (end.Equals(10))
            {
                string tmp = "10";
                end = Int32.Parse(tmp);
            }
            output = validation.Remove(validation.Length - 1, 1) + end;
            output = output.Replace('.', '-');

            return output;
        }

        /// <summary>
        /// Run adhoc rework for the entire lot
        /// </summary>
        /// <param name="inLot">Lot to rework</param>
        /// <param name="identifiedReworks">List of identified materials to be reworked with corresponding rework reasons </param>
        /// <param name="unIdentifiedReworks">List of unidentified materials to be reworked with corresponding rework reasons</param>
        /// <param name="recoveryProcedure">Procedure stack containing the recovery procedure (cannot be empty?)</param>
        /// <param name="comment">Comment to add to the lot</param>
        /// <param name="err">Error Message</param>
        /// <returns>True if successful</returns>
        public override bool ReworkLot(Lot inLot, List<string[]> identifiedReworks, List<string[]> unIdentifiedReworks, string recoveryProcedure, string comment, out string err)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Run special branch rework for the entire lot
        /// </summary>
        /// <param name="inLot">Lot to rework</param>
        /// <param name="identifiedReworks">List of identified materials to be reworked with corresponding rework reasons </param>
        /// <param name="unIdentifiedReworks">List of unidentified materials to be reworked with corresponding rework reasons</param>
        /// <param name="recoveryProcedure">Procedure stack containing the recovery procedure (cannot be empty?)</param>
        /// <param name="comment">Comment to add to the lot</param>
        /// <param name="reworkRunLot">Bool flag (T or F) whether the transaction should recover if the lot is running</param>
        /// <param name="removeFutureAction">Boolean flag. If it is set to T, the command removes all scheduled future actions before starting the recovery. If it is set to F (the default), the command fails if any future actions are scheduled.</param>
        /// <param name="startProcStack">A procedure stack that represents the first instruction of the recovery procedure.</param>
        /// <param name="finishProcStack">A procedure stack that represents the last instruction of the recovery procedure</param>
        /// <param name="resumeProcStack">A procedure stack that represents the starting instruction after the recovery is finished.</param>
        /// <param name="err">Error Message</param>
        /// <returns>True if successful</returns>
        public override bool ReworkLot(Lot inLot, List<string[]> identifiedReworks, List<string[]> unIdentifiedReworks, string recoveryProcedure, string comment, string reworkRunLot, string removeFutureAction, string startProcStack, string finishProcStack, string resumeProcStack, out string err)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Lot Start

        LotNumListRow[] Tmp = new LotNumListRow[1];
        CommandListRow[] Tmp2 = new CommandListRow[2];

        /// <summary>
        /// Schedules a new Lot
        /// </summary>
        /// <param name="LotPartNumber">Lot Part</param>
        /// <param name="LotType">Production or engineering lot</param>
        /// <param name="Priority">Lot Priority</param>
        /// <param name="Comment">Overall lot comment</param>
        /// <param name="StartSize">Number of wafer in Lot</param>
        /// <param name="StartDate">Lot start date</param>
        /// <param name="EndDate">Expected finish date</param>
        /// <param name="Cr1">Customer reference 1</param>
        /// <param name="Cr2">Customer reference 2</param>
        /// <param name="Cr3">Customer reference 3</param>
        /// <param name="CustomerName">Customer name</param>
        /// <param name="CustomerOrderNo">Customer order number</param>
        /// <param name="Engineer">Part managing engineer</param>
        /// <param name="NoOfLots">Nuber of lots with these specifications to start</param>
        /// <param name="LotNumber">Id of the new lot</param>
        /// <param name="Source">Source lot</param>
        /// <returns>True if successful</returns>
        override public bool ScheduleLot(string LotPartNumber, string LotType, string Priority, string Comment, string StartSize, string StartDate,
            string EndDate, string Cr1, string Cr2, string Cr3, string CustomerName, string CustomerOrderNo, string Engineer, string NoOfLots,
            string LotNumber, string Source)
        {
            try
            {
                Tmp[0] = new LotNumListRow();
                Tmp[0].lotNum = LotNumber;

                List<string> comments = Utilities.SplitLines(Comment, 50);
                CommentListRow[] promComment = new CommentListRow[comments.Count];

                for (int i = 0; i < promComment.Length; i++)
                {
                    promComment[i] = new CommentListRow();
                    promComment[i].comment = comments[i];
                }

                mPromiseManager.lotStart_Schedule(mUserId, mPassword, LotPartNumber, LotType, StartDate, Priority, EndDate, null, null, null,
                    StartSize, Engineer, null, comments[0], CustomerName, CustomerOrderNo, Cr1, Cr2, Cr3, NoOfLots, Tmp, promComment, out mInfo);
                if (mInfo != null)
                {
                    foreach (var prob in mInfo)
                    {
                        Console.WriteLine(prob.text);
                    }
                }

                LotQuery_HistListByStepOutputElementRow[] tmpHist = mPromiseManager.lotQuery_HistListByStep(mUserId, mPassword, LotNumber + ".1", "1", null, null, null);
                var message = System.Windows.Forms.MessageBox.Show("Lot Created with ID " + LotNumber + ".1", "Success", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
                System.IO.StreamWriter file = new System.IO.StreamWriter(LegacyLogFile, true);
                file.WriteLine("Lot Created with ID " + LotNumber + ".1");
                file.Close();
                mCurrentLot = LotNumber + ".1";
                return true;
            }
            catch (Exception e)
            {
                if (e.Message.Contains("There is no such user as"))
                {
                    string lines = e.Message;
                    System.IO.StreamWriter file = new System.IO.StreamWriter(LegacyLogFile, true);
                    file.WriteLine(lines);
                    file.Close();
                    var message = System.Windows.Forms.MessageBox.Show("No user with this ID exists\nPlease try another Engineer", "Error",
                        System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                    return false;
                }
                else if (e.Message.Contains("exists"))
                {
                    string lines = e.Message;
                    System.IO.StreamWriter file = new System.IO.StreamWriter(LegacyLogFile, true);
                    file.WriteLine(lines);
                    file.Close();
                    var message = System.Windows.Forms.MessageBox.Show("Lot Number already in use\nPlease try another Lot Number", "Error",
                        System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                    return false;
                }
                else if (e.Message.Contains("is not valid for command"))
                {
                    string lines = e.Message;
                    System.IO.StreamWriter file = new System.IO.StreamWriter(LegacyLogFile, true);
                    file.WriteLine(lines);
                    file.Close();
                    var message = System.Windows.Forms.MessageBox.Show("Lot created, but failed to add Components", "Error",
                        System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    return false;
                }
                else if (e.Message.Contains("xxxxxx.partId"))
                {
                    string lines = e.Message;
                    System.IO.StreamWriter file = new System.IO.StreamWriter(LegacyLogFile, true);
                    file.WriteLine(lines);
                    file.Close();
                    var message = System.Windows.Forms.MessageBox.Show("Please enter a Lot ID", "Error",
                        System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    return false;
                }
                else
                {
                    string lines = e.Message;
                    System.IO.StreamWriter file = new System.IO.StreamWriter(LegacyLogFile, true);
                    file.WriteLine(lines);
                    file.Close();
                    var message = System.Windows.Forms.MessageBox.Show("Unknown error\nPlease check that the information is valid\n" + e.Message, "Error",
                       System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    return false;
                }
            }
        }

        /// <summary>
        /// Start a scheduled Lot
        /// </summary>
        /// <param name="LotNumber">Lot ID to start</param>
        /// <param name="mats">List of Part ids of the consumed wafers</param>
        /// <param name="lots">List of lot ids that the consumed wafers come from</param>
        /// <param name="ids">List of consumed wafer ids</param>
        /// <param name="letters">List of letters to identify the wafers on the first step</param>
        /// <param name="TrkIn">Equipment to track into(usually CINPGROW)</param>
        /// <param name="TrkOut">Equipment to track out to (not used)</param>
        /// <returns>True if successful</returns>
        public override bool StartLot(string LotNumber, string[] mats, string[] lots, string[] ids, string[] letters, string TrkIn, string TrkOut)
        {
            try
            {

                FutaSetRow[] ignore;
                ActlSetRow[] ignore2;
                LotBasic Basic;
                LotExtended Extended;
                MatConListRow[] SourceLots = null;
                Dictionary<string, List<string>> sources = new Dictionary<string, List<string>>();

                var distinctLot = lots.Distinct().ToArray();
                SourceLots = new MatConListRow[distinctLot.Length];


                for (int i = 0; i < distinctLot.Length; i++)
                {
                    SourceLots[i] = new MatConListRow();
                    SourceLots[i].lotId = distinctLot[i];
                    List<string> lookdown = new List<string>();
                    for (int j = 0; j < lots.Length; j++)
                    {
                        if (lots[j] == distinctLot[i])
                        {
                            lookdown.Add(ids[j]);
                        }
                        PrintInfo(lots[i], out Basic, out Extended);
                        mats[i] = Basic.partid;
                    }
                    sources.Add(distinctLot[i], lookdown);
                    //SourceLots[i].qty = "0";
                    //SourceLots[i].uom = "";
                }

                foreach (MatConListRow mat in SourceLots)
                {
                    for (int i = 0; i < lots.Length; i++)
                    {
                        if (mat.lotId == lots[i])
                        {
                            //int conv = Convert.ToInt32(mat.qty);
                            //mat.qty = Convert.ToString(conv += 1);
                            List<string> lookup = sources[lots[i]];
                            MatConCompIdListRow[] temp = new MatConCompIdListRow[lookup.Count];
                            for (int j = 0; j < lookup.Count; j++)
                            {
                                temp[j] = new MatConCompIdListRow();
                                temp[j].compId = lookup[j];
                            }

                            mat.compIdList = temp;
                            mat.partId = mats[i];
                            //mat.uom = "";
                        }
                    }
                }

                CommentListRow[] comments = new CommentListRow[1];
                comments[0] = new CommentListRow();
                comments[0].comment = "Lot " + LotNumber + " started";
                System.IO.StreamWriter file = new System.IO.StreamWriter(LegacyLogFile, true);
                file.WriteLine("Attempting lot Start");
                mPromiseManager.lotStart_New(mUserId, mPassword, LotNumber, "CINPGROW", null, SourceLots, comments, out ignore, out ignore2, out mInfo);

                var message = System.Windows.Forms.MessageBox.Show(SourceLots.Length.ToString() + " components added to " + LotNumber, "Success",
                       System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);

                file.WriteLine(SourceLots.Length.ToString() + " components added to " + LotNumber);
                file.Close();
                if (TrkIn == "CINPGROW")
                {
                    AddLetters(LotNumber, ids, letters, TrkIn, TrkOut);
                }
                return true;

            }
            catch (Exception e)
            {
                string lines = "StartLot()" + e.Data + e.Message;
                System.IO.StreamWriter file = new System.IO.StreamWriter(LegacyLogFile, true);
                file.WriteLine(lines);
                file.Close();
                var message = System.Windows.Forms.MessageBox.Show("Error occured adding components\n" + e.Message, "Error",
                       System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                return false;
            }
        }

        /// <summary>
        /// Add the lettes to a lot during the first dcop
        /// </summary>
        /// <param name="LotNumber">Lot ID to run dcop on</param>
        /// <param name="ids">List of wafer ids</param>
        /// <param name="letters">Lois of identifying labels</param>
        /// <param name="TrkIn">Location to track into</param>
        /// <param name="TrkOut">Location to track out to (not used)</param>
        /// <remarks>This step only happens on some of </remarks>
        public override void AddLetters(string LotNumber, string[] ids, string[] letters, string TrkIn, string TrkOut)
        {
            Lot TmpLot;
            GetLotDetails(LotNumber, out TmpLot);
            try
            {
                CompValueListRow[] Comp = new CompValueListRow[ids.Length];
                for (int i = 0; i < letters.Length; i++)
                {
                    DataValueListRow[] Data = new DataValueListRow[1];
                    Data[0] = new DataValueListRow();
                    Data[0].data = letters[i];
                    Comp[i] = new CompValueListRow();
                    Comp[i].compId = ids[i];
                    Comp[i].data = Data;
                }

                Actl Ig1;
                CalcSetRow[] Ig2;
                TresDresTableRow[] Ig3;
                ChrtTableRow[] Ig4;
                WarningsRow[] Ig5;
                Recp Ig6;
                TallyTaskArrayRow[] Ig7;
                SubLot Ig8;
                ActlSetRow[] Ig9;
                ClassOutGoodBinsRow[] ig10;
                ClassOutBadBinsRow[] ig11;
                FutaSetRow[] ig12;
                RecpSetRow[] ig13;
                FutaSetRow[] ignore;
                ActlSetRow[] ignore2;

                mPromiseManager.lotTrack_IntoStep(mUserId, mPassword, LotNumber, TmpLot.mEquipment.AvaliableEquipment[0].ID, null, null, null, null, TmpLot.mEquipment.AvaliableEquipment[0].Location, null, out Ig6, out Ig7, out mInfo);
                mPromiseManager.tstEnter_LotData(mUserId, mPassword, LotNumber, "1", null, null, null, Comp, null, out Ig1, out Ig2, out Ig3, out Ig4, out Ig5, out mInfo);
                mPromiseManager.lotTrack_OutOfStep(mUserId, mPassword, LotNumber, null, TmpLot.mProcedure.FutureRecipe[0].Location, null, null, null, null, null, out Ig6, out Ig8, out Ig9, out ig10, out ig11, out ignore, out ig12, out ignore2, out ig13, out mInfo);

                var message = System.Windows.Forms.MessageBox.Show("Letters added to added to " + LotNumber + " and moved to C3CNORTH", "Success",
                      System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
            }
            catch
            {
                try
                {
                    CompValueListRow[] Comp = new CompValueListRow[ids.Length];
                    for (int i = 0; i < letters.Length; i++)
                    {
                        DataValueListRow[] Data = new DataValueListRow[2];
                        Data[0] = new DataValueListRow();
                        Data[0].data = letters[i];
                        Data[1] = new DataValueListRow();
                        Data[1].data = Microsoft.VisualBasic.Interaction.InputBox("Please enter the growth number for wafer " + Data[0].data, "Growth Number", "");
                        Comp[i] = new CompValueListRow();
                        Comp[i].compId = ids[i];
                        Comp[i].data = Data;
                    }

                    Actl Ig1;
                    CalcSetRow[] Ig2;
                    TresDresTableRow[] Ig3;
                    ChrtTableRow[] Ig4;
                    WarningsRow[] Ig5;
                    Recp Ig6;
                    SubLot Ig8;
                    ActlSetRow[] Ig9;
                    ClassOutGoodBinsRow[] ig10;
                    ClassOutBadBinsRow[] ig11;
                    FutaSetRow[] ig12;
                    RecpSetRow[] ig13;
                    FutaSetRow[] ignore;
                    ActlSetRow[] ignore2;

                    //mPromiseManager.lotTrack_IntoStep(mUserId, mPassword, LotNumber, TmpLot.mEquipment.AvaliableEquipment[0].ID, null, null, null, null, TmpLot.mEquipment.AvaliableEquipment[0].Location, null, out Ig6, out Ig7, out mInfo);
                    mPromiseManager.tstEnter_LotData(mUserId, mPassword, LotNumber, "1", null, null, null, Comp, null, out Ig1, out Ig2, out Ig3, out Ig4, out Ig5, out mInfo);
                    mPromiseManager.lotTrack_OutOfStep(mUserId, mPassword, LotNumber, null, TmpLot.mProcedure.FutureRecipe[0].Location, null, null, null, null, null, out Ig6, out Ig8, out Ig9, out ig10, out ig11, out ignore, out ig12, out ignore2, out ig13, out mInfo); ;
                    var message = System.Windows.Forms.MessageBox.Show("Letters and growth numbers added to " + LotNumber + " and moved to " + TmpLot.mProcedure.FutureRecipe[0].Location, "Success",
                          System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);

                }
                catch
                {
                    try
                    {
                        DataValueListRow[] Comp = new DataValueListRow[1];
                        Comp[0] = new DataValueListRow();
                        Comp[0].data = Microsoft.VisualBasic.Interaction.InputBox("Scout or Production?", "Enter Data", "").ToUpper();

                        Actl Ig1;
                        CalcSetRow[] Ig2;
                        TresDresTableRow[] Ig3;
                        ChrtTableRow[] Ig4;
                        WarningsRow[] Ig5;
                        Recp Ig6;
                        SubLot Ig8;
                        ActlSetRow[] Ig9;
                        ClassOutGoodBinsRow[] ig10;
                        ClassOutBadBinsRow[] ig11;
                        FutaSetRow[] ig12;
                        RecpSetRow[] ig13;
                        FutaSetRow[] ignore;
                        ActlSetRow[] ignore2;

                        mPromiseManager.tstEnter_LotData(mUserId, mPassword, LotNumber, "1", null, null, Comp, null, null, out Ig1, out Ig2, out Ig3, out Ig4, out Ig5, out mInfo);
                        mPromiseManager.lotTrack_OutOfStep(mUserId, mPassword, LotNumber, null, TmpLot.mProcedure.FutureRecipe[0].Location, null, null, null, null, null, out Ig6, out Ig8, out Ig9, out ig10, out ig11, out ignore, out ig12, out ignore2, out ig13, out mInfo); ;
                        var message = System.Windows.Forms.MessageBox.Show("Scout or production selected " + LotNumber + " and moved to " + TmpLot.mProcedure.FutureRecipe[0].Location, "Success",
                              System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
                    }
                    catch (Exception ex)
                    {
                        string lines = ex.Message;
                        System.IO.StreamWriter file = new System.IO.StreamWriter(LegacyLogFile, true);
                        file.WriteLine(lines);
                        file.Close();
                        try
                        {
                            Recp Ig6;
                            SubLot Ig8;
                            ActlSetRow[] Ig9;
                            ClassOutGoodBinsRow[] ig10;
                            ClassOutBadBinsRow[] ig11;
                            FutaSetRow[] ig12;
                            RecpSetRow[] ig13;
                            FutaSetRow[] ignore;
                            ActlSetRow[] ignore2;
                            mPromiseManager.lotTrack_OutOfStep(mUserId, mPassword, LotNumber, null, TmpLot.mProcedure.FutureRecipe[0].Location, null, null, null, null, null, out Ig6, out Ig8, out Ig9, out ig10, out ig11, out ignore, out ig12, out ignore2, out ig13, out mInfo);

                        }
                        finally
                        {
                        }
                        //var message = System.Windows.Forms.MessageBox.Show(ex.Message, "Error",
                        //      System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    }
                }
            }
            finally
            {
                MessageBox.Show("Initial DCOPs complete");
                System.IO.StreamWriter file = new System.IO.StreamWriter(LegacyLogFile, true);
                file.WriteLine("Initial DCOPs complete");
                file.Close();
            }
        }

        /// <summary>
        /// Get materals that could be consumed by a lot
        /// </summary>
        /// <param name="LotID">Lot ID</param>
        /// <param name="Materials">List of material Ids</param>
        /// <returns>True if successful</returns>
        public override bool GetPossibleMaterials(string LotID, out string[] Materials)
        {
            Materials = null;
            try
            {
                string ig1;
                string ig2;
                Actl ig3;
                ReqItemsTableRow[] ig4;
                ReqPartsTableRow[] ig5;
                string test = mPromiseManager.matTp_MatReqQry(mUserId, mPassword, LotID, null, null, out ig1, out ig2, out ig3, out ig4, out ig5, out mInfo);
                Materials = new string[ig5.Length];
                for (int i = 0; i < ig5.Length; i++)
                {
                    Materials[i] = ig5[i].partId;
                }
                return true;
            }
            catch (Exception e)
            {
                string lines = "GetPossibleMaterials" + e.Message;
                System.IO.StreamWriter file = new System.IO.StreamWriter(LegacyLogFile, true);
                file.WriteLine(lines);
                file.Close();
                var message = System.Windows.Forms.MessageBox.Show("Unknown error getting possible materials\n" + e.Message, "Error",
                      System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                return false;
            }
        }

        /// <summary>
        /// Get location that a lot should be tracked into
        /// </summary>
        /// <param name="LotID">Lot id</param>
        /// <param name="Location">Location to be tracked into</param>
        /// <returns>True if successful</returns>
        /// <remarks>Unused</remarks>
        public override bool GetTrackingLocation(string LotID, out string Location)
        {
            Location = "";
            try
            {
                RecpSetRow[] ig16;
                MitmSetRow[] ig18;
                MatrSetRow[] ig19;
                PiarSetRow[] ig20;
                FutaSetRow[] ig21;
                FutaMapRow[] ig22;
                LotInfoStepCmdAllowedRow[] ig23;
                InfoTextRow[] ig24;
                ActlSetRow[] temp = mPromiseManager.lotQuery_Multistep(mUserId, mPassword, LotID, "3", null, out ig16, out ig18, out ig19, out ig20, out ig21, out ig22, out ig23, out ig24);

                Location = temp[0].location;

                return true;
            }
            catch (Exception e)
            {
                var message = System.Windows.Forms.MessageBox.Show("Unknown error getting source lots\n" + e.Message, "Error",
                      System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                return false;
            }
        }

        /// <summary>
        /// Get source lots with a specific material
        /// </summary>
        /// <param name="material">Material to find source lots for</param>
        /// <param name="Source">List of source lots</param>
        /// <param name="location">Location to look</param>
        /// <returns>True if successful</returns>
        override public bool GetSourceLots(string material, out Dictionary<string, string[]> Source, string location)//string lotID, out List<string[]> Materials
        {

            Source = new Dictionary<string, string[]>();
            SrcLotTableRow[] mSource;
            InfoTextRow[] mInfo;

            try
            {

                mSource = mPromiseManager.matTp_ShwSrcLot(mUserId, mPassword, material, location, null, out mInfo);
                if (mInfo.Length != 0)
                {
                    var message = System.Windows.Forms.MessageBox.Show("No source lots found for " + material, "Error",
                      System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    return false;
                }

                foreach (SrcLotTableRow tmp in mSource)
                {
                    LotBasic basic;
                    LotExtended exten;
                    PromisLotSupportData supp;
                    PrintInfo(tmp.LotId, out basic, out exten, out supp);
                    string[] wafers = new string[supp.ComponentsList.Length];

                    for (int i = 0; i < wafers.Length; i++)
                    {
                        wafers[i] = supp.ComponentsList[i].compids;
                    }

                    Source.Add(tmp.LotId, wafers);

                }
                return true;
            }
            catch (Exception e)
            {
                string lines = "GetSourceLots" + e.Message;
                System.IO.StreamWriter file = new System.IO.StreamWriter(LegacyLogFile, true);
                file.WriteLine(lines);
                file.Close();
                var message = System.Windows.Forms.MessageBox.Show("Unknown error getting source lots\n" + e.Message, "Error",
                      System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                return false;
            }


        }
        #endregion

        #region Facet Coat

        /// <summary>
        /// Return a list of lot ids ready to be tracked into a piece of equipment
        /// </summary>
        /// <param name="equipment">Equipment ID</param>
        /// <param name="lots">List of lots ids</param>
        /// <returns>True if successful</returns>
        public override bool GetLotsAtEquip(string equipment, out List<string> lots)
        {
            string firstlot = null;
            lots = new List<string>();
            ActlSetRow[] fromprms;
            try
            {
                mPromiseManager.lotQuery_EqpLotList(mUserId, mPassword, equipment, null, null, out firstlot, out fromprms);
                foreach (ActlSetRow tmp in fromprms)
                {
                    lots.Add(tmp.lotid);

                }
                return true;
            }
            catch (Exception e)
            {
                try
                {
                    MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    System.IO.StreamWriter file = new System.IO.StreamWriter(LegacyLogFile, true);
                    file.Write(e.Message);
                    file.Close();
                }
                catch { return false; }
                return false;
            }
        }

        /// <summary>
        /// Add the lettes to a lot during the first dcop
        /// </summary>
        /// <param name="LotNumber">Lot ID to run dcop on</param>
        /// <param name="ids">List of wafer ids</param>
        /// <param name="letters">Lois of identifying labels</param>
        /// <remarks>Depricated</remarks>
        public override void AddLetters(string LotNumber, string[] ids, string[] letters)
        {
            try
            {
                CompValueListRow[] Comp = new CompValueListRow[ids.Length];
                for (int i = 0; i < letters.Length; i++)
                {
                    DataValueListRow[] Data = new DataValueListRow[1];
                    Data[0] = new DataValueListRow();
                    Data[0].data = letters[i];
                    Comp[i] = new CompValueListRow();
                    Comp[i].compId = ids[i];
                    Comp[i].data = Data;
                }

                Actl Ig1;
                CalcSetRow[] Ig2;
                TresDresTableRow[] Ig3;
                ChrtTableRow[] Ig4;
                WarningsRow[] Ig5;
                Recp Ig6;
                TallyTaskArrayRow[] Ig7;
                SubLot Ig8;
                ActlSetRow[] Ig9;
                ClassOutGoodBinsRow[] ig10;
                ClassOutBadBinsRow[] ig11;
                FutaSetRow[] ig12;
                RecpSetRow[] ig13;
                FutaSetRow[] ignore;
                ActlSetRow[] ignore2;

                mPromiseManager.lotTrack_IntoStep(mUserId, mPassword, LotNumber, "CINPGROW", null, null, null, null, null, null, out Ig6, out Ig7, out mInfo);
                mPromiseManager.tstEnter_LotData(mUserId, mPassword, LotNumber, "1", null, null, null, Comp, null, out Ig1, out Ig2, out Ig3, out Ig4, out Ig5, out mInfo);
                mPromiseManager.lotTrack_OutOfStep(mUserId, mPassword, LotNumber, null, "C3CNORTH", null, null, null, null, null, out Ig6, out Ig8, out Ig9, out ig10, out ig11, out ignore, out ig12, out ignore2, out ig13, out mInfo);

                var message = System.Windows.Forms.MessageBox.Show("Letters added to added to " + LotNumber, "Success",
                      System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
            }
            catch (Exception e)
            {
                string lines = e.Message;
                System.IO.StreamWriter file = new System.IO.StreamWriter(LegacyLogFile, true);
                file.WriteLine(lines);
                file.Close();
                var message = System.Windows.Forms.MessageBox.Show(e.Message, "Error",
                      System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            }
        }

        /// <summary>
        /// Run various functions associated with facet coat
        /// </summary>
        /// <param name="Lot">Lot to track</param>
        /// <param name="Ref1">Reference 1 (for DCOP)</param>
        /// <param name="Ref2">Reference 2 (for DCOP)</param>
        /// <param name="Ref3">Reference 3 (for DCOP)</param>
        /// <param name="coating">Coating number (for DCOP)</param>
        /// <param name="facet">Facet front or back (for DCOP)</param>
        /// <param name="held">True or false whether the lot will be held after the dcop (data out of range ect)</param>
        /// <param name="CurrentFunc">Current facetcoat state</param>
        /// <returns>True if successful</returns>
        public override bool RunFacetCoat(string Lot, string Ref1, string Ref2, string Ref3, string coating, string facet, bool held, Func CurrentFunc)
        {
            string time = DateTime.Now.ToString().Replace('/', '_');
            time = time.Replace(':', '_');
            time = time.Replace(' ', '_');
            string date = time.Substring(0, 8);

            Actl Ig1;
            CalcSetRow[] Ig2;
            TresDresTableRow[] Ig3;
            ChrtTableRow[] Ig4;
            WarningsRow[] Ig5;
            Recp Ig6;
            TallyTaskArrayRow[] Ig7;
            SubLot Ig8;
            ActlSetRow[] Ig9;
            ClassOutGoodBinsRow[] ig10;
            ClassOutBadBinsRow[] ig11;
            FutaSetRow[] ig12;
            RecpSetRow[] ig13;
            FutaSetRow[] ignore;
            ActlSetRow[] ignore2;

            try
            {
                DataValueListRow[] Data = new DataValueListRow[5];

                for (int i = 0; i < Data.Length; i++)
                {
                    Data[i] = new DataValueListRow();
                }

                Data[0].data = Ref1;
                Data[1].data = Ref2;
                Data[2].data = Ref3;
                Data[3].data = coating;
                Data[4].data = facet[0].ToString();
                List<string> message = new List<string>();

       #region FunctionSpecific
                switch (CurrentFunc)
                {
                    /* NEW Flow to be implemented */
                    /*
                    case Func.Stage1Coat:
                        //Track Lots into stage 1 coat
                        mPromiseManager.lotTrack_IntoStep(mUserId, mPassword, Lot, "CAPS904", null, null, "FD001.01", null, "CMOD16", null, out Ig6, out Ig7, out mInfo); //1st facet coating
                        message.Add("Lot " + Lot + " Tracked into coating");
                        break;
                    case Func.Stage2Coat:
                        //Track Lots into stage 2 coat
                        mPromiseManager.lotTrack_IntoStep(mUserId, mPassword, Lot, "CAPS904", null, null, "FD005.01", null, "CMOD16", null, out Ig6, out Ig7, out mInfo);
                        message.Add("Lot " + Lot + " Tracked into coating");
                        break;
                    case Func.Stage1Test:
                        //Track Lots out of stage 1 coat and into stage 1 test
                        mPromiseManager.lotTrack_OutOfStep(mUserId, mPassword, Lot, null, "CMOD16", null, null, null, null, null, out Ig6, out Ig8, out Ig9, out ig10, out ig11, out ignore, out ig12, out ignore2, out ig13, out mInfo);
                        message.Add("Lot " + Lot + " Tracked out of coating");
                        mPromiseManager.lotTrack_IntoStep(mUserId, mPassword, Lot, "CLAMBDA1", null, null, "MF003.01", null, "CMOD16", null, out Ig6, out Ig7, out mInfo);
                        message.Add("Lot " + Lot + " Tracked into testing");
                        break;
                    case Func.Stage2Test:
                        //Track Lots out of stage 2 coat and into stage 2 test
                        mPromiseManager.lotTrack_OutOfStep(mUserId, mPassword, Lot, null, "CMOD16", null, null, null, null, null, out Ig6, out Ig8, out Ig9, out ig10, out ig11, out ignore, out ig12, out ignore2, out ig13, out mInfo);
                        message.Add("Lot " + Lot + " Tracked out of coating");
                        mPromiseManager.lotTrack_IntoStep(mUserId, mPassword, Lot, "CLAMBDA1", null, null, "MF005.01", null, "CMOD16", null, out Ig6, out Ig7, out mInfo);
                        message.Add("Lot " + Lot + " Tracked into testing");
                        break;
                        */

                    case Func.Stage1Coat:
                        //Track Lots into stage 1 coat
   /*OLD flow*/                     mPromiseManager.lotTrack_IntoStep(mUserId, mPassword, Lot, "CAPS904", null, null, "CDF00.05", null, "CMOD16", null, out Ig6, out Ig7, out mInfo);
            //            mPromiseManager.lotTrack_IntoStep(mUserId, mPassword, Lot, "CAPS904", null, null, "FD001.01", null, "CMOD16", null, out Ig6, out Ig7, out mInfo); //1st facet coating
                        message.Add("Lot " + Lot + " Tracked into coating");
                        break;
                    case Func.Stage2Coat:
                        //Track Lots into stage 2 coat
    /*old flow*/                    mPromiseManager.lotTrack_IntoStep(mUserId, mPassword, Lot, "CAPS904", null, null, "CDF01.04", null, "CMOD16", null, out Ig6, out Ig7, out mInfo);
     //                   mPromiseManager.lotTrack_IntoStep(mUserId, mPassword, Lot, "CAPS904", null, null, "FD005.01", null, "CMOD16", null, out Ig6, out Ig7, out mInfo);
                        message.Add("Lot " + Lot + " Tracked into coating");
                        break;
                    case Func.Stage1Test:
                        //Track Lots out of stage 1 coat and into stage 1 test
                        mPromiseManager.lotTrack_OutOfStep(mUserId, mPassword, Lot, null, "CMOD16", null, null, null, null, null, out Ig6, out Ig8, out Ig9, out ig10, out ig11, out ignore, out ig12, out ignore2, out ig13, out mInfo);
                        message.Add("Lot " + Lot + " Tracked out of coating");
    /*old flow*/                    mPromiseManager.lotTrack_IntoStep(mUserId, mPassword, Lot, "CLAMBDA1", null, null, "CDM28.03", null, "CMOD16", null, out Ig6, out Ig7, out mInfo);
     //                   mPromiseManager.lotTrack_IntoStep(mUserId, mPassword, Lot, "CLAMBDA1", null, null, "MF003.01", null, "CMOD16", null, out Ig6, out Ig7, out mInfo);
                        message.Add("Lot " + Lot + " Tracked into testing");
                        break;
                    case Func.Stage2Test:
                        //Track Lots out of stage 2 coat and into stage 2 test
                        mPromiseManager.lotTrack_OutOfStep(mUserId, mPassword, Lot, null, "CMOD16", null, null, null, null, null, out Ig6, out Ig8, out Ig9, out ig10, out ig11, out ignore, out ig12, out ignore2, out ig13, out mInfo);
                        message.Add("Lot " + Lot + " Tracked out of coating");
    /*OLD FLOW*/                    mPromiseManager.lotTrack_IntoStep(mUserId, mPassword, Lot, "CLAMBDA1", null, null, "CDM28.03", null, "CMOD16", null, out Ig6, out Ig7, out mInfo);
            //            mPromiseManager.lotTrack_IntoStep(mUserId, mPassword, Lot, "CLAMBDA1", null, null, "MF005.01", null, "CMOD16", null, out Ig6, out Ig7, out mInfo);
                        message.Add("Lot " + Lot + " Tracked into testing");
                        break;


                    case Func.Stage1Results:
                        // Enter test Data and Track Lots out of stage 1 test 
                        mPromiseManager.tstEnter_LotData(mUserId, mPassword, Lot, "1", null, null, Data, null, null, out Ig1, out Ig2, out Ig3, out Ig4, out Ig5, out mInfo);
                        string[] tmpstr1 = { "Lot " + Lot + " updated:", "Ref1=" + Ref1, "Ref2=" + Ref2, "Ref3=" + Ref3, "Coating Num=" + coating, "FacetSide=" + facet };
                        message.AddRange(tmpstr1);
                        if (held == false)
                        {
                            RecpSetRow[] ig16;
                            MitmSetRow[] ig18;
                            MatrSetRow[] ig19;
                            PiarSetRow[] ig20;
                            FutaSetRow[] ig21;
                            FutaMapRow[] ig22;
                            LotInfoStepCmdAllowedRow[] ig23;
                            InfoTextRow[] ig24;
                            ActlSetRow[] temp = mPromiseManager.lotQuery_Multistep(mUserId, mPassword, Lot, "1", null, out ig16, out ig18, out ig19, out ig20, out ig21, out ig22, out ig23, out ig24);

                            mPromiseManager.lotTrack_OutOfStep(mUserId, mPassword, Lot, null, temp[0].location, null, null, null, null, null, out Ig6, out Ig8, out Ig9, out ig10, out ig11, out ignore, out ig12, out ignore2, out ig13, out mInfo);
                            message.Add("Lot " + Lot + " Tracked out of testing");
                        }
                        break;
                    case Func.Stage2Results:
                        // Enter test Data and Track Lots out of stage 2 test 
                        mPromiseManager.tstEnter_LotData(mUserId, mPassword, Lot, "1", null, null, Data, null, null, out Ig1, out Ig2, out Ig3, out Ig4, out Ig5, out mInfo);
                        string[] tmpstr = { "Lot " + Lot + " updated:", "Ref1=" + Ref1, "Ref2=" + Ref2, "Ref3=" + Ref3, "Coating Num=" + coating, "FacetSide=" + facet };
                        message.AddRange(tmpstr);
                        if (held == false)
                        {
                            mPromiseManager.lotTrack_OutOfStep(mUserId, mPassword, Lot, null, "CMOD14", null, null, null, null, null, out Ig6, out Ig8, out Ig9, out ig10, out ig11, out ignore, out ig12, out ignore2, out ig13, out mInfo);
                            message.Add("Lot " + Lot + " Tracked out of testing");
                        }
                        break;
                    default:
                        break;
                }
                #endregion

                System.IO.StreamWriter file = new System.IO.StreamWriter(LegacyLogFile + "_" + time + ".txt", true);
                if (message != null)
                {
                    foreach (string mes in message)
                    {
                        file.WriteLine(mes);
                    }
                }
                file.Close();
                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                System.IO.StreamWriter file = new System.IO.StreamWriter(LegacyLogFile + "_" + time + ".txt", true);
                file.WriteLine(e.Message);
                file.Close();
                return false;
            }
        }

        #endregion

        #region Batch Viewer
        /// <summary>
        /// How far though the update process are we?
        /// </summary>
        public override double ProgressPercent
        {
            get;
            set;
        }
        /// <summary>
        /// Get information about future stages for a lot
        /// </summary>
        /// <param name="lot">Input lot object</param>
        /// <param name="processCount">Future steps to look ahead</param>
        /// <param name="detailLot">Lot object with all the future recipe</param>
        /// <returns>True if successful</returns>
        public override bool GetFutureRecp(Lot lot, int processCount, out Lot detailLot)
        {
            detailLot = lot;
            try
            {
                RecpSetRow[] ig16;
                MitmSetRow[] ig18;
                MatrSetRow[] ig19;
                PiarSetRow[] ig20;
                FutaSetRow[] ig21;
                FutaMapRow[] ig22;
                LotInfoStepCmdAllowedRow[] ig23;
                InfoTextRow[] ig24;
                ActlSetRow[] temp = mPromiseManager.lotQuery_Multistep(mUserId, mPassword, lot.ID, processCount.ToString(), null, out ig16, out ig18, out ig19, out ig20, out ig21, out ig22, out ig23, out ig24);

                foreach (ActlSetRow Rec in temp)
                {
                    Lot.Recipe resp = new Lot.Recipe();
                    resp.ID = Rec.recpid;
                    resp.Location = Rec.location;
                    resp.State = Rec.state;
                    resp.Stage = Rec.stage;
                    detailLot.mProcedure.FutureRecipe.Add(resp);
                }
                detailLot.mSchedule.LotType = temp[0].lottype;
                LotBasic bas;
                LotExtended ext;
                PrintInfo(detailLot.ID, out bas, out ext);
                detailLot.mSchedule.ReqDate = ext.reqdtime;
                detailLot.mSchedule.ReqDate = detailLot.mSchedule.ReqDate.Remove(detailLot.mSchedule.ReqDate.Length - 12);
                detailLot.mSchedule.ReqDate = detailLot.mSchedule.ReqDate.Trim();
                return true;
            }
            catch
            {
                return false;
            }
        }
        /// <summary>
        /// Get lots based on query built in the abstact class
        /// </summary>
        /// <param name="query">Query string</param>
        /// <param name="SelectedLots">Lots to return</param>
        /// <returns>True if successful</returns>
        /// <remarks>Depreciated</remarks>
        protected override bool GetLots(string query, out Lot[] SelectedLots)
        {
            SelectedLots = null;
            try
            {

                WipLotsRow[] LotsOfInfo = mPromiseManager.getWipLots(mUserId, mPassword, query, null);
                Console.WriteLine("sip01 : " + "user id: " + mUserId + "password: " + (mPassword) + "  Query is : " + query);
                if (LotsOfInfo.GetLength(0) != 0)
                {
                    int NumLots = LotsOfInfo.GetLength(0);
                    SelectedLots = new Lot[NumLots];
                    for (int iLoop = 0; iLoop < NumLots; ++iLoop)
                    {
                        if (LotsOfInfo[iLoop].state != "SCHED" && !LotsOfInfo[iLoop].state.Contains("COM"))
                        {
                            Lot TmpLot = new Lot(LotsOfInfo[iLoop].lotId);
                            TmpLot.mProcedure.CurrentProcedure = LotsOfInfo[iLoop].curPrcdId;
                            TmpLot.mProcedure.CurrentProcedure = TmpLot.mProcedure.CurrentProcedure.Replace(" ", "");
                            TmpLot.mMaterial.PartID = LotsOfInfo[iLoop].partId;
                            TmpLot.mMaterial.PartID = TmpLot.mMaterial.PartID.Replace(" ", "");
                            TmpLot.mSchedule.Priority = LotsOfInfo[iLoop].priority.Trim();
                            TmpLot.mSchedule.LastChange = LotsOfInfo[iLoop].queueTime.Trim();
                            TmpLot.mProcedure.RecipeID = LotsOfInfo[iLoop].recpId;
                            TmpLot.mProcedure.Stage = LotsOfInfo[iLoop].stage;
                            TmpLot.mState.CurrentState = LotsOfInfo[iLoop].state;
                            TmpLot.mState.Location = LotsOfInfo[iLoop].location;
                            TmpLot.mEquipment.EquipmentID = LotsOfInfo[iLoop].eqpId.Trim();
                            TmpLot.mMaterial.CurrentQuantity = LotsOfInfo[iLoop].curMainQty.Trim();
                            //LotsOfInfo[iLoop].

                            //if (DateTime.TryParse(LotsOfInfo[iLoop].queueTime, out tmp)) TmpLot.QueueTime = tmp;
                            SelectedLots[iLoop] = TmpLot;
                        }
                    }
                }    // Successful but no lots in this location
                return true;
            }
            catch (System.Web.Services.Protocols.SoapException SoapErr)
            {
                MessageBox.Show(SoapErr.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                FileInfo info = new FileInfo(LegacyLogFile);

                if (!Utilities.IsFileLocked(info))
                {
                    System.IO.StreamWriter file = new System.IO.StreamWriter(LegacyLogFile, true);
                    file.Write(SoapErr.Message);
                    file.Close();
                }
                return false;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                FileInfo info = new FileInfo(LegacyLogFile);

                if (!Utilities.IsFileLocked(info))
                {
                    System.IO.StreamWriter file = new System.IO.StreamWriter(LegacyLogFile, true);
                    file.Write(e.Message);
                    file.Close();
                }
                return false;
            }
        }

        #endregion

        #endregion

        #endregion

    }
}
