﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oclaro.PromService
{
    /// <summary>
    /// Information about User that has logged in
    /// </summary>
    public class PastieUserDetails
    {


        private readonly string mWindowsID = null;
        private readonly string mPastieUsername = null;
        private readonly string mPromisUsername = null;
        private readonly string mPromisPassword = null;
        private readonly string mFullname = null;
        private readonly int mAccessLevel = -1;


        internal PastieUserDetails(string windowsID)
        {
            mWindowsID = windowsID;
        }

        internal PastieUserDetails(string windowsID, string pastieUsername, string promUsername, string promPassword, string fullname, int access)
        {
            mWindowsID = windowsID;
            mPastieUsername = pastieUsername;
            mPromisUsername = promUsername;
            mPromisPassword = promPassword;
            mFullname = fullname;
            mAccessLevel = access;
        }

        /// <summary>
        /// 
        /// </summary>
        public string PastieUsername
        {
            get { return mPastieUsername; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string PromisUsername
        {
            get { return mPromisUsername; }
            
        }

        /// <summary>
        /// 
        /// </summary>
        public string PromisPassword
        {
            get { return mPromisPassword; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Fullname
        {
            get { return mFullname; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int AccessLevel
        {
            get { return mAccessLevel; }
        }




    }
}
