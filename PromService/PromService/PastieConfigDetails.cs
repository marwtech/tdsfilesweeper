﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Oclaro.PromService
{
    /// <summary>
    /// 
    /// </summary>
    public class PastieConfigDetails
    {

        private struct PromisInstanceDetails
        {
            public string mName;
            public string mDescription;

            public PromisInstanceDetails(string name, string desc)
            {
                mName = name;
                mDescription = desc;
            }
        }

        private string[] mLumentumLoginDomains = null;
        private string[] mOclaroLoginDomains = null;
        private Dictionary<string, PromisInstanceDetails> mPromisInstances = null;
        private string mDefaultProductionInstance = null;

        private Dictionary<string, string> mMessyDatabaseInfo = null;




        /// <summary>
        /// 
        /// </summary>
        public string[] AvailableLumentumLoginDomains
        {
            get { return mLumentumLoginDomains; }
            internal set { mLumentumLoginDomains = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string[] AvailableOclaroLoginDomains
        {

            get { return mOclaroLoginDomains; }
            internal set { mOclaroLoginDomains = value; }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="description"></param>
        public void AddPromisInstance(string name, string description)
        {
            if (mPromisInstances == null) mPromisInstances = new Dictionary<string, PromisInstanceDetails>();

            if (mPromisInstances.ContainsKey(name))
            {
                throw new Exception("Duplicate Promis instance Name");

            }
            PromisInstanceDetails NewInstance = new PromisInstanceDetails(name, description);
            mPromisInstances.Add(NewInstance.mName, NewInstance);
        }

        /// <summary>
        /// 
        /// </summary>
        public string DefaultProductionInstance
        {
            get { return mDefaultProductionInstance; }
            internal set
            {
                if (!mPromisInstances.Keys.Contains(value)) throw new Exception("Invalid default production instance");
                mDefaultProductionInstance = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string[] PromisInstanceNames
        {
            get { return mPromisInstances.Keys.ToArray(); }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="selectedPromisInstance"></param>
        /// <returns></returns>
        public string GetXMLGatewayID(string selectedPromisInstance)
        {
            if (mPromisInstances.ContainsKey(selectedPromisInstance)) return mPromisInstances[selectedPromisInstance].mDescription;
            throw new Exception("Invalid Promis Instance");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="itemName"></param>
        /// <param name="itemValue"></param>
        public void AddMRPDataItem(string itemName, string itemValue)
        {
            if (mMessyDatabaseInfo == null) mMessyDatabaseInfo = new Dictionary<string, string>();

            mMessyDatabaseInfo.Add(itemName, itemValue);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="itemName"></param>
        /// <returns></returns>
        public string GetMRPData(string itemName)
        {

            return mMessyDatabaseInfo[itemName];
        }

    }
}
