﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Oclaro.PromService
{
    /// <summary>
    /// Log file management
    /// </summary>
    public static class LogFile
    {
        private static string mLogFile;

        /// <summary>
        /// Create the log file
        /// </summary>
        /// <param name="file">Logfile location</param>
        public static void Init(string file)
        {
            mLogFile = file;            
        }
        
        /// <summary>
        /// Write to log file
        /// </summary>
        /// <param name="line">Line to write</param>
        public static void Write (string line)
        {
            try
            {
                using (StreamWriter writer = new StreamWriter(mLogFile, true))
                {
                    writer.WriteLine("(" + DateTime.Now.ToString("HH:mm:ss") + ")" + line);
                }
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }
    }
}
