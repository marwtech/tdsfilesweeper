﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;

namespace Oclaro.PromService
{
    public enum DCOpStatus
    {
        DCOP_NONE_REQUIRED,
        DCOP_NEEDS_DATA,
        DCOP_COMPLETE
    }


    /// <summary>
    /// Lot details container
    /// </summary>
    public class Lot
    {

        /// <summary>
        /// Current procedure information
        /// </summary>
        /// 
        public struct Procedure
        {
            /// <summary>
            /// Current procedure ID
            /// </summary>
            public string CurrentProcedure { get; set; }
            /// <summary>
            /// Current stage in procedure lot is at
            /// </summary>
            public string CurrentProcedureStage { get; set; }
            /// <summary>
            /// Current Recipe ID
            /// </summary>
            public string RecipeID { get; set; }
            /// <summary>
            /// Name of current recipie
            /// </summary>
            public string RecipeTitle { get; set; }
            /// <summary>
            /// Alternate recipe ID
            /// </summary>
            public string AltRecipe { get; set; }
            /// <summary>
            /// Full details about the alternate recipe
            /// </summary>
            public AltRecipe FullAltRecipe { get; set; }
            /// <summary>
            /// List of operations at current step on default recipe
            /// </summary>
            public List<Operations> Ops { get; set; }
            /// <summary>
            /// List of DCOP at the current stage
            /// </summary>
            public List<DataCollectionOperations> DCOps { get; set; }
            /// <summary>
            /// Current stage
            /// </summary>
            public string Stage { get; set; }
            /// <summary>
            /// List of future recipe information
            /// </summary>
            public List<Recipe> FutureRecipe { get; set; }
            /// <summary>
            /// List of past recipes information
            /// </summary>
            public List<Recipe> PastRecipe { get; set; }
            /// <summary>
            /// List of lot notes at the current stage
            /// </summary>
            public List<string> Notes { get; set; }
            /// <summary>
            /// Is the lot available for rework
            /// </summary>
            public string ReworkAll { get; set; }
        };

        /// <summary>
        /// Information about the lot materials
        /// </summary>
        public struct Material
        {
            /// <summary>
            /// Current part ID
            /// </summary>
            public string PartID { get; set; }
            /// <summary>
            /// Current number of components in lot
            /// </summary>
            public string CurrentQuantity { get; set; }
            /// <summary>
            /// Number of control components that are currently in the lot.
            /// </summary>
            public string ControlComponentQuantity { get; set; }
            /// <summary>
            /// Type of material in lot
            /// </summary>
            public string MaterialType { get; set; }
            /// <summary>
            /// Full mask ID
            /// </summary>
            public string Mask { get; set; }
            /// <summary>
            /// Unique mask identifier
            /// </summary>
            public string MaskShort { get; set; }
            /// <summary>
            /// Managing engineer on lot
            /// </summary>
            public string Owner { get; set; }
            /// <summary>
            /// List of components in lot
            /// </summary>
            public List<Component> Components { get; set; }
            /// <summary>
            /// List of control components in lot
            /// </summary>
            public List<Component> ControlComponents { get; set; }
            /// <summary>
            /// List of lot parameters
            /// </summary>
            public List<Parameters> Parameters { get; set; }
            /// <summary>
            /// List of component specific parameters
            /// </summary>
            public List<Parameters> ComponentParameters { get; set; }
            /// <summary>
            /// Batch ID for source lot
            /// </summary>
            public string SourceBatchID { get; set; }
        };

        /// <summary>
        /// Information the current state the lot is in
        /// </summary>
        public struct State
        {
            /// <summary>
            /// Current state ID
            /// </summary>
            public string CurrentState { get; set; }
            /// <summary>
            /// Current location of the lot
            /// </summary>
            public string Location { get; set; }
            /// <summary>
            /// Events that have happened at this state
            /// </summary>
            public List<History> HistoryEvents { get; set; }
            /// <summary>
            /// Hold reson if lot is held
            /// </summary>
            public string HoldReason { get; set; }
            /// <summary>
            /// List of comments on lot
            /// </summary>
            public List<string> Comments { get; set; }
            /// <summary>
            /// List of holds currently on the lot
            /// </summary>
            public List<Hold> CurrentHolds { get; set; }

        };

        /// <summary>
        /// Information about the lot's schedule
        /// </summary>
        public struct Schedule
        {
            /// <summary>
            /// Priority of lot
            /// </summary>
            public string Priority { get; set; }
            /// <summary>
            /// Lot material type (engineering or production)
            /// </summary>
            public string LotType { get; set; }
            /// <summary>
            /// Date of last event
            /// </summary>
            public string LastChange { get; set; }
            /// <summary>
            /// Required finishing date of lot
            /// </summary>
            public string ReqDate { get; set; }
        };
        /// <summary>
        /// General infromation about the Promis Lot
        /// </summary>
        public struct LotInfo
        {
            /// <summary>
            /// Top level lot comment
            /// </summary>
            public string LotComment { get; set; }
            /// <summary>
            /// Customer reference field 1
            /// </summary>
            public string CustRef1 { get; set; }
            /// <summary>
            /// Customer reference field 2
            /// </summary>
            public string CustRef2 { get; set; }
            /// <summary>
            /// Customer reference field 3
            /// </summary>
            public string CustRef3 { get; set; }
            /// <summary>
            /// Name of customer
            /// </summary>
            public string CustomerName { get; set; }
            /// <summary>
            /// Used for SAP Production Order number
            /// </summary>
            public string CustomerOrderNumber { get; set; }
            /// <summary>
            /// Expected date for completion of processing
            /// </summary>
            public string EndDate { get; set; }
            /// <summary>
            /// Not sure????
            /// </summary>
            public string NumLots { get; set; }
            /// <summary>
            /// Planned start date
            /// </summary>
            public string StartDate { get; set; }
            /// <summary>
            /// Number of wafers started
            /// </summary>
            public string StartSize { get; set; }
        }

        /// <summary>
        /// Equipment information for a given lot
        /// </summary>
        public struct Equipment
        {
            /// <summary>
            /// Equipmnet id that the lot is currently running on
            /// </summary>
            public string EquipmentID { get; set; }
            /// <summary>
            /// Type of equipment the lot is running on
            /// </summary>
            public string EquipmentType { get; set; }
            /// <summary>
            /// List of equipment that the lot could be tracked into
            /// </summary>
            public List<EquipmentDet> AvaliableEquipment { get; set; }
        };

        /// <summary>
        /// Lot ID
        /// </summary>
        public string ID { set; get; }
        /// <summary>
        /// Lot procedure information
        /// </summary>
        public Procedure mProcedure;
        /// <summary>
        /// Lot material information
        /// </summary>
        public Material mMaterial;
        /// <summary>
        /// Lot state information
        /// </summary>
        public State mState;
        /// <summary>
        /// Lot schedule information
        /// </summary>
        public Schedule mSchedule;
        /// <summary>
        /// Lot equipment information
        /// </summary>
        public Equipment mEquipment;

        /// <summary>
        /// General lot information
        /// </summary>
        public LotInfo mInfo;

        //private OrderedDictionary mDCOperations;
        private DCOperations mDCOperations;
        /// <summary>
        /// New DCOps
        /// </summary>
        //public OrderedDictionary DCOperations { get { return mDCOperations; } } 

        public DCOperations LotDCOperation { get { return mDCOperations; } }

        /// <summary>
        /// Constructer for a lot Item 
        /// Initialises all lists to be added to later when building up lot items.
        /// </summary>
        /// <param name="LotId">Target lot ID</param>
        public Lot(string LotId)
        {
            ID = LotId;
            mProcedure = new Procedure();
            mMaterial = new Material();
            mState = new State();
            mSchedule = new Schedule();
            mProcedure.FutureRecipe = new List<Recipe>();
            mProcedure.PastRecipe = new List<Recipe>();
            mProcedure.Ops = new List<Operations>();
            mProcedure.DCOps = new List<DataCollectionOperations>();
            mProcedure.Notes = new List<string>();
            mState.HistoryEvents = new List<History>();
            mState.Comments = new List<string>();
            mState.CurrentHolds = new List<Hold>();
            mMaterial.Components = new List<Component>();
            mMaterial.ControlComponents = new List<Component>();
            mMaterial.Parameters = new List<Parameters>();
            mMaterial.ComponentParameters = new List<Parameters>();
            mEquipment.AvaliableEquipment = new List<EquipmentDet>();

            mDCOperations = new DCOperations();
        }

        public DCOpStatus NeedsDCOpData
        {
            get 
            {
                if (mProcedure.DCOps.Count != 0)
                {
                    DCOpStatus Status = DCOpStatus.DCOP_COMPLETE;
                    foreach (DataCollectionOperations Dc in mProcedure.DCOps)
                    {
                        if (!Dc.DataEntryHasBeenDone) { Status= DCOpStatus.DCOP_NEEDS_DATA; break; }
                    }
                    return Status;
                }
                else return DCOpStatus.DCOP_NONE_REQUIRED; 
            }
        }
        /// <summary>
        /// Detailed recipe information
        /// </summary>
        public struct Recipe
        {
            /// <summary>
            /// location associated with recipe
            /// </summary>
            public string Location { get; set; }
            /// <summary>
            /// Recipe ID
            /// </summary>
            public string ID { get; set; }
            /// <summary>
            /// State of the recipe
            /// </summary>
            public string State { get; set; }
            /// <summary>
            /// Stage the recipe is performed at
            /// </summary>
            public string Stage { get; set; }
            /// <summary>
            /// Description of the recipe
            /// </summary>
            public string Description { get; set; }
            /// <summary>
            /// Title of the recipe
            /// </summary>
            public string Title { get; set; }
            /// <summary>
            /// Procedure the Recipe is associated with
            /// </summary>
            public string Procedure { get; set; }
            /// <summary>
            /// Equipment capability the recipe is associated with
            /// </summary>
            public string Capability { get; set; }
            /// <summary>
            /// List of operations to be performed by the recipe
            /// </summary>
            public List<Operations> Operations { get; set; }
        };

        /// <summary>
        /// Operation information
        /// </summary>
        public struct Operations
        {
            /// <summary>
            /// operation ID
            /// </summary>
            public string ID { get; set; }
            /// <summary>
            /// Operation Description
            /// </summary>
            public string Desc { get; set; }
            /// <summary>
            /// Operation type (is it a DCOP?)
            /// </summary>
            public string Type { get; set; }

        };

        /// <summary>
        /// Lot histroy object
        /// </summary>
        public struct History
        {
            /// <summary>
            /// Type of event
            /// </summary>
            public string Type { get; set; }
            /// <summary>
            /// user who performed event
            /// </summary>
            public string User { get; set; }
            /// <summary>
            /// History description
            /// </summary>
            public string Desc { get; set; }
            /// <summary>
            /// Time of the event
            /// </summary>
            public string Time { get; set; }
        };

        /// <summary>
        /// Informatioon about data collection operations
        /// </summary>
        public struct DataCollectionOperations
        {
            /// <summary>
            /// Component the data is associated with
            /// </summary>
            public string Component { get; set; }
            /// <summary>
            /// HAs the test been completed?
            /// </summary>
            public string Tested { get; set; }
            /// <summary>
            /// Site of test
            /// </summary>
            public string Site { get; set; }
            /// <summary>
            /// Site number of test
            /// </summary>
            public string SiteNum { get; set; }
            /// <summary>
            /// Test prompt
            /// </summary>
            public string Prompt { get; set; }
            /// <summary>
            /// Possible test result options
            /// </summary>
            public string[] PickList { get; set; }
            /// <summary>
            /// lower limit of test data to be in acceptable range
            /// </summary>
            public string LowLimit { get; set; }
            /// <summary>
            /// Upper limit of test data to be in acceptable range
            /// </summary>
            public string UpLimit { get; set; }
            /// <summary>
            /// Test data units
            /// </summary>
            public string Units { get; set; }
            /// <summary>
            /// Test numer refernece
            /// </summary>
            public string TestNumber { get; set; }
            /// <summary>
            /// Time the data was entered
            /// </summary>
            public string TimeEntered { get; set; }
            /// <summary>
            /// Data entered if test is complete
            /// </summary>
            public string DataEntered { get; set; }
            /// <summary>
            /// Data entry has been done
            /// </summary>
            public bool DataEntryHasBeenDone { get; set; }
        };

        /// <summary>
        /// Lot component information
        /// </summary>
        public struct Component
        {
            /// <summary>
            /// Component ID
            /// </summary>
            public string ID { get; set; }
            /// <summary>
            /// State the component is in
            /// </summary>
            public string State { get; set; }
            /// <summary>
            /// If its a control component, what type is it
            /// </summary>
            public string ControlType { get; set; }
            /// <summary>
            /// If its a control component, should it be added or removed
            /// </summary>
            public string Action { get; set; }
            /// <summary>
            /// Component description
            /// </summary>
            public string Description { get; set; }
            /// <summary>
            /// Numer of that component in lot
            /// </summary>
            public string Quantity { get; set; }
        };

        /// <summary>
        /// Details about a piece of equipment
        /// </summary>
        public struct EquipmentDet
        {
            /// <summary>
            /// Equipment ID
            /// </summary>
            public string ID { get; set; }
            /// <summary>
            /// Equipment capability
            /// </summary>
            public string Capability { get; set; }
            /// <summary>
            /// All capabilities for selected kit
            /// </summary>
            public Dictionary<string,string> AllCapabilities { get; set; }
            /// <summary>
            /// Equipment type
            /// </summary>
            public string Type { get; set; }
            /// <summary>
            /// Equipment description
            /// </summary>
            public string Description { get; set; }
            /// <summary>
            /// Equipment location
            /// </summary>
            public string Location { get; set; }
            /// <summary>
            /// Availibility of equipment
            /// </summary>
            public string Avaliable { get; set; }
            /// <summary>
            /// Current activity on equipment
            /// </summary>
            public string Activity { get; set; }
        };

        /// <summary>
        /// Paramerther information
        /// </summary>
        public struct Parameters
        {
            /// <summary>
            /// Parameter name
            /// </summary>
            public string Name { get; set; }
            /// <summary>
            /// Parameter type
            /// </summary>
            public string Type { get; set; }
            /// <summary>
            /// Value of the parameter
            /// </summary>
            public string Value { get; set; }
            /// <summary>
            /// If the paraper is associated with a component, the component ID
            /// </summary>
            public string Component { get; set; }
            /// <summary>
            /// The 'Kind' of parameter
            /// </summary>
            public string Kind { get; set; }
        };

        /// <summary>
        /// Alternate recipe information
        /// </summary>
        public struct AltRecipe
        {
            /// <summary>
            /// Location of recipe
            /// </summary>
            public string Location { get; set; }
            /// <summary>
            /// Recipe ID
            /// </summary>
            public string ID { get; set; }
            /// <summary>
            /// State the recipe is run at
            /// </summary>
            public string State { get; set; }
            /// <summary>
            /// The stage the recipe is run at
            /// </summary>
            public string Stage { get; set; }
            /// <summary>
            /// Recipe description
            /// </summary>
            public string Description { get; set; }
            /// <summary>
            /// Recipe title
            /// </summary>
            public string Title { get; set; }
            /// <summary>
            /// Procedure the recipe is associated with
            /// </summary>
            public string Procedure { get; set; }
            /// <summary>
            /// Capability the recipe is associated with
            /// </summary>
            public string Capability { get; set; }
            /// <summary>
            /// List of operations the recipe has
            /// </summary>
            public List<Operations> Operations { get; set; }
            /// <summary>
            /// List of equipment the recipe can be run on
            /// </summary>
            public List<EquipmentDet> Equipment { get; set; }
        };

        /// <summary>
        /// Hold details
        /// </summary>
        public struct Hold
        {
            /// <summary>
            /// Hold code
            /// </summary>
            public string Code { get; set; }
            /// <summary>
            /// ID number of hold
            /// </summary>
            public string HoldNumber { get; set; }
            /// <summary>
            /// Time lot was held
            /// </summary>
            public string Time { get; set; }
            /// <summary>
            /// Reason for hold
            /// </summary>
            public string Reason { get; set; }
            /// <summary>
            /// Type of hold
            /// </summary>
            public string Type { get; set; }
            /// <summary>
            /// User that held lot
            /// </summary>
            public string User { get; set; }
        };
    }

    public class DataCollectionEntry
    {
        public DataCollectionEntry()
        {
        }
        /// <summary>
        /// Component the data is associated with
        /// </summary>
        public string Component { get; set; }
        /// <summary>
        /// HAs the test been completed?
        /// </summary>
        public string Tested { get; set; }
        /// <summary>
        /// Site of test
        /// </summary>
        public string Site { get; set; }
        /// <summary>
        /// Site number of test
        /// </summary>
        public string SiteNum { get; set; }
        /// <summary>
        /// Test prompt
        /// </summary>
        public string Prompt { get; set; }
        /// <summary>
        /// Possible test result options
        /// </summary>
        public string[] PickList { get; set; }
        /// <summary>
        /// lower limit of test data to be in acceptable range
        /// </summary>
        public string LowLimit { get; set; }
        /// <summary>
        /// Upper limit of test data to be in acceptable range
        /// </summary>
        public string UpLimit { get; set; }
        /// <summary>
        /// Test data units
        /// </summary>
        public string Units { get; set; }
        /// <summary>
        /// Test numer refernece
        /// </summary>
        public string TestNumber { get; set; }
        /// <summary>
        /// Time the data was entered
        /// </summary>
        public string TimeEntered { get; set; }
        /// <summary>
        /// Data entered if test is complete
        /// </summary>
        public string DataEntered { get; set; }
        /// <summary>
        /// Data entry has been done
        /// </summary>
        public bool DataEntryHasBeenDone { get; set; }

    }

 

    public class DataCollectionOp
    {
        private List<Lot.DataCollectionOperations> mDataEntryDefinitions = null;
        private string mName = null;

        public DataCollectionOp(string name)
        {
            mName = name;
            mDataEntryDefinitions = new List<Lot.DataCollectionOperations>();
        }

        public string Name
        {
            get { return mName; }
        }

        public List<Lot.DataCollectionOperations> DataEntries
        {
            get
            {
                return mDataEntryDefinitions;
            }
        }

    }

    public class DCOperations
    {
        private OrderedDictionary mOps = null;
        public DCOperations()
        {
            mOps = new OrderedDictionary();
        }

        public bool NeedsDCOpDataEntry
        {
            get
            {
                bool NeedsOpData = false;
                for (int iOp = 0; iOp < mOps.Count; ++iOp)
                {
                    DataCollectionOp ThisOp = (DataCollectionOp)mOps[iOp];
                    foreach ( Lot.DataCollectionOperations ThisEntry in ThisOp.DataEntries)
                    {
                        if (!ThisEntry.DataEntryHasBeenDone)
                        {
                            NeedsOpData = true;
                            break;
                        }
                    }
                }
                return NeedsOpData;
            }
        }

        public bool DCOpExists(string dcOpID)
        {
            return false;
        }

        public string[] DcOpNames
        {

            get { return null; }
        }

        public DataCollectionOp GetDCOp(string dcOpName)
        {
           return null; 
        }

        public OrderedDictionary DCOps
        {
            get { return mOps;}
        }

    }

}