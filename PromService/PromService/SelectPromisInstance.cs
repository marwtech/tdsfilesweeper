﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Oclaro.PromService
{
    /// <summary>
    /// 
    /// </summary>
    public partial class SelectPromisInstance : Form
    {
        private string mSelectedInstance = null;

        /// <summary>
        /// 
        /// </summary>
        public SelectPromisInstance(string[] promisInstanceNames)
        {
            InitializeComponent();

            //foreach (string name in promisInstanceNames)
            for (int iLoop = 0; iLoop<promisInstanceNames.GetLength(0); ++iLoop)
            {
                RadioButton InstanceOption = new RadioButton();
                InstanceOption.Width = (TextRenderer.MeasureText(InstanceOption.Text, InstanceOption.Font)).Width + 25;
                InstanceOption.Name = "RadioB_" + promisInstanceNames[iLoop];
                InstanceOption.Text = promisInstanceNames[iLoop];
                InstanceOption.Font = new Font(InstanceOption.Font.FontFamily.Name, (float)12.0);
                InstanceOption.Parent = GbPromisNames;
                InstanceOption.Width = (TextRenderer.MeasureText(InstanceOption.Text, InstanceOption.Font)).Width + 25;
                InstanceOption.Location = new Point(30, 30 + iLoop * 25);
                InstanceOption.Visible = true;
                InstanceOption.CheckedChanged += new System.EventHandler(this.PromisInstance_CheckedChanged);
            }

        }

        private void ButOk_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void PromisInstance_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton HitBut = (RadioButton)sender;
            if (HitBut.Checked) mSelectedInstance = HitBut.Text;
            else mSelectedInstance = "";
        }


        private void ButCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        /// <summary>
        /// 
        /// </summary>
        public string SelectedPromisInstance
        {
            get { return mSelectedInstance; }
        }
    }
}
