﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Security.Principal;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Drawing;

using System.ComponentModel;
using System.Net.Mail;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography;

namespace Oclaro.PromService
{
    /// <summary>
    /// Useful functions
    /// </summary>
    public class Utilities
    {
        /// <summary>
        /// Check to see if a file is locked by another process
        /// </summary>
        /// <param name="file">File to check</param>
        /// <returns>True if file is locked</returns>
        public static bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }

        /// <summary>
        /// Convert a number to a letter (1->A, 2->B)
        /// </summary>
        /// <param name="column">number to change</param>
        /// <returns>Converted string of characters</returns>
        public static string NumberToLetter(int column)
        {
            string columnString = "";
            decimal columnNumber = column;
            while (columnNumber > 0)
            {
                decimal currentLetterNumber = (columnNumber - 1) % 26;
                char currentLetter = (char)(currentLetterNumber + 65);
                columnString = currentLetter + columnString;
                columnNumber = (columnNumber - (currentLetterNumber + 1)) / 26;
            }
            return columnString;
        }

        /// <summary>
        /// Split a long string into a list of lines with a maximum length while preserving words
        /// </summary>
        /// <param name="comment">String to split</param>
        /// <param name="length">maximum length of line</param>
        /// <returns>List of split strings</returns>
        public static List<string> SplitLines(string comment, int length)
        {
            List<string> list = new List<string>();

            int partLength = length;
            string[] words = comment.Split(' ');
            string part = string.Empty;
            int partCounter = 0;
            foreach (var word in words)
            {
                if (part.Length + word.Length < partLength)
                {
                    part += string.IsNullOrEmpty(part) ? word : " " + word;
                }
                else
                {
                    list.Add(part);
                    part = word;
                    partCounter++;
                }
            }
            list.Add(part);

            return list;
        }

        /// <summary>
        /// Read all the lines from a text file
        /// </summary>
        /// <param name="Location">File location to read</param>
        /// <returns>All of the linas as a list of strings</returns>
        public static List<string> ReadTxtFile(string Location)
        {
            List<string> lines = new List<string>();

            try
            {
                using (StreamReader sr = new StreamReader(Location))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        lines.Add(line);
                    }
                }
            }
            catch
            {
                return null;
            }
            return lines;
        }

        /// <summary>
        /// Moves selected listbox items from one listbox to another
        /// </summary>
        /// <param name="source">Source listbox</param>
        /// <param name="destination">Destination listbox</param>
        public static void MoveListBoxItems(ListBox source, ListBox destination)
        {
            ListBox.SelectedObjectCollection sourceItems = source.SelectedItems;
            foreach (var item in sourceItems)
            {
                destination.Items.Add(item);
            }
            while (source.SelectedItems.Count > 0)
            {
                source.Items.Remove(source.SelectedItems[0]);
            }
        }

        /// <summary>
        /// Allows you to loop through all the chils controls of a control of a given type.
        /// So for example, loop through all the textboxes in a panel
        /// </summary>
        /// <param name="control">Control to loop at</param>
        /// <param name="type">Type of control to find</param>
        /// <returns>Enumerble of controls od the given type to loop through</returns>
        public static IEnumerable<Control> GetAllControls(Control control, Type type)
        {
            var controls = control.Controls.Cast<Control>();

            return controls.SelectMany(ctrl => GetAllControls(ctrl, type))
                                      .Concat(controls)
                                      .Where(c => c.GetType() == type);
        }

        /// <summary>
        /// Checks to see if the user is admin on local computer
        /// </summary>
        /// <returns>True if admin</returns>
        public static bool IsUserAdministrator()
        {
            //bool value to hold our return value
            bool isAdmin;
            try
            {
                //get the currently logged in user
                WindowsIdentity user = WindowsIdentity.GetCurrent();
                WindowsPrincipal principal = new WindowsPrincipal(user);
                isAdmin = principal.IsInRole(WindowsBuiltInRole.Administrator);
            }
            catch (UnauthorizedAccessException ex)
            {
                string exp = ex.Message;
                isAdmin = false;
            }
            catch (Exception ex)
            {
                string exp = ex.Message;
                isAdmin = false;
            }
            return isAdmin;
        }

        /// <summary>
        /// Changes the first letter of a wrod to uppercase for use with names
        /// </summary>
        /// <param name="s">String to change</param>
        /// <returns>String with first letter uppercased</returns>
        public static string UppercaseFirst(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            char[] a = s.ToCharArray();
            a[0] = char.ToUpper(a[0]);
            return new string(a);
        }

        /// <summary>
        /// Removes invalid file name characters
        /// </summary>
        /// <param name="strIn">String to clean</param>
        /// <returns>Cleaned string</returns>
        public static string CleanInput(string strIn)
        {
            try
            {
                string invalid = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());

                foreach (char c in invalid)
                {
                    strIn = strIn.Replace(c.ToString(), "");
                }
                return strIn;
            }
            catch { return strIn; }
        }

        /// <summary>
        /// 
        /// </summary>
        static bool invalid = false;
        /// <summary>
        /// Checks the string format is valid for an email
        /// Doesnt check the email exists, just that the string looks right
        /// </summary>
        /// <param name="strIn">String to check</param>
        /// <returns>True if string is valid to be an email</returns>
        public static bool IsValidEmail(string strIn)
        {
            invalid = false;
            if (String.IsNullOrEmpty(strIn))
                return false;

            // Use IdnMapping class to convert Unicode domain names. 
            try
            {
                strIn = Regex.Replace(strIn, @"(@)(.+)$", DomainMapper,
                                      RegexOptions.None);
            }
            catch
            {
                return false;
            }

            if (invalid)
                return false;

            // Return true if strIn is in valid e-mail format. 
            try
            {
                return Regex.IsMatch(strIn,
                      @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                      @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                      RegexOptions.IgnoreCase);
            }
            catch 
            {
                return false;
            }
        }

        static private string DomainMapper(Match match)
        {
            // IdnMapping class with default property values.
            IdnMapping idn = new IdnMapping();

            string domainName = match.Groups[2].Value;
            try
            {
                domainName = idn.GetAscii(domainName);
            }
            catch (ArgumentException)
            {
                invalid = true;
            }
            return match.Groups[1].Value + domainName;
        }
    }

    /// <summary>
    /// List box item that allows for coloured text and background
    /// </summary>
    public class MyListBoxItem
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="c">Text colour</param>
        /// <param name="m">Message</param>
        public MyListBoxItem(Color c, string m)
        {
            ItemColor = c;
            Message = m;
            BackColor = Color.Empty;
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="c">Text colour</param>
        /// <param name="b">background colour</param>
        /// <param name="m">Message</param>
        public MyListBoxItem(Color c, Color b, string m)
        {
            ItemColor = c;
            BackColor = b;
            Message = m;
        }
        /// <summary>
        /// New text colour
        /// </summary>
        public Color ItemColor { get; set; }
        /// <summary>
        /// New item background coloue
        /// </summary>
        public Color BackColor { get; set; }
        /// <summary>
        /// The actual text
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// List box measure event to allow for text wrapping and space between lines 
        /// </summary>
        /// <param name="sender">Listbox object</param>
        /// <param name="e">Arguments</param>
        public static void WrapList_MeasureItem(object sender, MeasureItemEventArgs e)
        {
            ListBox tmp = (ListBox)sender;
            MyListBoxItem item = tmp.Items[e.Index] as MyListBoxItem; // Get the current item and cast it to MyListBoxItem
            if (item != null)
            {
                e.ItemHeight = (int)e.Graphics.MeasureString(item.Message, tmp.Font, tmp.Width - SystemInformation.VerticalScrollBarWidth).Height + 5;
            }
            else
            {
                e.ItemHeight = (int)e.Graphics.MeasureString(tmp.Items[e.Index].ToString(), tmp.Font, tmp.Width - SystemInformation.VerticalScrollBarWidth).Height + 5;
            }
        }

        /// <summary>
        /// Listbox draw event to draw the wrapped text with coloured text and background
        /// </summary>
        /// <param name="sender">Listbox item</param>
        /// <param name="e">Arguements</param>
        public static void ColourText_DrawItem(object sender, DrawItemEventArgs e)
        {
            try
            {
                ListBox tmp = (ListBox)sender;
                MyListBoxItem item = tmp.Items[e.Index] as MyListBoxItem; // Get the current item and cast it to MyListBoxItem

                if (item != null)
                {
                    e.DrawBackground();
                    if(item.BackColor != Color.Empty)
                        e.Graphics.FillRectangle(new SolidBrush(item.BackColor), e.Bounds);
                    e.DrawFocusRectangle();
                    e.Graphics.DrawString( // Draw the appropriate text in the ListBox
                    item.Message, // The message linked to the item
                    e.Font, // Take the font from the listbox
                    new SolidBrush(item.ItemColor), // Set the color 
                    e.Bounds// Y pixel coordinate.  Multiply the index by the ItemHeight defined in the listbox.
                    );
                }
                else
                {
                    e.DrawBackground();
                    e.DrawFocusRectangle();
                    e.Graphics.DrawString(tmp.Items[e.Index].ToString(), e.Font, new SolidBrush(e.ForeColor), e.Bounds);
                    // The item isn't a MyListBoxItem, do something about it
                }
            }
            catch { }
        }
    }

    /// <summary>
    /// Object that can be used to send emails programatically
    /// </summary>
    public class Mail
    {
        SmtpClient mMailClient;

        string mFromMail;
        string mFromUser;
        string mFromPassword;

        List<MailMessage> mails;

        string message1 = "";
        string message2 = "";

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="host">Host Ip address</param>
        /// <param name="fromMail">Sender email</param>
        /// <param name="fromUser">Sender user</param>
        /// <param name="fromPass">Sender password</param>
        /// <param name="enableSecurity">Bool to override the security protocols</param>
        public Mail(string host, string fromMail, string fromUser, string fromPass, bool enableSecurity)
        {
            mFromMail = fromMail;
            mFromPassword = fromPass;
            mFromUser = fromUser;

            mMailClient = new SmtpClient();
            mMailClient.Host = host;
            mMailClient.EnableSsl = enableSecurity;
            mMailClient.SendCompleted += new SendCompletedEventHandler(mMailClient_SendCompleted);
            if (enableSecurity)
            {
                ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate,
                 X509Chain chain, SslPolicyErrors sslPolicyErrors)
                { return true; };
            }

            mMailClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            //mMailClient.UseDefaultCredentials = true;
            mMailClient.Credentials = new System.Net.NetworkCredential(mFromUser, mFromPassword);

            mails = new List<MailMessage>();
        }

        /// <summary>
        /// Send email to multople recipiants
        /// </summary>
        /// <param name="recipiants">List of Recipients [address, send true or false]</param>
        /// <param name="subject">Message subject</param>
        /// <param name="message">Message body text</param>
        /// <param name="attatchments">Attatchments to send in the email</param>
        public void SendAllMail(List<object[]> recipiants, string subject, string message, List<string> attatchments)
        {
            message1 = "Emails sent to:";
            message2 = "Emails failed to send to:";
            int counter = 0;

            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress(mFromMail, "SPC Testing Results", Encoding.UTF8);
            mailMessage.Body = message;
            mailMessage.BodyEncoding = Encoding.UTF8;
            mailMessage.Subject = subject;
            mailMessage.SubjectEncoding = Encoding.UTF8;

            if (attatchments != null)
            {
                foreach (string att in attatchments)
                {
                    mailMessage.Attachments.Add(new Attachment(att));
                }
            }

            foreach (object[] str in recipiants)
            {
                bool toSend = (bool)str[0];
                string email = (string)str[1];

                if (toSend)
                {
                    //SendMail(email, subject, message, attatchments, counter);
                    MailAddress tmpTo = new MailAddress(email, "SPC Testing Results", Encoding.UTF8);
                    if (!mailMessage.To.Contains(tmpTo))
                    {
                        mailMessage.To.Add(tmpTo);
                        message1 = string.Join("\n", new string[] { message1, email });
                    }
                    counter++;

                }
                else
                {
                    message2 = string.Join("\n", new string[] { message2, email });
                }
            }
            //string messageName = "SPC Message : 0 : " + mails[0].To.First().Address;
            //SendNextMessage(0, messageName);

            string messageName = "SPC Message : Error";
            mMailClient.SendAsync(mailMessage, messageName);

            string result = string.Join("\n", new string[] { message1, message2 });
            MessageBox.Show(result);
        }

        /// <summary>
        /// Send individual e-mail
        /// </summary>
        /// <param name="toMail">Email to send message to</param>
        /// <param name="subject">Message subject</param>
        /// <param name="message">Message body text</param>
        /// <param name="attatchments">Attatchment list</param>
        /// <param name="messageNum">Message identifier</param>
        /// <param name="fromName">Name of teh sender</param>
        public void SendMail(string toMail, string subject, string message,string fromName ,List<string> attatchments, int messageNum)
        {
            MailAddress from = new MailAddress(mFromMail, fromName, Encoding.UTF8);
            MailAddress to = new MailAddress(toMail, "", Encoding.UTF8);
            MailMessage mailMessage = new MailMessage(from, to);
            mailMessage.Body = message;
            mailMessage.BodyEncoding = Encoding.UTF8;
            mailMessage.IsBodyHtml = true;
            mailMessage.Subject = subject;
            mailMessage.SubjectEncoding = Encoding.UTF8;

            if (attatchments != null)
            {
                foreach (string att in attatchments)
                {
                    mailMessage.Attachments.Add(new Attachment(att));
                }
            }

            string messageName = "SPC Message : " + messageNum.ToString() + " : " + toMail;

            mMailClient.SendAsync(mailMessage, messageName);

            Console.WriteLine("Sending message...");
            Console.WriteLine("Goodbye.");


        }

        void SendNextMessage(int messageIndex, string messageString)
        {
            if (!(messageIndex > mails.Count - 1))
                mMailClient.SendAsync(mails[messageIndex], messageString);
        }

        void mMailClient_SendCompleted(object sender, AsyncCompletedEventArgs e)
        {
            // Get the unique identifier for this asynchronous operation.
            string message = (string)e.UserState;
            string[] token = message.Split(':');

            if (e.Cancelled)
            {
                Console.WriteLine("[{0}] Send canceled.", token[0]);
            }
            if (e.Error != null)
            {
                //LogFile.Write(e.Error.Message);
                MessageBox.Show(e.Error.Message);
                Console.WriteLine("[{0}] {1}", token[0], e.Error.Message);
            }
            else
            {
                //LogFile.Write("Message Sent");
                MessageBox.Show("Message Sent");
                Console.WriteLine("Message sent.");
            }
            //SendNextMessage((Convert.ToInt32(token[1])) + 1, string.Join(":", new string[] { token[0], (Convert.ToInt32(token[1]) + 1).ToString(), token[2] }));

        }
    }
}
