﻿namespace Oclaro.PromService
{
    partial class SelectPromisInstance
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SelectPromisInstance));
            this.ButOk = new System.Windows.Forms.Button();
            this.ButCancel = new System.Windows.Forms.Button();
            this.GbPromisNames = new System.Windows.Forms.GroupBox();
            this.lblInfo = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ButOk
            // 
            this.ButOk.Location = new System.Drawing.Point(312, 12);
            this.ButOk.Name = "ButOk";
            this.ButOk.Size = new System.Drawing.Size(93, 32);
            this.ButOk.TabIndex = 0;
            this.ButOk.Text = "Ok";
            this.ButOk.UseVisualStyleBackColor = true;
            this.ButOk.Click += new System.EventHandler(this.ButOk_Click);
            // 
            // ButCancel
            // 
            this.ButCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.ButCancel.Location = new System.Drawing.Point(312, 60);
            this.ButCancel.Name = "ButCancel";
            this.ButCancel.Size = new System.Drawing.Size(93, 32);
            this.ButCancel.TabIndex = 1;
            this.ButCancel.Text = "Cancel";
            this.ButCancel.UseVisualStyleBackColor = true;
            this.ButCancel.Click += new System.EventHandler(this.ButCancel_Click);
            // 
            // GbPromisNames
            // 
            this.GbPromisNames.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.GbPromisNames.Dock = System.Windows.Forms.DockStyle.Left;
            this.GbPromisNames.Location = new System.Drawing.Point(0, 0);
            this.GbPromisNames.Name = "GbPromisNames";
            this.GbPromisNames.Size = new System.Drawing.Size(269, 201);
            this.GbPromisNames.TabIndex = 2;
            this.GbPromisNames.TabStop = false;
            this.GbPromisNames.Text = "Select Required Instance";
            // 
            // lblInfo
            // 
            this.lblInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInfo.Location = new System.Drawing.Point(296, 119);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(109, 46);
            this.lblInfo.TabIndex = 3;
            this.lblInfo.Text = "(Press \'CANCEL\' for default Production)";
            // 
            // SelectPromisInstance
            // 
            this.AcceptButton = this.ButOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.ButCancel;
            this.ClientSize = new System.Drawing.Size(426, 201);
            this.Controls.Add(this.lblInfo);
            this.Controls.Add(this.GbPromisNames);
            this.Controls.Add(this.ButCancel);
            this.Controls.Add(this.ButOk);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SelectPromisInstance";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "SelectPromisInstance";
            this.TopMost = true;
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button ButOk;
        private System.Windows.Forms.Button ButCancel;
        private System.Windows.Forms.GroupBox GbPromisNames;
        private System.Windows.Forms.Label lblInfo;
    }
}